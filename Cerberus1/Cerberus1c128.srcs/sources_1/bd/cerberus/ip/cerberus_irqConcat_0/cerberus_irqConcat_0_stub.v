// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Thu Jul  2 17:48:11 2020
// Host        : talisker running 64-bit Debian GNU/Linux 9.12 (stretch)
// Command     : write_verilog -force -mode synth_stub
//               /home/maurice/build/Cerberus1c128/Cerberus1c128.srcs/sources_1/bd/cerberus/ip/cerberus_irqConcat_0/cerberus_irqConcat_0_stub.v
// Design      : cerberus_irqConcat_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "xlconcat_v2_1_1_xlconcat,Vivado 2018.2" *)
module cerberus_irqConcat_0(In0, dout)
/* synthesis syn_black_box black_box_pad_pin="In0[0:0],dout[0:0]" */;
  input [0:0]In0;
  output [0:0]dout;
endmodule
