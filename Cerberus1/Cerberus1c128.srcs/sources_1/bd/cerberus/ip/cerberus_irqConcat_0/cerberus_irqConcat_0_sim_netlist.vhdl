-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Thu Jul  2 17:48:12 2020
-- Host        : talisker running 64-bit Debian GNU/Linux 9.12 (stretch)
-- Command     : write_vhdl -force -mode funcsim
--               /home/maurice/build/Cerberus1c128/Cerberus1c128.srcs/sources_1/bd/cerberus/ip/cerberus_irqConcat_0/cerberus_irqConcat_0_sim_netlist.vhdl
-- Design      : cerberus_irqConcat_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity cerberus_irqConcat_0 is
  port (
    In0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of cerberus_irqConcat_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of cerberus_irqConcat_0 : entity is "cerberus_irqConcat_0,xlconcat_v2_1_1_xlconcat,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of cerberus_irqConcat_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of cerberus_irqConcat_0 : entity is "xlconcat_v2_1_1_xlconcat,Vivado 2018.2";
end cerberus_irqConcat_0;

architecture STRUCTURE of cerberus_irqConcat_0 is
  signal \^in0\ : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  \^in0\(0) <= In0(0);
  dout(0) <= \^in0\(0);
end STRUCTURE;
