-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Thu Jul  2 17:48:11 2020
-- Host        : talisker running 64-bit Debian GNU/Linux 9.12 (stretch)
-- Command     : write_vhdl -force -mode synth_stub
--               /home/maurice/build/Cerberus1c128/Cerberus1c128.srcs/sources_1/bd/cerberus/ip/cerberus_irqConcat_0/cerberus_irqConcat_0_stub.vhdl
-- Design      : cerberus_irqConcat_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity cerberus_irqConcat_0 is
  Port ( 
    In0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );

end cerberus_irqConcat_0;

architecture stub of cerberus_irqConcat_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "In0[0:0],dout[0:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "xlconcat_v2_1_1_xlconcat,Vivado 2018.2";
begin
end;
