onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib cerberus_opt

do {wave.do}

view wave
view structure
view signals

do {cerberus.udo}

run -all

quit -force
