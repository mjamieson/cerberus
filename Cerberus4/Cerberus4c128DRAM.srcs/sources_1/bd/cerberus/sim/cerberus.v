//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
//Date        : Thu Aug  1 09:43:31 2019
//Host        : talisker running 64-bit Debian GNU/Linux 9.9 (stretch)
//Command     : generate_target cerberus.bd
//Design      : cerberus
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "cerberus,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=cerberus,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=78,numReposBlks=44,numNonXlnxBlks=4,numHierBlks=34,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "cerberus.hwdef" *) 
module cerberus
   (DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    led_o,
    pb_i);
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR ADDR" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME DDR, AXI_ARBITRATION_SCHEME TDM, BURST_LENGTH 8, CAN_DEBUG false, CAS_LATENCY 11, CAS_WRITE_LATENCY 11, CS_ENABLED true, DATA_MASK_ENABLED true, DATA_WIDTH 8, MEMORY_TYPE COMPONENTS, MEM_ADDR_MAP ROW_COLUMN_BANK, SLOT Single, TIMEPERIOD_PS 1250" *) inout [14:0]DDR_addr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR BA" *) inout [2:0]DDR_ba;
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR CAS_N" *) inout DDR_cas_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR CK_N" *) inout DDR_ck_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR CK_P" *) inout DDR_ck_p;
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR CKE" *) inout DDR_cke;
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR CS_N" *) inout DDR_cs_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR DM" *) inout [3:0]DDR_dm;
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR DQ" *) inout [31:0]DDR_dq;
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR DQS_N" *) inout [3:0]DDR_dqs_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR DQS_P" *) inout [3:0]DDR_dqs_p;
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR ODT" *) inout DDR_odt;
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR RAS_N" *) inout DDR_ras_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR RESET_N" *) inout DDR_reset_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:ddrx:1.0 DDR WE_N" *) inout DDR_we_n;
  (* X_INTERFACE_INFO = "xilinx.com:display_processing_system7:fixedio:1.0 FIXED_IO DDR_VRN" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME FIXED_IO, CAN_DEBUG false" *) inout FIXED_IO_ddr_vrn;
  (* X_INTERFACE_INFO = "xilinx.com:display_processing_system7:fixedio:1.0 FIXED_IO DDR_VRP" *) inout FIXED_IO_ddr_vrp;
  (* X_INTERFACE_INFO = "xilinx.com:display_processing_system7:fixedio:1.0 FIXED_IO MIO" *) inout [53:0]FIXED_IO_mio;
  (* X_INTERFACE_INFO = "xilinx.com:display_processing_system7:fixedio:1.0 FIXED_IO PS_CLK" *) inout FIXED_IO_ps_clk;
  (* X_INTERFACE_INFO = "xilinx.com:display_processing_system7:fixedio:1.0 FIXED_IO PS_PORB" *) inout FIXED_IO_ps_porb;
  (* X_INTERFACE_INFO = "xilinx.com:display_processing_system7:fixedio:1.0 FIXED_IO PS_SRSTB" *) inout FIXED_IO_ps_srstb;
  output [3:0]led_o;
  input [3:0]pb_i;

  wire FCLK_CLK0;
  wire [31:0]S01_AXI_1_ARADDR;
  wire [2:0]S01_AXI_1_ARPROT;
  wire [0:0]S01_AXI_1_ARREADY;
  wire [0:0]S01_AXI_1_ARVALID;
  wire [31:0]S01_AXI_1_AWADDR;
  wire [2:0]S01_AXI_1_AWPROT;
  wire [0:0]S01_AXI_1_AWREADY;
  wire [0:0]S01_AXI_1_AWVALID;
  wire [0:0]S01_AXI_1_BREADY;
  wire [1:0]S01_AXI_1_BRESP;
  wire [0:0]S01_AXI_1_BVALID;
  wire [63:0]S01_AXI_1_RDATA;
  wire [0:0]S01_AXI_1_RREADY;
  wire [1:0]S01_AXI_1_RRESP;
  wire [0:0]S01_AXI_1_RVALID;
  wire [63:0]S01_AXI_1_WDATA;
  wire [0:0]S01_AXI_1_WREADY;
  wire [7:0]S01_AXI_1_WSTRB;
  wire [0:0]S01_AXI_1_WVALID;
  wire [31:0]S02_AXI_1_ARADDR;
  wire [2:0]S02_AXI_1_ARPROT;
  wire [0:0]S02_AXI_1_ARREADY;
  wire [0:0]S02_AXI_1_ARVALID;
  wire [31:0]S02_AXI_1_AWADDR;
  wire [2:0]S02_AXI_1_AWPROT;
  wire [0:0]S02_AXI_1_AWREADY;
  wire [0:0]S02_AXI_1_AWVALID;
  wire [0:0]S02_AXI_1_BREADY;
  wire [1:0]S02_AXI_1_BRESP;
  wire [0:0]S02_AXI_1_BVALID;
  wire [63:0]S02_AXI_1_RDATA;
  wire [0:0]S02_AXI_1_RREADY;
  wire [1:0]S02_AXI_1_RRESP;
  wire [0:0]S02_AXI_1_RVALID;
  wire [63:0]S02_AXI_1_WDATA;
  wire [0:0]S02_AXI_1_WREADY;
  wire [7:0]S02_AXI_1_WSTRB;
  wire [0:0]S02_AXI_1_WVALID;
  wire [31:0]S03_AXI_1_ARADDR;
  wire [2:0]S03_AXI_1_ARPROT;
  wire [0:0]S03_AXI_1_ARREADY;
  wire [0:0]S03_AXI_1_ARVALID;
  wire [31:0]S03_AXI_1_AWADDR;
  wire [2:0]S03_AXI_1_AWPROT;
  wire [0:0]S03_AXI_1_AWREADY;
  wire [0:0]S03_AXI_1_AWVALID;
  wire [0:0]S03_AXI_1_BREADY;
  wire [1:0]S03_AXI_1_BRESP;
  wire [0:0]S03_AXI_1_BVALID;
  wire [63:0]S03_AXI_1_RDATA;
  wire [0:0]S03_AXI_1_RREADY;
  wire [1:0]S03_AXI_1_RRESP;
  wire [0:0]S03_AXI_1_RVALID;
  wire [63:0]S03_AXI_1_WDATA;
  wire [0:0]S03_AXI_1_WREADY;
  wire [7:0]S03_AXI_1_WSTRB;
  wire [0:0]S03_AXI_1_WVALID;
  wire [31:0]S_AXI_MEM_11_ARADDR;
  wire [1:0]S_AXI_MEM_11_ARBURST;
  wire [3:0]S_AXI_MEM_11_ARCACHE;
  wire [11:0]S_AXI_MEM_11_ARID;
  wire [7:0]S_AXI_MEM_11_ARLEN;
  wire S_AXI_MEM_11_ARLOCK;
  wire [2:0]S_AXI_MEM_11_ARPROT;
  wire S_AXI_MEM_11_ARREADY;
  wire [2:0]S_AXI_MEM_11_ARSIZE;
  wire S_AXI_MEM_11_ARVALID;
  wire [31:0]S_AXI_MEM_11_AWADDR;
  wire [1:0]S_AXI_MEM_11_AWBURST;
  wire [3:0]S_AXI_MEM_11_AWCACHE;
  wire [11:0]S_AXI_MEM_11_AWID;
  wire [7:0]S_AXI_MEM_11_AWLEN;
  wire S_AXI_MEM_11_AWLOCK;
  wire [2:0]S_AXI_MEM_11_AWPROT;
  wire S_AXI_MEM_11_AWREADY;
  wire [2:0]S_AXI_MEM_11_AWSIZE;
  wire S_AXI_MEM_11_AWVALID;
  wire [11:0]S_AXI_MEM_11_BID;
  wire S_AXI_MEM_11_BREADY;
  wire [1:0]S_AXI_MEM_11_BRESP;
  wire S_AXI_MEM_11_BVALID;
  wire [31:0]S_AXI_MEM_11_RDATA;
  wire [11:0]S_AXI_MEM_11_RID;
  wire S_AXI_MEM_11_RLAST;
  wire S_AXI_MEM_11_RREADY;
  wire [1:0]S_AXI_MEM_11_RRESP;
  wire S_AXI_MEM_11_RVALID;
  wire [31:0]S_AXI_MEM_11_WDATA;
  wire S_AXI_MEM_11_WLAST;
  wire S_AXI_MEM_11_WREADY;
  wire [3:0]S_AXI_MEM_11_WSTRB;
  wire S_AXI_MEM_11_WVALID;
  wire [31:0]S_AXI_MEM_12_ARADDR;
  wire [1:0]S_AXI_MEM_12_ARBURST;
  wire [3:0]S_AXI_MEM_12_ARCACHE;
  wire [11:0]S_AXI_MEM_12_ARID;
  wire [7:0]S_AXI_MEM_12_ARLEN;
  wire S_AXI_MEM_12_ARLOCK;
  wire [2:0]S_AXI_MEM_12_ARPROT;
  wire S_AXI_MEM_12_ARREADY;
  wire [2:0]S_AXI_MEM_12_ARSIZE;
  wire S_AXI_MEM_12_ARVALID;
  wire [31:0]S_AXI_MEM_12_AWADDR;
  wire [1:0]S_AXI_MEM_12_AWBURST;
  wire [3:0]S_AXI_MEM_12_AWCACHE;
  wire [11:0]S_AXI_MEM_12_AWID;
  wire [7:0]S_AXI_MEM_12_AWLEN;
  wire S_AXI_MEM_12_AWLOCK;
  wire [2:0]S_AXI_MEM_12_AWPROT;
  wire S_AXI_MEM_12_AWREADY;
  wire [2:0]S_AXI_MEM_12_AWSIZE;
  wire S_AXI_MEM_12_AWVALID;
  wire [11:0]S_AXI_MEM_12_BID;
  wire S_AXI_MEM_12_BREADY;
  wire [1:0]S_AXI_MEM_12_BRESP;
  wire S_AXI_MEM_12_BVALID;
  wire [31:0]S_AXI_MEM_12_RDATA;
  wire [11:0]S_AXI_MEM_12_RID;
  wire S_AXI_MEM_12_RLAST;
  wire S_AXI_MEM_12_RREADY;
  wire [1:0]S_AXI_MEM_12_RRESP;
  wire S_AXI_MEM_12_RVALID;
  wire [31:0]S_AXI_MEM_12_WDATA;
  wire S_AXI_MEM_12_WLAST;
  wire S_AXI_MEM_12_WREADY;
  wire [3:0]S_AXI_MEM_12_WSTRB;
  wire S_AXI_MEM_12_WVALID;
  wire [31:0]S_AXI_PSX_ARADDR;
  wire [1:0]S_AXI_PSX_ARBURST;
  wire [3:0]S_AXI_PSX_ARCACHE;
  wire [11:0]S_AXI_PSX_ARID;
  wire [3:0]S_AXI_PSX_ARLEN;
  wire [1:0]S_AXI_PSX_ARLOCK;
  wire [2:0]S_AXI_PSX_ARPROT;
  wire [3:0]S_AXI_PSX_ARQOS;
  wire S_AXI_PSX_ARREADY;
  wire [2:0]S_AXI_PSX_ARSIZE;
  wire S_AXI_PSX_ARVALID;
  wire [31:0]S_AXI_PSX_AWADDR;
  wire [1:0]S_AXI_PSX_AWBURST;
  wire [3:0]S_AXI_PSX_AWCACHE;
  wire [11:0]S_AXI_PSX_AWID;
  wire [3:0]S_AXI_PSX_AWLEN;
  wire [1:0]S_AXI_PSX_AWLOCK;
  wire [2:0]S_AXI_PSX_AWPROT;
  wire [3:0]S_AXI_PSX_AWQOS;
  wire S_AXI_PSX_AWREADY;
  wire [2:0]S_AXI_PSX_AWSIZE;
  wire S_AXI_PSX_AWVALID;
  wire [11:0]S_AXI_PSX_BID;
  wire S_AXI_PSX_BREADY;
  wire [1:0]S_AXI_PSX_BRESP;
  wire S_AXI_PSX_BVALID;
  wire [31:0]S_AXI_PSX_RDATA;
  wire [11:0]S_AXI_PSX_RID;
  wire S_AXI_PSX_RLAST;
  wire S_AXI_PSX_RREADY;
  wire [1:0]S_AXI_PSX_RRESP;
  wire S_AXI_PSX_RVALID;
  wire [31:0]S_AXI_PSX_WDATA;
  wire [11:0]S_AXI_PSX_WID;
  wire S_AXI_PSX_WLAST;
  wire S_AXI_PSX_WREADY;
  wire [3:0]S_AXI_PSX_WSTRB;
  wire S_AXI_PSX_WVALID;
  wire [31:0]axi_interconnect_0_M00_AXI_ARADDR;
  wire [1:0]axi_interconnect_0_M00_AXI_ARBURST;
  wire [3:0]axi_interconnect_0_M00_AXI_ARCACHE;
  wire [3:0]axi_interconnect_0_M00_AXI_ARLEN;
  wire [1:0]axi_interconnect_0_M00_AXI_ARLOCK;
  wire [2:0]axi_interconnect_0_M00_AXI_ARPROT;
  wire [3:0]axi_interconnect_0_M00_AXI_ARQOS;
  wire axi_interconnect_0_M00_AXI_ARREADY;
  wire [2:0]axi_interconnect_0_M00_AXI_ARSIZE;
  wire axi_interconnect_0_M00_AXI_ARVALID;
  wire [31:0]axi_interconnect_0_M00_AXI_AWADDR;
  wire [1:0]axi_interconnect_0_M00_AXI_AWBURST;
  wire [3:0]axi_interconnect_0_M00_AXI_AWCACHE;
  wire [3:0]axi_interconnect_0_M00_AXI_AWLEN;
  wire [1:0]axi_interconnect_0_M00_AXI_AWLOCK;
  wire [2:0]axi_interconnect_0_M00_AXI_AWPROT;
  wire [3:0]axi_interconnect_0_M00_AXI_AWQOS;
  wire axi_interconnect_0_M00_AXI_AWREADY;
  wire [2:0]axi_interconnect_0_M00_AXI_AWSIZE;
  wire axi_interconnect_0_M00_AXI_AWVALID;
  wire axi_interconnect_0_M00_AXI_BREADY;
  wire [1:0]axi_interconnect_0_M00_AXI_BRESP;
  wire axi_interconnect_0_M00_AXI_BVALID;
  wire [63:0]axi_interconnect_0_M00_AXI_RDATA;
  wire axi_interconnect_0_M00_AXI_RLAST;
  wire axi_interconnect_0_M00_AXI_RREADY;
  wire [1:0]axi_interconnect_0_M00_AXI_RRESP;
  wire axi_interconnect_0_M00_AXI_RVALID;
  wire [63:0]axi_interconnect_0_M00_AXI_WDATA;
  wire axi_interconnect_0_M00_AXI_WLAST;
  wire axi_interconnect_0_M00_AXI_WREADY;
  wire [7:0]axi_interconnect_0_M00_AXI_WSTRB;
  wire axi_interconnect_0_M00_AXI_WVALID;
  wire cerberusProcessor1_irq;
  wire cerberusProcessor2_irq;
  wire cerberusProcessor3_irq;
  wire [31:0]cerberusProcessor_M01_AXI_ARADDR;
  wire [2:0]cerberusProcessor_M01_AXI_ARPROT;
  wire [0:0]cerberusProcessor_M01_AXI_ARREADY;
  wire [0:0]cerberusProcessor_M01_AXI_ARVALID;
  wire [31:0]cerberusProcessor_M01_AXI_AWADDR;
  wire [2:0]cerberusProcessor_M01_AXI_AWPROT;
  wire [0:0]cerberusProcessor_M01_AXI_AWREADY;
  wire [0:0]cerberusProcessor_M01_AXI_AWVALID;
  wire [0:0]cerberusProcessor_M01_AXI_BREADY;
  wire [1:0]cerberusProcessor_M01_AXI_BRESP;
  wire [0:0]cerberusProcessor_M01_AXI_BVALID;
  wire [63:0]cerberusProcessor_M01_AXI_RDATA;
  wire [0:0]cerberusProcessor_M01_AXI_RREADY;
  wire [1:0]cerberusProcessor_M01_AXI_RRESP;
  wire [0:0]cerberusProcessor_M01_AXI_RVALID;
  wire [63:0]cerberusProcessor_M01_AXI_WDATA;
  wire [0:0]cerberusProcessor_M01_AXI_WREADY;
  wire [7:0]cerberusProcessor_M01_AXI_WSTRB;
  wire [0:0]cerberusProcessor_M01_AXI_WVALID;
  wire cerberusProcessor_irq;
  wire [3:0]irqConcat_dout;
  wire [0:0]porReset1_interconnect_aresetn;
  wire [0:0]porReset_peripheral_aresetn;
  wire por_resetn;
  wire [14:0]processing_system7_0_DDR_ADDR;
  wire [2:0]processing_system7_0_DDR_BA;
  wire processing_system7_0_DDR_CAS_N;
  wire processing_system7_0_DDR_CKE;
  wire processing_system7_0_DDR_CK_N;
  wire processing_system7_0_DDR_CK_P;
  wire processing_system7_0_DDR_CS_N;
  wire [3:0]processing_system7_0_DDR_DM;
  wire [31:0]processing_system7_0_DDR_DQ;
  wire [3:0]processing_system7_0_DDR_DQS_N;
  wire [3:0]processing_system7_0_DDR_DQS_P;
  wire processing_system7_0_DDR_ODT;
  wire processing_system7_0_DDR_RAS_N;
  wire processing_system7_0_DDR_RESET_N;
  wire processing_system7_0_DDR_WE_N;
  wire processing_system7_0_FIXED_IO_DDR_VRN;
  wire processing_system7_0_FIXED_IO_DDR_VRP;
  wire [53:0]processing_system7_0_FIXED_IO_MIO;
  wire processing_system7_0_FIXED_IO_PS_CLK;
  wire processing_system7_0_FIXED_IO_PS_PORB;
  wire processing_system7_0_FIXED_IO_PS_SRSTB;
  wire [6:0]processing_system7_0_GPIO_O;
  wire [31:0]psAxiInterconnect_M00_AXI_ARADDR;
  wire psAxiInterconnect_M00_AXI_ARREADY;
  wire psAxiInterconnect_M00_AXI_ARVALID;
  wire [31:0]psAxiInterconnect_M00_AXI_AWADDR;
  wire psAxiInterconnect_M00_AXI_AWREADY;
  wire psAxiInterconnect_M00_AXI_AWVALID;
  wire psAxiInterconnect_M00_AXI_BREADY;
  wire [1:0]psAxiInterconnect_M00_AXI_BRESP;
  wire psAxiInterconnect_M00_AXI_BVALID;
  wire [31:0]psAxiInterconnect_M00_AXI_RDATA;
  wire psAxiInterconnect_M00_AXI_RREADY;
  wire [1:0]psAxiInterconnect_M00_AXI_RRESP;
  wire psAxiInterconnect_M00_AXI_RVALID;
  wire [31:0]psAxiInterconnect_M00_AXI_WDATA;
  wire psAxiInterconnect_M00_AXI_WREADY;
  wire [3:0]psAxiInterconnect_M00_AXI_WSTRB;
  wire psAxiInterconnect_M00_AXI_WVALID;
  wire [31:0]psAxiInterconnect_M01_AXI_ARADDR;
  wire psAxiInterconnect_M01_AXI_ARREADY;
  wire psAxiInterconnect_M01_AXI_ARVALID;
  wire [31:0]psAxiInterconnect_M01_AXI_AWADDR;
  wire psAxiInterconnect_M01_AXI_AWREADY;
  wire psAxiInterconnect_M01_AXI_AWVALID;
  wire psAxiInterconnect_M01_AXI_BREADY;
  wire [1:0]psAxiInterconnect_M01_AXI_BRESP;
  wire psAxiInterconnect_M01_AXI_BVALID;
  wire [31:0]psAxiInterconnect_M01_AXI_RDATA;
  wire psAxiInterconnect_M01_AXI_RREADY;
  wire [1:0]psAxiInterconnect_M01_AXI_RRESP;
  wire psAxiInterconnect_M01_AXI_RVALID;
  wire [31:0]psAxiInterconnect_M01_AXI_WDATA;
  wire psAxiInterconnect_M01_AXI_WREADY;
  wire [3:0]psAxiInterconnect_M01_AXI_WSTRB;
  wire psAxiInterconnect_M01_AXI_WVALID;
  wire [31:0]psAxiInterconnect_M02_AXI_ARADDR;
  wire [1:0]psAxiInterconnect_M02_AXI_ARBURST;
  wire [3:0]psAxiInterconnect_M02_AXI_ARCACHE;
  wire [11:0]psAxiInterconnect_M02_AXI_ARID;
  wire [7:0]psAxiInterconnect_M02_AXI_ARLEN;
  wire psAxiInterconnect_M02_AXI_ARLOCK;
  wire [2:0]psAxiInterconnect_M02_AXI_ARPROT;
  wire psAxiInterconnect_M02_AXI_ARREADY;
  wire [2:0]psAxiInterconnect_M02_AXI_ARSIZE;
  wire psAxiInterconnect_M02_AXI_ARVALID;
  wire [31:0]psAxiInterconnect_M02_AXI_AWADDR;
  wire [1:0]psAxiInterconnect_M02_AXI_AWBURST;
  wire [3:0]psAxiInterconnect_M02_AXI_AWCACHE;
  wire [11:0]psAxiInterconnect_M02_AXI_AWID;
  wire [7:0]psAxiInterconnect_M02_AXI_AWLEN;
  wire psAxiInterconnect_M02_AXI_AWLOCK;
  wire [2:0]psAxiInterconnect_M02_AXI_AWPROT;
  wire psAxiInterconnect_M02_AXI_AWREADY;
  wire [2:0]psAxiInterconnect_M02_AXI_AWSIZE;
  wire psAxiInterconnect_M02_AXI_AWVALID;
  wire [11:0]psAxiInterconnect_M02_AXI_BID;
  wire psAxiInterconnect_M02_AXI_BREADY;
  wire [1:0]psAxiInterconnect_M02_AXI_BRESP;
  wire psAxiInterconnect_M02_AXI_BVALID;
  wire [31:0]psAxiInterconnect_M02_AXI_RDATA;
  wire [11:0]psAxiInterconnect_M02_AXI_RID;
  wire psAxiInterconnect_M02_AXI_RLAST;
  wire psAxiInterconnect_M02_AXI_RREADY;
  wire [1:0]psAxiInterconnect_M02_AXI_RRESP;
  wire psAxiInterconnect_M02_AXI_RVALID;
  wire [31:0]psAxiInterconnect_M02_AXI_WDATA;
  wire psAxiInterconnect_M02_AXI_WLAST;
  wire psAxiInterconnect_M02_AXI_WREADY;
  wire [3:0]psAxiInterconnect_M02_AXI_WSTRB;
  wire psAxiInterconnect_M02_AXI_WVALID;
  wire [31:0]psAxiInterconnect_M03_AXI_ARADDR;
  wire [1:0]psAxiInterconnect_M03_AXI_ARBURST;
  wire [3:0]psAxiInterconnect_M03_AXI_ARCACHE;
  wire [11:0]psAxiInterconnect_M03_AXI_ARID;
  wire [7:0]psAxiInterconnect_M03_AXI_ARLEN;
  wire psAxiInterconnect_M03_AXI_ARLOCK;
  wire [2:0]psAxiInterconnect_M03_AXI_ARPROT;
  wire psAxiInterconnect_M03_AXI_ARREADY;
  wire [2:0]psAxiInterconnect_M03_AXI_ARSIZE;
  wire psAxiInterconnect_M03_AXI_ARVALID;
  wire [31:0]psAxiInterconnect_M03_AXI_AWADDR;
  wire [1:0]psAxiInterconnect_M03_AXI_AWBURST;
  wire [3:0]psAxiInterconnect_M03_AXI_AWCACHE;
  wire [11:0]psAxiInterconnect_M03_AXI_AWID;
  wire [7:0]psAxiInterconnect_M03_AXI_AWLEN;
  wire psAxiInterconnect_M03_AXI_AWLOCK;
  wire [2:0]psAxiInterconnect_M03_AXI_AWPROT;
  wire psAxiInterconnect_M03_AXI_AWREADY;
  wire [2:0]psAxiInterconnect_M03_AXI_AWSIZE;
  wire psAxiInterconnect_M03_AXI_AWVALID;
  wire [11:0]psAxiInterconnect_M03_AXI_BID;
  wire psAxiInterconnect_M03_AXI_BREADY;
  wire [1:0]psAxiInterconnect_M03_AXI_BRESP;
  wire psAxiInterconnect_M03_AXI_BVALID;
  wire [31:0]psAxiInterconnect_M03_AXI_RDATA;
  wire [11:0]psAxiInterconnect_M03_AXI_RID;
  wire psAxiInterconnect_M03_AXI_RLAST;
  wire psAxiInterconnect_M03_AXI_RREADY;
  wire [1:0]psAxiInterconnect_M03_AXI_RRESP;
  wire psAxiInterconnect_M03_AXI_RVALID;
  wire [31:0]psAxiInterconnect_M03_AXI_WDATA;
  wire psAxiInterconnect_M03_AXI_WLAST;
  wire psAxiInterconnect_M03_AXI_WREADY;
  wire [3:0]psAxiInterconnect_M03_AXI_WSTRB;
  wire psAxiInterconnect_M03_AXI_WVALID;
  wire psirq;
  wire [0:0]resetSlice_Dout;
  wire \^subprocessorClk ;

  cerberusProcessor_imp_S4RG0Y cerberusProcessor
       (.M01_AXI_araddr(cerberusProcessor_M01_AXI_ARADDR),
        .M01_AXI_arprot(cerberusProcessor_M01_AXI_ARPROT),
        .M01_AXI_arready(cerberusProcessor_M01_AXI_ARREADY),
        .M01_AXI_arvalid(cerberusProcessor_M01_AXI_ARVALID),
        .M01_AXI_awaddr(cerberusProcessor_M01_AXI_AWADDR),
        .M01_AXI_awprot(cerberusProcessor_M01_AXI_AWPROT),
        .M01_AXI_awready(cerberusProcessor_M01_AXI_AWREADY),
        .M01_AXI_awvalid(cerberusProcessor_M01_AXI_AWVALID),
        .M01_AXI_bready(cerberusProcessor_M01_AXI_BREADY),
        .M01_AXI_bresp(cerberusProcessor_M01_AXI_BRESP),
        .M01_AXI_bvalid(cerberusProcessor_M01_AXI_BVALID),
        .M01_AXI_rdata(cerberusProcessor_M01_AXI_RDATA),
        .M01_AXI_rready(cerberusProcessor_M01_AXI_RREADY),
        .M01_AXI_rresp(cerberusProcessor_M01_AXI_RRESP),
        .M01_AXI_rvalid(cerberusProcessor_M01_AXI_RVALID),
        .M01_AXI_wdata(cerberusProcessor_M01_AXI_WDATA),
        .M01_AXI_wready(cerberusProcessor_M01_AXI_WREADY),
        .M01_AXI_wstrb(cerberusProcessor_M01_AXI_WSTRB),
        .M01_AXI_wvalid(cerberusProcessor_M01_AXI_WVALID),
        .S_AXI_MEM_araddr(psAxiInterconnect_M02_AXI_ARADDR),
        .S_AXI_MEM_arburst(psAxiInterconnect_M02_AXI_ARBURST),
        .S_AXI_MEM_arcache(psAxiInterconnect_M02_AXI_ARCACHE),
        .S_AXI_MEM_arid(psAxiInterconnect_M02_AXI_ARID),
        .S_AXI_MEM_arlen(psAxiInterconnect_M02_AXI_ARLEN),
        .S_AXI_MEM_arlock(psAxiInterconnect_M02_AXI_ARLOCK),
        .S_AXI_MEM_arprot(psAxiInterconnect_M02_AXI_ARPROT),
        .S_AXI_MEM_arready(psAxiInterconnect_M02_AXI_ARREADY),
        .S_AXI_MEM_arsize(psAxiInterconnect_M02_AXI_ARSIZE),
        .S_AXI_MEM_arvalid(psAxiInterconnect_M02_AXI_ARVALID),
        .S_AXI_MEM_awaddr(psAxiInterconnect_M02_AXI_AWADDR),
        .S_AXI_MEM_awburst(psAxiInterconnect_M02_AXI_AWBURST),
        .S_AXI_MEM_awcache(psAxiInterconnect_M02_AXI_AWCACHE),
        .S_AXI_MEM_awid(psAxiInterconnect_M02_AXI_AWID),
        .S_AXI_MEM_awlen(psAxiInterconnect_M02_AXI_AWLEN),
        .S_AXI_MEM_awlock(psAxiInterconnect_M02_AXI_AWLOCK),
        .S_AXI_MEM_awprot(psAxiInterconnect_M02_AXI_AWPROT),
        .S_AXI_MEM_awready(psAxiInterconnect_M02_AXI_AWREADY),
        .S_AXI_MEM_awsize(psAxiInterconnect_M02_AXI_AWSIZE),
        .S_AXI_MEM_awvalid(psAxiInterconnect_M02_AXI_AWVALID),
        .S_AXI_MEM_bid(psAxiInterconnect_M02_AXI_BID),
        .S_AXI_MEM_bready(psAxiInterconnect_M02_AXI_BREADY),
        .S_AXI_MEM_bresp(psAxiInterconnect_M02_AXI_BRESP),
        .S_AXI_MEM_bvalid(psAxiInterconnect_M02_AXI_BVALID),
        .S_AXI_MEM_rdata(psAxiInterconnect_M02_AXI_RDATA),
        .S_AXI_MEM_rid(psAxiInterconnect_M02_AXI_RID),
        .S_AXI_MEM_rlast(psAxiInterconnect_M02_AXI_RLAST),
        .S_AXI_MEM_rready(psAxiInterconnect_M02_AXI_RREADY),
        .S_AXI_MEM_rresp(psAxiInterconnect_M02_AXI_RRESP),
        .S_AXI_MEM_rvalid(psAxiInterconnect_M02_AXI_RVALID),
        .S_AXI_MEM_wdata(psAxiInterconnect_M02_AXI_WDATA),
        .S_AXI_MEM_wlast(psAxiInterconnect_M02_AXI_WLAST),
        .S_AXI_MEM_wready(psAxiInterconnect_M02_AXI_WREADY),
        .S_AXI_MEM_wstrb(psAxiInterconnect_M02_AXI_WSTRB),
        .S_AXI_MEM_wvalid(psAxiInterconnect_M02_AXI_WVALID),
        .irq(cerberusProcessor_irq),
        .por_resetn(por_resetn),
        .riscv_clk(\^subprocessorClk ),
        .riscv_resetn(resetSlice_Dout),
        .s_axi_aclk(FCLK_CLK0),
        .s_axi_aresetn(porReset_peripheral_aresetn));
  cerberusProcessor1_imp_IKNMEZ cerberusProcessor1
       (.M01_AXI_araddr(S01_AXI_1_ARADDR),
        .M01_AXI_arprot(S01_AXI_1_ARPROT),
        .M01_AXI_arready(S01_AXI_1_ARREADY),
        .M01_AXI_arvalid(S01_AXI_1_ARVALID),
        .M01_AXI_awaddr(S01_AXI_1_AWADDR),
        .M01_AXI_awprot(S01_AXI_1_AWPROT),
        .M01_AXI_awready(S01_AXI_1_AWREADY),
        .M01_AXI_awvalid(S01_AXI_1_AWVALID),
        .M01_AXI_bready(S01_AXI_1_BREADY),
        .M01_AXI_bresp(S01_AXI_1_BRESP),
        .M01_AXI_bvalid(S01_AXI_1_BVALID),
        .M01_AXI_rdata(S01_AXI_1_RDATA),
        .M01_AXI_rready(S01_AXI_1_RREADY),
        .M01_AXI_rresp(S01_AXI_1_RRESP),
        .M01_AXI_rvalid(S01_AXI_1_RVALID),
        .M01_AXI_wdata(S01_AXI_1_WDATA),
        .M01_AXI_wready(S01_AXI_1_WREADY),
        .M01_AXI_wstrb(S01_AXI_1_WSTRB),
        .M01_AXI_wvalid(S01_AXI_1_WVALID),
        .S_AXI_MEM_araddr(psAxiInterconnect_M03_AXI_ARADDR),
        .S_AXI_MEM_arburst(psAxiInterconnect_M03_AXI_ARBURST),
        .S_AXI_MEM_arcache(psAxiInterconnect_M03_AXI_ARCACHE),
        .S_AXI_MEM_arid(psAxiInterconnect_M03_AXI_ARID),
        .S_AXI_MEM_arlen(psAxiInterconnect_M03_AXI_ARLEN),
        .S_AXI_MEM_arlock(psAxiInterconnect_M03_AXI_ARLOCK),
        .S_AXI_MEM_arprot(psAxiInterconnect_M03_AXI_ARPROT),
        .S_AXI_MEM_arready(psAxiInterconnect_M03_AXI_ARREADY),
        .S_AXI_MEM_arsize(psAxiInterconnect_M03_AXI_ARSIZE),
        .S_AXI_MEM_arvalid(psAxiInterconnect_M03_AXI_ARVALID),
        .S_AXI_MEM_awaddr(psAxiInterconnect_M03_AXI_AWADDR),
        .S_AXI_MEM_awburst(psAxiInterconnect_M03_AXI_AWBURST),
        .S_AXI_MEM_awcache(psAxiInterconnect_M03_AXI_AWCACHE),
        .S_AXI_MEM_awid(psAxiInterconnect_M03_AXI_AWID),
        .S_AXI_MEM_awlen(psAxiInterconnect_M03_AXI_AWLEN),
        .S_AXI_MEM_awlock(psAxiInterconnect_M03_AXI_AWLOCK),
        .S_AXI_MEM_awprot(psAxiInterconnect_M03_AXI_AWPROT),
        .S_AXI_MEM_awready(psAxiInterconnect_M03_AXI_AWREADY),
        .S_AXI_MEM_awsize(psAxiInterconnect_M03_AXI_AWSIZE),
        .S_AXI_MEM_awvalid(psAxiInterconnect_M03_AXI_AWVALID),
        .S_AXI_MEM_bid(psAxiInterconnect_M03_AXI_BID),
        .S_AXI_MEM_bready(psAxiInterconnect_M03_AXI_BREADY),
        .S_AXI_MEM_bresp(psAxiInterconnect_M03_AXI_BRESP),
        .S_AXI_MEM_bvalid(psAxiInterconnect_M03_AXI_BVALID),
        .S_AXI_MEM_rdata(psAxiInterconnect_M03_AXI_RDATA),
        .S_AXI_MEM_rid(psAxiInterconnect_M03_AXI_RID),
        .S_AXI_MEM_rlast(psAxiInterconnect_M03_AXI_RLAST),
        .S_AXI_MEM_rready(psAxiInterconnect_M03_AXI_RREADY),
        .S_AXI_MEM_rresp(psAxiInterconnect_M03_AXI_RRESP),
        .S_AXI_MEM_rvalid(psAxiInterconnect_M03_AXI_RVALID),
        .S_AXI_MEM_wdata(psAxiInterconnect_M03_AXI_WDATA),
        .S_AXI_MEM_wlast(psAxiInterconnect_M03_AXI_WLAST),
        .S_AXI_MEM_wready(psAxiInterconnect_M03_AXI_WREADY),
        .S_AXI_MEM_wstrb(psAxiInterconnect_M03_AXI_WSTRB),
        .S_AXI_MEM_wvalid(psAxiInterconnect_M03_AXI_WVALID),
        .irq(cerberusProcessor1_irq),
        .por_resetn(por_resetn),
        .riscv_clk(\^subprocessorClk ),
        .riscv_resetn(resetSlice_Dout),
        .s_axi_aclk(FCLK_CLK0),
        .s_axi_aresetn(porReset_peripheral_aresetn));
  cerberusProcessor2_imp_Y9ISJI cerberusProcessor2
       (.M01_AXI_araddr(S02_AXI_1_ARADDR),
        .M01_AXI_arprot(S02_AXI_1_ARPROT),
        .M01_AXI_arready(S02_AXI_1_ARREADY),
        .M01_AXI_arvalid(S02_AXI_1_ARVALID),
        .M01_AXI_awaddr(S02_AXI_1_AWADDR),
        .M01_AXI_awprot(S02_AXI_1_AWPROT),
        .M01_AXI_awready(S02_AXI_1_AWREADY),
        .M01_AXI_awvalid(S02_AXI_1_AWVALID),
        .M01_AXI_bready(S02_AXI_1_BREADY),
        .M01_AXI_bresp(S02_AXI_1_BRESP),
        .M01_AXI_bvalid(S02_AXI_1_BVALID),
        .M01_AXI_rdata(S02_AXI_1_RDATA),
        .M01_AXI_rready(S02_AXI_1_RREADY),
        .M01_AXI_rresp(S02_AXI_1_RRESP),
        .M01_AXI_rvalid(S02_AXI_1_RVALID),
        .M01_AXI_wdata(S02_AXI_1_WDATA),
        .M01_AXI_wready(S02_AXI_1_WREADY),
        .M01_AXI_wstrb(S02_AXI_1_WSTRB),
        .M01_AXI_wvalid(S02_AXI_1_WVALID),
        .S_AXI_MEM_araddr(S_AXI_MEM_11_ARADDR),
        .S_AXI_MEM_arburst(S_AXI_MEM_11_ARBURST),
        .S_AXI_MEM_arcache(S_AXI_MEM_11_ARCACHE),
        .S_AXI_MEM_arid(S_AXI_MEM_11_ARID),
        .S_AXI_MEM_arlen(S_AXI_MEM_11_ARLEN),
        .S_AXI_MEM_arlock(S_AXI_MEM_11_ARLOCK),
        .S_AXI_MEM_arprot(S_AXI_MEM_11_ARPROT),
        .S_AXI_MEM_arready(S_AXI_MEM_11_ARREADY),
        .S_AXI_MEM_arsize(S_AXI_MEM_11_ARSIZE),
        .S_AXI_MEM_arvalid(S_AXI_MEM_11_ARVALID),
        .S_AXI_MEM_awaddr(S_AXI_MEM_11_AWADDR),
        .S_AXI_MEM_awburst(S_AXI_MEM_11_AWBURST),
        .S_AXI_MEM_awcache(S_AXI_MEM_11_AWCACHE),
        .S_AXI_MEM_awid(S_AXI_MEM_11_AWID),
        .S_AXI_MEM_awlen(S_AXI_MEM_11_AWLEN),
        .S_AXI_MEM_awlock(S_AXI_MEM_11_AWLOCK),
        .S_AXI_MEM_awprot(S_AXI_MEM_11_AWPROT),
        .S_AXI_MEM_awready(S_AXI_MEM_11_AWREADY),
        .S_AXI_MEM_awsize(S_AXI_MEM_11_AWSIZE),
        .S_AXI_MEM_awvalid(S_AXI_MEM_11_AWVALID),
        .S_AXI_MEM_bid(S_AXI_MEM_11_BID),
        .S_AXI_MEM_bready(S_AXI_MEM_11_BREADY),
        .S_AXI_MEM_bresp(S_AXI_MEM_11_BRESP),
        .S_AXI_MEM_bvalid(S_AXI_MEM_11_BVALID),
        .S_AXI_MEM_rdata(S_AXI_MEM_11_RDATA),
        .S_AXI_MEM_rid(S_AXI_MEM_11_RID),
        .S_AXI_MEM_rlast(S_AXI_MEM_11_RLAST),
        .S_AXI_MEM_rready(S_AXI_MEM_11_RREADY),
        .S_AXI_MEM_rresp(S_AXI_MEM_11_RRESP),
        .S_AXI_MEM_rvalid(S_AXI_MEM_11_RVALID),
        .S_AXI_MEM_wdata(S_AXI_MEM_11_WDATA),
        .S_AXI_MEM_wlast(S_AXI_MEM_11_WLAST),
        .S_AXI_MEM_wready(S_AXI_MEM_11_WREADY),
        .S_AXI_MEM_wstrb(S_AXI_MEM_11_WSTRB),
        .S_AXI_MEM_wvalid(S_AXI_MEM_11_WVALID),
        .irq(cerberusProcessor2_irq),
        .por_resetn(por_resetn),
        .riscv_clk(\^subprocessorClk ),
        .riscv_resetn(resetSlice_Dout),
        .s_axi_aclk(FCLK_CLK0),
        .s_axi_aresetn(porReset_peripheral_aresetn));
  cerberusProcessor3_imp_TZ1NST cerberusProcessor3
       (.M01_AXI_araddr(S03_AXI_1_ARADDR),
        .M01_AXI_arprot(S03_AXI_1_ARPROT),
        .M01_AXI_arready(S03_AXI_1_ARREADY),
        .M01_AXI_arvalid(S03_AXI_1_ARVALID),
        .M01_AXI_awaddr(S03_AXI_1_AWADDR),
        .M01_AXI_awprot(S03_AXI_1_AWPROT),
        .M01_AXI_awready(S03_AXI_1_AWREADY),
        .M01_AXI_awvalid(S03_AXI_1_AWVALID),
        .M01_AXI_bready(S03_AXI_1_BREADY),
        .M01_AXI_bresp(S03_AXI_1_BRESP),
        .M01_AXI_bvalid(S03_AXI_1_BVALID),
        .M01_AXI_rdata(S03_AXI_1_RDATA),
        .M01_AXI_rready(S03_AXI_1_RREADY),
        .M01_AXI_rresp(S03_AXI_1_RRESP),
        .M01_AXI_rvalid(S03_AXI_1_RVALID),
        .M01_AXI_wdata(S03_AXI_1_WDATA),
        .M01_AXI_wready(S03_AXI_1_WREADY),
        .M01_AXI_wstrb(S03_AXI_1_WSTRB),
        .M01_AXI_wvalid(S03_AXI_1_WVALID),
        .S_AXI_MEM_araddr(S_AXI_MEM_12_ARADDR),
        .S_AXI_MEM_arburst(S_AXI_MEM_12_ARBURST),
        .S_AXI_MEM_arcache(S_AXI_MEM_12_ARCACHE),
        .S_AXI_MEM_arid(S_AXI_MEM_12_ARID),
        .S_AXI_MEM_arlen(S_AXI_MEM_12_ARLEN),
        .S_AXI_MEM_arlock(S_AXI_MEM_12_ARLOCK),
        .S_AXI_MEM_arprot(S_AXI_MEM_12_ARPROT),
        .S_AXI_MEM_arready(S_AXI_MEM_12_ARREADY),
        .S_AXI_MEM_arsize(S_AXI_MEM_12_ARSIZE),
        .S_AXI_MEM_arvalid(S_AXI_MEM_12_ARVALID),
        .S_AXI_MEM_awaddr(S_AXI_MEM_12_AWADDR),
        .S_AXI_MEM_awburst(S_AXI_MEM_12_AWBURST),
        .S_AXI_MEM_awcache(S_AXI_MEM_12_AWCACHE),
        .S_AXI_MEM_awid(S_AXI_MEM_12_AWID),
        .S_AXI_MEM_awlen(S_AXI_MEM_12_AWLEN),
        .S_AXI_MEM_awlock(S_AXI_MEM_12_AWLOCK),
        .S_AXI_MEM_awprot(S_AXI_MEM_12_AWPROT),
        .S_AXI_MEM_awready(S_AXI_MEM_12_AWREADY),
        .S_AXI_MEM_awsize(S_AXI_MEM_12_AWSIZE),
        .S_AXI_MEM_awvalid(S_AXI_MEM_12_AWVALID),
        .S_AXI_MEM_bid(S_AXI_MEM_12_BID),
        .S_AXI_MEM_bready(S_AXI_MEM_12_BREADY),
        .S_AXI_MEM_bresp(S_AXI_MEM_12_BRESP),
        .S_AXI_MEM_bvalid(S_AXI_MEM_12_BVALID),
        .S_AXI_MEM_rdata(S_AXI_MEM_12_RDATA),
        .S_AXI_MEM_rid(S_AXI_MEM_12_RID),
        .S_AXI_MEM_rlast(S_AXI_MEM_12_RLAST),
        .S_AXI_MEM_rready(S_AXI_MEM_12_RREADY),
        .S_AXI_MEM_rresp(S_AXI_MEM_12_RRESP),
        .S_AXI_MEM_rvalid(S_AXI_MEM_12_RVALID),
        .S_AXI_MEM_wdata(S_AXI_MEM_12_WDATA),
        .S_AXI_MEM_wlast(S_AXI_MEM_12_WLAST),
        .S_AXI_MEM_wready(S_AXI_MEM_12_WREADY),
        .S_AXI_MEM_wstrb(S_AXI_MEM_12_WSTRB),
        .S_AXI_MEM_wvalid(S_AXI_MEM_12_WVALID),
        .irq(cerberusProcessor3_irq),
        .por_resetn(por_resetn),
        .riscv_clk(\^subprocessorClk ),
        .riscv_resetn(resetSlice_Dout),
        .s_axi_aclk(FCLK_CLK0),
        .s_axi_aresetn(porReset_peripheral_aresetn));
  cerberus_porReset_1 cerberusReset
       (.aux_reset_in(1'b1),
        .dcm_locked(1'b1),
        .ext_reset_in(por_resetn),
        .interconnect_aresetn(porReset1_interconnect_aresetn),
        .mb_debug_sys_rst(1'b0),
        .peripheral_aresetn(porReset_peripheral_aresetn),
        .slowest_sync_clk(FCLK_CLK0));
  cerberus_irqConcat_0 irqConcat
       (.In0(cerberusProcessor_irq),
        .In1(cerberusProcessor1_irq),
        .In2(cerberusProcessor2_irq),
        .In3(cerberusProcessor3_irq),
        .dout(irqConcat_dout));
  (* BMM_INFO_PROCESSOR = "arm > cerberus cerberusProcessor/psBramController cerberus cerberusProcessor1/psBramController cerberus cerberusProcessor2/psBramController cerberus cerberusProcessor3/psBramController" *) 
  (* KEEP_HIERARCHY = "yes" *) 
  cerberus_processing_system7_0_0 processing_system7_0
       (.DDR_Addr(DDR_addr[14:0]),
        .DDR_BankAddr(DDR_ba[2:0]),
        .DDR_CAS_n(DDR_cas_n),
        .DDR_CKE(DDR_cke),
        .DDR_CS_n(DDR_cs_n),
        .DDR_Clk(DDR_ck_p),
        .DDR_Clk_n(DDR_ck_n),
        .DDR_DM(DDR_dm[3:0]),
        .DDR_DQ(DDR_dq[31:0]),
        .DDR_DQS(DDR_dqs_p[3:0]),
        .DDR_DQS_n(DDR_dqs_n[3:0]),
        .DDR_DRSTB(DDR_reset_n),
        .DDR_ODT(DDR_odt),
        .DDR_RAS_n(DDR_ras_n),
        .DDR_VRN(FIXED_IO_ddr_vrn),
        .DDR_VRP(FIXED_IO_ddr_vrp),
        .DDR_WEB(DDR_we_n),
        .FCLK_CLK0(FCLK_CLK0),
        .FCLK_RESET0_N(por_resetn),
        .GPIO_I({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .GPIO_O(processing_system7_0_GPIO_O),
        .IRQ_F2P(psirq),
        .MIO(FIXED_IO_mio[53:0]),
        .M_AXI_GP0_ACLK(FCLK_CLK0),
        .M_AXI_GP0_ARADDR(S_AXI_PSX_ARADDR),
        .M_AXI_GP0_ARBURST(S_AXI_PSX_ARBURST),
        .M_AXI_GP0_ARCACHE(S_AXI_PSX_ARCACHE),
        .M_AXI_GP0_ARID(S_AXI_PSX_ARID),
        .M_AXI_GP0_ARLEN(S_AXI_PSX_ARLEN),
        .M_AXI_GP0_ARLOCK(S_AXI_PSX_ARLOCK),
        .M_AXI_GP0_ARPROT(S_AXI_PSX_ARPROT),
        .M_AXI_GP0_ARQOS(S_AXI_PSX_ARQOS),
        .M_AXI_GP0_ARREADY(S_AXI_PSX_ARREADY),
        .M_AXI_GP0_ARSIZE(S_AXI_PSX_ARSIZE),
        .M_AXI_GP0_ARVALID(S_AXI_PSX_ARVALID),
        .M_AXI_GP0_AWADDR(S_AXI_PSX_AWADDR),
        .M_AXI_GP0_AWBURST(S_AXI_PSX_AWBURST),
        .M_AXI_GP0_AWCACHE(S_AXI_PSX_AWCACHE),
        .M_AXI_GP0_AWID(S_AXI_PSX_AWID),
        .M_AXI_GP0_AWLEN(S_AXI_PSX_AWLEN),
        .M_AXI_GP0_AWLOCK(S_AXI_PSX_AWLOCK),
        .M_AXI_GP0_AWPROT(S_AXI_PSX_AWPROT),
        .M_AXI_GP0_AWQOS(S_AXI_PSX_AWQOS),
        .M_AXI_GP0_AWREADY(S_AXI_PSX_AWREADY),
        .M_AXI_GP0_AWSIZE(S_AXI_PSX_AWSIZE),
        .M_AXI_GP0_AWVALID(S_AXI_PSX_AWVALID),
        .M_AXI_GP0_BID(S_AXI_PSX_BID),
        .M_AXI_GP0_BREADY(S_AXI_PSX_BREADY),
        .M_AXI_GP0_BRESP(S_AXI_PSX_BRESP),
        .M_AXI_GP0_BVALID(S_AXI_PSX_BVALID),
        .M_AXI_GP0_RDATA(S_AXI_PSX_RDATA),
        .M_AXI_GP0_RID(S_AXI_PSX_RID),
        .M_AXI_GP0_RLAST(S_AXI_PSX_RLAST),
        .M_AXI_GP0_RREADY(S_AXI_PSX_RREADY),
        .M_AXI_GP0_RRESP(S_AXI_PSX_RRESP),
        .M_AXI_GP0_RVALID(S_AXI_PSX_RVALID),
        .M_AXI_GP0_WDATA(S_AXI_PSX_WDATA),
        .M_AXI_GP0_WID(S_AXI_PSX_WID),
        .M_AXI_GP0_WLAST(S_AXI_PSX_WLAST),
        .M_AXI_GP0_WREADY(S_AXI_PSX_WREADY),
        .M_AXI_GP0_WSTRB(S_AXI_PSX_WSTRB),
        .M_AXI_GP0_WVALID(S_AXI_PSX_WVALID),
        .PS_CLK(FIXED_IO_ps_clk),
        .PS_PORB(FIXED_IO_ps_porb),
        .PS_SRSTB(FIXED_IO_ps_srstb),
        .S_AXI_HP0_ACLK(FCLK_CLK0),
        .S_AXI_HP0_ARADDR(axi_interconnect_0_M00_AXI_ARADDR),
        .S_AXI_HP0_ARBURST(axi_interconnect_0_M00_AXI_ARBURST),
        .S_AXI_HP0_ARCACHE(axi_interconnect_0_M00_AXI_ARCACHE),
        .S_AXI_HP0_ARID({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .S_AXI_HP0_ARLEN(axi_interconnect_0_M00_AXI_ARLEN),
        .S_AXI_HP0_ARLOCK(axi_interconnect_0_M00_AXI_ARLOCK),
        .S_AXI_HP0_ARPROT(axi_interconnect_0_M00_AXI_ARPROT),
        .S_AXI_HP0_ARQOS(axi_interconnect_0_M00_AXI_ARQOS),
        .S_AXI_HP0_ARREADY(axi_interconnect_0_M00_AXI_ARREADY),
        .S_AXI_HP0_ARSIZE(axi_interconnect_0_M00_AXI_ARSIZE),
        .S_AXI_HP0_ARVALID(axi_interconnect_0_M00_AXI_ARVALID),
        .S_AXI_HP0_AWADDR(axi_interconnect_0_M00_AXI_AWADDR),
        .S_AXI_HP0_AWBURST(axi_interconnect_0_M00_AXI_AWBURST),
        .S_AXI_HP0_AWCACHE(axi_interconnect_0_M00_AXI_AWCACHE),
        .S_AXI_HP0_AWID({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .S_AXI_HP0_AWLEN(axi_interconnect_0_M00_AXI_AWLEN),
        .S_AXI_HP0_AWLOCK(axi_interconnect_0_M00_AXI_AWLOCK),
        .S_AXI_HP0_AWPROT(axi_interconnect_0_M00_AXI_AWPROT),
        .S_AXI_HP0_AWQOS(axi_interconnect_0_M00_AXI_AWQOS),
        .S_AXI_HP0_AWREADY(axi_interconnect_0_M00_AXI_AWREADY),
        .S_AXI_HP0_AWSIZE(axi_interconnect_0_M00_AXI_AWSIZE),
        .S_AXI_HP0_AWVALID(axi_interconnect_0_M00_AXI_AWVALID),
        .S_AXI_HP0_BREADY(axi_interconnect_0_M00_AXI_BREADY),
        .S_AXI_HP0_BRESP(axi_interconnect_0_M00_AXI_BRESP),
        .S_AXI_HP0_BVALID(axi_interconnect_0_M00_AXI_BVALID),
        .S_AXI_HP0_RDATA(axi_interconnect_0_M00_AXI_RDATA),
        .S_AXI_HP0_RDISSUECAP1_EN(1'b0),
        .S_AXI_HP0_RLAST(axi_interconnect_0_M00_AXI_RLAST),
        .S_AXI_HP0_RREADY(axi_interconnect_0_M00_AXI_RREADY),
        .S_AXI_HP0_RRESP(axi_interconnect_0_M00_AXI_RRESP),
        .S_AXI_HP0_RVALID(axi_interconnect_0_M00_AXI_RVALID),
        .S_AXI_HP0_WDATA(axi_interconnect_0_M00_AXI_WDATA),
        .S_AXI_HP0_WID({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .S_AXI_HP0_WLAST(axi_interconnect_0_M00_AXI_WLAST),
        .S_AXI_HP0_WREADY(axi_interconnect_0_M00_AXI_WREADY),
        .S_AXI_HP0_WRISSUECAP1_EN(1'b0),
        .S_AXI_HP0_WSTRB(axi_interconnect_0_M00_AXI_WSTRB),
        .S_AXI_HP0_WVALID(axi_interconnect_0_M00_AXI_WVALID),
        .USB0_VBUS_PWRFAULT(1'b0));
  cerberus_psAxiInterconnect_0 psAxiInterconnect
       (.ACLK(FCLK_CLK0),
        .ARESETN(porReset1_interconnect_aresetn),
        .M00_ACLK(FCLK_CLK0),
        .M00_ARESETN(porReset_peripheral_aresetn),
        .M00_AXI_araddr(psAxiInterconnect_M00_AXI_ARADDR),
        .M00_AXI_arready(psAxiInterconnect_M00_AXI_ARREADY),
        .M00_AXI_arvalid(psAxiInterconnect_M00_AXI_ARVALID),
        .M00_AXI_awaddr(psAxiInterconnect_M00_AXI_AWADDR),
        .M00_AXI_awready(psAxiInterconnect_M00_AXI_AWREADY),
        .M00_AXI_awvalid(psAxiInterconnect_M00_AXI_AWVALID),
        .M00_AXI_bready(psAxiInterconnect_M00_AXI_BREADY),
        .M00_AXI_bresp(psAxiInterconnect_M00_AXI_BRESP),
        .M00_AXI_bvalid(psAxiInterconnect_M00_AXI_BVALID),
        .M00_AXI_rdata(psAxiInterconnect_M00_AXI_RDATA),
        .M00_AXI_rready(psAxiInterconnect_M00_AXI_RREADY),
        .M00_AXI_rresp(psAxiInterconnect_M00_AXI_RRESP),
        .M00_AXI_rvalid(psAxiInterconnect_M00_AXI_RVALID),
        .M00_AXI_wdata(psAxiInterconnect_M00_AXI_WDATA),
        .M00_AXI_wready(psAxiInterconnect_M00_AXI_WREADY),
        .M00_AXI_wstrb(psAxiInterconnect_M00_AXI_WSTRB),
        .M00_AXI_wvalid(psAxiInterconnect_M00_AXI_WVALID),
        .M01_ACLK(FCLK_CLK0),
        .M01_ARESETN(porReset_peripheral_aresetn),
        .M01_AXI_araddr(psAxiInterconnect_M01_AXI_ARADDR),
        .M01_AXI_arready(psAxiInterconnect_M01_AXI_ARREADY),
        .M01_AXI_arvalid(psAxiInterconnect_M01_AXI_ARVALID),
        .M01_AXI_awaddr(psAxiInterconnect_M01_AXI_AWADDR),
        .M01_AXI_awready(psAxiInterconnect_M01_AXI_AWREADY),
        .M01_AXI_awvalid(psAxiInterconnect_M01_AXI_AWVALID),
        .M01_AXI_bready(psAxiInterconnect_M01_AXI_BREADY),
        .M01_AXI_bresp(psAxiInterconnect_M01_AXI_BRESP),
        .M01_AXI_bvalid(psAxiInterconnect_M01_AXI_BVALID),
        .M01_AXI_rdata(psAxiInterconnect_M01_AXI_RDATA),
        .M01_AXI_rready(psAxiInterconnect_M01_AXI_RREADY),
        .M01_AXI_rresp(psAxiInterconnect_M01_AXI_RRESP),
        .M01_AXI_rvalid(psAxiInterconnect_M01_AXI_RVALID),
        .M01_AXI_wdata(psAxiInterconnect_M01_AXI_WDATA),
        .M01_AXI_wready(psAxiInterconnect_M01_AXI_WREADY),
        .M01_AXI_wstrb(psAxiInterconnect_M01_AXI_WSTRB),
        .M01_AXI_wvalid(psAxiInterconnect_M01_AXI_WVALID),
        .M02_ACLK(FCLK_CLK0),
        .M02_ARESETN(porReset_peripheral_aresetn),
        .M02_AXI_araddr(psAxiInterconnect_M02_AXI_ARADDR),
        .M02_AXI_arburst(psAxiInterconnect_M02_AXI_ARBURST),
        .M02_AXI_arcache(psAxiInterconnect_M02_AXI_ARCACHE),
        .M02_AXI_arid(psAxiInterconnect_M02_AXI_ARID),
        .M02_AXI_arlen(psAxiInterconnect_M02_AXI_ARLEN),
        .M02_AXI_arlock(psAxiInterconnect_M02_AXI_ARLOCK),
        .M02_AXI_arprot(psAxiInterconnect_M02_AXI_ARPROT),
        .M02_AXI_arready(psAxiInterconnect_M02_AXI_ARREADY),
        .M02_AXI_arsize(psAxiInterconnect_M02_AXI_ARSIZE),
        .M02_AXI_arvalid(psAxiInterconnect_M02_AXI_ARVALID),
        .M02_AXI_awaddr(psAxiInterconnect_M02_AXI_AWADDR),
        .M02_AXI_awburst(psAxiInterconnect_M02_AXI_AWBURST),
        .M02_AXI_awcache(psAxiInterconnect_M02_AXI_AWCACHE),
        .M02_AXI_awid(psAxiInterconnect_M02_AXI_AWID),
        .M02_AXI_awlen(psAxiInterconnect_M02_AXI_AWLEN),
        .M02_AXI_awlock(psAxiInterconnect_M02_AXI_AWLOCK),
        .M02_AXI_awprot(psAxiInterconnect_M02_AXI_AWPROT),
        .M02_AXI_awready(psAxiInterconnect_M02_AXI_AWREADY),
        .M02_AXI_awsize(psAxiInterconnect_M02_AXI_AWSIZE),
        .M02_AXI_awvalid(psAxiInterconnect_M02_AXI_AWVALID),
        .M02_AXI_bid(psAxiInterconnect_M02_AXI_BID),
        .M02_AXI_bready(psAxiInterconnect_M02_AXI_BREADY),
        .M02_AXI_bresp(psAxiInterconnect_M02_AXI_BRESP),
        .M02_AXI_bvalid(psAxiInterconnect_M02_AXI_BVALID),
        .M02_AXI_rdata(psAxiInterconnect_M02_AXI_RDATA),
        .M02_AXI_rid(psAxiInterconnect_M02_AXI_RID),
        .M02_AXI_rlast(psAxiInterconnect_M02_AXI_RLAST),
        .M02_AXI_rready(psAxiInterconnect_M02_AXI_RREADY),
        .M02_AXI_rresp(psAxiInterconnect_M02_AXI_RRESP),
        .M02_AXI_rvalid(psAxiInterconnect_M02_AXI_RVALID),
        .M02_AXI_wdata(psAxiInterconnect_M02_AXI_WDATA),
        .M02_AXI_wlast(psAxiInterconnect_M02_AXI_WLAST),
        .M02_AXI_wready(psAxiInterconnect_M02_AXI_WREADY),
        .M02_AXI_wstrb(psAxiInterconnect_M02_AXI_WSTRB),
        .M02_AXI_wvalid(psAxiInterconnect_M02_AXI_WVALID),
        .M03_ACLK(FCLK_CLK0),
        .M03_ARESETN(porReset_peripheral_aresetn),
        .M03_AXI_araddr(psAxiInterconnect_M03_AXI_ARADDR),
        .M03_AXI_arburst(psAxiInterconnect_M03_AXI_ARBURST),
        .M03_AXI_arcache(psAxiInterconnect_M03_AXI_ARCACHE),
        .M03_AXI_arid(psAxiInterconnect_M03_AXI_ARID),
        .M03_AXI_arlen(psAxiInterconnect_M03_AXI_ARLEN),
        .M03_AXI_arlock(psAxiInterconnect_M03_AXI_ARLOCK),
        .M03_AXI_arprot(psAxiInterconnect_M03_AXI_ARPROT),
        .M03_AXI_arready(psAxiInterconnect_M03_AXI_ARREADY),
        .M03_AXI_arsize(psAxiInterconnect_M03_AXI_ARSIZE),
        .M03_AXI_arvalid(psAxiInterconnect_M03_AXI_ARVALID),
        .M03_AXI_awaddr(psAxiInterconnect_M03_AXI_AWADDR),
        .M03_AXI_awburst(psAxiInterconnect_M03_AXI_AWBURST),
        .M03_AXI_awcache(psAxiInterconnect_M03_AXI_AWCACHE),
        .M03_AXI_awid(psAxiInterconnect_M03_AXI_AWID),
        .M03_AXI_awlen(psAxiInterconnect_M03_AXI_AWLEN),
        .M03_AXI_awlock(psAxiInterconnect_M03_AXI_AWLOCK),
        .M03_AXI_awprot(psAxiInterconnect_M03_AXI_AWPROT),
        .M03_AXI_awready(psAxiInterconnect_M03_AXI_AWREADY),
        .M03_AXI_awsize(psAxiInterconnect_M03_AXI_AWSIZE),
        .M03_AXI_awvalid(psAxiInterconnect_M03_AXI_AWVALID),
        .M03_AXI_bid(psAxiInterconnect_M03_AXI_BID),
        .M03_AXI_bready(psAxiInterconnect_M03_AXI_BREADY),
        .M03_AXI_bresp(psAxiInterconnect_M03_AXI_BRESP),
        .M03_AXI_bvalid(psAxiInterconnect_M03_AXI_BVALID),
        .M03_AXI_rdata(psAxiInterconnect_M03_AXI_RDATA),
        .M03_AXI_rid(psAxiInterconnect_M03_AXI_RID),
        .M03_AXI_rlast(psAxiInterconnect_M03_AXI_RLAST),
        .M03_AXI_rready(psAxiInterconnect_M03_AXI_RREADY),
        .M03_AXI_rresp(psAxiInterconnect_M03_AXI_RRESP),
        .M03_AXI_rvalid(psAxiInterconnect_M03_AXI_RVALID),
        .M03_AXI_wdata(psAxiInterconnect_M03_AXI_WDATA),
        .M03_AXI_wlast(psAxiInterconnect_M03_AXI_WLAST),
        .M03_AXI_wready(psAxiInterconnect_M03_AXI_WREADY),
        .M03_AXI_wstrb(psAxiInterconnect_M03_AXI_WSTRB),
        .M03_AXI_wvalid(psAxiInterconnect_M03_AXI_WVALID),
        .M04_ACLK(FCLK_CLK0),
        .M04_ARESETN(porReset_peripheral_aresetn),
        .M04_AXI_araddr(S_AXI_MEM_11_ARADDR),
        .M04_AXI_arburst(S_AXI_MEM_11_ARBURST),
        .M04_AXI_arcache(S_AXI_MEM_11_ARCACHE),
        .M04_AXI_arid(S_AXI_MEM_11_ARID),
        .M04_AXI_arlen(S_AXI_MEM_11_ARLEN),
        .M04_AXI_arlock(S_AXI_MEM_11_ARLOCK),
        .M04_AXI_arprot(S_AXI_MEM_11_ARPROT),
        .M04_AXI_arready(S_AXI_MEM_11_ARREADY),
        .M04_AXI_arsize(S_AXI_MEM_11_ARSIZE),
        .M04_AXI_arvalid(S_AXI_MEM_11_ARVALID),
        .M04_AXI_awaddr(S_AXI_MEM_11_AWADDR),
        .M04_AXI_awburst(S_AXI_MEM_11_AWBURST),
        .M04_AXI_awcache(S_AXI_MEM_11_AWCACHE),
        .M04_AXI_awid(S_AXI_MEM_11_AWID),
        .M04_AXI_awlen(S_AXI_MEM_11_AWLEN),
        .M04_AXI_awlock(S_AXI_MEM_11_AWLOCK),
        .M04_AXI_awprot(S_AXI_MEM_11_AWPROT),
        .M04_AXI_awready(S_AXI_MEM_11_AWREADY),
        .M04_AXI_awsize(S_AXI_MEM_11_AWSIZE),
        .M04_AXI_awvalid(S_AXI_MEM_11_AWVALID),
        .M04_AXI_bid(S_AXI_MEM_11_BID),
        .M04_AXI_bready(S_AXI_MEM_11_BREADY),
        .M04_AXI_bresp(S_AXI_MEM_11_BRESP),
        .M04_AXI_bvalid(S_AXI_MEM_11_BVALID),
        .M04_AXI_rdata(S_AXI_MEM_11_RDATA),
        .M04_AXI_rid(S_AXI_MEM_11_RID),
        .M04_AXI_rlast(S_AXI_MEM_11_RLAST),
        .M04_AXI_rready(S_AXI_MEM_11_RREADY),
        .M04_AXI_rresp(S_AXI_MEM_11_RRESP),
        .M04_AXI_rvalid(S_AXI_MEM_11_RVALID),
        .M04_AXI_wdata(S_AXI_MEM_11_WDATA),
        .M04_AXI_wlast(S_AXI_MEM_11_WLAST),
        .M04_AXI_wready(S_AXI_MEM_11_WREADY),
        .M04_AXI_wstrb(S_AXI_MEM_11_WSTRB),
        .M04_AXI_wvalid(S_AXI_MEM_11_WVALID),
        .M05_ACLK(FCLK_CLK0),
        .M05_ARESETN(porReset_peripheral_aresetn),
        .M05_AXI_araddr(S_AXI_MEM_12_ARADDR),
        .M05_AXI_arburst(S_AXI_MEM_12_ARBURST),
        .M05_AXI_arcache(S_AXI_MEM_12_ARCACHE),
        .M05_AXI_arid(S_AXI_MEM_12_ARID),
        .M05_AXI_arlen(S_AXI_MEM_12_ARLEN),
        .M05_AXI_arlock(S_AXI_MEM_12_ARLOCK),
        .M05_AXI_arprot(S_AXI_MEM_12_ARPROT),
        .M05_AXI_arready(S_AXI_MEM_12_ARREADY),
        .M05_AXI_arsize(S_AXI_MEM_12_ARSIZE),
        .M05_AXI_arvalid(S_AXI_MEM_12_ARVALID),
        .M05_AXI_awaddr(S_AXI_MEM_12_AWADDR),
        .M05_AXI_awburst(S_AXI_MEM_12_AWBURST),
        .M05_AXI_awcache(S_AXI_MEM_12_AWCACHE),
        .M05_AXI_awid(S_AXI_MEM_12_AWID),
        .M05_AXI_awlen(S_AXI_MEM_12_AWLEN),
        .M05_AXI_awlock(S_AXI_MEM_12_AWLOCK),
        .M05_AXI_awprot(S_AXI_MEM_12_AWPROT),
        .M05_AXI_awready(S_AXI_MEM_12_AWREADY),
        .M05_AXI_awsize(S_AXI_MEM_12_AWSIZE),
        .M05_AXI_awvalid(S_AXI_MEM_12_AWVALID),
        .M05_AXI_bid(S_AXI_MEM_12_BID),
        .M05_AXI_bready(S_AXI_MEM_12_BREADY),
        .M05_AXI_bresp(S_AXI_MEM_12_BRESP),
        .M05_AXI_bvalid(S_AXI_MEM_12_BVALID),
        .M05_AXI_rdata(S_AXI_MEM_12_RDATA),
        .M05_AXI_rid(S_AXI_MEM_12_RID),
        .M05_AXI_rlast(S_AXI_MEM_12_RLAST),
        .M05_AXI_rready(S_AXI_MEM_12_RREADY),
        .M05_AXI_rresp(S_AXI_MEM_12_RRESP),
        .M05_AXI_rvalid(S_AXI_MEM_12_RVALID),
        .M05_AXI_wdata(S_AXI_MEM_12_WDATA),
        .M05_AXI_wlast(S_AXI_MEM_12_WLAST),
        .M05_AXI_wready(S_AXI_MEM_12_WREADY),
        .M05_AXI_wstrb(S_AXI_MEM_12_WSTRB),
        .M05_AXI_wvalid(S_AXI_MEM_12_WVALID),
        .S00_ACLK(FCLK_CLK0),
        .S00_ARESETN(porReset_peripheral_aresetn),
        .S00_AXI_araddr(S_AXI_PSX_ARADDR),
        .S00_AXI_arburst(S_AXI_PSX_ARBURST),
        .S00_AXI_arcache(S_AXI_PSX_ARCACHE),
        .S00_AXI_arid(S_AXI_PSX_ARID),
        .S00_AXI_arlen(S_AXI_PSX_ARLEN),
        .S00_AXI_arlock(S_AXI_PSX_ARLOCK),
        .S00_AXI_arprot(S_AXI_PSX_ARPROT),
        .S00_AXI_arqos(S_AXI_PSX_ARQOS),
        .S00_AXI_arready(S_AXI_PSX_ARREADY),
        .S00_AXI_arsize(S_AXI_PSX_ARSIZE),
        .S00_AXI_arvalid(S_AXI_PSX_ARVALID),
        .S00_AXI_awaddr(S_AXI_PSX_AWADDR),
        .S00_AXI_awburst(S_AXI_PSX_AWBURST),
        .S00_AXI_awcache(S_AXI_PSX_AWCACHE),
        .S00_AXI_awid(S_AXI_PSX_AWID),
        .S00_AXI_awlen(S_AXI_PSX_AWLEN),
        .S00_AXI_awlock(S_AXI_PSX_AWLOCK),
        .S00_AXI_awprot(S_AXI_PSX_AWPROT),
        .S00_AXI_awqos(S_AXI_PSX_AWQOS),
        .S00_AXI_awready(S_AXI_PSX_AWREADY),
        .S00_AXI_awsize(S_AXI_PSX_AWSIZE),
        .S00_AXI_awvalid(S_AXI_PSX_AWVALID),
        .S00_AXI_bid(S_AXI_PSX_BID),
        .S00_AXI_bready(S_AXI_PSX_BREADY),
        .S00_AXI_bresp(S_AXI_PSX_BRESP),
        .S00_AXI_bvalid(S_AXI_PSX_BVALID),
        .S00_AXI_rdata(S_AXI_PSX_RDATA),
        .S00_AXI_rid(S_AXI_PSX_RID),
        .S00_AXI_rlast(S_AXI_PSX_RLAST),
        .S00_AXI_rready(S_AXI_PSX_RREADY),
        .S00_AXI_rresp(S_AXI_PSX_RRESP),
        .S00_AXI_rvalid(S_AXI_PSX_RVALID),
        .S00_AXI_wdata(S_AXI_PSX_WDATA),
        .S00_AXI_wid(S_AXI_PSX_WID),
        .S00_AXI_wlast(S_AXI_PSX_WLAST),
        .S00_AXI_wready(S_AXI_PSX_WREADY),
        .S00_AXI_wstrb(S_AXI_PSX_WSTRB),
        .S00_AXI_wvalid(S_AXI_PSX_WVALID));
  cerberus_psDramAxiInterconnect_0 psDramAxiInterconnect
       (.ACLK(FCLK_CLK0),
        .ARESETN(porReset_peripheral_aresetn),
        .M00_ACLK(FCLK_CLK0),
        .M00_ARESETN(porReset_peripheral_aresetn),
        .M00_AXI_araddr(axi_interconnect_0_M00_AXI_ARADDR),
        .M00_AXI_arburst(axi_interconnect_0_M00_AXI_ARBURST),
        .M00_AXI_arcache(axi_interconnect_0_M00_AXI_ARCACHE),
        .M00_AXI_arlen(axi_interconnect_0_M00_AXI_ARLEN),
        .M00_AXI_arlock(axi_interconnect_0_M00_AXI_ARLOCK),
        .M00_AXI_arprot(axi_interconnect_0_M00_AXI_ARPROT),
        .M00_AXI_arqos(axi_interconnect_0_M00_AXI_ARQOS),
        .M00_AXI_arready(axi_interconnect_0_M00_AXI_ARREADY),
        .M00_AXI_arsize(axi_interconnect_0_M00_AXI_ARSIZE),
        .M00_AXI_arvalid(axi_interconnect_0_M00_AXI_ARVALID),
        .M00_AXI_awaddr(axi_interconnect_0_M00_AXI_AWADDR),
        .M00_AXI_awburst(axi_interconnect_0_M00_AXI_AWBURST),
        .M00_AXI_awcache(axi_interconnect_0_M00_AXI_AWCACHE),
        .M00_AXI_awlen(axi_interconnect_0_M00_AXI_AWLEN),
        .M00_AXI_awlock(axi_interconnect_0_M00_AXI_AWLOCK),
        .M00_AXI_awprot(axi_interconnect_0_M00_AXI_AWPROT),
        .M00_AXI_awqos(axi_interconnect_0_M00_AXI_AWQOS),
        .M00_AXI_awready(axi_interconnect_0_M00_AXI_AWREADY),
        .M00_AXI_awsize(axi_interconnect_0_M00_AXI_AWSIZE),
        .M00_AXI_awvalid(axi_interconnect_0_M00_AXI_AWVALID),
        .M00_AXI_bready(axi_interconnect_0_M00_AXI_BREADY),
        .M00_AXI_bresp(axi_interconnect_0_M00_AXI_BRESP),
        .M00_AXI_bvalid(axi_interconnect_0_M00_AXI_BVALID),
        .M00_AXI_rdata(axi_interconnect_0_M00_AXI_RDATA),
        .M00_AXI_rlast(axi_interconnect_0_M00_AXI_RLAST),
        .M00_AXI_rready(axi_interconnect_0_M00_AXI_RREADY),
        .M00_AXI_rresp(axi_interconnect_0_M00_AXI_RRESP),
        .M00_AXI_rvalid(axi_interconnect_0_M00_AXI_RVALID),
        .M00_AXI_wdata(axi_interconnect_0_M00_AXI_WDATA),
        .M00_AXI_wlast(axi_interconnect_0_M00_AXI_WLAST),
        .M00_AXI_wready(axi_interconnect_0_M00_AXI_WREADY),
        .M00_AXI_wstrb(axi_interconnect_0_M00_AXI_WSTRB),
        .M00_AXI_wvalid(axi_interconnect_0_M00_AXI_WVALID),
        .S00_ACLK(FCLK_CLK0),
        .S00_ARESETN(porReset_peripheral_aresetn),
        .S00_AXI_araddr(cerberusProcessor_M01_AXI_ARADDR),
        .S00_AXI_arprot(cerberusProcessor_M01_AXI_ARPROT),
        .S00_AXI_arready(cerberusProcessor_M01_AXI_ARREADY),
        .S00_AXI_arvalid(cerberusProcessor_M01_AXI_ARVALID),
        .S00_AXI_awaddr(cerberusProcessor_M01_AXI_AWADDR),
        .S00_AXI_awprot(cerberusProcessor_M01_AXI_AWPROT),
        .S00_AXI_awready(cerberusProcessor_M01_AXI_AWREADY),
        .S00_AXI_awvalid(cerberusProcessor_M01_AXI_AWVALID),
        .S00_AXI_bready(cerberusProcessor_M01_AXI_BREADY),
        .S00_AXI_bresp(cerberusProcessor_M01_AXI_BRESP),
        .S00_AXI_bvalid(cerberusProcessor_M01_AXI_BVALID),
        .S00_AXI_rdata(cerberusProcessor_M01_AXI_RDATA),
        .S00_AXI_rready(cerberusProcessor_M01_AXI_RREADY),
        .S00_AXI_rresp(cerberusProcessor_M01_AXI_RRESP),
        .S00_AXI_rvalid(cerberusProcessor_M01_AXI_RVALID),
        .S00_AXI_wdata(cerberusProcessor_M01_AXI_WDATA),
        .S00_AXI_wready(cerberusProcessor_M01_AXI_WREADY),
        .S00_AXI_wstrb(cerberusProcessor_M01_AXI_WSTRB),
        .S00_AXI_wvalid(cerberusProcessor_M01_AXI_WVALID),
        .S01_ACLK(FCLK_CLK0),
        .S01_ARESETN(porReset_peripheral_aresetn),
        .S01_AXI_araddr(S01_AXI_1_ARADDR),
        .S01_AXI_arprot(S01_AXI_1_ARPROT),
        .S01_AXI_arready(S01_AXI_1_ARREADY),
        .S01_AXI_arvalid(S01_AXI_1_ARVALID),
        .S01_AXI_awaddr(S01_AXI_1_AWADDR),
        .S01_AXI_awprot(S01_AXI_1_AWPROT),
        .S01_AXI_awready(S01_AXI_1_AWREADY),
        .S01_AXI_awvalid(S01_AXI_1_AWVALID),
        .S01_AXI_bready(S01_AXI_1_BREADY),
        .S01_AXI_bresp(S01_AXI_1_BRESP),
        .S01_AXI_bvalid(S01_AXI_1_BVALID),
        .S01_AXI_rdata(S01_AXI_1_RDATA),
        .S01_AXI_rready(S01_AXI_1_RREADY),
        .S01_AXI_rresp(S01_AXI_1_RRESP),
        .S01_AXI_rvalid(S01_AXI_1_RVALID),
        .S01_AXI_wdata(S01_AXI_1_WDATA),
        .S01_AXI_wready(S01_AXI_1_WREADY),
        .S01_AXI_wstrb(S01_AXI_1_WSTRB),
        .S01_AXI_wvalid(S01_AXI_1_WVALID),
        .S02_ACLK(FCLK_CLK0),
        .S02_ARESETN(porReset_peripheral_aresetn),
        .S02_AXI_araddr(S02_AXI_1_ARADDR),
        .S02_AXI_arprot(S02_AXI_1_ARPROT),
        .S02_AXI_arready(S02_AXI_1_ARREADY),
        .S02_AXI_arvalid(S02_AXI_1_ARVALID),
        .S02_AXI_awaddr(S02_AXI_1_AWADDR),
        .S02_AXI_awprot(S02_AXI_1_AWPROT),
        .S02_AXI_awready(S02_AXI_1_AWREADY),
        .S02_AXI_awvalid(S02_AXI_1_AWVALID),
        .S02_AXI_bready(S02_AXI_1_BREADY),
        .S02_AXI_bresp(S02_AXI_1_BRESP),
        .S02_AXI_bvalid(S02_AXI_1_BVALID),
        .S02_AXI_rdata(S02_AXI_1_RDATA),
        .S02_AXI_rready(S02_AXI_1_RREADY),
        .S02_AXI_rresp(S02_AXI_1_RRESP),
        .S02_AXI_rvalid(S02_AXI_1_RVALID),
        .S02_AXI_wdata(S02_AXI_1_WDATA),
        .S02_AXI_wready(S02_AXI_1_WREADY),
        .S02_AXI_wstrb(S02_AXI_1_WSTRB),
        .S02_AXI_wvalid(S02_AXI_1_WVALID),
        .S03_ACLK(FCLK_CLK0),
        .S03_ARESETN(porReset_peripheral_aresetn),
        .S03_AXI_araddr(S03_AXI_1_ARADDR),
        .S03_AXI_arprot(S03_AXI_1_ARPROT),
        .S03_AXI_arready(S03_AXI_1_ARREADY),
        .S03_AXI_arvalid(S03_AXI_1_ARVALID),
        .S03_AXI_awaddr(S03_AXI_1_AWADDR),
        .S03_AXI_awprot(S03_AXI_1_AWPROT),
        .S03_AXI_awready(S03_AXI_1_AWREADY),
        .S03_AXI_awvalid(S03_AXI_1_AWVALID),
        .S03_AXI_bready(S03_AXI_1_BREADY),
        .S03_AXI_bresp(S03_AXI_1_BRESP),
        .S03_AXI_bvalid(S03_AXI_1_BVALID),
        .S03_AXI_rdata(S03_AXI_1_RDATA),
        .S03_AXI_rready(S03_AXI_1_RREADY),
        .S03_AXI_rresp(S03_AXI_1_RRESP),
        .S03_AXI_rvalid(S03_AXI_1_RVALID),
        .S03_AXI_wdata(S03_AXI_1_WDATA),
        .S03_AXI_wready(S03_AXI_1_WREADY),
        .S03_AXI_wstrb(S03_AXI_1_WSTRB),
        .S03_AXI_wvalid(S03_AXI_1_WVALID));
  cerberus_psInterruptController_0 psInterruptController
       (.intr(irqConcat_dout),
        .irq(psirq),
        .s_axi_aclk(FCLK_CLK0),
        .s_axi_araddr(psAxiInterconnect_M00_AXI_ARADDR[8:0]),
        .s_axi_aresetn(porReset_peripheral_aresetn),
        .s_axi_arready(psAxiInterconnect_M00_AXI_ARREADY),
        .s_axi_arvalid(psAxiInterconnect_M00_AXI_ARVALID),
        .s_axi_awaddr(psAxiInterconnect_M00_AXI_AWADDR[8:0]),
        .s_axi_awready(psAxiInterconnect_M00_AXI_AWREADY),
        .s_axi_awvalid(psAxiInterconnect_M00_AXI_AWVALID),
        .s_axi_bready(psAxiInterconnect_M00_AXI_BREADY),
        .s_axi_bresp(psAxiInterconnect_M00_AXI_BRESP),
        .s_axi_bvalid(psAxiInterconnect_M00_AXI_BVALID),
        .s_axi_rdata(psAxiInterconnect_M00_AXI_RDATA),
        .s_axi_rready(psAxiInterconnect_M00_AXI_RREADY),
        .s_axi_rresp(psAxiInterconnect_M00_AXI_RRESP),
        .s_axi_rvalid(psAxiInterconnect_M00_AXI_RVALID),
        .s_axi_wdata(psAxiInterconnect_M00_AXI_WDATA),
        .s_axi_wready(psAxiInterconnect_M00_AXI_WREADY),
        .s_axi_wstrb(psAxiInterconnect_M00_AXI_WSTRB),
        .s_axi_wvalid(psAxiInterconnect_M00_AXI_WVALID));
  cerberus_resetSlice_0 resetSlice
       (.Din(processing_system7_0_GPIO_O),
        .Dout(resetSlice_Dout));
  cerberus_subprocessorClk_0 subprocessorClk
       (.clk_in1(FCLK_CLK0),
        .clk_out1(\^subprocessorClk ),
        .s_axi_aclk(FCLK_CLK0),
        .s_axi_araddr(psAxiInterconnect_M01_AXI_ARADDR[10:0]),
        .s_axi_aresetn(porReset_peripheral_aresetn),
        .s_axi_arready(psAxiInterconnect_M01_AXI_ARREADY),
        .s_axi_arvalid(psAxiInterconnect_M01_AXI_ARVALID),
        .s_axi_awaddr(psAxiInterconnect_M01_AXI_AWADDR[10:0]),
        .s_axi_awready(psAxiInterconnect_M01_AXI_AWREADY),
        .s_axi_awvalid(psAxiInterconnect_M01_AXI_AWVALID),
        .s_axi_bready(psAxiInterconnect_M01_AXI_BREADY),
        .s_axi_bresp(psAxiInterconnect_M01_AXI_BRESP),
        .s_axi_bvalid(psAxiInterconnect_M01_AXI_BVALID),
        .s_axi_rdata(psAxiInterconnect_M01_AXI_RDATA),
        .s_axi_rready(psAxiInterconnect_M01_AXI_RREADY),
        .s_axi_rresp(psAxiInterconnect_M01_AXI_RRESP),
        .s_axi_rvalid(psAxiInterconnect_M01_AXI_RVALID),
        .s_axi_wdata(psAxiInterconnect_M01_AXI_WDATA),
        .s_axi_wready(psAxiInterconnect_M01_AXI_WREADY),
        .s_axi_wstrb(psAxiInterconnect_M01_AXI_WSTRB),
        .s_axi_wvalid(psAxiInterconnect_M01_AXI_WVALID));
endmodule

module cerberusProcessor1_imp_IKNMEZ
   (M01_AXI_araddr,
    M01_AXI_arprot,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awprot,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wstrb,
    M01_AXI_wvalid,
    S_AXI_MEM_araddr,
    S_AXI_MEM_arburst,
    S_AXI_MEM_arcache,
    S_AXI_MEM_arid,
    S_AXI_MEM_arlen,
    S_AXI_MEM_arlock,
    S_AXI_MEM_arprot,
    S_AXI_MEM_arready,
    S_AXI_MEM_arsize,
    S_AXI_MEM_arvalid,
    S_AXI_MEM_awaddr,
    S_AXI_MEM_awburst,
    S_AXI_MEM_awcache,
    S_AXI_MEM_awid,
    S_AXI_MEM_awlen,
    S_AXI_MEM_awlock,
    S_AXI_MEM_awprot,
    S_AXI_MEM_awready,
    S_AXI_MEM_awsize,
    S_AXI_MEM_awvalid,
    S_AXI_MEM_bid,
    S_AXI_MEM_bready,
    S_AXI_MEM_bresp,
    S_AXI_MEM_bvalid,
    S_AXI_MEM_rdata,
    S_AXI_MEM_rid,
    S_AXI_MEM_rlast,
    S_AXI_MEM_rready,
    S_AXI_MEM_rresp,
    S_AXI_MEM_rvalid,
    S_AXI_MEM_wdata,
    S_AXI_MEM_wlast,
    S_AXI_MEM_wready,
    S_AXI_MEM_wstrb,
    S_AXI_MEM_wvalid,
    irq,
    por_resetn,
    riscv_clk,
    riscv_resetn,
    s_axi_aclk,
    s_axi_aresetn);
  output [31:0]M01_AXI_araddr;
  output [2:0]M01_AXI_arprot;
  input [0:0]M01_AXI_arready;
  output [0:0]M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  output [2:0]M01_AXI_awprot;
  input [0:0]M01_AXI_awready;
  output [0:0]M01_AXI_awvalid;
  output [0:0]M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input [0:0]M01_AXI_bvalid;
  input [63:0]M01_AXI_rdata;
  output [0:0]M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input [0:0]M01_AXI_rvalid;
  output [63:0]M01_AXI_wdata;
  input [0:0]M01_AXI_wready;
  output [7:0]M01_AXI_wstrb;
  output [0:0]M01_AXI_wvalid;
  input [31:0]S_AXI_MEM_araddr;
  input [1:0]S_AXI_MEM_arburst;
  input [3:0]S_AXI_MEM_arcache;
  input [11:0]S_AXI_MEM_arid;
  input [7:0]S_AXI_MEM_arlen;
  input S_AXI_MEM_arlock;
  input [2:0]S_AXI_MEM_arprot;
  output S_AXI_MEM_arready;
  input [2:0]S_AXI_MEM_arsize;
  input S_AXI_MEM_arvalid;
  input [31:0]S_AXI_MEM_awaddr;
  input [1:0]S_AXI_MEM_awburst;
  input [3:0]S_AXI_MEM_awcache;
  input [11:0]S_AXI_MEM_awid;
  input [7:0]S_AXI_MEM_awlen;
  input S_AXI_MEM_awlock;
  input [2:0]S_AXI_MEM_awprot;
  output S_AXI_MEM_awready;
  input [2:0]S_AXI_MEM_awsize;
  input S_AXI_MEM_awvalid;
  output [11:0]S_AXI_MEM_bid;
  input S_AXI_MEM_bready;
  output [1:0]S_AXI_MEM_bresp;
  output S_AXI_MEM_bvalid;
  output [31:0]S_AXI_MEM_rdata;
  output [11:0]S_AXI_MEM_rid;
  output S_AXI_MEM_rlast;
  input S_AXI_MEM_rready;
  output [1:0]S_AXI_MEM_rresp;
  output S_AXI_MEM_rvalid;
  input [31:0]S_AXI_MEM_wdata;
  input S_AXI_MEM_wlast;
  output S_AXI_MEM_wready;
  input [3:0]S_AXI_MEM_wstrb;
  input S_AXI_MEM_wvalid;
  output irq;
  input por_resetn;
  input riscv_clk;
  input riscv_resetn;
  input s_axi_aclk;
  input s_axi_aresetn;

  wire [31:0]Conn1_ARADDR;
  wire [2:0]Conn1_ARPROT;
  wire [0:0]Conn1_ARREADY;
  wire [0:0]Conn1_ARVALID;
  wire [31:0]Conn1_AWADDR;
  wire [2:0]Conn1_AWPROT;
  wire [0:0]Conn1_AWREADY;
  wire [0:0]Conn1_AWVALID;
  wire [0:0]Conn1_BREADY;
  wire [1:0]Conn1_BRESP;
  wire [0:0]Conn1_BVALID;
  wire [63:0]Conn1_RDATA;
  wire [0:0]Conn1_RREADY;
  wire [1:0]Conn1_RRESP;
  wire [0:0]Conn1_RVALID;
  wire [63:0]Conn1_WDATA;
  wire [0:0]Conn1_WREADY;
  wire [7:0]Conn1_WSTRB;
  wire [0:0]Conn1_WVALID;
  wire [31:0]Conn2_ARADDR;
  wire [1:0]Conn2_ARBURST;
  wire [3:0]Conn2_ARCACHE;
  wire [11:0]Conn2_ARID;
  wire [7:0]Conn2_ARLEN;
  wire Conn2_ARLOCK;
  wire [2:0]Conn2_ARPROT;
  wire Conn2_ARREADY;
  wire [2:0]Conn2_ARSIZE;
  wire Conn2_ARVALID;
  wire [31:0]Conn2_AWADDR;
  wire [1:0]Conn2_AWBURST;
  wire [3:0]Conn2_AWCACHE;
  wire [11:0]Conn2_AWID;
  wire [7:0]Conn2_AWLEN;
  wire Conn2_AWLOCK;
  wire [2:0]Conn2_AWPROT;
  wire Conn2_AWREADY;
  wire [2:0]Conn2_AWSIZE;
  wire Conn2_AWVALID;
  wire [11:0]Conn2_BID;
  wire Conn2_BREADY;
  wire [1:0]Conn2_BRESP;
  wire Conn2_BVALID;
  wire [31:0]Conn2_RDATA;
  wire [11:0]Conn2_RID;
  wire Conn2_RLAST;
  wire Conn2_RREADY;
  wire [1:0]Conn2_RRESP;
  wire Conn2_RVALID;
  wire [31:0]Conn2_WDATA;
  wire Conn2_WLAST;
  wire Conn2_WREADY;
  wire [3:0]Conn2_WSTRB;
  wire Conn2_WVALID;
  wire aux_reset_in_1;
  wire clk_in1_1;
  wire ext_reset_in_1;
  wire [31:0]picorv32_0_mem_axi_ARADDR;
  wire [2:0]picorv32_0_mem_axi_ARPROT;
  wire picorv32_0_mem_axi_ARREADY;
  wire picorv32_0_mem_axi_ARVALID;
  wire [31:0]picorv32_0_mem_axi_AWADDR;
  wire [2:0]picorv32_0_mem_axi_AWPROT;
  wire picorv32_0_mem_axi_AWREADY;
  wire picorv32_0_mem_axi_AWVALID;
  wire picorv32_0_mem_axi_BREADY;
  wire picorv32_0_mem_axi_BVALID;
  wire [31:0]picorv32_0_mem_axi_RDATA;
  wire picorv32_0_mem_axi_RREADY;
  wire picorv32_0_mem_axi_RVALID;
  wire [31:0]picorv32_0_mem_axi_WDATA;
  wire picorv32_0_mem_axi_WREADY;
  wire [3:0]picorv32_0_mem_axi_WSTRB;
  wire picorv32_0_mem_axi_WVALID;
  wire picorv32_0_trap;
  wire [16:0]psBramController_BRAM_PORTA_ADDR;
  wire psBramController_BRAM_PORTA_CLK;
  wire [31:0]psBramController_BRAM_PORTA_DIN;
  wire [31:0]psBramController_BRAM_PORTA_DOUT;
  wire psBramController_BRAM_PORTA_EN;
  wire psBramController_BRAM_PORTA_RST;
  wire [3:0]psBramController_BRAM_PORTA_WE;
  wire [16:0]psDramInterface_M00_AXI_ARADDR;
  wire [2:0]psDramInterface_M00_AXI_ARPROT;
  wire psDramInterface_M00_AXI_ARREADY;
  wire psDramInterface_M00_AXI_ARVALID;
  wire [16:0]psDramInterface_M00_AXI_AWADDR;
  wire [2:0]psDramInterface_M00_AXI_AWPROT;
  wire psDramInterface_M00_AXI_AWREADY;
  wire psDramInterface_M00_AXI_AWVALID;
  wire psDramInterface_M00_AXI_BREADY;
  wire [1:0]psDramInterface_M00_AXI_BRESP;
  wire psDramInterface_M00_AXI_BVALID;
  wire [31:0]psDramInterface_M00_AXI_RDATA;
  wire psDramInterface_M00_AXI_RREADY;
  wire [1:0]psDramInterface_M00_AXI_RRESP;
  wire psDramInterface_M00_AXI_RVALID;
  wire [31:0]psDramInterface_M00_AXI_WDATA;
  wire psDramInterface_M00_AXI_WREADY;
  wire [3:0]psDramInterface_M00_AXI_WSTRB;
  wire psDramInterface_M00_AXI_WVALID;
  wire [16:0]riscvBramController_BRAM_PORTA_ADDR;
  wire riscvBramController_BRAM_PORTA_CLK;
  wire [31:0]riscvBramController_BRAM_PORTA_DIN;
  wire [31:0]riscvBramController_BRAM_PORTA_DOUT;
  wire riscvBramController_BRAM_PORTA_EN;
  wire riscvBramController_BRAM_PORTA_RST;
  wire [3:0]riscvBramController_BRAM_PORTA_WE;
  wire [0:0]riscvReset_peripheral_aresetn;
  wire s_axi_aresetn_1;

  assign Conn1_ARREADY = M01_AXI_arready[0];
  assign Conn1_AWREADY = M01_AXI_awready[0];
  assign Conn1_BRESP = M01_AXI_bresp[1:0];
  assign Conn1_BVALID = M01_AXI_bvalid[0];
  assign Conn1_RDATA = M01_AXI_rdata[63:0];
  assign Conn1_RRESP = M01_AXI_rresp[1:0];
  assign Conn1_RVALID = M01_AXI_rvalid[0];
  assign Conn1_WREADY = M01_AXI_wready[0];
  assign Conn2_ARADDR = S_AXI_MEM_araddr[31:0];
  assign Conn2_ARBURST = S_AXI_MEM_arburst[1:0];
  assign Conn2_ARCACHE = S_AXI_MEM_arcache[3:0];
  assign Conn2_ARID = S_AXI_MEM_arid[11:0];
  assign Conn2_ARLEN = S_AXI_MEM_arlen[7:0];
  assign Conn2_ARLOCK = S_AXI_MEM_arlock;
  assign Conn2_ARPROT = S_AXI_MEM_arprot[2:0];
  assign Conn2_ARSIZE = S_AXI_MEM_arsize[2:0];
  assign Conn2_ARVALID = S_AXI_MEM_arvalid;
  assign Conn2_AWADDR = S_AXI_MEM_awaddr[31:0];
  assign Conn2_AWBURST = S_AXI_MEM_awburst[1:0];
  assign Conn2_AWCACHE = S_AXI_MEM_awcache[3:0];
  assign Conn2_AWID = S_AXI_MEM_awid[11:0];
  assign Conn2_AWLEN = S_AXI_MEM_awlen[7:0];
  assign Conn2_AWLOCK = S_AXI_MEM_awlock;
  assign Conn2_AWPROT = S_AXI_MEM_awprot[2:0];
  assign Conn2_AWSIZE = S_AXI_MEM_awsize[2:0];
  assign Conn2_AWVALID = S_AXI_MEM_awvalid;
  assign Conn2_BREADY = S_AXI_MEM_bready;
  assign Conn2_RREADY = S_AXI_MEM_rready;
  assign Conn2_WDATA = S_AXI_MEM_wdata[31:0];
  assign Conn2_WLAST = S_AXI_MEM_wlast;
  assign Conn2_WSTRB = S_AXI_MEM_wstrb[3:0];
  assign Conn2_WVALID = S_AXI_MEM_wvalid;
  assign M01_AXI_araddr[31:0] = Conn1_ARADDR;
  assign M01_AXI_arprot[2:0] = Conn1_ARPROT;
  assign M01_AXI_arvalid[0] = Conn1_ARVALID;
  assign M01_AXI_awaddr[31:0] = Conn1_AWADDR;
  assign M01_AXI_awprot[2:0] = Conn1_AWPROT;
  assign M01_AXI_awvalid[0] = Conn1_AWVALID;
  assign M01_AXI_bready[0] = Conn1_BREADY;
  assign M01_AXI_rready[0] = Conn1_RREADY;
  assign M01_AXI_wdata[63:0] = Conn1_WDATA;
  assign M01_AXI_wstrb[7:0] = Conn1_WSTRB;
  assign M01_AXI_wvalid[0] = Conn1_WVALID;
  assign S_AXI_MEM_arready = Conn2_ARREADY;
  assign S_AXI_MEM_awready = Conn2_AWREADY;
  assign S_AXI_MEM_bid[11:0] = Conn2_BID;
  assign S_AXI_MEM_bresp[1:0] = Conn2_BRESP;
  assign S_AXI_MEM_bvalid = Conn2_BVALID;
  assign S_AXI_MEM_rdata[31:0] = Conn2_RDATA;
  assign S_AXI_MEM_rid[11:0] = Conn2_RID;
  assign S_AXI_MEM_rlast = Conn2_RLAST;
  assign S_AXI_MEM_rresp[1:0] = Conn2_RRESP;
  assign S_AXI_MEM_rvalid = Conn2_RVALID;
  assign S_AXI_MEM_wready = Conn2_WREADY;
  assign aux_reset_in_1 = riscv_resetn;
  assign clk_in1_1 = s_axi_aclk;
  assign ext_reset_in_1 = por_resetn;
  assign irq = picorv32_0_trap;
  assign s_axi_aresetn_1 = s_axi_aresetn;
  cerberus_picorv32_0_0 picorv32_0
       (.clk(clk_in1_1),
        .irq({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .mem_axi_araddr(picorv32_0_mem_axi_ARADDR),
        .mem_axi_arprot(picorv32_0_mem_axi_ARPROT),
        .mem_axi_arready(picorv32_0_mem_axi_ARREADY),
        .mem_axi_arvalid(picorv32_0_mem_axi_ARVALID),
        .mem_axi_awaddr(picorv32_0_mem_axi_AWADDR),
        .mem_axi_awprot(picorv32_0_mem_axi_AWPROT),
        .mem_axi_awready(picorv32_0_mem_axi_AWREADY),
        .mem_axi_awvalid(picorv32_0_mem_axi_AWVALID),
        .mem_axi_bready(picorv32_0_mem_axi_BREADY),
        .mem_axi_bvalid(picorv32_0_mem_axi_BVALID),
        .mem_axi_rdata(picorv32_0_mem_axi_RDATA),
        .mem_axi_rready(picorv32_0_mem_axi_RREADY),
        .mem_axi_rvalid(picorv32_0_mem_axi_RVALID),
        .mem_axi_wdata(picorv32_0_mem_axi_WDATA),
        .mem_axi_wready(picorv32_0_mem_axi_WREADY),
        .mem_axi_wstrb(picorv32_0_mem_axi_WSTRB),
        .mem_axi_wvalid(picorv32_0_mem_axi_WVALID),
        .pcpi_rd({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pcpi_ready(1'b0),
        .pcpi_wait(1'b0),
        .pcpi_wr(1'b0),
        .resetn(riscvReset_peripheral_aresetn),
        .trap(picorv32_0_trap));
  (* BMM_INFO_ADDRESS_SPACE = "byte  0x40020000 32 > cerberus cerberusProcessor1/riscvBram" *) 
  (* KEEP_HIERARCHY = "yes" *) 
  cerberus_psBramController_0 psBramController
       (.bram_addr_a(psBramController_BRAM_PORTA_ADDR),
        .bram_clk_a(psBramController_BRAM_PORTA_CLK),
        .bram_en_a(psBramController_BRAM_PORTA_EN),
        .bram_rddata_a(psBramController_BRAM_PORTA_DOUT),
        .bram_rst_a(psBramController_BRAM_PORTA_RST),
        .bram_we_a(psBramController_BRAM_PORTA_WE),
        .bram_wrdata_a(psBramController_BRAM_PORTA_DIN),
        .s_axi_aclk(clk_in1_1),
        .s_axi_araddr(Conn2_ARADDR[16:0]),
        .s_axi_arburst(Conn2_ARBURST),
        .s_axi_arcache(Conn2_ARCACHE),
        .s_axi_aresetn(s_axi_aresetn_1),
        .s_axi_arid(Conn2_ARID),
        .s_axi_arlen(Conn2_ARLEN),
        .s_axi_arlock(Conn2_ARLOCK),
        .s_axi_arprot(Conn2_ARPROT),
        .s_axi_arready(Conn2_ARREADY),
        .s_axi_arsize(Conn2_ARSIZE),
        .s_axi_arvalid(Conn2_ARVALID),
        .s_axi_awaddr(Conn2_AWADDR[16:0]),
        .s_axi_awburst(Conn2_AWBURST),
        .s_axi_awcache(Conn2_AWCACHE),
        .s_axi_awid(Conn2_AWID),
        .s_axi_awlen(Conn2_AWLEN),
        .s_axi_awlock(Conn2_AWLOCK),
        .s_axi_awprot(Conn2_AWPROT),
        .s_axi_awready(Conn2_AWREADY),
        .s_axi_awsize(Conn2_AWSIZE),
        .s_axi_awvalid(Conn2_AWVALID),
        .s_axi_bid(Conn2_BID),
        .s_axi_bready(Conn2_BREADY),
        .s_axi_bresp(Conn2_BRESP),
        .s_axi_bvalid(Conn2_BVALID),
        .s_axi_rdata(Conn2_RDATA),
        .s_axi_rid(Conn2_RID),
        .s_axi_rlast(Conn2_RLAST),
        .s_axi_rready(Conn2_RREADY),
        .s_axi_rresp(Conn2_RRESP),
        .s_axi_rvalid(Conn2_RVALID),
        .s_axi_wdata(Conn2_WDATA),
        .s_axi_wlast(Conn2_WLAST),
        .s_axi_wready(Conn2_WREADY),
        .s_axi_wstrb(Conn2_WSTRB),
        .s_axi_wvalid(Conn2_WVALID));
  cerberus_psDramInterface_0 psDramInterface
       (.ACLK(clk_in1_1),
        .ARESETN(s_axi_aresetn_1),
        .M00_ACLK(clk_in1_1),
        .M00_ARESETN(s_axi_aresetn_1),
        .M00_AXI_araddr(psDramInterface_M00_AXI_ARADDR),
        .M00_AXI_arprot(psDramInterface_M00_AXI_ARPROT),
        .M00_AXI_arready(psDramInterface_M00_AXI_ARREADY),
        .M00_AXI_arvalid(psDramInterface_M00_AXI_ARVALID),
        .M00_AXI_awaddr(psDramInterface_M00_AXI_AWADDR),
        .M00_AXI_awprot(psDramInterface_M00_AXI_AWPROT),
        .M00_AXI_awready(psDramInterface_M00_AXI_AWREADY),
        .M00_AXI_awvalid(psDramInterface_M00_AXI_AWVALID),
        .M00_AXI_bready(psDramInterface_M00_AXI_BREADY),
        .M00_AXI_bresp(psDramInterface_M00_AXI_BRESP),
        .M00_AXI_bvalid(psDramInterface_M00_AXI_BVALID),
        .M00_AXI_rdata(psDramInterface_M00_AXI_RDATA),
        .M00_AXI_rready(psDramInterface_M00_AXI_RREADY),
        .M00_AXI_rresp(psDramInterface_M00_AXI_RRESP),
        .M00_AXI_rvalid(psDramInterface_M00_AXI_RVALID),
        .M00_AXI_wdata(psDramInterface_M00_AXI_WDATA),
        .M00_AXI_wready(psDramInterface_M00_AXI_WREADY),
        .M00_AXI_wstrb(psDramInterface_M00_AXI_WSTRB),
        .M00_AXI_wvalid(psDramInterface_M00_AXI_WVALID),
        .M01_ACLK(clk_in1_1),
        .M01_ARESETN(s_axi_aresetn_1),
        .M01_AXI_araddr(Conn1_ARADDR),
        .M01_AXI_arprot(Conn1_ARPROT),
        .M01_AXI_arready(Conn1_ARREADY),
        .M01_AXI_arvalid(Conn1_ARVALID),
        .M01_AXI_awaddr(Conn1_AWADDR),
        .M01_AXI_awprot(Conn1_AWPROT),
        .M01_AXI_awready(Conn1_AWREADY),
        .M01_AXI_awvalid(Conn1_AWVALID),
        .M01_AXI_bready(Conn1_BREADY),
        .M01_AXI_bresp(Conn1_BRESP),
        .M01_AXI_bvalid(Conn1_BVALID),
        .M01_AXI_rdata(Conn1_RDATA),
        .M01_AXI_rready(Conn1_RREADY),
        .M01_AXI_rresp(Conn1_RRESP),
        .M01_AXI_rvalid(Conn1_RVALID),
        .M01_AXI_wdata(Conn1_WDATA),
        .M01_AXI_wready(Conn1_WREADY),
        .M01_AXI_wstrb(Conn1_WSTRB),
        .M01_AXI_wvalid(Conn1_WVALID),
        .S00_ACLK(clk_in1_1),
        .S00_ARESETN(s_axi_aresetn_1),
        .S00_AXI_araddr(picorv32_0_mem_axi_ARADDR),
        .S00_AXI_arprot(picorv32_0_mem_axi_ARPROT),
        .S00_AXI_arready(picorv32_0_mem_axi_ARREADY),
        .S00_AXI_arvalid(picorv32_0_mem_axi_ARVALID),
        .S00_AXI_awaddr(picorv32_0_mem_axi_AWADDR),
        .S00_AXI_awprot(picorv32_0_mem_axi_AWPROT),
        .S00_AXI_awready(picorv32_0_mem_axi_AWREADY),
        .S00_AXI_awvalid(picorv32_0_mem_axi_AWVALID),
        .S00_AXI_bready(picorv32_0_mem_axi_BREADY),
        .S00_AXI_bvalid(picorv32_0_mem_axi_BVALID),
        .S00_AXI_rdata(picorv32_0_mem_axi_RDATA),
        .S00_AXI_rready(picorv32_0_mem_axi_RREADY),
        .S00_AXI_rvalid(picorv32_0_mem_axi_RVALID),
        .S00_AXI_wdata(picorv32_0_mem_axi_WDATA),
        .S00_AXI_wready(picorv32_0_mem_axi_WREADY),
        .S00_AXI_wstrb(picorv32_0_mem_axi_WSTRB),
        .S00_AXI_wvalid(picorv32_0_mem_axi_WVALID));
  cerberus_riscvBram_0 riscvBram
       (.addra({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,riscvBramController_BRAM_PORTA_ADDR}),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,psBramController_BRAM_PORTA_ADDR}),
        .clka(riscvBramController_BRAM_PORTA_CLK),
        .clkb(psBramController_BRAM_PORTA_CLK),
        .dina(riscvBramController_BRAM_PORTA_DIN),
        .dinb(psBramController_BRAM_PORTA_DIN),
        .douta(riscvBramController_BRAM_PORTA_DOUT),
        .doutb(psBramController_BRAM_PORTA_DOUT),
        .ena(riscvBramController_BRAM_PORTA_EN),
        .enb(psBramController_BRAM_PORTA_EN),
        .rsta(riscvBramController_BRAM_PORTA_RST),
        .rstb(psBramController_BRAM_PORTA_RST),
        .wea(riscvBramController_BRAM_PORTA_WE),
        .web(psBramController_BRAM_PORTA_WE));
  cerberus_riscvBramController_0 riscvBramController
       (.bram_addr_a(riscvBramController_BRAM_PORTA_ADDR),
        .bram_clk_a(riscvBramController_BRAM_PORTA_CLK),
        .bram_en_a(riscvBramController_BRAM_PORTA_EN),
        .bram_rddata_a(riscvBramController_BRAM_PORTA_DOUT),
        .bram_rst_a(riscvBramController_BRAM_PORTA_RST),
        .bram_we_a(riscvBramController_BRAM_PORTA_WE),
        .bram_wrdata_a(riscvBramController_BRAM_PORTA_DIN),
        .s_axi_aclk(clk_in1_1),
        .s_axi_araddr(psDramInterface_M00_AXI_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_1),
        .s_axi_arprot(psDramInterface_M00_AXI_ARPROT),
        .s_axi_arready(psDramInterface_M00_AXI_ARREADY),
        .s_axi_arvalid(psDramInterface_M00_AXI_ARVALID),
        .s_axi_awaddr(psDramInterface_M00_AXI_AWADDR),
        .s_axi_awprot(psDramInterface_M00_AXI_AWPROT),
        .s_axi_awready(psDramInterface_M00_AXI_AWREADY),
        .s_axi_awvalid(psDramInterface_M00_AXI_AWVALID),
        .s_axi_bready(psDramInterface_M00_AXI_BREADY),
        .s_axi_bresp(psDramInterface_M00_AXI_BRESP),
        .s_axi_bvalid(psDramInterface_M00_AXI_BVALID),
        .s_axi_rdata(psDramInterface_M00_AXI_RDATA),
        .s_axi_rready(psDramInterface_M00_AXI_RREADY),
        .s_axi_rresp(psDramInterface_M00_AXI_RRESP),
        .s_axi_rvalid(psDramInterface_M00_AXI_RVALID),
        .s_axi_wdata(psDramInterface_M00_AXI_WDATA),
        .s_axi_wready(psDramInterface_M00_AXI_WREADY),
        .s_axi_wstrb(psDramInterface_M00_AXI_WSTRB),
        .s_axi_wvalid(psDramInterface_M00_AXI_WVALID));
  cerberus_riscvReset_0 riscvReset
       (.aux_reset_in(aux_reset_in_1),
        .dcm_locked(1'b1),
        .ext_reset_in(ext_reset_in_1),
        .mb_debug_sys_rst(1'b0),
        .peripheral_aresetn(riscvReset_peripheral_aresetn),
        .slowest_sync_clk(clk_in1_1));
endmodule

module cerberusProcessor2_imp_Y9ISJI
   (M01_AXI_araddr,
    M01_AXI_arprot,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awprot,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wstrb,
    M01_AXI_wvalid,
    S_AXI_MEM_araddr,
    S_AXI_MEM_arburst,
    S_AXI_MEM_arcache,
    S_AXI_MEM_arid,
    S_AXI_MEM_arlen,
    S_AXI_MEM_arlock,
    S_AXI_MEM_arprot,
    S_AXI_MEM_arready,
    S_AXI_MEM_arsize,
    S_AXI_MEM_arvalid,
    S_AXI_MEM_awaddr,
    S_AXI_MEM_awburst,
    S_AXI_MEM_awcache,
    S_AXI_MEM_awid,
    S_AXI_MEM_awlen,
    S_AXI_MEM_awlock,
    S_AXI_MEM_awprot,
    S_AXI_MEM_awready,
    S_AXI_MEM_awsize,
    S_AXI_MEM_awvalid,
    S_AXI_MEM_bid,
    S_AXI_MEM_bready,
    S_AXI_MEM_bresp,
    S_AXI_MEM_bvalid,
    S_AXI_MEM_rdata,
    S_AXI_MEM_rid,
    S_AXI_MEM_rlast,
    S_AXI_MEM_rready,
    S_AXI_MEM_rresp,
    S_AXI_MEM_rvalid,
    S_AXI_MEM_wdata,
    S_AXI_MEM_wlast,
    S_AXI_MEM_wready,
    S_AXI_MEM_wstrb,
    S_AXI_MEM_wvalid,
    irq,
    por_resetn,
    riscv_clk,
    riscv_resetn,
    s_axi_aclk,
    s_axi_aresetn);
  output [31:0]M01_AXI_araddr;
  output [2:0]M01_AXI_arprot;
  input [0:0]M01_AXI_arready;
  output [0:0]M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  output [2:0]M01_AXI_awprot;
  input [0:0]M01_AXI_awready;
  output [0:0]M01_AXI_awvalid;
  output [0:0]M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input [0:0]M01_AXI_bvalid;
  input [63:0]M01_AXI_rdata;
  output [0:0]M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input [0:0]M01_AXI_rvalid;
  output [63:0]M01_AXI_wdata;
  input [0:0]M01_AXI_wready;
  output [7:0]M01_AXI_wstrb;
  output [0:0]M01_AXI_wvalid;
  input [31:0]S_AXI_MEM_araddr;
  input [1:0]S_AXI_MEM_arburst;
  input [3:0]S_AXI_MEM_arcache;
  input [11:0]S_AXI_MEM_arid;
  input [7:0]S_AXI_MEM_arlen;
  input S_AXI_MEM_arlock;
  input [2:0]S_AXI_MEM_arprot;
  output S_AXI_MEM_arready;
  input [2:0]S_AXI_MEM_arsize;
  input S_AXI_MEM_arvalid;
  input [31:0]S_AXI_MEM_awaddr;
  input [1:0]S_AXI_MEM_awburst;
  input [3:0]S_AXI_MEM_awcache;
  input [11:0]S_AXI_MEM_awid;
  input [7:0]S_AXI_MEM_awlen;
  input S_AXI_MEM_awlock;
  input [2:0]S_AXI_MEM_awprot;
  output S_AXI_MEM_awready;
  input [2:0]S_AXI_MEM_awsize;
  input S_AXI_MEM_awvalid;
  output [11:0]S_AXI_MEM_bid;
  input S_AXI_MEM_bready;
  output [1:0]S_AXI_MEM_bresp;
  output S_AXI_MEM_bvalid;
  output [31:0]S_AXI_MEM_rdata;
  output [11:0]S_AXI_MEM_rid;
  output S_AXI_MEM_rlast;
  input S_AXI_MEM_rready;
  output [1:0]S_AXI_MEM_rresp;
  output S_AXI_MEM_rvalid;
  input [31:0]S_AXI_MEM_wdata;
  input S_AXI_MEM_wlast;
  output S_AXI_MEM_wready;
  input [3:0]S_AXI_MEM_wstrb;
  input S_AXI_MEM_wvalid;
  output irq;
  input por_resetn;
  input riscv_clk;
  input riscv_resetn;
  input s_axi_aclk;
  input s_axi_aresetn;

  wire [31:0]Conn1_ARADDR;
  wire [2:0]Conn1_ARPROT;
  wire [0:0]Conn1_ARREADY;
  wire [0:0]Conn1_ARVALID;
  wire [31:0]Conn1_AWADDR;
  wire [2:0]Conn1_AWPROT;
  wire [0:0]Conn1_AWREADY;
  wire [0:0]Conn1_AWVALID;
  wire [0:0]Conn1_BREADY;
  wire [1:0]Conn1_BRESP;
  wire [0:0]Conn1_BVALID;
  wire [63:0]Conn1_RDATA;
  wire [0:0]Conn1_RREADY;
  wire [1:0]Conn1_RRESP;
  wire [0:0]Conn1_RVALID;
  wire [63:0]Conn1_WDATA;
  wire [0:0]Conn1_WREADY;
  wire [7:0]Conn1_WSTRB;
  wire [0:0]Conn1_WVALID;
  wire [31:0]Conn2_ARADDR;
  wire [1:0]Conn2_ARBURST;
  wire [3:0]Conn2_ARCACHE;
  wire [11:0]Conn2_ARID;
  wire [7:0]Conn2_ARLEN;
  wire Conn2_ARLOCK;
  wire [2:0]Conn2_ARPROT;
  wire Conn2_ARREADY;
  wire [2:0]Conn2_ARSIZE;
  wire Conn2_ARVALID;
  wire [31:0]Conn2_AWADDR;
  wire [1:0]Conn2_AWBURST;
  wire [3:0]Conn2_AWCACHE;
  wire [11:0]Conn2_AWID;
  wire [7:0]Conn2_AWLEN;
  wire Conn2_AWLOCK;
  wire [2:0]Conn2_AWPROT;
  wire Conn2_AWREADY;
  wire [2:0]Conn2_AWSIZE;
  wire Conn2_AWVALID;
  wire [11:0]Conn2_BID;
  wire Conn2_BREADY;
  wire [1:0]Conn2_BRESP;
  wire Conn2_BVALID;
  wire [31:0]Conn2_RDATA;
  wire [11:0]Conn2_RID;
  wire Conn2_RLAST;
  wire Conn2_RREADY;
  wire [1:0]Conn2_RRESP;
  wire Conn2_RVALID;
  wire [31:0]Conn2_WDATA;
  wire Conn2_WLAST;
  wire Conn2_WREADY;
  wire [3:0]Conn2_WSTRB;
  wire Conn2_WVALID;
  wire aux_reset_in_1;
  wire clk_in1_1;
  wire ext_reset_in_1;
  wire [31:0]picorv32_0_mem_axi_ARADDR;
  wire [2:0]picorv32_0_mem_axi_ARPROT;
  wire picorv32_0_mem_axi_ARREADY;
  wire picorv32_0_mem_axi_ARVALID;
  wire [31:0]picorv32_0_mem_axi_AWADDR;
  wire [2:0]picorv32_0_mem_axi_AWPROT;
  wire picorv32_0_mem_axi_AWREADY;
  wire picorv32_0_mem_axi_AWVALID;
  wire picorv32_0_mem_axi_BREADY;
  wire picorv32_0_mem_axi_BVALID;
  wire [31:0]picorv32_0_mem_axi_RDATA;
  wire picorv32_0_mem_axi_RREADY;
  wire picorv32_0_mem_axi_RVALID;
  wire [31:0]picorv32_0_mem_axi_WDATA;
  wire picorv32_0_mem_axi_WREADY;
  wire [3:0]picorv32_0_mem_axi_WSTRB;
  wire picorv32_0_mem_axi_WVALID;
  wire picorv32_0_trap;
  wire [16:0]psBramController_BRAM_PORTA_ADDR;
  wire psBramController_BRAM_PORTA_CLK;
  wire [31:0]psBramController_BRAM_PORTA_DIN;
  wire [31:0]psBramController_BRAM_PORTA_DOUT;
  wire psBramController_BRAM_PORTA_EN;
  wire psBramController_BRAM_PORTA_RST;
  wire [3:0]psBramController_BRAM_PORTA_WE;
  wire [16:0]psDramInterface_M00_AXI_ARADDR;
  wire [2:0]psDramInterface_M00_AXI_ARPROT;
  wire psDramInterface_M00_AXI_ARREADY;
  wire psDramInterface_M00_AXI_ARVALID;
  wire [16:0]psDramInterface_M00_AXI_AWADDR;
  wire [2:0]psDramInterface_M00_AXI_AWPROT;
  wire psDramInterface_M00_AXI_AWREADY;
  wire psDramInterface_M00_AXI_AWVALID;
  wire psDramInterface_M00_AXI_BREADY;
  wire [1:0]psDramInterface_M00_AXI_BRESP;
  wire psDramInterface_M00_AXI_BVALID;
  wire [31:0]psDramInterface_M00_AXI_RDATA;
  wire psDramInterface_M00_AXI_RREADY;
  wire [1:0]psDramInterface_M00_AXI_RRESP;
  wire psDramInterface_M00_AXI_RVALID;
  wire [31:0]psDramInterface_M00_AXI_WDATA;
  wire psDramInterface_M00_AXI_WREADY;
  wire [3:0]psDramInterface_M00_AXI_WSTRB;
  wire psDramInterface_M00_AXI_WVALID;
  wire [16:0]riscvBramController_BRAM_PORTA_ADDR;
  wire riscvBramController_BRAM_PORTA_CLK;
  wire [31:0]riscvBramController_BRAM_PORTA_DIN;
  wire [31:0]riscvBramController_BRAM_PORTA_DOUT;
  wire riscvBramController_BRAM_PORTA_EN;
  wire riscvBramController_BRAM_PORTA_RST;
  wire [3:0]riscvBramController_BRAM_PORTA_WE;
  wire [0:0]riscvReset_peripheral_aresetn;
  wire s_axi_aresetn_1;

  assign Conn1_ARREADY = M01_AXI_arready[0];
  assign Conn1_AWREADY = M01_AXI_awready[0];
  assign Conn1_BRESP = M01_AXI_bresp[1:0];
  assign Conn1_BVALID = M01_AXI_bvalid[0];
  assign Conn1_RDATA = M01_AXI_rdata[63:0];
  assign Conn1_RRESP = M01_AXI_rresp[1:0];
  assign Conn1_RVALID = M01_AXI_rvalid[0];
  assign Conn1_WREADY = M01_AXI_wready[0];
  assign Conn2_ARADDR = S_AXI_MEM_araddr[31:0];
  assign Conn2_ARBURST = S_AXI_MEM_arburst[1:0];
  assign Conn2_ARCACHE = S_AXI_MEM_arcache[3:0];
  assign Conn2_ARID = S_AXI_MEM_arid[11:0];
  assign Conn2_ARLEN = S_AXI_MEM_arlen[7:0];
  assign Conn2_ARLOCK = S_AXI_MEM_arlock;
  assign Conn2_ARPROT = S_AXI_MEM_arprot[2:0];
  assign Conn2_ARSIZE = S_AXI_MEM_arsize[2:0];
  assign Conn2_ARVALID = S_AXI_MEM_arvalid;
  assign Conn2_AWADDR = S_AXI_MEM_awaddr[31:0];
  assign Conn2_AWBURST = S_AXI_MEM_awburst[1:0];
  assign Conn2_AWCACHE = S_AXI_MEM_awcache[3:0];
  assign Conn2_AWID = S_AXI_MEM_awid[11:0];
  assign Conn2_AWLEN = S_AXI_MEM_awlen[7:0];
  assign Conn2_AWLOCK = S_AXI_MEM_awlock;
  assign Conn2_AWPROT = S_AXI_MEM_awprot[2:0];
  assign Conn2_AWSIZE = S_AXI_MEM_awsize[2:0];
  assign Conn2_AWVALID = S_AXI_MEM_awvalid;
  assign Conn2_BREADY = S_AXI_MEM_bready;
  assign Conn2_RREADY = S_AXI_MEM_rready;
  assign Conn2_WDATA = S_AXI_MEM_wdata[31:0];
  assign Conn2_WLAST = S_AXI_MEM_wlast;
  assign Conn2_WSTRB = S_AXI_MEM_wstrb[3:0];
  assign Conn2_WVALID = S_AXI_MEM_wvalid;
  assign M01_AXI_araddr[31:0] = Conn1_ARADDR;
  assign M01_AXI_arprot[2:0] = Conn1_ARPROT;
  assign M01_AXI_arvalid[0] = Conn1_ARVALID;
  assign M01_AXI_awaddr[31:0] = Conn1_AWADDR;
  assign M01_AXI_awprot[2:0] = Conn1_AWPROT;
  assign M01_AXI_awvalid[0] = Conn1_AWVALID;
  assign M01_AXI_bready[0] = Conn1_BREADY;
  assign M01_AXI_rready[0] = Conn1_RREADY;
  assign M01_AXI_wdata[63:0] = Conn1_WDATA;
  assign M01_AXI_wstrb[7:0] = Conn1_WSTRB;
  assign M01_AXI_wvalid[0] = Conn1_WVALID;
  assign S_AXI_MEM_arready = Conn2_ARREADY;
  assign S_AXI_MEM_awready = Conn2_AWREADY;
  assign S_AXI_MEM_bid[11:0] = Conn2_BID;
  assign S_AXI_MEM_bresp[1:0] = Conn2_BRESP;
  assign S_AXI_MEM_bvalid = Conn2_BVALID;
  assign S_AXI_MEM_rdata[31:0] = Conn2_RDATA;
  assign S_AXI_MEM_rid[11:0] = Conn2_RID;
  assign S_AXI_MEM_rlast = Conn2_RLAST;
  assign S_AXI_MEM_rresp[1:0] = Conn2_RRESP;
  assign S_AXI_MEM_rvalid = Conn2_RVALID;
  assign S_AXI_MEM_wready = Conn2_WREADY;
  assign aux_reset_in_1 = riscv_resetn;
  assign clk_in1_1 = s_axi_aclk;
  assign ext_reset_in_1 = por_resetn;
  assign irq = picorv32_0_trap;
  assign s_axi_aresetn_1 = s_axi_aresetn;
  cerberus_picorv32_0_18 picorv32_0
       (.clk(clk_in1_1),
        .irq({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .mem_axi_araddr(picorv32_0_mem_axi_ARADDR),
        .mem_axi_arprot(picorv32_0_mem_axi_ARPROT),
        .mem_axi_arready(picorv32_0_mem_axi_ARREADY),
        .mem_axi_arvalid(picorv32_0_mem_axi_ARVALID),
        .mem_axi_awaddr(picorv32_0_mem_axi_AWADDR),
        .mem_axi_awprot(picorv32_0_mem_axi_AWPROT),
        .mem_axi_awready(picorv32_0_mem_axi_AWREADY),
        .mem_axi_awvalid(picorv32_0_mem_axi_AWVALID),
        .mem_axi_bready(picorv32_0_mem_axi_BREADY),
        .mem_axi_bvalid(picorv32_0_mem_axi_BVALID),
        .mem_axi_rdata(picorv32_0_mem_axi_RDATA),
        .mem_axi_rready(picorv32_0_mem_axi_RREADY),
        .mem_axi_rvalid(picorv32_0_mem_axi_RVALID),
        .mem_axi_wdata(picorv32_0_mem_axi_WDATA),
        .mem_axi_wready(picorv32_0_mem_axi_WREADY),
        .mem_axi_wstrb(picorv32_0_mem_axi_WSTRB),
        .mem_axi_wvalid(picorv32_0_mem_axi_WVALID),
        .pcpi_rd({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pcpi_ready(1'b0),
        .pcpi_wait(1'b0),
        .pcpi_wr(1'b0),
        .resetn(riscvReset_peripheral_aresetn),
        .trap(picorv32_0_trap));
  (* BMM_INFO_ADDRESS_SPACE = "byte  0x40040000 32 > cerberus cerberusProcessor2/riscvBram" *) 
  (* KEEP_HIERARCHY = "yes" *) 
  cerberus_psBramController_18 psBramController
       (.bram_addr_a(psBramController_BRAM_PORTA_ADDR),
        .bram_clk_a(psBramController_BRAM_PORTA_CLK),
        .bram_en_a(psBramController_BRAM_PORTA_EN),
        .bram_rddata_a(psBramController_BRAM_PORTA_DOUT),
        .bram_rst_a(psBramController_BRAM_PORTA_RST),
        .bram_we_a(psBramController_BRAM_PORTA_WE),
        .bram_wrdata_a(psBramController_BRAM_PORTA_DIN),
        .s_axi_aclk(clk_in1_1),
        .s_axi_araddr(Conn2_ARADDR[16:0]),
        .s_axi_arburst(Conn2_ARBURST),
        .s_axi_arcache(Conn2_ARCACHE),
        .s_axi_aresetn(s_axi_aresetn_1),
        .s_axi_arid(Conn2_ARID),
        .s_axi_arlen(Conn2_ARLEN),
        .s_axi_arlock(Conn2_ARLOCK),
        .s_axi_arprot(Conn2_ARPROT),
        .s_axi_arready(Conn2_ARREADY),
        .s_axi_arsize(Conn2_ARSIZE),
        .s_axi_arvalid(Conn2_ARVALID),
        .s_axi_awaddr(Conn2_AWADDR[16:0]),
        .s_axi_awburst(Conn2_AWBURST),
        .s_axi_awcache(Conn2_AWCACHE),
        .s_axi_awid(Conn2_AWID),
        .s_axi_awlen(Conn2_AWLEN),
        .s_axi_awlock(Conn2_AWLOCK),
        .s_axi_awprot(Conn2_AWPROT),
        .s_axi_awready(Conn2_AWREADY),
        .s_axi_awsize(Conn2_AWSIZE),
        .s_axi_awvalid(Conn2_AWVALID),
        .s_axi_bid(Conn2_BID),
        .s_axi_bready(Conn2_BREADY),
        .s_axi_bresp(Conn2_BRESP),
        .s_axi_bvalid(Conn2_BVALID),
        .s_axi_rdata(Conn2_RDATA),
        .s_axi_rid(Conn2_RID),
        .s_axi_rlast(Conn2_RLAST),
        .s_axi_rready(Conn2_RREADY),
        .s_axi_rresp(Conn2_RRESP),
        .s_axi_rvalid(Conn2_RVALID),
        .s_axi_wdata(Conn2_WDATA),
        .s_axi_wlast(Conn2_WLAST),
        .s_axi_wready(Conn2_WREADY),
        .s_axi_wstrb(Conn2_WSTRB),
        .s_axi_wvalid(Conn2_WVALID));
  cerberus_psDramInterface_3 psDramInterface
       (.ACLK(clk_in1_1),
        .ARESETN(s_axi_aresetn_1),
        .M00_ACLK(clk_in1_1),
        .M00_ARESETN(s_axi_aresetn_1),
        .M00_AXI_araddr(psDramInterface_M00_AXI_ARADDR),
        .M00_AXI_arprot(psDramInterface_M00_AXI_ARPROT),
        .M00_AXI_arready(psDramInterface_M00_AXI_ARREADY),
        .M00_AXI_arvalid(psDramInterface_M00_AXI_ARVALID),
        .M00_AXI_awaddr(psDramInterface_M00_AXI_AWADDR),
        .M00_AXI_awprot(psDramInterface_M00_AXI_AWPROT),
        .M00_AXI_awready(psDramInterface_M00_AXI_AWREADY),
        .M00_AXI_awvalid(psDramInterface_M00_AXI_AWVALID),
        .M00_AXI_bready(psDramInterface_M00_AXI_BREADY),
        .M00_AXI_bresp(psDramInterface_M00_AXI_BRESP),
        .M00_AXI_bvalid(psDramInterface_M00_AXI_BVALID),
        .M00_AXI_rdata(psDramInterface_M00_AXI_RDATA),
        .M00_AXI_rready(psDramInterface_M00_AXI_RREADY),
        .M00_AXI_rresp(psDramInterface_M00_AXI_RRESP),
        .M00_AXI_rvalid(psDramInterface_M00_AXI_RVALID),
        .M00_AXI_wdata(psDramInterface_M00_AXI_WDATA),
        .M00_AXI_wready(psDramInterface_M00_AXI_WREADY),
        .M00_AXI_wstrb(psDramInterface_M00_AXI_WSTRB),
        .M00_AXI_wvalid(psDramInterface_M00_AXI_WVALID),
        .M01_ACLK(clk_in1_1),
        .M01_ARESETN(s_axi_aresetn_1),
        .M01_AXI_araddr(Conn1_ARADDR),
        .M01_AXI_arprot(Conn1_ARPROT),
        .M01_AXI_arready(Conn1_ARREADY),
        .M01_AXI_arvalid(Conn1_ARVALID),
        .M01_AXI_awaddr(Conn1_AWADDR),
        .M01_AXI_awprot(Conn1_AWPROT),
        .M01_AXI_awready(Conn1_AWREADY),
        .M01_AXI_awvalid(Conn1_AWVALID),
        .M01_AXI_bready(Conn1_BREADY),
        .M01_AXI_bresp(Conn1_BRESP),
        .M01_AXI_bvalid(Conn1_BVALID),
        .M01_AXI_rdata(Conn1_RDATA),
        .M01_AXI_rready(Conn1_RREADY),
        .M01_AXI_rresp(Conn1_RRESP),
        .M01_AXI_rvalid(Conn1_RVALID),
        .M01_AXI_wdata(Conn1_WDATA),
        .M01_AXI_wready(Conn1_WREADY),
        .M01_AXI_wstrb(Conn1_WSTRB),
        .M01_AXI_wvalid(Conn1_WVALID),
        .S00_ACLK(clk_in1_1),
        .S00_ARESETN(s_axi_aresetn_1),
        .S00_AXI_araddr(picorv32_0_mem_axi_ARADDR),
        .S00_AXI_arprot(picorv32_0_mem_axi_ARPROT),
        .S00_AXI_arready(picorv32_0_mem_axi_ARREADY),
        .S00_AXI_arvalid(picorv32_0_mem_axi_ARVALID),
        .S00_AXI_awaddr(picorv32_0_mem_axi_AWADDR),
        .S00_AXI_awprot(picorv32_0_mem_axi_AWPROT),
        .S00_AXI_awready(picorv32_0_mem_axi_AWREADY),
        .S00_AXI_awvalid(picorv32_0_mem_axi_AWVALID),
        .S00_AXI_bready(picorv32_0_mem_axi_BREADY),
        .S00_AXI_bvalid(picorv32_0_mem_axi_BVALID),
        .S00_AXI_rdata(picorv32_0_mem_axi_RDATA),
        .S00_AXI_rready(picorv32_0_mem_axi_RREADY),
        .S00_AXI_rvalid(picorv32_0_mem_axi_RVALID),
        .S00_AXI_wdata(picorv32_0_mem_axi_WDATA),
        .S00_AXI_wready(picorv32_0_mem_axi_WREADY),
        .S00_AXI_wstrb(picorv32_0_mem_axi_WSTRB),
        .S00_AXI_wvalid(picorv32_0_mem_axi_WVALID));
  cerberus_riscvBram_18 riscvBram
       (.addra({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,riscvBramController_BRAM_PORTA_ADDR}),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,psBramController_BRAM_PORTA_ADDR}),
        .clka(riscvBramController_BRAM_PORTA_CLK),
        .clkb(psBramController_BRAM_PORTA_CLK),
        .dina(riscvBramController_BRAM_PORTA_DIN),
        .dinb(psBramController_BRAM_PORTA_DIN),
        .douta(riscvBramController_BRAM_PORTA_DOUT),
        .doutb(psBramController_BRAM_PORTA_DOUT),
        .ena(riscvBramController_BRAM_PORTA_EN),
        .enb(psBramController_BRAM_PORTA_EN),
        .rsta(riscvBramController_BRAM_PORTA_RST),
        .rstb(psBramController_BRAM_PORTA_RST),
        .wea(riscvBramController_BRAM_PORTA_WE),
        .web(psBramController_BRAM_PORTA_WE));
  cerberus_riscvBramController_18 riscvBramController
       (.bram_addr_a(riscvBramController_BRAM_PORTA_ADDR),
        .bram_clk_a(riscvBramController_BRAM_PORTA_CLK),
        .bram_en_a(riscvBramController_BRAM_PORTA_EN),
        .bram_rddata_a(riscvBramController_BRAM_PORTA_DOUT),
        .bram_rst_a(riscvBramController_BRAM_PORTA_RST),
        .bram_we_a(riscvBramController_BRAM_PORTA_WE),
        .bram_wrdata_a(riscvBramController_BRAM_PORTA_DIN),
        .s_axi_aclk(clk_in1_1),
        .s_axi_araddr(psDramInterface_M00_AXI_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_1),
        .s_axi_arprot(psDramInterface_M00_AXI_ARPROT),
        .s_axi_arready(psDramInterface_M00_AXI_ARREADY),
        .s_axi_arvalid(psDramInterface_M00_AXI_ARVALID),
        .s_axi_awaddr(psDramInterface_M00_AXI_AWADDR),
        .s_axi_awprot(psDramInterface_M00_AXI_AWPROT),
        .s_axi_awready(psDramInterface_M00_AXI_AWREADY),
        .s_axi_awvalid(psDramInterface_M00_AXI_AWVALID),
        .s_axi_bready(psDramInterface_M00_AXI_BREADY),
        .s_axi_bresp(psDramInterface_M00_AXI_BRESP),
        .s_axi_bvalid(psDramInterface_M00_AXI_BVALID),
        .s_axi_rdata(psDramInterface_M00_AXI_RDATA),
        .s_axi_rready(psDramInterface_M00_AXI_RREADY),
        .s_axi_rresp(psDramInterface_M00_AXI_RRESP),
        .s_axi_rvalid(psDramInterface_M00_AXI_RVALID),
        .s_axi_wdata(psDramInterface_M00_AXI_WDATA),
        .s_axi_wready(psDramInterface_M00_AXI_WREADY),
        .s_axi_wstrb(psDramInterface_M00_AXI_WSTRB),
        .s_axi_wvalid(psDramInterface_M00_AXI_WVALID));
  cerberus_riscvReset_18 riscvReset
       (.aux_reset_in(aux_reset_in_1),
        .dcm_locked(1'b1),
        .ext_reset_in(ext_reset_in_1),
        .mb_debug_sys_rst(1'b0),
        .peripheral_aresetn(riscvReset_peripheral_aresetn),
        .slowest_sync_clk(clk_in1_1));
endmodule

module cerberusProcessor3_imp_TZ1NST
   (M01_AXI_araddr,
    M01_AXI_arprot,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awprot,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wstrb,
    M01_AXI_wvalid,
    S_AXI_MEM_araddr,
    S_AXI_MEM_arburst,
    S_AXI_MEM_arcache,
    S_AXI_MEM_arid,
    S_AXI_MEM_arlen,
    S_AXI_MEM_arlock,
    S_AXI_MEM_arprot,
    S_AXI_MEM_arready,
    S_AXI_MEM_arsize,
    S_AXI_MEM_arvalid,
    S_AXI_MEM_awaddr,
    S_AXI_MEM_awburst,
    S_AXI_MEM_awcache,
    S_AXI_MEM_awid,
    S_AXI_MEM_awlen,
    S_AXI_MEM_awlock,
    S_AXI_MEM_awprot,
    S_AXI_MEM_awready,
    S_AXI_MEM_awsize,
    S_AXI_MEM_awvalid,
    S_AXI_MEM_bid,
    S_AXI_MEM_bready,
    S_AXI_MEM_bresp,
    S_AXI_MEM_bvalid,
    S_AXI_MEM_rdata,
    S_AXI_MEM_rid,
    S_AXI_MEM_rlast,
    S_AXI_MEM_rready,
    S_AXI_MEM_rresp,
    S_AXI_MEM_rvalid,
    S_AXI_MEM_wdata,
    S_AXI_MEM_wlast,
    S_AXI_MEM_wready,
    S_AXI_MEM_wstrb,
    S_AXI_MEM_wvalid,
    irq,
    por_resetn,
    riscv_clk,
    riscv_resetn,
    s_axi_aclk,
    s_axi_aresetn);
  output [31:0]M01_AXI_araddr;
  output [2:0]M01_AXI_arprot;
  input [0:0]M01_AXI_arready;
  output [0:0]M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  output [2:0]M01_AXI_awprot;
  input [0:0]M01_AXI_awready;
  output [0:0]M01_AXI_awvalid;
  output [0:0]M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input [0:0]M01_AXI_bvalid;
  input [63:0]M01_AXI_rdata;
  output [0:0]M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input [0:0]M01_AXI_rvalid;
  output [63:0]M01_AXI_wdata;
  input [0:0]M01_AXI_wready;
  output [7:0]M01_AXI_wstrb;
  output [0:0]M01_AXI_wvalid;
  input [31:0]S_AXI_MEM_araddr;
  input [1:0]S_AXI_MEM_arburst;
  input [3:0]S_AXI_MEM_arcache;
  input [11:0]S_AXI_MEM_arid;
  input [7:0]S_AXI_MEM_arlen;
  input S_AXI_MEM_arlock;
  input [2:0]S_AXI_MEM_arprot;
  output S_AXI_MEM_arready;
  input [2:0]S_AXI_MEM_arsize;
  input S_AXI_MEM_arvalid;
  input [31:0]S_AXI_MEM_awaddr;
  input [1:0]S_AXI_MEM_awburst;
  input [3:0]S_AXI_MEM_awcache;
  input [11:0]S_AXI_MEM_awid;
  input [7:0]S_AXI_MEM_awlen;
  input S_AXI_MEM_awlock;
  input [2:0]S_AXI_MEM_awprot;
  output S_AXI_MEM_awready;
  input [2:0]S_AXI_MEM_awsize;
  input S_AXI_MEM_awvalid;
  output [11:0]S_AXI_MEM_bid;
  input S_AXI_MEM_bready;
  output [1:0]S_AXI_MEM_bresp;
  output S_AXI_MEM_bvalid;
  output [31:0]S_AXI_MEM_rdata;
  output [11:0]S_AXI_MEM_rid;
  output S_AXI_MEM_rlast;
  input S_AXI_MEM_rready;
  output [1:0]S_AXI_MEM_rresp;
  output S_AXI_MEM_rvalid;
  input [31:0]S_AXI_MEM_wdata;
  input S_AXI_MEM_wlast;
  output S_AXI_MEM_wready;
  input [3:0]S_AXI_MEM_wstrb;
  input S_AXI_MEM_wvalid;
  output irq;
  input por_resetn;
  input riscv_clk;
  input riscv_resetn;
  input s_axi_aclk;
  input s_axi_aresetn;

  wire [31:0]Conn1_ARADDR;
  wire [2:0]Conn1_ARPROT;
  wire [0:0]Conn1_ARREADY;
  wire [0:0]Conn1_ARVALID;
  wire [31:0]Conn1_AWADDR;
  wire [2:0]Conn1_AWPROT;
  wire [0:0]Conn1_AWREADY;
  wire [0:0]Conn1_AWVALID;
  wire [0:0]Conn1_BREADY;
  wire [1:0]Conn1_BRESP;
  wire [0:0]Conn1_BVALID;
  wire [63:0]Conn1_RDATA;
  wire [0:0]Conn1_RREADY;
  wire [1:0]Conn1_RRESP;
  wire [0:0]Conn1_RVALID;
  wire [63:0]Conn1_WDATA;
  wire [0:0]Conn1_WREADY;
  wire [7:0]Conn1_WSTRB;
  wire [0:0]Conn1_WVALID;
  wire [31:0]Conn2_ARADDR;
  wire [1:0]Conn2_ARBURST;
  wire [3:0]Conn2_ARCACHE;
  wire [11:0]Conn2_ARID;
  wire [7:0]Conn2_ARLEN;
  wire Conn2_ARLOCK;
  wire [2:0]Conn2_ARPROT;
  wire Conn2_ARREADY;
  wire [2:0]Conn2_ARSIZE;
  wire Conn2_ARVALID;
  wire [31:0]Conn2_AWADDR;
  wire [1:0]Conn2_AWBURST;
  wire [3:0]Conn2_AWCACHE;
  wire [11:0]Conn2_AWID;
  wire [7:0]Conn2_AWLEN;
  wire Conn2_AWLOCK;
  wire [2:0]Conn2_AWPROT;
  wire Conn2_AWREADY;
  wire [2:0]Conn2_AWSIZE;
  wire Conn2_AWVALID;
  wire [11:0]Conn2_BID;
  wire Conn2_BREADY;
  wire [1:0]Conn2_BRESP;
  wire Conn2_BVALID;
  wire [31:0]Conn2_RDATA;
  wire [11:0]Conn2_RID;
  wire Conn2_RLAST;
  wire Conn2_RREADY;
  wire [1:0]Conn2_RRESP;
  wire Conn2_RVALID;
  wire [31:0]Conn2_WDATA;
  wire Conn2_WLAST;
  wire Conn2_WREADY;
  wire [3:0]Conn2_WSTRB;
  wire Conn2_WVALID;
  wire aux_reset_in_1;
  wire clk_in1_1;
  wire ext_reset_in_1;
  wire [31:0]picorv32_0_mem_axi_ARADDR;
  wire [2:0]picorv32_0_mem_axi_ARPROT;
  wire picorv32_0_mem_axi_ARREADY;
  wire picorv32_0_mem_axi_ARVALID;
  wire [31:0]picorv32_0_mem_axi_AWADDR;
  wire [2:0]picorv32_0_mem_axi_AWPROT;
  wire picorv32_0_mem_axi_AWREADY;
  wire picorv32_0_mem_axi_AWVALID;
  wire picorv32_0_mem_axi_BREADY;
  wire picorv32_0_mem_axi_BVALID;
  wire [31:0]picorv32_0_mem_axi_RDATA;
  wire picorv32_0_mem_axi_RREADY;
  wire picorv32_0_mem_axi_RVALID;
  wire [31:0]picorv32_0_mem_axi_WDATA;
  wire picorv32_0_mem_axi_WREADY;
  wire [3:0]picorv32_0_mem_axi_WSTRB;
  wire picorv32_0_mem_axi_WVALID;
  wire picorv32_0_trap;
  wire [16:0]psBramController_BRAM_PORTA_ADDR;
  wire psBramController_BRAM_PORTA_CLK;
  wire [31:0]psBramController_BRAM_PORTA_DIN;
  wire [31:0]psBramController_BRAM_PORTA_DOUT;
  wire psBramController_BRAM_PORTA_EN;
  wire psBramController_BRAM_PORTA_RST;
  wire [3:0]psBramController_BRAM_PORTA_WE;
  wire [16:0]psDramInterface_M00_AXI_ARADDR;
  wire [2:0]psDramInterface_M00_AXI_ARPROT;
  wire psDramInterface_M00_AXI_ARREADY;
  wire psDramInterface_M00_AXI_ARVALID;
  wire [16:0]psDramInterface_M00_AXI_AWADDR;
  wire [2:0]psDramInterface_M00_AXI_AWPROT;
  wire psDramInterface_M00_AXI_AWREADY;
  wire psDramInterface_M00_AXI_AWVALID;
  wire psDramInterface_M00_AXI_BREADY;
  wire [1:0]psDramInterface_M00_AXI_BRESP;
  wire psDramInterface_M00_AXI_BVALID;
  wire [31:0]psDramInterface_M00_AXI_RDATA;
  wire psDramInterface_M00_AXI_RREADY;
  wire [1:0]psDramInterface_M00_AXI_RRESP;
  wire psDramInterface_M00_AXI_RVALID;
  wire [31:0]psDramInterface_M00_AXI_WDATA;
  wire psDramInterface_M00_AXI_WREADY;
  wire [3:0]psDramInterface_M00_AXI_WSTRB;
  wire psDramInterface_M00_AXI_WVALID;
  wire [16:0]riscvBramController_BRAM_PORTA_ADDR;
  wire riscvBramController_BRAM_PORTA_CLK;
  wire [31:0]riscvBramController_BRAM_PORTA_DIN;
  wire [31:0]riscvBramController_BRAM_PORTA_DOUT;
  wire riscvBramController_BRAM_PORTA_EN;
  wire riscvBramController_BRAM_PORTA_RST;
  wire [3:0]riscvBramController_BRAM_PORTA_WE;
  wire [0:0]riscvReset_peripheral_aresetn;
  wire s_axi_aresetn_1;

  assign Conn1_ARREADY = M01_AXI_arready[0];
  assign Conn1_AWREADY = M01_AXI_awready[0];
  assign Conn1_BRESP = M01_AXI_bresp[1:0];
  assign Conn1_BVALID = M01_AXI_bvalid[0];
  assign Conn1_RDATA = M01_AXI_rdata[63:0];
  assign Conn1_RRESP = M01_AXI_rresp[1:0];
  assign Conn1_RVALID = M01_AXI_rvalid[0];
  assign Conn1_WREADY = M01_AXI_wready[0];
  assign Conn2_ARADDR = S_AXI_MEM_araddr[31:0];
  assign Conn2_ARBURST = S_AXI_MEM_arburst[1:0];
  assign Conn2_ARCACHE = S_AXI_MEM_arcache[3:0];
  assign Conn2_ARID = S_AXI_MEM_arid[11:0];
  assign Conn2_ARLEN = S_AXI_MEM_arlen[7:0];
  assign Conn2_ARLOCK = S_AXI_MEM_arlock;
  assign Conn2_ARPROT = S_AXI_MEM_arprot[2:0];
  assign Conn2_ARSIZE = S_AXI_MEM_arsize[2:0];
  assign Conn2_ARVALID = S_AXI_MEM_arvalid;
  assign Conn2_AWADDR = S_AXI_MEM_awaddr[31:0];
  assign Conn2_AWBURST = S_AXI_MEM_awburst[1:0];
  assign Conn2_AWCACHE = S_AXI_MEM_awcache[3:0];
  assign Conn2_AWID = S_AXI_MEM_awid[11:0];
  assign Conn2_AWLEN = S_AXI_MEM_awlen[7:0];
  assign Conn2_AWLOCK = S_AXI_MEM_awlock;
  assign Conn2_AWPROT = S_AXI_MEM_awprot[2:0];
  assign Conn2_AWSIZE = S_AXI_MEM_awsize[2:0];
  assign Conn2_AWVALID = S_AXI_MEM_awvalid;
  assign Conn2_BREADY = S_AXI_MEM_bready;
  assign Conn2_RREADY = S_AXI_MEM_rready;
  assign Conn2_WDATA = S_AXI_MEM_wdata[31:0];
  assign Conn2_WLAST = S_AXI_MEM_wlast;
  assign Conn2_WSTRB = S_AXI_MEM_wstrb[3:0];
  assign Conn2_WVALID = S_AXI_MEM_wvalid;
  assign M01_AXI_araddr[31:0] = Conn1_ARADDR;
  assign M01_AXI_arprot[2:0] = Conn1_ARPROT;
  assign M01_AXI_arvalid[0] = Conn1_ARVALID;
  assign M01_AXI_awaddr[31:0] = Conn1_AWADDR;
  assign M01_AXI_awprot[2:0] = Conn1_AWPROT;
  assign M01_AXI_awvalid[0] = Conn1_AWVALID;
  assign M01_AXI_bready[0] = Conn1_BREADY;
  assign M01_AXI_rready[0] = Conn1_RREADY;
  assign M01_AXI_wdata[63:0] = Conn1_WDATA;
  assign M01_AXI_wstrb[7:0] = Conn1_WSTRB;
  assign M01_AXI_wvalid[0] = Conn1_WVALID;
  assign S_AXI_MEM_arready = Conn2_ARREADY;
  assign S_AXI_MEM_awready = Conn2_AWREADY;
  assign S_AXI_MEM_bid[11:0] = Conn2_BID;
  assign S_AXI_MEM_bresp[1:0] = Conn2_BRESP;
  assign S_AXI_MEM_bvalid = Conn2_BVALID;
  assign S_AXI_MEM_rdata[31:0] = Conn2_RDATA;
  assign S_AXI_MEM_rid[11:0] = Conn2_RID;
  assign S_AXI_MEM_rlast = Conn2_RLAST;
  assign S_AXI_MEM_rresp[1:0] = Conn2_RRESP;
  assign S_AXI_MEM_rvalid = Conn2_RVALID;
  assign S_AXI_MEM_wready = Conn2_WREADY;
  assign aux_reset_in_1 = riscv_resetn;
  assign clk_in1_1 = s_axi_aclk;
  assign ext_reset_in_1 = por_resetn;
  assign irq = picorv32_0_trap;
  assign s_axi_aresetn_1 = s_axi_aresetn;
  cerberus_picorv32_0_16 picorv32_0
       (.clk(clk_in1_1),
        .irq({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .mem_axi_araddr(picorv32_0_mem_axi_ARADDR),
        .mem_axi_arprot(picorv32_0_mem_axi_ARPROT),
        .mem_axi_arready(picorv32_0_mem_axi_ARREADY),
        .mem_axi_arvalid(picorv32_0_mem_axi_ARVALID),
        .mem_axi_awaddr(picorv32_0_mem_axi_AWADDR),
        .mem_axi_awprot(picorv32_0_mem_axi_AWPROT),
        .mem_axi_awready(picorv32_0_mem_axi_AWREADY),
        .mem_axi_awvalid(picorv32_0_mem_axi_AWVALID),
        .mem_axi_bready(picorv32_0_mem_axi_BREADY),
        .mem_axi_bvalid(picorv32_0_mem_axi_BVALID),
        .mem_axi_rdata(picorv32_0_mem_axi_RDATA),
        .mem_axi_rready(picorv32_0_mem_axi_RREADY),
        .mem_axi_rvalid(picorv32_0_mem_axi_RVALID),
        .mem_axi_wdata(picorv32_0_mem_axi_WDATA),
        .mem_axi_wready(picorv32_0_mem_axi_WREADY),
        .mem_axi_wstrb(picorv32_0_mem_axi_WSTRB),
        .mem_axi_wvalid(picorv32_0_mem_axi_WVALID),
        .pcpi_rd({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pcpi_ready(1'b0),
        .pcpi_wait(1'b0),
        .pcpi_wr(1'b0),
        .resetn(riscvReset_peripheral_aresetn),
        .trap(picorv32_0_trap));
  (* BMM_INFO_ADDRESS_SPACE = "byte  0x40060000 32 > cerberus cerberusProcessor3/riscvBram" *) 
  (* KEEP_HIERARCHY = "yes" *) 
  cerberus_psBramController_16 psBramController
       (.bram_addr_a(psBramController_BRAM_PORTA_ADDR),
        .bram_clk_a(psBramController_BRAM_PORTA_CLK),
        .bram_en_a(psBramController_BRAM_PORTA_EN),
        .bram_rddata_a(psBramController_BRAM_PORTA_DOUT),
        .bram_rst_a(psBramController_BRAM_PORTA_RST),
        .bram_we_a(psBramController_BRAM_PORTA_WE),
        .bram_wrdata_a(psBramController_BRAM_PORTA_DIN),
        .s_axi_aclk(clk_in1_1),
        .s_axi_araddr(Conn2_ARADDR[16:0]),
        .s_axi_arburst(Conn2_ARBURST),
        .s_axi_arcache(Conn2_ARCACHE),
        .s_axi_aresetn(s_axi_aresetn_1),
        .s_axi_arid(Conn2_ARID),
        .s_axi_arlen(Conn2_ARLEN),
        .s_axi_arlock(Conn2_ARLOCK),
        .s_axi_arprot(Conn2_ARPROT),
        .s_axi_arready(Conn2_ARREADY),
        .s_axi_arsize(Conn2_ARSIZE),
        .s_axi_arvalid(Conn2_ARVALID),
        .s_axi_awaddr(Conn2_AWADDR[16:0]),
        .s_axi_awburst(Conn2_AWBURST),
        .s_axi_awcache(Conn2_AWCACHE),
        .s_axi_awid(Conn2_AWID),
        .s_axi_awlen(Conn2_AWLEN),
        .s_axi_awlock(Conn2_AWLOCK),
        .s_axi_awprot(Conn2_AWPROT),
        .s_axi_awready(Conn2_AWREADY),
        .s_axi_awsize(Conn2_AWSIZE),
        .s_axi_awvalid(Conn2_AWVALID),
        .s_axi_bid(Conn2_BID),
        .s_axi_bready(Conn2_BREADY),
        .s_axi_bresp(Conn2_BRESP),
        .s_axi_bvalid(Conn2_BVALID),
        .s_axi_rdata(Conn2_RDATA),
        .s_axi_rid(Conn2_RID),
        .s_axi_rlast(Conn2_RLAST),
        .s_axi_rready(Conn2_RREADY),
        .s_axi_rresp(Conn2_RRESP),
        .s_axi_rvalid(Conn2_RVALID),
        .s_axi_wdata(Conn2_WDATA),
        .s_axi_wlast(Conn2_WLAST),
        .s_axi_wready(Conn2_WREADY),
        .s_axi_wstrb(Conn2_WSTRB),
        .s_axi_wvalid(Conn2_WVALID));
  cerberus_psDramInterface_1 psDramInterface
       (.ACLK(clk_in1_1),
        .ARESETN(s_axi_aresetn_1),
        .M00_ACLK(clk_in1_1),
        .M00_ARESETN(s_axi_aresetn_1),
        .M00_AXI_araddr(psDramInterface_M00_AXI_ARADDR),
        .M00_AXI_arprot(psDramInterface_M00_AXI_ARPROT),
        .M00_AXI_arready(psDramInterface_M00_AXI_ARREADY),
        .M00_AXI_arvalid(psDramInterface_M00_AXI_ARVALID),
        .M00_AXI_awaddr(psDramInterface_M00_AXI_AWADDR),
        .M00_AXI_awprot(psDramInterface_M00_AXI_AWPROT),
        .M00_AXI_awready(psDramInterface_M00_AXI_AWREADY),
        .M00_AXI_awvalid(psDramInterface_M00_AXI_AWVALID),
        .M00_AXI_bready(psDramInterface_M00_AXI_BREADY),
        .M00_AXI_bresp(psDramInterface_M00_AXI_BRESP),
        .M00_AXI_bvalid(psDramInterface_M00_AXI_BVALID),
        .M00_AXI_rdata(psDramInterface_M00_AXI_RDATA),
        .M00_AXI_rready(psDramInterface_M00_AXI_RREADY),
        .M00_AXI_rresp(psDramInterface_M00_AXI_RRESP),
        .M00_AXI_rvalid(psDramInterface_M00_AXI_RVALID),
        .M00_AXI_wdata(psDramInterface_M00_AXI_WDATA),
        .M00_AXI_wready(psDramInterface_M00_AXI_WREADY),
        .M00_AXI_wstrb(psDramInterface_M00_AXI_WSTRB),
        .M00_AXI_wvalid(psDramInterface_M00_AXI_WVALID),
        .M01_ACLK(clk_in1_1),
        .M01_ARESETN(s_axi_aresetn_1),
        .M01_AXI_araddr(Conn1_ARADDR),
        .M01_AXI_arprot(Conn1_ARPROT),
        .M01_AXI_arready(Conn1_ARREADY),
        .M01_AXI_arvalid(Conn1_ARVALID),
        .M01_AXI_awaddr(Conn1_AWADDR),
        .M01_AXI_awprot(Conn1_AWPROT),
        .M01_AXI_awready(Conn1_AWREADY),
        .M01_AXI_awvalid(Conn1_AWVALID),
        .M01_AXI_bready(Conn1_BREADY),
        .M01_AXI_bresp(Conn1_BRESP),
        .M01_AXI_bvalid(Conn1_BVALID),
        .M01_AXI_rdata(Conn1_RDATA),
        .M01_AXI_rready(Conn1_RREADY),
        .M01_AXI_rresp(Conn1_RRESP),
        .M01_AXI_rvalid(Conn1_RVALID),
        .M01_AXI_wdata(Conn1_WDATA),
        .M01_AXI_wready(Conn1_WREADY),
        .M01_AXI_wstrb(Conn1_WSTRB),
        .M01_AXI_wvalid(Conn1_WVALID),
        .S00_ACLK(clk_in1_1),
        .S00_ARESETN(s_axi_aresetn_1),
        .S00_AXI_araddr(picorv32_0_mem_axi_ARADDR),
        .S00_AXI_arprot(picorv32_0_mem_axi_ARPROT),
        .S00_AXI_arready(picorv32_0_mem_axi_ARREADY),
        .S00_AXI_arvalid(picorv32_0_mem_axi_ARVALID),
        .S00_AXI_awaddr(picorv32_0_mem_axi_AWADDR),
        .S00_AXI_awprot(picorv32_0_mem_axi_AWPROT),
        .S00_AXI_awready(picorv32_0_mem_axi_AWREADY),
        .S00_AXI_awvalid(picorv32_0_mem_axi_AWVALID),
        .S00_AXI_bready(picorv32_0_mem_axi_BREADY),
        .S00_AXI_bvalid(picorv32_0_mem_axi_BVALID),
        .S00_AXI_rdata(picorv32_0_mem_axi_RDATA),
        .S00_AXI_rready(picorv32_0_mem_axi_RREADY),
        .S00_AXI_rvalid(picorv32_0_mem_axi_RVALID),
        .S00_AXI_wdata(picorv32_0_mem_axi_WDATA),
        .S00_AXI_wready(picorv32_0_mem_axi_WREADY),
        .S00_AXI_wstrb(picorv32_0_mem_axi_WSTRB),
        .S00_AXI_wvalid(picorv32_0_mem_axi_WVALID));
  cerberus_riscvBram_16 riscvBram
       (.addra({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,riscvBramController_BRAM_PORTA_ADDR}),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,psBramController_BRAM_PORTA_ADDR}),
        .clka(riscvBramController_BRAM_PORTA_CLK),
        .clkb(psBramController_BRAM_PORTA_CLK),
        .dina(riscvBramController_BRAM_PORTA_DIN),
        .dinb(psBramController_BRAM_PORTA_DIN),
        .douta(riscvBramController_BRAM_PORTA_DOUT),
        .doutb(psBramController_BRAM_PORTA_DOUT),
        .ena(riscvBramController_BRAM_PORTA_EN),
        .enb(psBramController_BRAM_PORTA_EN),
        .rsta(riscvBramController_BRAM_PORTA_RST),
        .rstb(psBramController_BRAM_PORTA_RST),
        .wea(riscvBramController_BRAM_PORTA_WE),
        .web(psBramController_BRAM_PORTA_WE));
  cerberus_riscvBramController_16 riscvBramController
       (.bram_addr_a(riscvBramController_BRAM_PORTA_ADDR),
        .bram_clk_a(riscvBramController_BRAM_PORTA_CLK),
        .bram_en_a(riscvBramController_BRAM_PORTA_EN),
        .bram_rddata_a(riscvBramController_BRAM_PORTA_DOUT),
        .bram_rst_a(riscvBramController_BRAM_PORTA_RST),
        .bram_we_a(riscvBramController_BRAM_PORTA_WE),
        .bram_wrdata_a(riscvBramController_BRAM_PORTA_DIN),
        .s_axi_aclk(clk_in1_1),
        .s_axi_araddr(psDramInterface_M00_AXI_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_1),
        .s_axi_arprot(psDramInterface_M00_AXI_ARPROT),
        .s_axi_arready(psDramInterface_M00_AXI_ARREADY),
        .s_axi_arvalid(psDramInterface_M00_AXI_ARVALID),
        .s_axi_awaddr(psDramInterface_M00_AXI_AWADDR),
        .s_axi_awprot(psDramInterface_M00_AXI_AWPROT),
        .s_axi_awready(psDramInterface_M00_AXI_AWREADY),
        .s_axi_awvalid(psDramInterface_M00_AXI_AWVALID),
        .s_axi_bready(psDramInterface_M00_AXI_BREADY),
        .s_axi_bresp(psDramInterface_M00_AXI_BRESP),
        .s_axi_bvalid(psDramInterface_M00_AXI_BVALID),
        .s_axi_rdata(psDramInterface_M00_AXI_RDATA),
        .s_axi_rready(psDramInterface_M00_AXI_RREADY),
        .s_axi_rresp(psDramInterface_M00_AXI_RRESP),
        .s_axi_rvalid(psDramInterface_M00_AXI_RVALID),
        .s_axi_wdata(psDramInterface_M00_AXI_WDATA),
        .s_axi_wready(psDramInterface_M00_AXI_WREADY),
        .s_axi_wstrb(psDramInterface_M00_AXI_WSTRB),
        .s_axi_wvalid(psDramInterface_M00_AXI_WVALID));
  cerberus_riscvReset_16 riscvReset
       (.aux_reset_in(aux_reset_in_1),
        .dcm_locked(1'b1),
        .ext_reset_in(ext_reset_in_1),
        .mb_debug_sys_rst(1'b0),
        .peripheral_aresetn(riscvReset_peripheral_aresetn),
        .slowest_sync_clk(clk_in1_1));
endmodule

module cerberusProcessor_imp_S4RG0Y
   (M01_AXI_araddr,
    M01_AXI_arprot,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awprot,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wstrb,
    M01_AXI_wvalid,
    S_AXI_MEM_araddr,
    S_AXI_MEM_arburst,
    S_AXI_MEM_arcache,
    S_AXI_MEM_arid,
    S_AXI_MEM_arlen,
    S_AXI_MEM_arlock,
    S_AXI_MEM_arprot,
    S_AXI_MEM_arready,
    S_AXI_MEM_arsize,
    S_AXI_MEM_arvalid,
    S_AXI_MEM_awaddr,
    S_AXI_MEM_awburst,
    S_AXI_MEM_awcache,
    S_AXI_MEM_awid,
    S_AXI_MEM_awlen,
    S_AXI_MEM_awlock,
    S_AXI_MEM_awprot,
    S_AXI_MEM_awready,
    S_AXI_MEM_awsize,
    S_AXI_MEM_awvalid,
    S_AXI_MEM_bid,
    S_AXI_MEM_bready,
    S_AXI_MEM_bresp,
    S_AXI_MEM_bvalid,
    S_AXI_MEM_rdata,
    S_AXI_MEM_rid,
    S_AXI_MEM_rlast,
    S_AXI_MEM_rready,
    S_AXI_MEM_rresp,
    S_AXI_MEM_rvalid,
    S_AXI_MEM_wdata,
    S_AXI_MEM_wlast,
    S_AXI_MEM_wready,
    S_AXI_MEM_wstrb,
    S_AXI_MEM_wvalid,
    irq,
    por_resetn,
    riscv_clk,
    riscv_resetn,
    s_axi_aclk,
    s_axi_aresetn);
  output [31:0]M01_AXI_araddr;
  output [2:0]M01_AXI_arprot;
  input [0:0]M01_AXI_arready;
  output [0:0]M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  output [2:0]M01_AXI_awprot;
  input [0:0]M01_AXI_awready;
  output [0:0]M01_AXI_awvalid;
  output [0:0]M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input [0:0]M01_AXI_bvalid;
  input [63:0]M01_AXI_rdata;
  output [0:0]M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input [0:0]M01_AXI_rvalid;
  output [63:0]M01_AXI_wdata;
  input [0:0]M01_AXI_wready;
  output [7:0]M01_AXI_wstrb;
  output [0:0]M01_AXI_wvalid;
  input [31:0]S_AXI_MEM_araddr;
  input [1:0]S_AXI_MEM_arburst;
  input [3:0]S_AXI_MEM_arcache;
  input [11:0]S_AXI_MEM_arid;
  input [7:0]S_AXI_MEM_arlen;
  input S_AXI_MEM_arlock;
  input [2:0]S_AXI_MEM_arprot;
  output S_AXI_MEM_arready;
  input [2:0]S_AXI_MEM_arsize;
  input S_AXI_MEM_arvalid;
  input [31:0]S_AXI_MEM_awaddr;
  input [1:0]S_AXI_MEM_awburst;
  input [3:0]S_AXI_MEM_awcache;
  input [11:0]S_AXI_MEM_awid;
  input [7:0]S_AXI_MEM_awlen;
  input S_AXI_MEM_awlock;
  input [2:0]S_AXI_MEM_awprot;
  output S_AXI_MEM_awready;
  input [2:0]S_AXI_MEM_awsize;
  input S_AXI_MEM_awvalid;
  output [11:0]S_AXI_MEM_bid;
  input S_AXI_MEM_bready;
  output [1:0]S_AXI_MEM_bresp;
  output S_AXI_MEM_bvalid;
  output [31:0]S_AXI_MEM_rdata;
  output [11:0]S_AXI_MEM_rid;
  output S_AXI_MEM_rlast;
  input S_AXI_MEM_rready;
  output [1:0]S_AXI_MEM_rresp;
  output S_AXI_MEM_rvalid;
  input [31:0]S_AXI_MEM_wdata;
  input S_AXI_MEM_wlast;
  output S_AXI_MEM_wready;
  input [3:0]S_AXI_MEM_wstrb;
  input S_AXI_MEM_wvalid;
  output irq;
  input por_resetn;
  input riscv_clk;
  input riscv_resetn;
  input s_axi_aclk;
  input s_axi_aresetn;

  wire [31:0]Conn1_ARADDR;
  wire [2:0]Conn1_ARPROT;
  wire [0:0]Conn1_ARREADY;
  wire [0:0]Conn1_ARVALID;
  wire [31:0]Conn1_AWADDR;
  wire [2:0]Conn1_AWPROT;
  wire [0:0]Conn1_AWREADY;
  wire [0:0]Conn1_AWVALID;
  wire [0:0]Conn1_BREADY;
  wire [1:0]Conn1_BRESP;
  wire [0:0]Conn1_BVALID;
  wire [63:0]Conn1_RDATA;
  wire [0:0]Conn1_RREADY;
  wire [1:0]Conn1_RRESP;
  wire [0:0]Conn1_RVALID;
  wire [63:0]Conn1_WDATA;
  wire [0:0]Conn1_WREADY;
  wire [7:0]Conn1_WSTRB;
  wire [0:0]Conn1_WVALID;
  wire [31:0]Conn2_ARADDR;
  wire [1:0]Conn2_ARBURST;
  wire [3:0]Conn2_ARCACHE;
  wire [11:0]Conn2_ARID;
  wire [7:0]Conn2_ARLEN;
  wire Conn2_ARLOCK;
  wire [2:0]Conn2_ARPROT;
  wire Conn2_ARREADY;
  wire [2:0]Conn2_ARSIZE;
  wire Conn2_ARVALID;
  wire [31:0]Conn2_AWADDR;
  wire [1:0]Conn2_AWBURST;
  wire [3:0]Conn2_AWCACHE;
  wire [11:0]Conn2_AWID;
  wire [7:0]Conn2_AWLEN;
  wire Conn2_AWLOCK;
  wire [2:0]Conn2_AWPROT;
  wire Conn2_AWREADY;
  wire [2:0]Conn2_AWSIZE;
  wire Conn2_AWVALID;
  wire [11:0]Conn2_BID;
  wire Conn2_BREADY;
  wire [1:0]Conn2_BRESP;
  wire Conn2_BVALID;
  wire [31:0]Conn2_RDATA;
  wire [11:0]Conn2_RID;
  wire Conn2_RLAST;
  wire Conn2_RREADY;
  wire [1:0]Conn2_RRESP;
  wire Conn2_RVALID;
  wire [31:0]Conn2_WDATA;
  wire Conn2_WLAST;
  wire Conn2_WREADY;
  wire [3:0]Conn2_WSTRB;
  wire Conn2_WVALID;
  wire aux_reset_in_1;
  wire clk_in1_1;
  wire ext_reset_in_1;
  wire [31:0]picorv32_0_mem_axi_ARADDR;
  wire [2:0]picorv32_0_mem_axi_ARPROT;
  wire picorv32_0_mem_axi_ARREADY;
  wire picorv32_0_mem_axi_ARVALID;
  wire [31:0]picorv32_0_mem_axi_AWADDR;
  wire [2:0]picorv32_0_mem_axi_AWPROT;
  wire picorv32_0_mem_axi_AWREADY;
  wire picorv32_0_mem_axi_AWVALID;
  wire picorv32_0_mem_axi_BREADY;
  wire picorv32_0_mem_axi_BVALID;
  wire [31:0]picorv32_0_mem_axi_RDATA;
  wire picorv32_0_mem_axi_RREADY;
  wire picorv32_0_mem_axi_RVALID;
  wire [31:0]picorv32_0_mem_axi_WDATA;
  wire picorv32_0_mem_axi_WREADY;
  wire [3:0]picorv32_0_mem_axi_WSTRB;
  wire picorv32_0_mem_axi_WVALID;
  wire picorv32_0_trap;
  wire [16:0]psBramController_BRAM_PORTA_ADDR;
  wire psBramController_BRAM_PORTA_CLK;
  wire [31:0]psBramController_BRAM_PORTA_DIN;
  wire [31:0]psBramController_BRAM_PORTA_DOUT;
  wire psBramController_BRAM_PORTA_EN;
  wire psBramController_BRAM_PORTA_RST;
  wire [3:0]psBramController_BRAM_PORTA_WE;
  wire [16:0]psDramInterface_M00_AXI_ARADDR;
  wire [2:0]psDramInterface_M00_AXI_ARPROT;
  wire psDramInterface_M00_AXI_ARREADY;
  wire psDramInterface_M00_AXI_ARVALID;
  wire [16:0]psDramInterface_M00_AXI_AWADDR;
  wire [2:0]psDramInterface_M00_AXI_AWPROT;
  wire psDramInterface_M00_AXI_AWREADY;
  wire psDramInterface_M00_AXI_AWVALID;
  wire psDramInterface_M00_AXI_BREADY;
  wire [1:0]psDramInterface_M00_AXI_BRESP;
  wire psDramInterface_M00_AXI_BVALID;
  wire [31:0]psDramInterface_M00_AXI_RDATA;
  wire psDramInterface_M00_AXI_RREADY;
  wire [1:0]psDramInterface_M00_AXI_RRESP;
  wire psDramInterface_M00_AXI_RVALID;
  wire [31:0]psDramInterface_M00_AXI_WDATA;
  wire psDramInterface_M00_AXI_WREADY;
  wire [3:0]psDramInterface_M00_AXI_WSTRB;
  wire psDramInterface_M00_AXI_WVALID;
  wire [16:0]riscvBramController_BRAM_PORTA_ADDR;
  wire riscvBramController_BRAM_PORTA_CLK;
  wire [31:0]riscvBramController_BRAM_PORTA_DIN;
  wire [31:0]riscvBramController_BRAM_PORTA_DOUT;
  wire riscvBramController_BRAM_PORTA_EN;
  wire riscvBramController_BRAM_PORTA_RST;
  wire [3:0]riscvBramController_BRAM_PORTA_WE;
  wire [0:0]riscvReset_peripheral_aresetn;
  wire s_axi_aresetn_1;

  assign Conn1_ARREADY = M01_AXI_arready[0];
  assign Conn1_AWREADY = M01_AXI_awready[0];
  assign Conn1_BRESP = M01_AXI_bresp[1:0];
  assign Conn1_BVALID = M01_AXI_bvalid[0];
  assign Conn1_RDATA = M01_AXI_rdata[63:0];
  assign Conn1_RRESP = M01_AXI_rresp[1:0];
  assign Conn1_RVALID = M01_AXI_rvalid[0];
  assign Conn1_WREADY = M01_AXI_wready[0];
  assign Conn2_ARADDR = S_AXI_MEM_araddr[31:0];
  assign Conn2_ARBURST = S_AXI_MEM_arburst[1:0];
  assign Conn2_ARCACHE = S_AXI_MEM_arcache[3:0];
  assign Conn2_ARID = S_AXI_MEM_arid[11:0];
  assign Conn2_ARLEN = S_AXI_MEM_arlen[7:0];
  assign Conn2_ARLOCK = S_AXI_MEM_arlock;
  assign Conn2_ARPROT = S_AXI_MEM_arprot[2:0];
  assign Conn2_ARSIZE = S_AXI_MEM_arsize[2:0];
  assign Conn2_ARVALID = S_AXI_MEM_arvalid;
  assign Conn2_AWADDR = S_AXI_MEM_awaddr[31:0];
  assign Conn2_AWBURST = S_AXI_MEM_awburst[1:0];
  assign Conn2_AWCACHE = S_AXI_MEM_awcache[3:0];
  assign Conn2_AWID = S_AXI_MEM_awid[11:0];
  assign Conn2_AWLEN = S_AXI_MEM_awlen[7:0];
  assign Conn2_AWLOCK = S_AXI_MEM_awlock;
  assign Conn2_AWPROT = S_AXI_MEM_awprot[2:0];
  assign Conn2_AWSIZE = S_AXI_MEM_awsize[2:0];
  assign Conn2_AWVALID = S_AXI_MEM_awvalid;
  assign Conn2_BREADY = S_AXI_MEM_bready;
  assign Conn2_RREADY = S_AXI_MEM_rready;
  assign Conn2_WDATA = S_AXI_MEM_wdata[31:0];
  assign Conn2_WLAST = S_AXI_MEM_wlast;
  assign Conn2_WSTRB = S_AXI_MEM_wstrb[3:0];
  assign Conn2_WVALID = S_AXI_MEM_wvalid;
  assign M01_AXI_araddr[31:0] = Conn1_ARADDR;
  assign M01_AXI_arprot[2:0] = Conn1_ARPROT;
  assign M01_AXI_arvalid[0] = Conn1_ARVALID;
  assign M01_AXI_awaddr[31:0] = Conn1_AWADDR;
  assign M01_AXI_awprot[2:0] = Conn1_AWPROT;
  assign M01_AXI_awvalid[0] = Conn1_AWVALID;
  assign M01_AXI_bready[0] = Conn1_BREADY;
  assign M01_AXI_rready[0] = Conn1_RREADY;
  assign M01_AXI_wdata[63:0] = Conn1_WDATA;
  assign M01_AXI_wstrb[7:0] = Conn1_WSTRB;
  assign M01_AXI_wvalid[0] = Conn1_WVALID;
  assign S_AXI_MEM_arready = Conn2_ARREADY;
  assign S_AXI_MEM_awready = Conn2_AWREADY;
  assign S_AXI_MEM_bid[11:0] = Conn2_BID;
  assign S_AXI_MEM_bresp[1:0] = Conn2_BRESP;
  assign S_AXI_MEM_bvalid = Conn2_BVALID;
  assign S_AXI_MEM_rdata[31:0] = Conn2_RDATA;
  assign S_AXI_MEM_rid[11:0] = Conn2_RID;
  assign S_AXI_MEM_rlast = Conn2_RLAST;
  assign S_AXI_MEM_rresp[1:0] = Conn2_RRESP;
  assign S_AXI_MEM_rvalid = Conn2_RVALID;
  assign S_AXI_MEM_wready = Conn2_WREADY;
  assign aux_reset_in_1 = riscv_resetn;
  assign clk_in1_1 = s_axi_aclk;
  assign ext_reset_in_1 = por_resetn;
  assign irq = picorv32_0_trap;
  assign s_axi_aresetn_1 = s_axi_aresetn;
  cerberus_picorv32_0_17 picorv32_0
       (.clk(clk_in1_1),
        .irq({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .mem_axi_araddr(picorv32_0_mem_axi_ARADDR),
        .mem_axi_arprot(picorv32_0_mem_axi_ARPROT),
        .mem_axi_arready(picorv32_0_mem_axi_ARREADY),
        .mem_axi_arvalid(picorv32_0_mem_axi_ARVALID),
        .mem_axi_awaddr(picorv32_0_mem_axi_AWADDR),
        .mem_axi_awprot(picorv32_0_mem_axi_AWPROT),
        .mem_axi_awready(picorv32_0_mem_axi_AWREADY),
        .mem_axi_awvalid(picorv32_0_mem_axi_AWVALID),
        .mem_axi_bready(picorv32_0_mem_axi_BREADY),
        .mem_axi_bvalid(picorv32_0_mem_axi_BVALID),
        .mem_axi_rdata(picorv32_0_mem_axi_RDATA),
        .mem_axi_rready(picorv32_0_mem_axi_RREADY),
        .mem_axi_rvalid(picorv32_0_mem_axi_RVALID),
        .mem_axi_wdata(picorv32_0_mem_axi_WDATA),
        .mem_axi_wready(picorv32_0_mem_axi_WREADY),
        .mem_axi_wstrb(picorv32_0_mem_axi_WSTRB),
        .mem_axi_wvalid(picorv32_0_mem_axi_WVALID),
        .pcpi_rd({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .pcpi_ready(1'b0),
        .pcpi_wait(1'b0),
        .pcpi_wr(1'b0),
        .resetn(riscvReset_peripheral_aresetn),
        .trap(picorv32_0_trap));
  (* BMM_INFO_ADDRESS_SPACE = "byte  0x40000000 32 > cerberus cerberusProcessor/riscvBram" *) 
  (* KEEP_HIERARCHY = "yes" *) 
  cerberus_psBramController_17 psBramController
       (.bram_addr_a(psBramController_BRAM_PORTA_ADDR),
        .bram_clk_a(psBramController_BRAM_PORTA_CLK),
        .bram_en_a(psBramController_BRAM_PORTA_EN),
        .bram_rddata_a(psBramController_BRAM_PORTA_DOUT),
        .bram_rst_a(psBramController_BRAM_PORTA_RST),
        .bram_we_a(psBramController_BRAM_PORTA_WE),
        .bram_wrdata_a(psBramController_BRAM_PORTA_DIN),
        .s_axi_aclk(clk_in1_1),
        .s_axi_araddr(Conn2_ARADDR[16:0]),
        .s_axi_arburst(Conn2_ARBURST),
        .s_axi_arcache(Conn2_ARCACHE),
        .s_axi_aresetn(s_axi_aresetn_1),
        .s_axi_arid(Conn2_ARID),
        .s_axi_arlen(Conn2_ARLEN),
        .s_axi_arlock(Conn2_ARLOCK),
        .s_axi_arprot(Conn2_ARPROT),
        .s_axi_arready(Conn2_ARREADY),
        .s_axi_arsize(Conn2_ARSIZE),
        .s_axi_arvalid(Conn2_ARVALID),
        .s_axi_awaddr(Conn2_AWADDR[16:0]),
        .s_axi_awburst(Conn2_AWBURST),
        .s_axi_awcache(Conn2_AWCACHE),
        .s_axi_awid(Conn2_AWID),
        .s_axi_awlen(Conn2_AWLEN),
        .s_axi_awlock(Conn2_AWLOCK),
        .s_axi_awprot(Conn2_AWPROT),
        .s_axi_awready(Conn2_AWREADY),
        .s_axi_awsize(Conn2_AWSIZE),
        .s_axi_awvalid(Conn2_AWVALID),
        .s_axi_bid(Conn2_BID),
        .s_axi_bready(Conn2_BREADY),
        .s_axi_bresp(Conn2_BRESP),
        .s_axi_bvalid(Conn2_BVALID),
        .s_axi_rdata(Conn2_RDATA),
        .s_axi_rid(Conn2_RID),
        .s_axi_rlast(Conn2_RLAST),
        .s_axi_rready(Conn2_RREADY),
        .s_axi_rresp(Conn2_RRESP),
        .s_axi_rvalid(Conn2_RVALID),
        .s_axi_wdata(Conn2_WDATA),
        .s_axi_wlast(Conn2_WLAST),
        .s_axi_wready(Conn2_WREADY),
        .s_axi_wstrb(Conn2_WSTRB),
        .s_axi_wvalid(Conn2_WVALID));
  cerberus_psDramInterface_2 psDramInterface
       (.ACLK(clk_in1_1),
        .ARESETN(s_axi_aresetn_1),
        .M00_ACLK(clk_in1_1),
        .M00_ARESETN(s_axi_aresetn_1),
        .M00_AXI_araddr(psDramInterface_M00_AXI_ARADDR),
        .M00_AXI_arprot(psDramInterface_M00_AXI_ARPROT),
        .M00_AXI_arready(psDramInterface_M00_AXI_ARREADY),
        .M00_AXI_arvalid(psDramInterface_M00_AXI_ARVALID),
        .M00_AXI_awaddr(psDramInterface_M00_AXI_AWADDR),
        .M00_AXI_awprot(psDramInterface_M00_AXI_AWPROT),
        .M00_AXI_awready(psDramInterface_M00_AXI_AWREADY),
        .M00_AXI_awvalid(psDramInterface_M00_AXI_AWVALID),
        .M00_AXI_bready(psDramInterface_M00_AXI_BREADY),
        .M00_AXI_bresp(psDramInterface_M00_AXI_BRESP),
        .M00_AXI_bvalid(psDramInterface_M00_AXI_BVALID),
        .M00_AXI_rdata(psDramInterface_M00_AXI_RDATA),
        .M00_AXI_rready(psDramInterface_M00_AXI_RREADY),
        .M00_AXI_rresp(psDramInterface_M00_AXI_RRESP),
        .M00_AXI_rvalid(psDramInterface_M00_AXI_RVALID),
        .M00_AXI_wdata(psDramInterface_M00_AXI_WDATA),
        .M00_AXI_wready(psDramInterface_M00_AXI_WREADY),
        .M00_AXI_wstrb(psDramInterface_M00_AXI_WSTRB),
        .M00_AXI_wvalid(psDramInterface_M00_AXI_WVALID),
        .M01_ACLK(clk_in1_1),
        .M01_ARESETN(s_axi_aresetn_1),
        .M01_AXI_araddr(Conn1_ARADDR),
        .M01_AXI_arprot(Conn1_ARPROT),
        .M01_AXI_arready(Conn1_ARREADY),
        .M01_AXI_arvalid(Conn1_ARVALID),
        .M01_AXI_awaddr(Conn1_AWADDR),
        .M01_AXI_awprot(Conn1_AWPROT),
        .M01_AXI_awready(Conn1_AWREADY),
        .M01_AXI_awvalid(Conn1_AWVALID),
        .M01_AXI_bready(Conn1_BREADY),
        .M01_AXI_bresp(Conn1_BRESP),
        .M01_AXI_bvalid(Conn1_BVALID),
        .M01_AXI_rdata(Conn1_RDATA),
        .M01_AXI_rready(Conn1_RREADY),
        .M01_AXI_rresp(Conn1_RRESP),
        .M01_AXI_rvalid(Conn1_RVALID),
        .M01_AXI_wdata(Conn1_WDATA),
        .M01_AXI_wready(Conn1_WREADY),
        .M01_AXI_wstrb(Conn1_WSTRB),
        .M01_AXI_wvalid(Conn1_WVALID),
        .S00_ACLK(clk_in1_1),
        .S00_ARESETN(s_axi_aresetn_1),
        .S00_AXI_araddr(picorv32_0_mem_axi_ARADDR),
        .S00_AXI_arprot(picorv32_0_mem_axi_ARPROT),
        .S00_AXI_arready(picorv32_0_mem_axi_ARREADY),
        .S00_AXI_arvalid(picorv32_0_mem_axi_ARVALID),
        .S00_AXI_awaddr(picorv32_0_mem_axi_AWADDR),
        .S00_AXI_awprot(picorv32_0_mem_axi_AWPROT),
        .S00_AXI_awready(picorv32_0_mem_axi_AWREADY),
        .S00_AXI_awvalid(picorv32_0_mem_axi_AWVALID),
        .S00_AXI_bready(picorv32_0_mem_axi_BREADY),
        .S00_AXI_bvalid(picorv32_0_mem_axi_BVALID),
        .S00_AXI_rdata(picorv32_0_mem_axi_RDATA),
        .S00_AXI_rready(picorv32_0_mem_axi_RREADY),
        .S00_AXI_rvalid(picorv32_0_mem_axi_RVALID),
        .S00_AXI_wdata(picorv32_0_mem_axi_WDATA),
        .S00_AXI_wready(picorv32_0_mem_axi_WREADY),
        .S00_AXI_wstrb(picorv32_0_mem_axi_WSTRB),
        .S00_AXI_wvalid(picorv32_0_mem_axi_WVALID));
  cerberus_riscvBram_17 riscvBram
       (.addra({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,riscvBramController_BRAM_PORTA_ADDR}),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,psBramController_BRAM_PORTA_ADDR}),
        .clka(riscvBramController_BRAM_PORTA_CLK),
        .clkb(psBramController_BRAM_PORTA_CLK),
        .dina(riscvBramController_BRAM_PORTA_DIN),
        .dinb(psBramController_BRAM_PORTA_DIN),
        .douta(riscvBramController_BRAM_PORTA_DOUT),
        .doutb(psBramController_BRAM_PORTA_DOUT),
        .ena(riscvBramController_BRAM_PORTA_EN),
        .enb(psBramController_BRAM_PORTA_EN),
        .rsta(riscvBramController_BRAM_PORTA_RST),
        .rstb(psBramController_BRAM_PORTA_RST),
        .wea(riscvBramController_BRAM_PORTA_WE),
        .web(psBramController_BRAM_PORTA_WE));
  cerberus_riscvBramController_17 riscvBramController
       (.bram_addr_a(riscvBramController_BRAM_PORTA_ADDR),
        .bram_clk_a(riscvBramController_BRAM_PORTA_CLK),
        .bram_en_a(riscvBramController_BRAM_PORTA_EN),
        .bram_rddata_a(riscvBramController_BRAM_PORTA_DOUT),
        .bram_rst_a(riscvBramController_BRAM_PORTA_RST),
        .bram_we_a(riscvBramController_BRAM_PORTA_WE),
        .bram_wrdata_a(riscvBramController_BRAM_PORTA_DIN),
        .s_axi_aclk(clk_in1_1),
        .s_axi_araddr(psDramInterface_M00_AXI_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_1),
        .s_axi_arprot(psDramInterface_M00_AXI_ARPROT),
        .s_axi_arready(psDramInterface_M00_AXI_ARREADY),
        .s_axi_arvalid(psDramInterface_M00_AXI_ARVALID),
        .s_axi_awaddr(psDramInterface_M00_AXI_AWADDR),
        .s_axi_awprot(psDramInterface_M00_AXI_AWPROT),
        .s_axi_awready(psDramInterface_M00_AXI_AWREADY),
        .s_axi_awvalid(psDramInterface_M00_AXI_AWVALID),
        .s_axi_bready(psDramInterface_M00_AXI_BREADY),
        .s_axi_bresp(psDramInterface_M00_AXI_BRESP),
        .s_axi_bvalid(psDramInterface_M00_AXI_BVALID),
        .s_axi_rdata(psDramInterface_M00_AXI_RDATA),
        .s_axi_rready(psDramInterface_M00_AXI_RREADY),
        .s_axi_rresp(psDramInterface_M00_AXI_RRESP),
        .s_axi_rvalid(psDramInterface_M00_AXI_RVALID),
        .s_axi_wdata(psDramInterface_M00_AXI_WDATA),
        .s_axi_wready(psDramInterface_M00_AXI_WREADY),
        .s_axi_wstrb(psDramInterface_M00_AXI_WSTRB),
        .s_axi_wvalid(psDramInterface_M00_AXI_WVALID));
  cerberus_riscvReset_17 riscvReset
       (.aux_reset_in(aux_reset_in_1),
        .dcm_locked(1'b1),
        .ext_reset_in(ext_reset_in_1),
        .mb_debug_sys_rst(1'b0),
        .peripheral_aresetn(riscvReset_peripheral_aresetn),
        .slowest_sync_clk(clk_in1_1));
endmodule

module cerberus_psAxiInterconnect_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arready,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awready,
    M00_AXI_awvalid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    M01_ACLK,
    M01_ARESETN,
    M01_AXI_araddr,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wstrb,
    M01_AXI_wvalid,
    M02_ACLK,
    M02_ARESETN,
    M02_AXI_araddr,
    M02_AXI_arburst,
    M02_AXI_arcache,
    M02_AXI_arid,
    M02_AXI_arlen,
    M02_AXI_arlock,
    M02_AXI_arprot,
    M02_AXI_arready,
    M02_AXI_arsize,
    M02_AXI_arvalid,
    M02_AXI_awaddr,
    M02_AXI_awburst,
    M02_AXI_awcache,
    M02_AXI_awid,
    M02_AXI_awlen,
    M02_AXI_awlock,
    M02_AXI_awprot,
    M02_AXI_awready,
    M02_AXI_awsize,
    M02_AXI_awvalid,
    M02_AXI_bid,
    M02_AXI_bready,
    M02_AXI_bresp,
    M02_AXI_bvalid,
    M02_AXI_rdata,
    M02_AXI_rid,
    M02_AXI_rlast,
    M02_AXI_rready,
    M02_AXI_rresp,
    M02_AXI_rvalid,
    M02_AXI_wdata,
    M02_AXI_wlast,
    M02_AXI_wready,
    M02_AXI_wstrb,
    M02_AXI_wvalid,
    M03_ACLK,
    M03_ARESETN,
    M03_AXI_araddr,
    M03_AXI_arburst,
    M03_AXI_arcache,
    M03_AXI_arid,
    M03_AXI_arlen,
    M03_AXI_arlock,
    M03_AXI_arprot,
    M03_AXI_arready,
    M03_AXI_arsize,
    M03_AXI_arvalid,
    M03_AXI_awaddr,
    M03_AXI_awburst,
    M03_AXI_awcache,
    M03_AXI_awid,
    M03_AXI_awlen,
    M03_AXI_awlock,
    M03_AXI_awprot,
    M03_AXI_awready,
    M03_AXI_awsize,
    M03_AXI_awvalid,
    M03_AXI_bid,
    M03_AXI_bready,
    M03_AXI_bresp,
    M03_AXI_bvalid,
    M03_AXI_rdata,
    M03_AXI_rid,
    M03_AXI_rlast,
    M03_AXI_rready,
    M03_AXI_rresp,
    M03_AXI_rvalid,
    M03_AXI_wdata,
    M03_AXI_wlast,
    M03_AXI_wready,
    M03_AXI_wstrb,
    M03_AXI_wvalid,
    M04_ACLK,
    M04_ARESETN,
    M04_AXI_araddr,
    M04_AXI_arburst,
    M04_AXI_arcache,
    M04_AXI_arid,
    M04_AXI_arlen,
    M04_AXI_arlock,
    M04_AXI_arprot,
    M04_AXI_arready,
    M04_AXI_arsize,
    M04_AXI_arvalid,
    M04_AXI_awaddr,
    M04_AXI_awburst,
    M04_AXI_awcache,
    M04_AXI_awid,
    M04_AXI_awlen,
    M04_AXI_awlock,
    M04_AXI_awprot,
    M04_AXI_awready,
    M04_AXI_awsize,
    M04_AXI_awvalid,
    M04_AXI_bid,
    M04_AXI_bready,
    M04_AXI_bresp,
    M04_AXI_bvalid,
    M04_AXI_rdata,
    M04_AXI_rid,
    M04_AXI_rlast,
    M04_AXI_rready,
    M04_AXI_rresp,
    M04_AXI_rvalid,
    M04_AXI_wdata,
    M04_AXI_wlast,
    M04_AXI_wready,
    M04_AXI_wstrb,
    M04_AXI_wvalid,
    M05_ACLK,
    M05_ARESETN,
    M05_AXI_araddr,
    M05_AXI_arburst,
    M05_AXI_arcache,
    M05_AXI_arid,
    M05_AXI_arlen,
    M05_AXI_arlock,
    M05_AXI_arprot,
    M05_AXI_arready,
    M05_AXI_arsize,
    M05_AXI_arvalid,
    M05_AXI_awaddr,
    M05_AXI_awburst,
    M05_AXI_awcache,
    M05_AXI_awid,
    M05_AXI_awlen,
    M05_AXI_awlock,
    M05_AXI_awprot,
    M05_AXI_awready,
    M05_AXI_awsize,
    M05_AXI_awvalid,
    M05_AXI_bid,
    M05_AXI_bready,
    M05_AXI_bresp,
    M05_AXI_bvalid,
    M05_AXI_rdata,
    M05_AXI_rid,
    M05_AXI_rlast,
    M05_AXI_rready,
    M05_AXI_rresp,
    M05_AXI_rvalid,
    M05_AXI_wdata,
    M05_AXI_wlast,
    M05_AXI_wready,
    M05_AXI_wstrb,
    M05_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arburst,
    S00_AXI_arcache,
    S00_AXI_arid,
    S00_AXI_arlen,
    S00_AXI_arlock,
    S00_AXI_arprot,
    S00_AXI_arqos,
    S00_AXI_arready,
    S00_AXI_arsize,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awburst,
    S00_AXI_awcache,
    S00_AXI_awid,
    S00_AXI_awlen,
    S00_AXI_awlock,
    S00_AXI_awprot,
    S00_AXI_awqos,
    S00_AXI_awready,
    S00_AXI_awsize,
    S00_AXI_awvalid,
    S00_AXI_bid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rid,
    S00_AXI_rlast,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wid,
    S00_AXI_wlast,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input ARESETN;
  input M00_ACLK;
  input M00_ARESETN;
  output [31:0]M00_AXI_araddr;
  input M00_AXI_arready;
  output M00_AXI_arvalid;
  output [31:0]M00_AXI_awaddr;
  input M00_AXI_awready;
  output M00_AXI_awvalid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  input M01_ACLK;
  input M01_ARESETN;
  output [31:0]M01_AXI_araddr;
  input M01_AXI_arready;
  output M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  input M01_AXI_awready;
  output M01_AXI_awvalid;
  output M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input M01_AXI_bvalid;
  input [31:0]M01_AXI_rdata;
  output M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input M01_AXI_rvalid;
  output [31:0]M01_AXI_wdata;
  input M01_AXI_wready;
  output [3:0]M01_AXI_wstrb;
  output M01_AXI_wvalid;
  input M02_ACLK;
  input M02_ARESETN;
  output [31:0]M02_AXI_araddr;
  output [1:0]M02_AXI_arburst;
  output [3:0]M02_AXI_arcache;
  output [11:0]M02_AXI_arid;
  output [7:0]M02_AXI_arlen;
  output M02_AXI_arlock;
  output [2:0]M02_AXI_arprot;
  input M02_AXI_arready;
  output [2:0]M02_AXI_arsize;
  output M02_AXI_arvalid;
  output [31:0]M02_AXI_awaddr;
  output [1:0]M02_AXI_awburst;
  output [3:0]M02_AXI_awcache;
  output [11:0]M02_AXI_awid;
  output [7:0]M02_AXI_awlen;
  output M02_AXI_awlock;
  output [2:0]M02_AXI_awprot;
  input M02_AXI_awready;
  output [2:0]M02_AXI_awsize;
  output M02_AXI_awvalid;
  input [11:0]M02_AXI_bid;
  output M02_AXI_bready;
  input [1:0]M02_AXI_bresp;
  input M02_AXI_bvalid;
  input [31:0]M02_AXI_rdata;
  input [11:0]M02_AXI_rid;
  input M02_AXI_rlast;
  output M02_AXI_rready;
  input [1:0]M02_AXI_rresp;
  input M02_AXI_rvalid;
  output [31:0]M02_AXI_wdata;
  output M02_AXI_wlast;
  input M02_AXI_wready;
  output [3:0]M02_AXI_wstrb;
  output M02_AXI_wvalid;
  input M03_ACLK;
  input M03_ARESETN;
  output [31:0]M03_AXI_araddr;
  output [1:0]M03_AXI_arburst;
  output [3:0]M03_AXI_arcache;
  output [11:0]M03_AXI_arid;
  output [7:0]M03_AXI_arlen;
  output M03_AXI_arlock;
  output [2:0]M03_AXI_arprot;
  input M03_AXI_arready;
  output [2:0]M03_AXI_arsize;
  output M03_AXI_arvalid;
  output [31:0]M03_AXI_awaddr;
  output [1:0]M03_AXI_awburst;
  output [3:0]M03_AXI_awcache;
  output [11:0]M03_AXI_awid;
  output [7:0]M03_AXI_awlen;
  output M03_AXI_awlock;
  output [2:0]M03_AXI_awprot;
  input M03_AXI_awready;
  output [2:0]M03_AXI_awsize;
  output M03_AXI_awvalid;
  input [11:0]M03_AXI_bid;
  output M03_AXI_bready;
  input [1:0]M03_AXI_bresp;
  input M03_AXI_bvalid;
  input [31:0]M03_AXI_rdata;
  input [11:0]M03_AXI_rid;
  input M03_AXI_rlast;
  output M03_AXI_rready;
  input [1:0]M03_AXI_rresp;
  input M03_AXI_rvalid;
  output [31:0]M03_AXI_wdata;
  output M03_AXI_wlast;
  input M03_AXI_wready;
  output [3:0]M03_AXI_wstrb;
  output M03_AXI_wvalid;
  input M04_ACLK;
  input M04_ARESETN;
  output [31:0]M04_AXI_araddr;
  output [1:0]M04_AXI_arburst;
  output [3:0]M04_AXI_arcache;
  output [11:0]M04_AXI_arid;
  output [7:0]M04_AXI_arlen;
  output M04_AXI_arlock;
  output [2:0]M04_AXI_arprot;
  input M04_AXI_arready;
  output [2:0]M04_AXI_arsize;
  output M04_AXI_arvalid;
  output [31:0]M04_AXI_awaddr;
  output [1:0]M04_AXI_awburst;
  output [3:0]M04_AXI_awcache;
  output [11:0]M04_AXI_awid;
  output [7:0]M04_AXI_awlen;
  output M04_AXI_awlock;
  output [2:0]M04_AXI_awprot;
  input M04_AXI_awready;
  output [2:0]M04_AXI_awsize;
  output M04_AXI_awvalid;
  input [11:0]M04_AXI_bid;
  output M04_AXI_bready;
  input [1:0]M04_AXI_bresp;
  input M04_AXI_bvalid;
  input [31:0]M04_AXI_rdata;
  input [11:0]M04_AXI_rid;
  input M04_AXI_rlast;
  output M04_AXI_rready;
  input [1:0]M04_AXI_rresp;
  input M04_AXI_rvalid;
  output [31:0]M04_AXI_wdata;
  output M04_AXI_wlast;
  input M04_AXI_wready;
  output [3:0]M04_AXI_wstrb;
  output M04_AXI_wvalid;
  input M05_ACLK;
  input M05_ARESETN;
  output [31:0]M05_AXI_araddr;
  output [1:0]M05_AXI_arburst;
  output [3:0]M05_AXI_arcache;
  output [11:0]M05_AXI_arid;
  output [7:0]M05_AXI_arlen;
  output M05_AXI_arlock;
  output [2:0]M05_AXI_arprot;
  input M05_AXI_arready;
  output [2:0]M05_AXI_arsize;
  output M05_AXI_arvalid;
  output [31:0]M05_AXI_awaddr;
  output [1:0]M05_AXI_awburst;
  output [3:0]M05_AXI_awcache;
  output [11:0]M05_AXI_awid;
  output [7:0]M05_AXI_awlen;
  output M05_AXI_awlock;
  output [2:0]M05_AXI_awprot;
  input M05_AXI_awready;
  output [2:0]M05_AXI_awsize;
  output M05_AXI_awvalid;
  input [11:0]M05_AXI_bid;
  output M05_AXI_bready;
  input [1:0]M05_AXI_bresp;
  input M05_AXI_bvalid;
  input [31:0]M05_AXI_rdata;
  input [11:0]M05_AXI_rid;
  input M05_AXI_rlast;
  output M05_AXI_rready;
  input [1:0]M05_AXI_rresp;
  input M05_AXI_rvalid;
  output [31:0]M05_AXI_wdata;
  output M05_AXI_wlast;
  input M05_AXI_wready;
  output [3:0]M05_AXI_wstrb;
  output M05_AXI_wvalid;
  input S00_ACLK;
  input S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [1:0]S00_AXI_arburst;
  input [3:0]S00_AXI_arcache;
  input [11:0]S00_AXI_arid;
  input [3:0]S00_AXI_arlen;
  input [1:0]S00_AXI_arlock;
  input [2:0]S00_AXI_arprot;
  input [3:0]S00_AXI_arqos;
  output S00_AXI_arready;
  input [2:0]S00_AXI_arsize;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [1:0]S00_AXI_awburst;
  input [3:0]S00_AXI_awcache;
  input [11:0]S00_AXI_awid;
  input [3:0]S00_AXI_awlen;
  input [1:0]S00_AXI_awlock;
  input [2:0]S00_AXI_awprot;
  input [3:0]S00_AXI_awqos;
  output S00_AXI_awready;
  input [2:0]S00_AXI_awsize;
  input S00_AXI_awvalid;
  output [11:0]S00_AXI_bid;
  input S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  output [11:0]S00_AXI_rid;
  output S00_AXI_rlast;
  input S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  input [11:0]S00_AXI_wid;
  input S00_AXI_wlast;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire M00_ACLK_1;
  wire M00_ARESETN_1;
  wire M01_ACLK_1;
  wire M01_ARESETN_1;
  wire M02_ACLK_1;
  wire M02_ARESETN_1;
  wire M03_ACLK_1;
  wire M03_ARESETN_1;
  wire M04_ACLK_1;
  wire M04_ARESETN_1;
  wire M05_ACLK_1;
  wire M05_ARESETN_1;
  wire S00_ACLK_1;
  wire S00_ARESETN_1;
  wire [31:0]m00_couplers_to_psAxiInterconnect_ARADDR;
  wire m00_couplers_to_psAxiInterconnect_ARREADY;
  wire m00_couplers_to_psAxiInterconnect_ARVALID;
  wire [31:0]m00_couplers_to_psAxiInterconnect_AWADDR;
  wire m00_couplers_to_psAxiInterconnect_AWREADY;
  wire m00_couplers_to_psAxiInterconnect_AWVALID;
  wire m00_couplers_to_psAxiInterconnect_BREADY;
  wire [1:0]m00_couplers_to_psAxiInterconnect_BRESP;
  wire m00_couplers_to_psAxiInterconnect_BVALID;
  wire [31:0]m00_couplers_to_psAxiInterconnect_RDATA;
  wire m00_couplers_to_psAxiInterconnect_RREADY;
  wire [1:0]m00_couplers_to_psAxiInterconnect_RRESP;
  wire m00_couplers_to_psAxiInterconnect_RVALID;
  wire [31:0]m00_couplers_to_psAxiInterconnect_WDATA;
  wire m00_couplers_to_psAxiInterconnect_WREADY;
  wire [3:0]m00_couplers_to_psAxiInterconnect_WSTRB;
  wire m00_couplers_to_psAxiInterconnect_WVALID;
  wire [31:0]m01_couplers_to_psAxiInterconnect_ARADDR;
  wire m01_couplers_to_psAxiInterconnect_ARREADY;
  wire m01_couplers_to_psAxiInterconnect_ARVALID;
  wire [31:0]m01_couplers_to_psAxiInterconnect_AWADDR;
  wire m01_couplers_to_psAxiInterconnect_AWREADY;
  wire m01_couplers_to_psAxiInterconnect_AWVALID;
  wire m01_couplers_to_psAxiInterconnect_BREADY;
  wire [1:0]m01_couplers_to_psAxiInterconnect_BRESP;
  wire m01_couplers_to_psAxiInterconnect_BVALID;
  wire [31:0]m01_couplers_to_psAxiInterconnect_RDATA;
  wire m01_couplers_to_psAxiInterconnect_RREADY;
  wire [1:0]m01_couplers_to_psAxiInterconnect_RRESP;
  wire m01_couplers_to_psAxiInterconnect_RVALID;
  wire [31:0]m01_couplers_to_psAxiInterconnect_WDATA;
  wire m01_couplers_to_psAxiInterconnect_WREADY;
  wire [3:0]m01_couplers_to_psAxiInterconnect_WSTRB;
  wire m01_couplers_to_psAxiInterconnect_WVALID;
  wire [31:0]m02_couplers_to_psAxiInterconnect_ARADDR;
  wire [1:0]m02_couplers_to_psAxiInterconnect_ARBURST;
  wire [3:0]m02_couplers_to_psAxiInterconnect_ARCACHE;
  wire [11:0]m02_couplers_to_psAxiInterconnect_ARID;
  wire [7:0]m02_couplers_to_psAxiInterconnect_ARLEN;
  wire m02_couplers_to_psAxiInterconnect_ARLOCK;
  wire [2:0]m02_couplers_to_psAxiInterconnect_ARPROT;
  wire m02_couplers_to_psAxiInterconnect_ARREADY;
  wire [2:0]m02_couplers_to_psAxiInterconnect_ARSIZE;
  wire m02_couplers_to_psAxiInterconnect_ARVALID;
  wire [31:0]m02_couplers_to_psAxiInterconnect_AWADDR;
  wire [1:0]m02_couplers_to_psAxiInterconnect_AWBURST;
  wire [3:0]m02_couplers_to_psAxiInterconnect_AWCACHE;
  wire [11:0]m02_couplers_to_psAxiInterconnect_AWID;
  wire [7:0]m02_couplers_to_psAxiInterconnect_AWLEN;
  wire m02_couplers_to_psAxiInterconnect_AWLOCK;
  wire [2:0]m02_couplers_to_psAxiInterconnect_AWPROT;
  wire m02_couplers_to_psAxiInterconnect_AWREADY;
  wire [2:0]m02_couplers_to_psAxiInterconnect_AWSIZE;
  wire m02_couplers_to_psAxiInterconnect_AWVALID;
  wire [11:0]m02_couplers_to_psAxiInterconnect_BID;
  wire m02_couplers_to_psAxiInterconnect_BREADY;
  wire [1:0]m02_couplers_to_psAxiInterconnect_BRESP;
  wire m02_couplers_to_psAxiInterconnect_BVALID;
  wire [31:0]m02_couplers_to_psAxiInterconnect_RDATA;
  wire [11:0]m02_couplers_to_psAxiInterconnect_RID;
  wire m02_couplers_to_psAxiInterconnect_RLAST;
  wire m02_couplers_to_psAxiInterconnect_RREADY;
  wire [1:0]m02_couplers_to_psAxiInterconnect_RRESP;
  wire m02_couplers_to_psAxiInterconnect_RVALID;
  wire [31:0]m02_couplers_to_psAxiInterconnect_WDATA;
  wire m02_couplers_to_psAxiInterconnect_WLAST;
  wire m02_couplers_to_psAxiInterconnect_WREADY;
  wire [3:0]m02_couplers_to_psAxiInterconnect_WSTRB;
  wire m02_couplers_to_psAxiInterconnect_WVALID;
  wire [31:0]m03_couplers_to_psAxiInterconnect_ARADDR;
  wire [1:0]m03_couplers_to_psAxiInterconnect_ARBURST;
  wire [3:0]m03_couplers_to_psAxiInterconnect_ARCACHE;
  wire [11:0]m03_couplers_to_psAxiInterconnect_ARID;
  wire [7:0]m03_couplers_to_psAxiInterconnect_ARLEN;
  wire m03_couplers_to_psAxiInterconnect_ARLOCK;
  wire [2:0]m03_couplers_to_psAxiInterconnect_ARPROT;
  wire m03_couplers_to_psAxiInterconnect_ARREADY;
  wire [2:0]m03_couplers_to_psAxiInterconnect_ARSIZE;
  wire m03_couplers_to_psAxiInterconnect_ARVALID;
  wire [31:0]m03_couplers_to_psAxiInterconnect_AWADDR;
  wire [1:0]m03_couplers_to_psAxiInterconnect_AWBURST;
  wire [3:0]m03_couplers_to_psAxiInterconnect_AWCACHE;
  wire [11:0]m03_couplers_to_psAxiInterconnect_AWID;
  wire [7:0]m03_couplers_to_psAxiInterconnect_AWLEN;
  wire m03_couplers_to_psAxiInterconnect_AWLOCK;
  wire [2:0]m03_couplers_to_psAxiInterconnect_AWPROT;
  wire m03_couplers_to_psAxiInterconnect_AWREADY;
  wire [2:0]m03_couplers_to_psAxiInterconnect_AWSIZE;
  wire m03_couplers_to_psAxiInterconnect_AWVALID;
  wire [11:0]m03_couplers_to_psAxiInterconnect_BID;
  wire m03_couplers_to_psAxiInterconnect_BREADY;
  wire [1:0]m03_couplers_to_psAxiInterconnect_BRESP;
  wire m03_couplers_to_psAxiInterconnect_BVALID;
  wire [31:0]m03_couplers_to_psAxiInterconnect_RDATA;
  wire [11:0]m03_couplers_to_psAxiInterconnect_RID;
  wire m03_couplers_to_psAxiInterconnect_RLAST;
  wire m03_couplers_to_psAxiInterconnect_RREADY;
  wire [1:0]m03_couplers_to_psAxiInterconnect_RRESP;
  wire m03_couplers_to_psAxiInterconnect_RVALID;
  wire [31:0]m03_couplers_to_psAxiInterconnect_WDATA;
  wire m03_couplers_to_psAxiInterconnect_WLAST;
  wire m03_couplers_to_psAxiInterconnect_WREADY;
  wire [3:0]m03_couplers_to_psAxiInterconnect_WSTRB;
  wire m03_couplers_to_psAxiInterconnect_WVALID;
  wire [31:0]m04_couplers_to_psAxiInterconnect_ARADDR;
  wire [1:0]m04_couplers_to_psAxiInterconnect_ARBURST;
  wire [3:0]m04_couplers_to_psAxiInterconnect_ARCACHE;
  wire [11:0]m04_couplers_to_psAxiInterconnect_ARID;
  wire [7:0]m04_couplers_to_psAxiInterconnect_ARLEN;
  wire m04_couplers_to_psAxiInterconnect_ARLOCK;
  wire [2:0]m04_couplers_to_psAxiInterconnect_ARPROT;
  wire m04_couplers_to_psAxiInterconnect_ARREADY;
  wire [2:0]m04_couplers_to_psAxiInterconnect_ARSIZE;
  wire m04_couplers_to_psAxiInterconnect_ARVALID;
  wire [31:0]m04_couplers_to_psAxiInterconnect_AWADDR;
  wire [1:0]m04_couplers_to_psAxiInterconnect_AWBURST;
  wire [3:0]m04_couplers_to_psAxiInterconnect_AWCACHE;
  wire [11:0]m04_couplers_to_psAxiInterconnect_AWID;
  wire [7:0]m04_couplers_to_psAxiInterconnect_AWLEN;
  wire m04_couplers_to_psAxiInterconnect_AWLOCK;
  wire [2:0]m04_couplers_to_psAxiInterconnect_AWPROT;
  wire m04_couplers_to_psAxiInterconnect_AWREADY;
  wire [2:0]m04_couplers_to_psAxiInterconnect_AWSIZE;
  wire m04_couplers_to_psAxiInterconnect_AWVALID;
  wire [11:0]m04_couplers_to_psAxiInterconnect_BID;
  wire m04_couplers_to_psAxiInterconnect_BREADY;
  wire [1:0]m04_couplers_to_psAxiInterconnect_BRESP;
  wire m04_couplers_to_psAxiInterconnect_BVALID;
  wire [31:0]m04_couplers_to_psAxiInterconnect_RDATA;
  wire [11:0]m04_couplers_to_psAxiInterconnect_RID;
  wire m04_couplers_to_psAxiInterconnect_RLAST;
  wire m04_couplers_to_psAxiInterconnect_RREADY;
  wire [1:0]m04_couplers_to_psAxiInterconnect_RRESP;
  wire m04_couplers_to_psAxiInterconnect_RVALID;
  wire [31:0]m04_couplers_to_psAxiInterconnect_WDATA;
  wire m04_couplers_to_psAxiInterconnect_WLAST;
  wire m04_couplers_to_psAxiInterconnect_WREADY;
  wire [3:0]m04_couplers_to_psAxiInterconnect_WSTRB;
  wire m04_couplers_to_psAxiInterconnect_WVALID;
  wire [31:0]m05_couplers_to_psAxiInterconnect_ARADDR;
  wire [1:0]m05_couplers_to_psAxiInterconnect_ARBURST;
  wire [3:0]m05_couplers_to_psAxiInterconnect_ARCACHE;
  wire [11:0]m05_couplers_to_psAxiInterconnect_ARID;
  wire [7:0]m05_couplers_to_psAxiInterconnect_ARLEN;
  wire m05_couplers_to_psAxiInterconnect_ARLOCK;
  wire [2:0]m05_couplers_to_psAxiInterconnect_ARPROT;
  wire m05_couplers_to_psAxiInterconnect_ARREADY;
  wire [2:0]m05_couplers_to_psAxiInterconnect_ARSIZE;
  wire m05_couplers_to_psAxiInterconnect_ARVALID;
  wire [31:0]m05_couplers_to_psAxiInterconnect_AWADDR;
  wire [1:0]m05_couplers_to_psAxiInterconnect_AWBURST;
  wire [3:0]m05_couplers_to_psAxiInterconnect_AWCACHE;
  wire [11:0]m05_couplers_to_psAxiInterconnect_AWID;
  wire [7:0]m05_couplers_to_psAxiInterconnect_AWLEN;
  wire m05_couplers_to_psAxiInterconnect_AWLOCK;
  wire [2:0]m05_couplers_to_psAxiInterconnect_AWPROT;
  wire m05_couplers_to_psAxiInterconnect_AWREADY;
  wire [2:0]m05_couplers_to_psAxiInterconnect_AWSIZE;
  wire m05_couplers_to_psAxiInterconnect_AWVALID;
  wire [11:0]m05_couplers_to_psAxiInterconnect_BID;
  wire m05_couplers_to_psAxiInterconnect_BREADY;
  wire [1:0]m05_couplers_to_psAxiInterconnect_BRESP;
  wire m05_couplers_to_psAxiInterconnect_BVALID;
  wire [31:0]m05_couplers_to_psAxiInterconnect_RDATA;
  wire [11:0]m05_couplers_to_psAxiInterconnect_RID;
  wire m05_couplers_to_psAxiInterconnect_RLAST;
  wire m05_couplers_to_psAxiInterconnect_RREADY;
  wire [1:0]m05_couplers_to_psAxiInterconnect_RRESP;
  wire m05_couplers_to_psAxiInterconnect_RVALID;
  wire [31:0]m05_couplers_to_psAxiInterconnect_WDATA;
  wire m05_couplers_to_psAxiInterconnect_WLAST;
  wire m05_couplers_to_psAxiInterconnect_WREADY;
  wire [3:0]m05_couplers_to_psAxiInterconnect_WSTRB;
  wire m05_couplers_to_psAxiInterconnect_WVALID;
  wire psAxiInterconnect_ACLK_net;
  wire psAxiInterconnect_ARESETN_net;
  wire [31:0]psAxiInterconnect_to_s00_couplers_ARADDR;
  wire [1:0]psAxiInterconnect_to_s00_couplers_ARBURST;
  wire [3:0]psAxiInterconnect_to_s00_couplers_ARCACHE;
  wire [11:0]psAxiInterconnect_to_s00_couplers_ARID;
  wire [3:0]psAxiInterconnect_to_s00_couplers_ARLEN;
  wire [1:0]psAxiInterconnect_to_s00_couplers_ARLOCK;
  wire [2:0]psAxiInterconnect_to_s00_couplers_ARPROT;
  wire [3:0]psAxiInterconnect_to_s00_couplers_ARQOS;
  wire psAxiInterconnect_to_s00_couplers_ARREADY;
  wire [2:0]psAxiInterconnect_to_s00_couplers_ARSIZE;
  wire psAxiInterconnect_to_s00_couplers_ARVALID;
  wire [31:0]psAxiInterconnect_to_s00_couplers_AWADDR;
  wire [1:0]psAxiInterconnect_to_s00_couplers_AWBURST;
  wire [3:0]psAxiInterconnect_to_s00_couplers_AWCACHE;
  wire [11:0]psAxiInterconnect_to_s00_couplers_AWID;
  wire [3:0]psAxiInterconnect_to_s00_couplers_AWLEN;
  wire [1:0]psAxiInterconnect_to_s00_couplers_AWLOCK;
  wire [2:0]psAxiInterconnect_to_s00_couplers_AWPROT;
  wire [3:0]psAxiInterconnect_to_s00_couplers_AWQOS;
  wire psAxiInterconnect_to_s00_couplers_AWREADY;
  wire [2:0]psAxiInterconnect_to_s00_couplers_AWSIZE;
  wire psAxiInterconnect_to_s00_couplers_AWVALID;
  wire [11:0]psAxiInterconnect_to_s00_couplers_BID;
  wire psAxiInterconnect_to_s00_couplers_BREADY;
  wire [1:0]psAxiInterconnect_to_s00_couplers_BRESP;
  wire psAxiInterconnect_to_s00_couplers_BVALID;
  wire [31:0]psAxiInterconnect_to_s00_couplers_RDATA;
  wire [11:0]psAxiInterconnect_to_s00_couplers_RID;
  wire psAxiInterconnect_to_s00_couplers_RLAST;
  wire psAxiInterconnect_to_s00_couplers_RREADY;
  wire [1:0]psAxiInterconnect_to_s00_couplers_RRESP;
  wire psAxiInterconnect_to_s00_couplers_RVALID;
  wire [31:0]psAxiInterconnect_to_s00_couplers_WDATA;
  wire [11:0]psAxiInterconnect_to_s00_couplers_WID;
  wire psAxiInterconnect_to_s00_couplers_WLAST;
  wire psAxiInterconnect_to_s00_couplers_WREADY;
  wire [3:0]psAxiInterconnect_to_s00_couplers_WSTRB;
  wire psAxiInterconnect_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_xbar_ARADDR;
  wire [1:0]s00_couplers_to_xbar_ARBURST;
  wire [3:0]s00_couplers_to_xbar_ARCACHE;
  wire [11:0]s00_couplers_to_xbar_ARID;
  wire [7:0]s00_couplers_to_xbar_ARLEN;
  wire [0:0]s00_couplers_to_xbar_ARLOCK;
  wire [2:0]s00_couplers_to_xbar_ARPROT;
  wire [3:0]s00_couplers_to_xbar_ARQOS;
  wire [0:0]s00_couplers_to_xbar_ARREADY;
  wire [2:0]s00_couplers_to_xbar_ARSIZE;
  wire s00_couplers_to_xbar_ARVALID;
  wire [31:0]s00_couplers_to_xbar_AWADDR;
  wire [1:0]s00_couplers_to_xbar_AWBURST;
  wire [3:0]s00_couplers_to_xbar_AWCACHE;
  wire [11:0]s00_couplers_to_xbar_AWID;
  wire [7:0]s00_couplers_to_xbar_AWLEN;
  wire [0:0]s00_couplers_to_xbar_AWLOCK;
  wire [2:0]s00_couplers_to_xbar_AWPROT;
  wire [3:0]s00_couplers_to_xbar_AWQOS;
  wire [0:0]s00_couplers_to_xbar_AWREADY;
  wire [2:0]s00_couplers_to_xbar_AWSIZE;
  wire s00_couplers_to_xbar_AWVALID;
  wire [11:0]s00_couplers_to_xbar_BID;
  wire s00_couplers_to_xbar_BREADY;
  wire [1:0]s00_couplers_to_xbar_BRESP;
  wire [0:0]s00_couplers_to_xbar_BVALID;
  wire [31:0]s00_couplers_to_xbar_RDATA;
  wire [11:0]s00_couplers_to_xbar_RID;
  wire [0:0]s00_couplers_to_xbar_RLAST;
  wire s00_couplers_to_xbar_RREADY;
  wire [1:0]s00_couplers_to_xbar_RRESP;
  wire [0:0]s00_couplers_to_xbar_RVALID;
  wire [31:0]s00_couplers_to_xbar_WDATA;
  wire s00_couplers_to_xbar_WLAST;
  wire [0:0]s00_couplers_to_xbar_WREADY;
  wire [3:0]s00_couplers_to_xbar_WSTRB;
  wire s00_couplers_to_xbar_WVALID;
  wire [31:0]xbar_to_m00_couplers_ARADDR;
  wire [1:0]xbar_to_m00_couplers_ARBURST;
  wire [3:0]xbar_to_m00_couplers_ARCACHE;
  wire [11:0]xbar_to_m00_couplers_ARID;
  wire [7:0]xbar_to_m00_couplers_ARLEN;
  wire [0:0]xbar_to_m00_couplers_ARLOCK;
  wire [2:0]xbar_to_m00_couplers_ARPROT;
  wire [3:0]xbar_to_m00_couplers_ARQOS;
  wire xbar_to_m00_couplers_ARREADY;
  wire [3:0]xbar_to_m00_couplers_ARREGION;
  wire [2:0]xbar_to_m00_couplers_ARSIZE;
  wire [0:0]xbar_to_m00_couplers_ARVALID;
  wire [31:0]xbar_to_m00_couplers_AWADDR;
  wire [1:0]xbar_to_m00_couplers_AWBURST;
  wire [3:0]xbar_to_m00_couplers_AWCACHE;
  wire [11:0]xbar_to_m00_couplers_AWID;
  wire [7:0]xbar_to_m00_couplers_AWLEN;
  wire [0:0]xbar_to_m00_couplers_AWLOCK;
  wire [2:0]xbar_to_m00_couplers_AWPROT;
  wire [3:0]xbar_to_m00_couplers_AWQOS;
  wire xbar_to_m00_couplers_AWREADY;
  wire [3:0]xbar_to_m00_couplers_AWREGION;
  wire [2:0]xbar_to_m00_couplers_AWSIZE;
  wire [0:0]xbar_to_m00_couplers_AWVALID;
  wire [11:0]xbar_to_m00_couplers_BID;
  wire [0:0]xbar_to_m00_couplers_BREADY;
  wire [1:0]xbar_to_m00_couplers_BRESP;
  wire xbar_to_m00_couplers_BVALID;
  wire [31:0]xbar_to_m00_couplers_RDATA;
  wire [11:0]xbar_to_m00_couplers_RID;
  wire xbar_to_m00_couplers_RLAST;
  wire [0:0]xbar_to_m00_couplers_RREADY;
  wire [1:0]xbar_to_m00_couplers_RRESP;
  wire xbar_to_m00_couplers_RVALID;
  wire [31:0]xbar_to_m00_couplers_WDATA;
  wire [0:0]xbar_to_m00_couplers_WLAST;
  wire xbar_to_m00_couplers_WREADY;
  wire [3:0]xbar_to_m00_couplers_WSTRB;
  wire [0:0]xbar_to_m00_couplers_WVALID;
  wire [63:32]xbar_to_m01_couplers_ARADDR;
  wire [3:2]xbar_to_m01_couplers_ARBURST;
  wire [7:4]xbar_to_m01_couplers_ARCACHE;
  wire [23:12]xbar_to_m01_couplers_ARID;
  wire [15:8]xbar_to_m01_couplers_ARLEN;
  wire [1:1]xbar_to_m01_couplers_ARLOCK;
  wire [5:3]xbar_to_m01_couplers_ARPROT;
  wire [7:4]xbar_to_m01_couplers_ARQOS;
  wire xbar_to_m01_couplers_ARREADY;
  wire [7:4]xbar_to_m01_couplers_ARREGION;
  wire [5:3]xbar_to_m01_couplers_ARSIZE;
  wire [1:1]xbar_to_m01_couplers_ARVALID;
  wire [63:32]xbar_to_m01_couplers_AWADDR;
  wire [3:2]xbar_to_m01_couplers_AWBURST;
  wire [7:4]xbar_to_m01_couplers_AWCACHE;
  wire [23:12]xbar_to_m01_couplers_AWID;
  wire [15:8]xbar_to_m01_couplers_AWLEN;
  wire [1:1]xbar_to_m01_couplers_AWLOCK;
  wire [5:3]xbar_to_m01_couplers_AWPROT;
  wire [7:4]xbar_to_m01_couplers_AWQOS;
  wire xbar_to_m01_couplers_AWREADY;
  wire [7:4]xbar_to_m01_couplers_AWREGION;
  wire [5:3]xbar_to_m01_couplers_AWSIZE;
  wire [1:1]xbar_to_m01_couplers_AWVALID;
  wire [11:0]xbar_to_m01_couplers_BID;
  wire [1:1]xbar_to_m01_couplers_BREADY;
  wire [1:0]xbar_to_m01_couplers_BRESP;
  wire xbar_to_m01_couplers_BVALID;
  wire [31:0]xbar_to_m01_couplers_RDATA;
  wire [11:0]xbar_to_m01_couplers_RID;
  wire xbar_to_m01_couplers_RLAST;
  wire [1:1]xbar_to_m01_couplers_RREADY;
  wire [1:0]xbar_to_m01_couplers_RRESP;
  wire xbar_to_m01_couplers_RVALID;
  wire [63:32]xbar_to_m01_couplers_WDATA;
  wire [1:1]xbar_to_m01_couplers_WLAST;
  wire xbar_to_m01_couplers_WREADY;
  wire [7:4]xbar_to_m01_couplers_WSTRB;
  wire [1:1]xbar_to_m01_couplers_WVALID;
  wire [95:64]xbar_to_m02_couplers_ARADDR;
  wire [5:4]xbar_to_m02_couplers_ARBURST;
  wire [11:8]xbar_to_m02_couplers_ARCACHE;
  wire [35:24]xbar_to_m02_couplers_ARID;
  wire [23:16]xbar_to_m02_couplers_ARLEN;
  wire [2:2]xbar_to_m02_couplers_ARLOCK;
  wire [8:6]xbar_to_m02_couplers_ARPROT;
  wire xbar_to_m02_couplers_ARREADY;
  wire [8:6]xbar_to_m02_couplers_ARSIZE;
  wire [2:2]xbar_to_m02_couplers_ARVALID;
  wire [95:64]xbar_to_m02_couplers_AWADDR;
  wire [5:4]xbar_to_m02_couplers_AWBURST;
  wire [11:8]xbar_to_m02_couplers_AWCACHE;
  wire [35:24]xbar_to_m02_couplers_AWID;
  wire [23:16]xbar_to_m02_couplers_AWLEN;
  wire [2:2]xbar_to_m02_couplers_AWLOCK;
  wire [8:6]xbar_to_m02_couplers_AWPROT;
  wire xbar_to_m02_couplers_AWREADY;
  wire [8:6]xbar_to_m02_couplers_AWSIZE;
  wire [2:2]xbar_to_m02_couplers_AWVALID;
  wire [11:0]xbar_to_m02_couplers_BID;
  wire [2:2]xbar_to_m02_couplers_BREADY;
  wire [1:0]xbar_to_m02_couplers_BRESP;
  wire xbar_to_m02_couplers_BVALID;
  wire [31:0]xbar_to_m02_couplers_RDATA;
  wire [11:0]xbar_to_m02_couplers_RID;
  wire xbar_to_m02_couplers_RLAST;
  wire [2:2]xbar_to_m02_couplers_RREADY;
  wire [1:0]xbar_to_m02_couplers_RRESP;
  wire xbar_to_m02_couplers_RVALID;
  wire [95:64]xbar_to_m02_couplers_WDATA;
  wire [2:2]xbar_to_m02_couplers_WLAST;
  wire xbar_to_m02_couplers_WREADY;
  wire [11:8]xbar_to_m02_couplers_WSTRB;
  wire [2:2]xbar_to_m02_couplers_WVALID;
  wire [127:96]xbar_to_m03_couplers_ARADDR;
  wire [7:6]xbar_to_m03_couplers_ARBURST;
  wire [15:12]xbar_to_m03_couplers_ARCACHE;
  wire [47:36]xbar_to_m03_couplers_ARID;
  wire [31:24]xbar_to_m03_couplers_ARLEN;
  wire [3:3]xbar_to_m03_couplers_ARLOCK;
  wire [11:9]xbar_to_m03_couplers_ARPROT;
  wire xbar_to_m03_couplers_ARREADY;
  wire [11:9]xbar_to_m03_couplers_ARSIZE;
  wire [3:3]xbar_to_m03_couplers_ARVALID;
  wire [127:96]xbar_to_m03_couplers_AWADDR;
  wire [7:6]xbar_to_m03_couplers_AWBURST;
  wire [15:12]xbar_to_m03_couplers_AWCACHE;
  wire [47:36]xbar_to_m03_couplers_AWID;
  wire [31:24]xbar_to_m03_couplers_AWLEN;
  wire [3:3]xbar_to_m03_couplers_AWLOCK;
  wire [11:9]xbar_to_m03_couplers_AWPROT;
  wire xbar_to_m03_couplers_AWREADY;
  wire [11:9]xbar_to_m03_couplers_AWSIZE;
  wire [3:3]xbar_to_m03_couplers_AWVALID;
  wire [11:0]xbar_to_m03_couplers_BID;
  wire [3:3]xbar_to_m03_couplers_BREADY;
  wire [1:0]xbar_to_m03_couplers_BRESP;
  wire xbar_to_m03_couplers_BVALID;
  wire [31:0]xbar_to_m03_couplers_RDATA;
  wire [11:0]xbar_to_m03_couplers_RID;
  wire xbar_to_m03_couplers_RLAST;
  wire [3:3]xbar_to_m03_couplers_RREADY;
  wire [1:0]xbar_to_m03_couplers_RRESP;
  wire xbar_to_m03_couplers_RVALID;
  wire [127:96]xbar_to_m03_couplers_WDATA;
  wire [3:3]xbar_to_m03_couplers_WLAST;
  wire xbar_to_m03_couplers_WREADY;
  wire [15:12]xbar_to_m03_couplers_WSTRB;
  wire [3:3]xbar_to_m03_couplers_WVALID;
  wire [159:128]xbar_to_m04_couplers_ARADDR;
  wire [9:8]xbar_to_m04_couplers_ARBURST;
  wire [19:16]xbar_to_m04_couplers_ARCACHE;
  wire [59:48]xbar_to_m04_couplers_ARID;
  wire [39:32]xbar_to_m04_couplers_ARLEN;
  wire [4:4]xbar_to_m04_couplers_ARLOCK;
  wire [14:12]xbar_to_m04_couplers_ARPROT;
  wire xbar_to_m04_couplers_ARREADY;
  wire [14:12]xbar_to_m04_couplers_ARSIZE;
  wire [4:4]xbar_to_m04_couplers_ARVALID;
  wire [159:128]xbar_to_m04_couplers_AWADDR;
  wire [9:8]xbar_to_m04_couplers_AWBURST;
  wire [19:16]xbar_to_m04_couplers_AWCACHE;
  wire [59:48]xbar_to_m04_couplers_AWID;
  wire [39:32]xbar_to_m04_couplers_AWLEN;
  wire [4:4]xbar_to_m04_couplers_AWLOCK;
  wire [14:12]xbar_to_m04_couplers_AWPROT;
  wire xbar_to_m04_couplers_AWREADY;
  wire [14:12]xbar_to_m04_couplers_AWSIZE;
  wire [4:4]xbar_to_m04_couplers_AWVALID;
  wire [11:0]xbar_to_m04_couplers_BID;
  wire [4:4]xbar_to_m04_couplers_BREADY;
  wire [1:0]xbar_to_m04_couplers_BRESP;
  wire xbar_to_m04_couplers_BVALID;
  wire [31:0]xbar_to_m04_couplers_RDATA;
  wire [11:0]xbar_to_m04_couplers_RID;
  wire xbar_to_m04_couplers_RLAST;
  wire [4:4]xbar_to_m04_couplers_RREADY;
  wire [1:0]xbar_to_m04_couplers_RRESP;
  wire xbar_to_m04_couplers_RVALID;
  wire [159:128]xbar_to_m04_couplers_WDATA;
  wire [4:4]xbar_to_m04_couplers_WLAST;
  wire xbar_to_m04_couplers_WREADY;
  wire [19:16]xbar_to_m04_couplers_WSTRB;
  wire [4:4]xbar_to_m04_couplers_WVALID;
  wire [191:160]xbar_to_m05_couplers_ARADDR;
  wire [11:10]xbar_to_m05_couplers_ARBURST;
  wire [23:20]xbar_to_m05_couplers_ARCACHE;
  wire [71:60]xbar_to_m05_couplers_ARID;
  wire [47:40]xbar_to_m05_couplers_ARLEN;
  wire [5:5]xbar_to_m05_couplers_ARLOCK;
  wire [17:15]xbar_to_m05_couplers_ARPROT;
  wire xbar_to_m05_couplers_ARREADY;
  wire [17:15]xbar_to_m05_couplers_ARSIZE;
  wire [5:5]xbar_to_m05_couplers_ARVALID;
  wire [191:160]xbar_to_m05_couplers_AWADDR;
  wire [11:10]xbar_to_m05_couplers_AWBURST;
  wire [23:20]xbar_to_m05_couplers_AWCACHE;
  wire [71:60]xbar_to_m05_couplers_AWID;
  wire [47:40]xbar_to_m05_couplers_AWLEN;
  wire [5:5]xbar_to_m05_couplers_AWLOCK;
  wire [17:15]xbar_to_m05_couplers_AWPROT;
  wire xbar_to_m05_couplers_AWREADY;
  wire [17:15]xbar_to_m05_couplers_AWSIZE;
  wire [5:5]xbar_to_m05_couplers_AWVALID;
  wire [11:0]xbar_to_m05_couplers_BID;
  wire [5:5]xbar_to_m05_couplers_BREADY;
  wire [1:0]xbar_to_m05_couplers_BRESP;
  wire xbar_to_m05_couplers_BVALID;
  wire [31:0]xbar_to_m05_couplers_RDATA;
  wire [11:0]xbar_to_m05_couplers_RID;
  wire xbar_to_m05_couplers_RLAST;
  wire [5:5]xbar_to_m05_couplers_RREADY;
  wire [1:0]xbar_to_m05_couplers_RRESP;
  wire xbar_to_m05_couplers_RVALID;
  wire [191:160]xbar_to_m05_couplers_WDATA;
  wire [5:5]xbar_to_m05_couplers_WLAST;
  wire xbar_to_m05_couplers_WREADY;
  wire [23:20]xbar_to_m05_couplers_WSTRB;
  wire [5:5]xbar_to_m05_couplers_WVALID;

  assign M00_ACLK_1 = M00_ACLK;
  assign M00_ARESETN_1 = M00_ARESETN;
  assign M00_AXI_araddr[31:0] = m00_couplers_to_psAxiInterconnect_ARADDR;
  assign M00_AXI_arvalid = m00_couplers_to_psAxiInterconnect_ARVALID;
  assign M00_AXI_awaddr[31:0] = m00_couplers_to_psAxiInterconnect_AWADDR;
  assign M00_AXI_awvalid = m00_couplers_to_psAxiInterconnect_AWVALID;
  assign M00_AXI_bready = m00_couplers_to_psAxiInterconnect_BREADY;
  assign M00_AXI_rready = m00_couplers_to_psAxiInterconnect_RREADY;
  assign M00_AXI_wdata[31:0] = m00_couplers_to_psAxiInterconnect_WDATA;
  assign M00_AXI_wstrb[3:0] = m00_couplers_to_psAxiInterconnect_WSTRB;
  assign M00_AXI_wvalid = m00_couplers_to_psAxiInterconnect_WVALID;
  assign M01_ACLK_1 = M01_ACLK;
  assign M01_ARESETN_1 = M01_ARESETN;
  assign M01_AXI_araddr[31:0] = m01_couplers_to_psAxiInterconnect_ARADDR;
  assign M01_AXI_arvalid = m01_couplers_to_psAxiInterconnect_ARVALID;
  assign M01_AXI_awaddr[31:0] = m01_couplers_to_psAxiInterconnect_AWADDR;
  assign M01_AXI_awvalid = m01_couplers_to_psAxiInterconnect_AWVALID;
  assign M01_AXI_bready = m01_couplers_to_psAxiInterconnect_BREADY;
  assign M01_AXI_rready = m01_couplers_to_psAxiInterconnect_RREADY;
  assign M01_AXI_wdata[31:0] = m01_couplers_to_psAxiInterconnect_WDATA;
  assign M01_AXI_wstrb[3:0] = m01_couplers_to_psAxiInterconnect_WSTRB;
  assign M01_AXI_wvalid = m01_couplers_to_psAxiInterconnect_WVALID;
  assign M02_ACLK_1 = M02_ACLK;
  assign M02_ARESETN_1 = M02_ARESETN;
  assign M02_AXI_araddr[31:0] = m02_couplers_to_psAxiInterconnect_ARADDR;
  assign M02_AXI_arburst[1:0] = m02_couplers_to_psAxiInterconnect_ARBURST;
  assign M02_AXI_arcache[3:0] = m02_couplers_to_psAxiInterconnect_ARCACHE;
  assign M02_AXI_arid[11:0] = m02_couplers_to_psAxiInterconnect_ARID;
  assign M02_AXI_arlen[7:0] = m02_couplers_to_psAxiInterconnect_ARLEN;
  assign M02_AXI_arlock = m02_couplers_to_psAxiInterconnect_ARLOCK;
  assign M02_AXI_arprot[2:0] = m02_couplers_to_psAxiInterconnect_ARPROT;
  assign M02_AXI_arsize[2:0] = m02_couplers_to_psAxiInterconnect_ARSIZE;
  assign M02_AXI_arvalid = m02_couplers_to_psAxiInterconnect_ARVALID;
  assign M02_AXI_awaddr[31:0] = m02_couplers_to_psAxiInterconnect_AWADDR;
  assign M02_AXI_awburst[1:0] = m02_couplers_to_psAxiInterconnect_AWBURST;
  assign M02_AXI_awcache[3:0] = m02_couplers_to_psAxiInterconnect_AWCACHE;
  assign M02_AXI_awid[11:0] = m02_couplers_to_psAxiInterconnect_AWID;
  assign M02_AXI_awlen[7:0] = m02_couplers_to_psAxiInterconnect_AWLEN;
  assign M02_AXI_awlock = m02_couplers_to_psAxiInterconnect_AWLOCK;
  assign M02_AXI_awprot[2:0] = m02_couplers_to_psAxiInterconnect_AWPROT;
  assign M02_AXI_awsize[2:0] = m02_couplers_to_psAxiInterconnect_AWSIZE;
  assign M02_AXI_awvalid = m02_couplers_to_psAxiInterconnect_AWVALID;
  assign M02_AXI_bready = m02_couplers_to_psAxiInterconnect_BREADY;
  assign M02_AXI_rready = m02_couplers_to_psAxiInterconnect_RREADY;
  assign M02_AXI_wdata[31:0] = m02_couplers_to_psAxiInterconnect_WDATA;
  assign M02_AXI_wlast = m02_couplers_to_psAxiInterconnect_WLAST;
  assign M02_AXI_wstrb[3:0] = m02_couplers_to_psAxiInterconnect_WSTRB;
  assign M02_AXI_wvalid = m02_couplers_to_psAxiInterconnect_WVALID;
  assign M03_ACLK_1 = M03_ACLK;
  assign M03_ARESETN_1 = M03_ARESETN;
  assign M03_AXI_araddr[31:0] = m03_couplers_to_psAxiInterconnect_ARADDR;
  assign M03_AXI_arburst[1:0] = m03_couplers_to_psAxiInterconnect_ARBURST;
  assign M03_AXI_arcache[3:0] = m03_couplers_to_psAxiInterconnect_ARCACHE;
  assign M03_AXI_arid[11:0] = m03_couplers_to_psAxiInterconnect_ARID;
  assign M03_AXI_arlen[7:0] = m03_couplers_to_psAxiInterconnect_ARLEN;
  assign M03_AXI_arlock = m03_couplers_to_psAxiInterconnect_ARLOCK;
  assign M03_AXI_arprot[2:0] = m03_couplers_to_psAxiInterconnect_ARPROT;
  assign M03_AXI_arsize[2:0] = m03_couplers_to_psAxiInterconnect_ARSIZE;
  assign M03_AXI_arvalid = m03_couplers_to_psAxiInterconnect_ARVALID;
  assign M03_AXI_awaddr[31:0] = m03_couplers_to_psAxiInterconnect_AWADDR;
  assign M03_AXI_awburst[1:0] = m03_couplers_to_psAxiInterconnect_AWBURST;
  assign M03_AXI_awcache[3:0] = m03_couplers_to_psAxiInterconnect_AWCACHE;
  assign M03_AXI_awid[11:0] = m03_couplers_to_psAxiInterconnect_AWID;
  assign M03_AXI_awlen[7:0] = m03_couplers_to_psAxiInterconnect_AWLEN;
  assign M03_AXI_awlock = m03_couplers_to_psAxiInterconnect_AWLOCK;
  assign M03_AXI_awprot[2:0] = m03_couplers_to_psAxiInterconnect_AWPROT;
  assign M03_AXI_awsize[2:0] = m03_couplers_to_psAxiInterconnect_AWSIZE;
  assign M03_AXI_awvalid = m03_couplers_to_psAxiInterconnect_AWVALID;
  assign M03_AXI_bready = m03_couplers_to_psAxiInterconnect_BREADY;
  assign M03_AXI_rready = m03_couplers_to_psAxiInterconnect_RREADY;
  assign M03_AXI_wdata[31:0] = m03_couplers_to_psAxiInterconnect_WDATA;
  assign M03_AXI_wlast = m03_couplers_to_psAxiInterconnect_WLAST;
  assign M03_AXI_wstrb[3:0] = m03_couplers_to_psAxiInterconnect_WSTRB;
  assign M03_AXI_wvalid = m03_couplers_to_psAxiInterconnect_WVALID;
  assign M04_ACLK_1 = M04_ACLK;
  assign M04_ARESETN_1 = M04_ARESETN;
  assign M04_AXI_araddr[31:0] = m04_couplers_to_psAxiInterconnect_ARADDR;
  assign M04_AXI_arburst[1:0] = m04_couplers_to_psAxiInterconnect_ARBURST;
  assign M04_AXI_arcache[3:0] = m04_couplers_to_psAxiInterconnect_ARCACHE;
  assign M04_AXI_arid[11:0] = m04_couplers_to_psAxiInterconnect_ARID;
  assign M04_AXI_arlen[7:0] = m04_couplers_to_psAxiInterconnect_ARLEN;
  assign M04_AXI_arlock = m04_couplers_to_psAxiInterconnect_ARLOCK;
  assign M04_AXI_arprot[2:0] = m04_couplers_to_psAxiInterconnect_ARPROT;
  assign M04_AXI_arsize[2:0] = m04_couplers_to_psAxiInterconnect_ARSIZE;
  assign M04_AXI_arvalid = m04_couplers_to_psAxiInterconnect_ARVALID;
  assign M04_AXI_awaddr[31:0] = m04_couplers_to_psAxiInterconnect_AWADDR;
  assign M04_AXI_awburst[1:0] = m04_couplers_to_psAxiInterconnect_AWBURST;
  assign M04_AXI_awcache[3:0] = m04_couplers_to_psAxiInterconnect_AWCACHE;
  assign M04_AXI_awid[11:0] = m04_couplers_to_psAxiInterconnect_AWID;
  assign M04_AXI_awlen[7:0] = m04_couplers_to_psAxiInterconnect_AWLEN;
  assign M04_AXI_awlock = m04_couplers_to_psAxiInterconnect_AWLOCK;
  assign M04_AXI_awprot[2:0] = m04_couplers_to_psAxiInterconnect_AWPROT;
  assign M04_AXI_awsize[2:0] = m04_couplers_to_psAxiInterconnect_AWSIZE;
  assign M04_AXI_awvalid = m04_couplers_to_psAxiInterconnect_AWVALID;
  assign M04_AXI_bready = m04_couplers_to_psAxiInterconnect_BREADY;
  assign M04_AXI_rready = m04_couplers_to_psAxiInterconnect_RREADY;
  assign M04_AXI_wdata[31:0] = m04_couplers_to_psAxiInterconnect_WDATA;
  assign M04_AXI_wlast = m04_couplers_to_psAxiInterconnect_WLAST;
  assign M04_AXI_wstrb[3:0] = m04_couplers_to_psAxiInterconnect_WSTRB;
  assign M04_AXI_wvalid = m04_couplers_to_psAxiInterconnect_WVALID;
  assign M05_ACLK_1 = M05_ACLK;
  assign M05_ARESETN_1 = M05_ARESETN;
  assign M05_AXI_araddr[31:0] = m05_couplers_to_psAxiInterconnect_ARADDR;
  assign M05_AXI_arburst[1:0] = m05_couplers_to_psAxiInterconnect_ARBURST;
  assign M05_AXI_arcache[3:0] = m05_couplers_to_psAxiInterconnect_ARCACHE;
  assign M05_AXI_arid[11:0] = m05_couplers_to_psAxiInterconnect_ARID;
  assign M05_AXI_arlen[7:0] = m05_couplers_to_psAxiInterconnect_ARLEN;
  assign M05_AXI_arlock = m05_couplers_to_psAxiInterconnect_ARLOCK;
  assign M05_AXI_arprot[2:0] = m05_couplers_to_psAxiInterconnect_ARPROT;
  assign M05_AXI_arsize[2:0] = m05_couplers_to_psAxiInterconnect_ARSIZE;
  assign M05_AXI_arvalid = m05_couplers_to_psAxiInterconnect_ARVALID;
  assign M05_AXI_awaddr[31:0] = m05_couplers_to_psAxiInterconnect_AWADDR;
  assign M05_AXI_awburst[1:0] = m05_couplers_to_psAxiInterconnect_AWBURST;
  assign M05_AXI_awcache[3:0] = m05_couplers_to_psAxiInterconnect_AWCACHE;
  assign M05_AXI_awid[11:0] = m05_couplers_to_psAxiInterconnect_AWID;
  assign M05_AXI_awlen[7:0] = m05_couplers_to_psAxiInterconnect_AWLEN;
  assign M05_AXI_awlock = m05_couplers_to_psAxiInterconnect_AWLOCK;
  assign M05_AXI_awprot[2:0] = m05_couplers_to_psAxiInterconnect_AWPROT;
  assign M05_AXI_awsize[2:0] = m05_couplers_to_psAxiInterconnect_AWSIZE;
  assign M05_AXI_awvalid = m05_couplers_to_psAxiInterconnect_AWVALID;
  assign M05_AXI_bready = m05_couplers_to_psAxiInterconnect_BREADY;
  assign M05_AXI_rready = m05_couplers_to_psAxiInterconnect_RREADY;
  assign M05_AXI_wdata[31:0] = m05_couplers_to_psAxiInterconnect_WDATA;
  assign M05_AXI_wlast = m05_couplers_to_psAxiInterconnect_WLAST;
  assign M05_AXI_wstrb[3:0] = m05_couplers_to_psAxiInterconnect_WSTRB;
  assign M05_AXI_wvalid = m05_couplers_to_psAxiInterconnect_WVALID;
  assign S00_ACLK_1 = S00_ACLK;
  assign S00_ARESETN_1 = S00_ARESETN;
  assign S00_AXI_arready = psAxiInterconnect_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = psAxiInterconnect_to_s00_couplers_AWREADY;
  assign S00_AXI_bid[11:0] = psAxiInterconnect_to_s00_couplers_BID;
  assign S00_AXI_bresp[1:0] = psAxiInterconnect_to_s00_couplers_BRESP;
  assign S00_AXI_bvalid = psAxiInterconnect_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = psAxiInterconnect_to_s00_couplers_RDATA;
  assign S00_AXI_rid[11:0] = psAxiInterconnect_to_s00_couplers_RID;
  assign S00_AXI_rlast = psAxiInterconnect_to_s00_couplers_RLAST;
  assign S00_AXI_rresp[1:0] = psAxiInterconnect_to_s00_couplers_RRESP;
  assign S00_AXI_rvalid = psAxiInterconnect_to_s00_couplers_RVALID;
  assign S00_AXI_wready = psAxiInterconnect_to_s00_couplers_WREADY;
  assign m00_couplers_to_psAxiInterconnect_ARREADY = M00_AXI_arready;
  assign m00_couplers_to_psAxiInterconnect_AWREADY = M00_AXI_awready;
  assign m00_couplers_to_psAxiInterconnect_BRESP = M00_AXI_bresp[1:0];
  assign m00_couplers_to_psAxiInterconnect_BVALID = M00_AXI_bvalid;
  assign m00_couplers_to_psAxiInterconnect_RDATA = M00_AXI_rdata[31:0];
  assign m00_couplers_to_psAxiInterconnect_RRESP = M00_AXI_rresp[1:0];
  assign m00_couplers_to_psAxiInterconnect_RVALID = M00_AXI_rvalid;
  assign m00_couplers_to_psAxiInterconnect_WREADY = M00_AXI_wready;
  assign m01_couplers_to_psAxiInterconnect_ARREADY = M01_AXI_arready;
  assign m01_couplers_to_psAxiInterconnect_AWREADY = M01_AXI_awready;
  assign m01_couplers_to_psAxiInterconnect_BRESP = M01_AXI_bresp[1:0];
  assign m01_couplers_to_psAxiInterconnect_BVALID = M01_AXI_bvalid;
  assign m01_couplers_to_psAxiInterconnect_RDATA = M01_AXI_rdata[31:0];
  assign m01_couplers_to_psAxiInterconnect_RRESP = M01_AXI_rresp[1:0];
  assign m01_couplers_to_psAxiInterconnect_RVALID = M01_AXI_rvalid;
  assign m01_couplers_to_psAxiInterconnect_WREADY = M01_AXI_wready;
  assign m02_couplers_to_psAxiInterconnect_ARREADY = M02_AXI_arready;
  assign m02_couplers_to_psAxiInterconnect_AWREADY = M02_AXI_awready;
  assign m02_couplers_to_psAxiInterconnect_BID = M02_AXI_bid[11:0];
  assign m02_couplers_to_psAxiInterconnect_BRESP = M02_AXI_bresp[1:0];
  assign m02_couplers_to_psAxiInterconnect_BVALID = M02_AXI_bvalid;
  assign m02_couplers_to_psAxiInterconnect_RDATA = M02_AXI_rdata[31:0];
  assign m02_couplers_to_psAxiInterconnect_RID = M02_AXI_rid[11:0];
  assign m02_couplers_to_psAxiInterconnect_RLAST = M02_AXI_rlast;
  assign m02_couplers_to_psAxiInterconnect_RRESP = M02_AXI_rresp[1:0];
  assign m02_couplers_to_psAxiInterconnect_RVALID = M02_AXI_rvalid;
  assign m02_couplers_to_psAxiInterconnect_WREADY = M02_AXI_wready;
  assign m03_couplers_to_psAxiInterconnect_ARREADY = M03_AXI_arready;
  assign m03_couplers_to_psAxiInterconnect_AWREADY = M03_AXI_awready;
  assign m03_couplers_to_psAxiInterconnect_BID = M03_AXI_bid[11:0];
  assign m03_couplers_to_psAxiInterconnect_BRESP = M03_AXI_bresp[1:0];
  assign m03_couplers_to_psAxiInterconnect_BVALID = M03_AXI_bvalid;
  assign m03_couplers_to_psAxiInterconnect_RDATA = M03_AXI_rdata[31:0];
  assign m03_couplers_to_psAxiInterconnect_RID = M03_AXI_rid[11:0];
  assign m03_couplers_to_psAxiInterconnect_RLAST = M03_AXI_rlast;
  assign m03_couplers_to_psAxiInterconnect_RRESP = M03_AXI_rresp[1:0];
  assign m03_couplers_to_psAxiInterconnect_RVALID = M03_AXI_rvalid;
  assign m03_couplers_to_psAxiInterconnect_WREADY = M03_AXI_wready;
  assign m04_couplers_to_psAxiInterconnect_ARREADY = M04_AXI_arready;
  assign m04_couplers_to_psAxiInterconnect_AWREADY = M04_AXI_awready;
  assign m04_couplers_to_psAxiInterconnect_BID = M04_AXI_bid[11:0];
  assign m04_couplers_to_psAxiInterconnect_BRESP = M04_AXI_bresp[1:0];
  assign m04_couplers_to_psAxiInterconnect_BVALID = M04_AXI_bvalid;
  assign m04_couplers_to_psAxiInterconnect_RDATA = M04_AXI_rdata[31:0];
  assign m04_couplers_to_psAxiInterconnect_RID = M04_AXI_rid[11:0];
  assign m04_couplers_to_psAxiInterconnect_RLAST = M04_AXI_rlast;
  assign m04_couplers_to_psAxiInterconnect_RRESP = M04_AXI_rresp[1:0];
  assign m04_couplers_to_psAxiInterconnect_RVALID = M04_AXI_rvalid;
  assign m04_couplers_to_psAxiInterconnect_WREADY = M04_AXI_wready;
  assign m05_couplers_to_psAxiInterconnect_ARREADY = M05_AXI_arready;
  assign m05_couplers_to_psAxiInterconnect_AWREADY = M05_AXI_awready;
  assign m05_couplers_to_psAxiInterconnect_BID = M05_AXI_bid[11:0];
  assign m05_couplers_to_psAxiInterconnect_BRESP = M05_AXI_bresp[1:0];
  assign m05_couplers_to_psAxiInterconnect_BVALID = M05_AXI_bvalid;
  assign m05_couplers_to_psAxiInterconnect_RDATA = M05_AXI_rdata[31:0];
  assign m05_couplers_to_psAxiInterconnect_RID = M05_AXI_rid[11:0];
  assign m05_couplers_to_psAxiInterconnect_RLAST = M05_AXI_rlast;
  assign m05_couplers_to_psAxiInterconnect_RRESP = M05_AXI_rresp[1:0];
  assign m05_couplers_to_psAxiInterconnect_RVALID = M05_AXI_rvalid;
  assign m05_couplers_to_psAxiInterconnect_WREADY = M05_AXI_wready;
  assign psAxiInterconnect_ACLK_net = ACLK;
  assign psAxiInterconnect_ARESETN_net = ARESETN;
  assign psAxiInterconnect_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign psAxiInterconnect_to_s00_couplers_ARBURST = S00_AXI_arburst[1:0];
  assign psAxiInterconnect_to_s00_couplers_ARCACHE = S00_AXI_arcache[3:0];
  assign psAxiInterconnect_to_s00_couplers_ARID = S00_AXI_arid[11:0];
  assign psAxiInterconnect_to_s00_couplers_ARLEN = S00_AXI_arlen[3:0];
  assign psAxiInterconnect_to_s00_couplers_ARLOCK = S00_AXI_arlock[1:0];
  assign psAxiInterconnect_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign psAxiInterconnect_to_s00_couplers_ARQOS = S00_AXI_arqos[3:0];
  assign psAxiInterconnect_to_s00_couplers_ARSIZE = S00_AXI_arsize[2:0];
  assign psAxiInterconnect_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign psAxiInterconnect_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign psAxiInterconnect_to_s00_couplers_AWBURST = S00_AXI_awburst[1:0];
  assign psAxiInterconnect_to_s00_couplers_AWCACHE = S00_AXI_awcache[3:0];
  assign psAxiInterconnect_to_s00_couplers_AWID = S00_AXI_awid[11:0];
  assign psAxiInterconnect_to_s00_couplers_AWLEN = S00_AXI_awlen[3:0];
  assign psAxiInterconnect_to_s00_couplers_AWLOCK = S00_AXI_awlock[1:0];
  assign psAxiInterconnect_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign psAxiInterconnect_to_s00_couplers_AWQOS = S00_AXI_awqos[3:0];
  assign psAxiInterconnect_to_s00_couplers_AWSIZE = S00_AXI_awsize[2:0];
  assign psAxiInterconnect_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign psAxiInterconnect_to_s00_couplers_BREADY = S00_AXI_bready;
  assign psAxiInterconnect_to_s00_couplers_RREADY = S00_AXI_rready;
  assign psAxiInterconnect_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign psAxiInterconnect_to_s00_couplers_WID = S00_AXI_wid[11:0];
  assign psAxiInterconnect_to_s00_couplers_WLAST = S00_AXI_wlast;
  assign psAxiInterconnect_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign psAxiInterconnect_to_s00_couplers_WVALID = S00_AXI_wvalid;
  m00_couplers_imp_1AHJTCP m00_couplers
       (.M_ACLK(M00_ACLK_1),
        .M_ARESETN(M00_ARESETN_1),
        .M_AXI_araddr(m00_couplers_to_psAxiInterconnect_ARADDR),
        .M_AXI_arready(m00_couplers_to_psAxiInterconnect_ARREADY),
        .M_AXI_arvalid(m00_couplers_to_psAxiInterconnect_ARVALID),
        .M_AXI_awaddr(m00_couplers_to_psAxiInterconnect_AWADDR),
        .M_AXI_awready(m00_couplers_to_psAxiInterconnect_AWREADY),
        .M_AXI_awvalid(m00_couplers_to_psAxiInterconnect_AWVALID),
        .M_AXI_bready(m00_couplers_to_psAxiInterconnect_BREADY),
        .M_AXI_bresp(m00_couplers_to_psAxiInterconnect_BRESP),
        .M_AXI_bvalid(m00_couplers_to_psAxiInterconnect_BVALID),
        .M_AXI_rdata(m00_couplers_to_psAxiInterconnect_RDATA),
        .M_AXI_rready(m00_couplers_to_psAxiInterconnect_RREADY),
        .M_AXI_rresp(m00_couplers_to_psAxiInterconnect_RRESP),
        .M_AXI_rvalid(m00_couplers_to_psAxiInterconnect_RVALID),
        .M_AXI_wdata(m00_couplers_to_psAxiInterconnect_WDATA),
        .M_AXI_wready(m00_couplers_to_psAxiInterconnect_WREADY),
        .M_AXI_wstrb(m00_couplers_to_psAxiInterconnect_WSTRB),
        .M_AXI_wvalid(m00_couplers_to_psAxiInterconnect_WVALID),
        .S_ACLK(psAxiInterconnect_ACLK_net),
        .S_ARESETN(psAxiInterconnect_ARESETN_net),
        .S_AXI_araddr(xbar_to_m00_couplers_ARADDR),
        .S_AXI_arburst(xbar_to_m00_couplers_ARBURST),
        .S_AXI_arcache(xbar_to_m00_couplers_ARCACHE),
        .S_AXI_arid(xbar_to_m00_couplers_ARID),
        .S_AXI_arlen(xbar_to_m00_couplers_ARLEN),
        .S_AXI_arlock(xbar_to_m00_couplers_ARLOCK),
        .S_AXI_arprot(xbar_to_m00_couplers_ARPROT),
        .S_AXI_arqos(xbar_to_m00_couplers_ARQOS),
        .S_AXI_arready(xbar_to_m00_couplers_ARREADY),
        .S_AXI_arregion(xbar_to_m00_couplers_ARREGION),
        .S_AXI_arsize(xbar_to_m00_couplers_ARSIZE),
        .S_AXI_arvalid(xbar_to_m00_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m00_couplers_AWADDR),
        .S_AXI_awburst(xbar_to_m00_couplers_AWBURST),
        .S_AXI_awcache(xbar_to_m00_couplers_AWCACHE),
        .S_AXI_awid(xbar_to_m00_couplers_AWID),
        .S_AXI_awlen(xbar_to_m00_couplers_AWLEN),
        .S_AXI_awlock(xbar_to_m00_couplers_AWLOCK),
        .S_AXI_awprot(xbar_to_m00_couplers_AWPROT),
        .S_AXI_awqos(xbar_to_m00_couplers_AWQOS),
        .S_AXI_awready(xbar_to_m00_couplers_AWREADY),
        .S_AXI_awregion(xbar_to_m00_couplers_AWREGION),
        .S_AXI_awsize(xbar_to_m00_couplers_AWSIZE),
        .S_AXI_awvalid(xbar_to_m00_couplers_AWVALID),
        .S_AXI_bid(xbar_to_m00_couplers_BID),
        .S_AXI_bready(xbar_to_m00_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m00_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m00_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m00_couplers_RDATA),
        .S_AXI_rid(xbar_to_m00_couplers_RID),
        .S_AXI_rlast(xbar_to_m00_couplers_RLAST),
        .S_AXI_rready(xbar_to_m00_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m00_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m00_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m00_couplers_WDATA),
        .S_AXI_wlast(xbar_to_m00_couplers_WLAST),
        .S_AXI_wready(xbar_to_m00_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m00_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m00_couplers_WVALID));
  m01_couplers_imp_K7OWQG m01_couplers
       (.M_ACLK(M01_ACLK_1),
        .M_ARESETN(M01_ARESETN_1),
        .M_AXI_araddr(m01_couplers_to_psAxiInterconnect_ARADDR),
        .M_AXI_arready(m01_couplers_to_psAxiInterconnect_ARREADY),
        .M_AXI_arvalid(m01_couplers_to_psAxiInterconnect_ARVALID),
        .M_AXI_awaddr(m01_couplers_to_psAxiInterconnect_AWADDR),
        .M_AXI_awready(m01_couplers_to_psAxiInterconnect_AWREADY),
        .M_AXI_awvalid(m01_couplers_to_psAxiInterconnect_AWVALID),
        .M_AXI_bready(m01_couplers_to_psAxiInterconnect_BREADY),
        .M_AXI_bresp(m01_couplers_to_psAxiInterconnect_BRESP),
        .M_AXI_bvalid(m01_couplers_to_psAxiInterconnect_BVALID),
        .M_AXI_rdata(m01_couplers_to_psAxiInterconnect_RDATA),
        .M_AXI_rready(m01_couplers_to_psAxiInterconnect_RREADY),
        .M_AXI_rresp(m01_couplers_to_psAxiInterconnect_RRESP),
        .M_AXI_rvalid(m01_couplers_to_psAxiInterconnect_RVALID),
        .M_AXI_wdata(m01_couplers_to_psAxiInterconnect_WDATA),
        .M_AXI_wready(m01_couplers_to_psAxiInterconnect_WREADY),
        .M_AXI_wstrb(m01_couplers_to_psAxiInterconnect_WSTRB),
        .M_AXI_wvalid(m01_couplers_to_psAxiInterconnect_WVALID),
        .S_ACLK(psAxiInterconnect_ACLK_net),
        .S_ARESETN(psAxiInterconnect_ARESETN_net),
        .S_AXI_araddr(xbar_to_m01_couplers_ARADDR),
        .S_AXI_arburst(xbar_to_m01_couplers_ARBURST),
        .S_AXI_arcache(xbar_to_m01_couplers_ARCACHE),
        .S_AXI_arid(xbar_to_m01_couplers_ARID),
        .S_AXI_arlen(xbar_to_m01_couplers_ARLEN),
        .S_AXI_arlock(xbar_to_m01_couplers_ARLOCK),
        .S_AXI_arprot(xbar_to_m01_couplers_ARPROT),
        .S_AXI_arqos(xbar_to_m01_couplers_ARQOS),
        .S_AXI_arready(xbar_to_m01_couplers_ARREADY),
        .S_AXI_arregion(xbar_to_m01_couplers_ARREGION),
        .S_AXI_arsize(xbar_to_m01_couplers_ARSIZE),
        .S_AXI_arvalid(xbar_to_m01_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m01_couplers_AWADDR),
        .S_AXI_awburst(xbar_to_m01_couplers_AWBURST),
        .S_AXI_awcache(xbar_to_m01_couplers_AWCACHE),
        .S_AXI_awid(xbar_to_m01_couplers_AWID),
        .S_AXI_awlen(xbar_to_m01_couplers_AWLEN),
        .S_AXI_awlock(xbar_to_m01_couplers_AWLOCK),
        .S_AXI_awprot(xbar_to_m01_couplers_AWPROT),
        .S_AXI_awqos(xbar_to_m01_couplers_AWQOS),
        .S_AXI_awready(xbar_to_m01_couplers_AWREADY),
        .S_AXI_awregion(xbar_to_m01_couplers_AWREGION),
        .S_AXI_awsize(xbar_to_m01_couplers_AWSIZE),
        .S_AXI_awvalid(xbar_to_m01_couplers_AWVALID),
        .S_AXI_bid(xbar_to_m01_couplers_BID),
        .S_AXI_bready(xbar_to_m01_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m01_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m01_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m01_couplers_RDATA),
        .S_AXI_rid(xbar_to_m01_couplers_RID),
        .S_AXI_rlast(xbar_to_m01_couplers_RLAST),
        .S_AXI_rready(xbar_to_m01_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m01_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m01_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m01_couplers_WDATA),
        .S_AXI_wlast(xbar_to_m01_couplers_WLAST),
        .S_AXI_wready(xbar_to_m01_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m01_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m01_couplers_WVALID));
  m02_couplers_imp_191VXEI m02_couplers
       (.M_ACLK(M02_ACLK_1),
        .M_ARESETN(M02_ARESETN_1),
        .M_AXI_araddr(m02_couplers_to_psAxiInterconnect_ARADDR),
        .M_AXI_arburst(m02_couplers_to_psAxiInterconnect_ARBURST),
        .M_AXI_arcache(m02_couplers_to_psAxiInterconnect_ARCACHE),
        .M_AXI_arid(m02_couplers_to_psAxiInterconnect_ARID),
        .M_AXI_arlen(m02_couplers_to_psAxiInterconnect_ARLEN),
        .M_AXI_arlock(m02_couplers_to_psAxiInterconnect_ARLOCK),
        .M_AXI_arprot(m02_couplers_to_psAxiInterconnect_ARPROT),
        .M_AXI_arready(m02_couplers_to_psAxiInterconnect_ARREADY),
        .M_AXI_arsize(m02_couplers_to_psAxiInterconnect_ARSIZE),
        .M_AXI_arvalid(m02_couplers_to_psAxiInterconnect_ARVALID),
        .M_AXI_awaddr(m02_couplers_to_psAxiInterconnect_AWADDR),
        .M_AXI_awburst(m02_couplers_to_psAxiInterconnect_AWBURST),
        .M_AXI_awcache(m02_couplers_to_psAxiInterconnect_AWCACHE),
        .M_AXI_awid(m02_couplers_to_psAxiInterconnect_AWID),
        .M_AXI_awlen(m02_couplers_to_psAxiInterconnect_AWLEN),
        .M_AXI_awlock(m02_couplers_to_psAxiInterconnect_AWLOCK),
        .M_AXI_awprot(m02_couplers_to_psAxiInterconnect_AWPROT),
        .M_AXI_awready(m02_couplers_to_psAxiInterconnect_AWREADY),
        .M_AXI_awsize(m02_couplers_to_psAxiInterconnect_AWSIZE),
        .M_AXI_awvalid(m02_couplers_to_psAxiInterconnect_AWVALID),
        .M_AXI_bid(m02_couplers_to_psAxiInterconnect_BID),
        .M_AXI_bready(m02_couplers_to_psAxiInterconnect_BREADY),
        .M_AXI_bresp(m02_couplers_to_psAxiInterconnect_BRESP),
        .M_AXI_bvalid(m02_couplers_to_psAxiInterconnect_BVALID),
        .M_AXI_rdata(m02_couplers_to_psAxiInterconnect_RDATA),
        .M_AXI_rid(m02_couplers_to_psAxiInterconnect_RID),
        .M_AXI_rlast(m02_couplers_to_psAxiInterconnect_RLAST),
        .M_AXI_rready(m02_couplers_to_psAxiInterconnect_RREADY),
        .M_AXI_rresp(m02_couplers_to_psAxiInterconnect_RRESP),
        .M_AXI_rvalid(m02_couplers_to_psAxiInterconnect_RVALID),
        .M_AXI_wdata(m02_couplers_to_psAxiInterconnect_WDATA),
        .M_AXI_wlast(m02_couplers_to_psAxiInterconnect_WLAST),
        .M_AXI_wready(m02_couplers_to_psAxiInterconnect_WREADY),
        .M_AXI_wstrb(m02_couplers_to_psAxiInterconnect_WSTRB),
        .M_AXI_wvalid(m02_couplers_to_psAxiInterconnect_WVALID),
        .S_ACLK(psAxiInterconnect_ACLK_net),
        .S_ARESETN(psAxiInterconnect_ARESETN_net),
        .S_AXI_araddr(xbar_to_m02_couplers_ARADDR),
        .S_AXI_arburst(xbar_to_m02_couplers_ARBURST),
        .S_AXI_arcache(xbar_to_m02_couplers_ARCACHE),
        .S_AXI_arid(xbar_to_m02_couplers_ARID),
        .S_AXI_arlen(xbar_to_m02_couplers_ARLEN),
        .S_AXI_arlock(xbar_to_m02_couplers_ARLOCK),
        .S_AXI_arprot(xbar_to_m02_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m02_couplers_ARREADY),
        .S_AXI_arsize(xbar_to_m02_couplers_ARSIZE),
        .S_AXI_arvalid(xbar_to_m02_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m02_couplers_AWADDR),
        .S_AXI_awburst(xbar_to_m02_couplers_AWBURST),
        .S_AXI_awcache(xbar_to_m02_couplers_AWCACHE),
        .S_AXI_awid(xbar_to_m02_couplers_AWID),
        .S_AXI_awlen(xbar_to_m02_couplers_AWLEN),
        .S_AXI_awlock(xbar_to_m02_couplers_AWLOCK),
        .S_AXI_awprot(xbar_to_m02_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m02_couplers_AWREADY),
        .S_AXI_awsize(xbar_to_m02_couplers_AWSIZE),
        .S_AXI_awvalid(xbar_to_m02_couplers_AWVALID),
        .S_AXI_bid(xbar_to_m02_couplers_BID),
        .S_AXI_bready(xbar_to_m02_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m02_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m02_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m02_couplers_RDATA),
        .S_AXI_rid(xbar_to_m02_couplers_RID),
        .S_AXI_rlast(xbar_to_m02_couplers_RLAST),
        .S_AXI_rready(xbar_to_m02_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m02_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m02_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m02_couplers_WDATA),
        .S_AXI_wlast(xbar_to_m02_couplers_WLAST),
        .S_AXI_wready(xbar_to_m02_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m02_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m02_couplers_WVALID));
  m03_couplers_imp_LDNU4B m03_couplers
       (.M_ACLK(M03_ACLK_1),
        .M_ARESETN(M03_ARESETN_1),
        .M_AXI_araddr(m03_couplers_to_psAxiInterconnect_ARADDR),
        .M_AXI_arburst(m03_couplers_to_psAxiInterconnect_ARBURST),
        .M_AXI_arcache(m03_couplers_to_psAxiInterconnect_ARCACHE),
        .M_AXI_arid(m03_couplers_to_psAxiInterconnect_ARID),
        .M_AXI_arlen(m03_couplers_to_psAxiInterconnect_ARLEN),
        .M_AXI_arlock(m03_couplers_to_psAxiInterconnect_ARLOCK),
        .M_AXI_arprot(m03_couplers_to_psAxiInterconnect_ARPROT),
        .M_AXI_arready(m03_couplers_to_psAxiInterconnect_ARREADY),
        .M_AXI_arsize(m03_couplers_to_psAxiInterconnect_ARSIZE),
        .M_AXI_arvalid(m03_couplers_to_psAxiInterconnect_ARVALID),
        .M_AXI_awaddr(m03_couplers_to_psAxiInterconnect_AWADDR),
        .M_AXI_awburst(m03_couplers_to_psAxiInterconnect_AWBURST),
        .M_AXI_awcache(m03_couplers_to_psAxiInterconnect_AWCACHE),
        .M_AXI_awid(m03_couplers_to_psAxiInterconnect_AWID),
        .M_AXI_awlen(m03_couplers_to_psAxiInterconnect_AWLEN),
        .M_AXI_awlock(m03_couplers_to_psAxiInterconnect_AWLOCK),
        .M_AXI_awprot(m03_couplers_to_psAxiInterconnect_AWPROT),
        .M_AXI_awready(m03_couplers_to_psAxiInterconnect_AWREADY),
        .M_AXI_awsize(m03_couplers_to_psAxiInterconnect_AWSIZE),
        .M_AXI_awvalid(m03_couplers_to_psAxiInterconnect_AWVALID),
        .M_AXI_bid(m03_couplers_to_psAxiInterconnect_BID),
        .M_AXI_bready(m03_couplers_to_psAxiInterconnect_BREADY),
        .M_AXI_bresp(m03_couplers_to_psAxiInterconnect_BRESP),
        .M_AXI_bvalid(m03_couplers_to_psAxiInterconnect_BVALID),
        .M_AXI_rdata(m03_couplers_to_psAxiInterconnect_RDATA),
        .M_AXI_rid(m03_couplers_to_psAxiInterconnect_RID),
        .M_AXI_rlast(m03_couplers_to_psAxiInterconnect_RLAST),
        .M_AXI_rready(m03_couplers_to_psAxiInterconnect_RREADY),
        .M_AXI_rresp(m03_couplers_to_psAxiInterconnect_RRESP),
        .M_AXI_rvalid(m03_couplers_to_psAxiInterconnect_RVALID),
        .M_AXI_wdata(m03_couplers_to_psAxiInterconnect_WDATA),
        .M_AXI_wlast(m03_couplers_to_psAxiInterconnect_WLAST),
        .M_AXI_wready(m03_couplers_to_psAxiInterconnect_WREADY),
        .M_AXI_wstrb(m03_couplers_to_psAxiInterconnect_WSTRB),
        .M_AXI_wvalid(m03_couplers_to_psAxiInterconnect_WVALID),
        .S_ACLK(psAxiInterconnect_ACLK_net),
        .S_ARESETN(psAxiInterconnect_ARESETN_net),
        .S_AXI_araddr(xbar_to_m03_couplers_ARADDR),
        .S_AXI_arburst(xbar_to_m03_couplers_ARBURST),
        .S_AXI_arcache(xbar_to_m03_couplers_ARCACHE),
        .S_AXI_arid(xbar_to_m03_couplers_ARID),
        .S_AXI_arlen(xbar_to_m03_couplers_ARLEN),
        .S_AXI_arlock(xbar_to_m03_couplers_ARLOCK),
        .S_AXI_arprot(xbar_to_m03_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m03_couplers_ARREADY),
        .S_AXI_arsize(xbar_to_m03_couplers_ARSIZE),
        .S_AXI_arvalid(xbar_to_m03_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m03_couplers_AWADDR),
        .S_AXI_awburst(xbar_to_m03_couplers_AWBURST),
        .S_AXI_awcache(xbar_to_m03_couplers_AWCACHE),
        .S_AXI_awid(xbar_to_m03_couplers_AWID),
        .S_AXI_awlen(xbar_to_m03_couplers_AWLEN),
        .S_AXI_awlock(xbar_to_m03_couplers_AWLOCK),
        .S_AXI_awprot(xbar_to_m03_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m03_couplers_AWREADY),
        .S_AXI_awsize(xbar_to_m03_couplers_AWSIZE),
        .S_AXI_awvalid(xbar_to_m03_couplers_AWVALID),
        .S_AXI_bid(xbar_to_m03_couplers_BID),
        .S_AXI_bready(xbar_to_m03_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m03_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m03_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m03_couplers_RDATA),
        .S_AXI_rid(xbar_to_m03_couplers_RID),
        .S_AXI_rlast(xbar_to_m03_couplers_RLAST),
        .S_AXI_rready(xbar_to_m03_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m03_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m03_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m03_couplers_WDATA),
        .S_AXI_wlast(xbar_to_m03_couplers_WLAST),
        .S_AXI_wready(xbar_to_m03_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m03_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m03_couplers_WVALID));
  m04_couplers_imp_1BRG4TB m04_couplers
       (.M_ACLK(M04_ACLK_1),
        .M_ARESETN(M04_ARESETN_1),
        .M_AXI_araddr(m04_couplers_to_psAxiInterconnect_ARADDR),
        .M_AXI_arburst(m04_couplers_to_psAxiInterconnect_ARBURST),
        .M_AXI_arcache(m04_couplers_to_psAxiInterconnect_ARCACHE),
        .M_AXI_arid(m04_couplers_to_psAxiInterconnect_ARID),
        .M_AXI_arlen(m04_couplers_to_psAxiInterconnect_ARLEN),
        .M_AXI_arlock(m04_couplers_to_psAxiInterconnect_ARLOCK),
        .M_AXI_arprot(m04_couplers_to_psAxiInterconnect_ARPROT),
        .M_AXI_arready(m04_couplers_to_psAxiInterconnect_ARREADY),
        .M_AXI_arsize(m04_couplers_to_psAxiInterconnect_ARSIZE),
        .M_AXI_arvalid(m04_couplers_to_psAxiInterconnect_ARVALID),
        .M_AXI_awaddr(m04_couplers_to_psAxiInterconnect_AWADDR),
        .M_AXI_awburst(m04_couplers_to_psAxiInterconnect_AWBURST),
        .M_AXI_awcache(m04_couplers_to_psAxiInterconnect_AWCACHE),
        .M_AXI_awid(m04_couplers_to_psAxiInterconnect_AWID),
        .M_AXI_awlen(m04_couplers_to_psAxiInterconnect_AWLEN),
        .M_AXI_awlock(m04_couplers_to_psAxiInterconnect_AWLOCK),
        .M_AXI_awprot(m04_couplers_to_psAxiInterconnect_AWPROT),
        .M_AXI_awready(m04_couplers_to_psAxiInterconnect_AWREADY),
        .M_AXI_awsize(m04_couplers_to_psAxiInterconnect_AWSIZE),
        .M_AXI_awvalid(m04_couplers_to_psAxiInterconnect_AWVALID),
        .M_AXI_bid(m04_couplers_to_psAxiInterconnect_BID),
        .M_AXI_bready(m04_couplers_to_psAxiInterconnect_BREADY),
        .M_AXI_bresp(m04_couplers_to_psAxiInterconnect_BRESP),
        .M_AXI_bvalid(m04_couplers_to_psAxiInterconnect_BVALID),
        .M_AXI_rdata(m04_couplers_to_psAxiInterconnect_RDATA),
        .M_AXI_rid(m04_couplers_to_psAxiInterconnect_RID),
        .M_AXI_rlast(m04_couplers_to_psAxiInterconnect_RLAST),
        .M_AXI_rready(m04_couplers_to_psAxiInterconnect_RREADY),
        .M_AXI_rresp(m04_couplers_to_psAxiInterconnect_RRESP),
        .M_AXI_rvalid(m04_couplers_to_psAxiInterconnect_RVALID),
        .M_AXI_wdata(m04_couplers_to_psAxiInterconnect_WDATA),
        .M_AXI_wlast(m04_couplers_to_psAxiInterconnect_WLAST),
        .M_AXI_wready(m04_couplers_to_psAxiInterconnect_WREADY),
        .M_AXI_wstrb(m04_couplers_to_psAxiInterconnect_WSTRB),
        .M_AXI_wvalid(m04_couplers_to_psAxiInterconnect_WVALID),
        .S_ACLK(psAxiInterconnect_ACLK_net),
        .S_ARESETN(psAxiInterconnect_ARESETN_net),
        .S_AXI_araddr(xbar_to_m04_couplers_ARADDR),
        .S_AXI_arburst(xbar_to_m04_couplers_ARBURST),
        .S_AXI_arcache(xbar_to_m04_couplers_ARCACHE),
        .S_AXI_arid(xbar_to_m04_couplers_ARID),
        .S_AXI_arlen(xbar_to_m04_couplers_ARLEN),
        .S_AXI_arlock(xbar_to_m04_couplers_ARLOCK),
        .S_AXI_arprot(xbar_to_m04_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m04_couplers_ARREADY),
        .S_AXI_arsize(xbar_to_m04_couplers_ARSIZE),
        .S_AXI_arvalid(xbar_to_m04_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m04_couplers_AWADDR),
        .S_AXI_awburst(xbar_to_m04_couplers_AWBURST),
        .S_AXI_awcache(xbar_to_m04_couplers_AWCACHE),
        .S_AXI_awid(xbar_to_m04_couplers_AWID),
        .S_AXI_awlen(xbar_to_m04_couplers_AWLEN),
        .S_AXI_awlock(xbar_to_m04_couplers_AWLOCK),
        .S_AXI_awprot(xbar_to_m04_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m04_couplers_AWREADY),
        .S_AXI_awsize(xbar_to_m04_couplers_AWSIZE),
        .S_AXI_awvalid(xbar_to_m04_couplers_AWVALID),
        .S_AXI_bid(xbar_to_m04_couplers_BID),
        .S_AXI_bready(xbar_to_m04_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m04_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m04_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m04_couplers_RDATA),
        .S_AXI_rid(xbar_to_m04_couplers_RID),
        .S_AXI_rlast(xbar_to_m04_couplers_RLAST),
        .S_AXI_rready(xbar_to_m04_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m04_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m04_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m04_couplers_WDATA),
        .S_AXI_wlast(xbar_to_m04_couplers_WLAST),
        .S_AXI_wready(xbar_to_m04_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m04_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m04_couplers_WVALID));
  m05_couplers_imp_IPNQ9Q m05_couplers
       (.M_ACLK(M05_ACLK_1),
        .M_ARESETN(M05_ARESETN_1),
        .M_AXI_araddr(m05_couplers_to_psAxiInterconnect_ARADDR),
        .M_AXI_arburst(m05_couplers_to_psAxiInterconnect_ARBURST),
        .M_AXI_arcache(m05_couplers_to_psAxiInterconnect_ARCACHE),
        .M_AXI_arid(m05_couplers_to_psAxiInterconnect_ARID),
        .M_AXI_arlen(m05_couplers_to_psAxiInterconnect_ARLEN),
        .M_AXI_arlock(m05_couplers_to_psAxiInterconnect_ARLOCK),
        .M_AXI_arprot(m05_couplers_to_psAxiInterconnect_ARPROT),
        .M_AXI_arready(m05_couplers_to_psAxiInterconnect_ARREADY),
        .M_AXI_arsize(m05_couplers_to_psAxiInterconnect_ARSIZE),
        .M_AXI_arvalid(m05_couplers_to_psAxiInterconnect_ARVALID),
        .M_AXI_awaddr(m05_couplers_to_psAxiInterconnect_AWADDR),
        .M_AXI_awburst(m05_couplers_to_psAxiInterconnect_AWBURST),
        .M_AXI_awcache(m05_couplers_to_psAxiInterconnect_AWCACHE),
        .M_AXI_awid(m05_couplers_to_psAxiInterconnect_AWID),
        .M_AXI_awlen(m05_couplers_to_psAxiInterconnect_AWLEN),
        .M_AXI_awlock(m05_couplers_to_psAxiInterconnect_AWLOCK),
        .M_AXI_awprot(m05_couplers_to_psAxiInterconnect_AWPROT),
        .M_AXI_awready(m05_couplers_to_psAxiInterconnect_AWREADY),
        .M_AXI_awsize(m05_couplers_to_psAxiInterconnect_AWSIZE),
        .M_AXI_awvalid(m05_couplers_to_psAxiInterconnect_AWVALID),
        .M_AXI_bid(m05_couplers_to_psAxiInterconnect_BID),
        .M_AXI_bready(m05_couplers_to_psAxiInterconnect_BREADY),
        .M_AXI_bresp(m05_couplers_to_psAxiInterconnect_BRESP),
        .M_AXI_bvalid(m05_couplers_to_psAxiInterconnect_BVALID),
        .M_AXI_rdata(m05_couplers_to_psAxiInterconnect_RDATA),
        .M_AXI_rid(m05_couplers_to_psAxiInterconnect_RID),
        .M_AXI_rlast(m05_couplers_to_psAxiInterconnect_RLAST),
        .M_AXI_rready(m05_couplers_to_psAxiInterconnect_RREADY),
        .M_AXI_rresp(m05_couplers_to_psAxiInterconnect_RRESP),
        .M_AXI_rvalid(m05_couplers_to_psAxiInterconnect_RVALID),
        .M_AXI_wdata(m05_couplers_to_psAxiInterconnect_WDATA),
        .M_AXI_wlast(m05_couplers_to_psAxiInterconnect_WLAST),
        .M_AXI_wready(m05_couplers_to_psAxiInterconnect_WREADY),
        .M_AXI_wstrb(m05_couplers_to_psAxiInterconnect_WSTRB),
        .M_AXI_wvalid(m05_couplers_to_psAxiInterconnect_WVALID),
        .S_ACLK(psAxiInterconnect_ACLK_net),
        .S_ARESETN(psAxiInterconnect_ARESETN_net),
        .S_AXI_araddr(xbar_to_m05_couplers_ARADDR),
        .S_AXI_arburst(xbar_to_m05_couplers_ARBURST),
        .S_AXI_arcache(xbar_to_m05_couplers_ARCACHE),
        .S_AXI_arid(xbar_to_m05_couplers_ARID),
        .S_AXI_arlen(xbar_to_m05_couplers_ARLEN),
        .S_AXI_arlock(xbar_to_m05_couplers_ARLOCK),
        .S_AXI_arprot(xbar_to_m05_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m05_couplers_ARREADY),
        .S_AXI_arsize(xbar_to_m05_couplers_ARSIZE),
        .S_AXI_arvalid(xbar_to_m05_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m05_couplers_AWADDR),
        .S_AXI_awburst(xbar_to_m05_couplers_AWBURST),
        .S_AXI_awcache(xbar_to_m05_couplers_AWCACHE),
        .S_AXI_awid(xbar_to_m05_couplers_AWID),
        .S_AXI_awlen(xbar_to_m05_couplers_AWLEN),
        .S_AXI_awlock(xbar_to_m05_couplers_AWLOCK),
        .S_AXI_awprot(xbar_to_m05_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m05_couplers_AWREADY),
        .S_AXI_awsize(xbar_to_m05_couplers_AWSIZE),
        .S_AXI_awvalid(xbar_to_m05_couplers_AWVALID),
        .S_AXI_bid(xbar_to_m05_couplers_BID),
        .S_AXI_bready(xbar_to_m05_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m05_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m05_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m05_couplers_RDATA),
        .S_AXI_rid(xbar_to_m05_couplers_RID),
        .S_AXI_rlast(xbar_to_m05_couplers_RLAST),
        .S_AXI_rready(xbar_to_m05_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m05_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m05_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m05_couplers_WDATA),
        .S_AXI_wlast(xbar_to_m05_couplers_WLAST),
        .S_AXI_wready(xbar_to_m05_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m05_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m05_couplers_WVALID));
  s00_couplers_imp_QA0Z6J s00_couplers
       (.M_ACLK(psAxiInterconnect_ACLK_net),
        .M_ARESETN(psAxiInterconnect_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_xbar_ARADDR),
        .M_AXI_arburst(s00_couplers_to_xbar_ARBURST),
        .M_AXI_arcache(s00_couplers_to_xbar_ARCACHE),
        .M_AXI_arid(s00_couplers_to_xbar_ARID),
        .M_AXI_arlen(s00_couplers_to_xbar_ARLEN),
        .M_AXI_arlock(s00_couplers_to_xbar_ARLOCK),
        .M_AXI_arprot(s00_couplers_to_xbar_ARPROT),
        .M_AXI_arqos(s00_couplers_to_xbar_ARQOS),
        .M_AXI_arready(s00_couplers_to_xbar_ARREADY),
        .M_AXI_arsize(s00_couplers_to_xbar_ARSIZE),
        .M_AXI_arvalid(s00_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_xbar_AWADDR),
        .M_AXI_awburst(s00_couplers_to_xbar_AWBURST),
        .M_AXI_awcache(s00_couplers_to_xbar_AWCACHE),
        .M_AXI_awid(s00_couplers_to_xbar_AWID),
        .M_AXI_awlen(s00_couplers_to_xbar_AWLEN),
        .M_AXI_awlock(s00_couplers_to_xbar_AWLOCK),
        .M_AXI_awprot(s00_couplers_to_xbar_AWPROT),
        .M_AXI_awqos(s00_couplers_to_xbar_AWQOS),
        .M_AXI_awready(s00_couplers_to_xbar_AWREADY),
        .M_AXI_awsize(s00_couplers_to_xbar_AWSIZE),
        .M_AXI_awvalid(s00_couplers_to_xbar_AWVALID),
        .M_AXI_bid(s00_couplers_to_xbar_BID),
        .M_AXI_bready(s00_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s00_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s00_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s00_couplers_to_xbar_RDATA),
        .M_AXI_rid(s00_couplers_to_xbar_RID),
        .M_AXI_rlast(s00_couplers_to_xbar_RLAST),
        .M_AXI_rready(s00_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s00_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s00_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s00_couplers_to_xbar_WDATA),
        .M_AXI_wlast(s00_couplers_to_xbar_WLAST),
        .M_AXI_wready(s00_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s00_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_xbar_WVALID),
        .S_ACLK(S00_ACLK_1),
        .S_ARESETN(S00_ARESETN_1),
        .S_AXI_araddr(psAxiInterconnect_to_s00_couplers_ARADDR),
        .S_AXI_arburst(psAxiInterconnect_to_s00_couplers_ARBURST),
        .S_AXI_arcache(psAxiInterconnect_to_s00_couplers_ARCACHE),
        .S_AXI_arid(psAxiInterconnect_to_s00_couplers_ARID),
        .S_AXI_arlen(psAxiInterconnect_to_s00_couplers_ARLEN),
        .S_AXI_arlock(psAxiInterconnect_to_s00_couplers_ARLOCK),
        .S_AXI_arprot(psAxiInterconnect_to_s00_couplers_ARPROT),
        .S_AXI_arqos(psAxiInterconnect_to_s00_couplers_ARQOS),
        .S_AXI_arready(psAxiInterconnect_to_s00_couplers_ARREADY),
        .S_AXI_arsize(psAxiInterconnect_to_s00_couplers_ARSIZE),
        .S_AXI_arvalid(psAxiInterconnect_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(psAxiInterconnect_to_s00_couplers_AWADDR),
        .S_AXI_awburst(psAxiInterconnect_to_s00_couplers_AWBURST),
        .S_AXI_awcache(psAxiInterconnect_to_s00_couplers_AWCACHE),
        .S_AXI_awid(psAxiInterconnect_to_s00_couplers_AWID),
        .S_AXI_awlen(psAxiInterconnect_to_s00_couplers_AWLEN),
        .S_AXI_awlock(psAxiInterconnect_to_s00_couplers_AWLOCK),
        .S_AXI_awprot(psAxiInterconnect_to_s00_couplers_AWPROT),
        .S_AXI_awqos(psAxiInterconnect_to_s00_couplers_AWQOS),
        .S_AXI_awready(psAxiInterconnect_to_s00_couplers_AWREADY),
        .S_AXI_awsize(psAxiInterconnect_to_s00_couplers_AWSIZE),
        .S_AXI_awvalid(psAxiInterconnect_to_s00_couplers_AWVALID),
        .S_AXI_bid(psAxiInterconnect_to_s00_couplers_BID),
        .S_AXI_bready(psAxiInterconnect_to_s00_couplers_BREADY),
        .S_AXI_bresp(psAxiInterconnect_to_s00_couplers_BRESP),
        .S_AXI_bvalid(psAxiInterconnect_to_s00_couplers_BVALID),
        .S_AXI_rdata(psAxiInterconnect_to_s00_couplers_RDATA),
        .S_AXI_rid(psAxiInterconnect_to_s00_couplers_RID),
        .S_AXI_rlast(psAxiInterconnect_to_s00_couplers_RLAST),
        .S_AXI_rready(psAxiInterconnect_to_s00_couplers_RREADY),
        .S_AXI_rresp(psAxiInterconnect_to_s00_couplers_RRESP),
        .S_AXI_rvalid(psAxiInterconnect_to_s00_couplers_RVALID),
        .S_AXI_wdata(psAxiInterconnect_to_s00_couplers_WDATA),
        .S_AXI_wid(psAxiInterconnect_to_s00_couplers_WID),
        .S_AXI_wlast(psAxiInterconnect_to_s00_couplers_WLAST),
        .S_AXI_wready(psAxiInterconnect_to_s00_couplers_WREADY),
        .S_AXI_wstrb(psAxiInterconnect_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(psAxiInterconnect_to_s00_couplers_WVALID));
  cerberus_xbar_0 xbar
       (.aclk(psAxiInterconnect_ACLK_net),
        .aresetn(psAxiInterconnect_ARESETN_net),
        .m_axi_araddr({xbar_to_m05_couplers_ARADDR,xbar_to_m04_couplers_ARADDR,xbar_to_m03_couplers_ARADDR,xbar_to_m02_couplers_ARADDR,xbar_to_m01_couplers_ARADDR,xbar_to_m00_couplers_ARADDR}),
        .m_axi_arburst({xbar_to_m05_couplers_ARBURST,xbar_to_m04_couplers_ARBURST,xbar_to_m03_couplers_ARBURST,xbar_to_m02_couplers_ARBURST,xbar_to_m01_couplers_ARBURST,xbar_to_m00_couplers_ARBURST}),
        .m_axi_arcache({xbar_to_m05_couplers_ARCACHE,xbar_to_m04_couplers_ARCACHE,xbar_to_m03_couplers_ARCACHE,xbar_to_m02_couplers_ARCACHE,xbar_to_m01_couplers_ARCACHE,xbar_to_m00_couplers_ARCACHE}),
        .m_axi_arid({xbar_to_m05_couplers_ARID,xbar_to_m04_couplers_ARID,xbar_to_m03_couplers_ARID,xbar_to_m02_couplers_ARID,xbar_to_m01_couplers_ARID,xbar_to_m00_couplers_ARID}),
        .m_axi_arlen({xbar_to_m05_couplers_ARLEN,xbar_to_m04_couplers_ARLEN,xbar_to_m03_couplers_ARLEN,xbar_to_m02_couplers_ARLEN,xbar_to_m01_couplers_ARLEN,xbar_to_m00_couplers_ARLEN}),
        .m_axi_arlock({xbar_to_m05_couplers_ARLOCK,xbar_to_m04_couplers_ARLOCK,xbar_to_m03_couplers_ARLOCK,xbar_to_m02_couplers_ARLOCK,xbar_to_m01_couplers_ARLOCK,xbar_to_m00_couplers_ARLOCK}),
        .m_axi_arprot({xbar_to_m05_couplers_ARPROT,xbar_to_m04_couplers_ARPROT,xbar_to_m03_couplers_ARPROT,xbar_to_m02_couplers_ARPROT,xbar_to_m01_couplers_ARPROT,xbar_to_m00_couplers_ARPROT}),
        .m_axi_arqos({xbar_to_m01_couplers_ARQOS,xbar_to_m00_couplers_ARQOS}),
        .m_axi_arready({xbar_to_m05_couplers_ARREADY,xbar_to_m04_couplers_ARREADY,xbar_to_m03_couplers_ARREADY,xbar_to_m02_couplers_ARREADY,xbar_to_m01_couplers_ARREADY,xbar_to_m00_couplers_ARREADY}),
        .m_axi_arregion({xbar_to_m01_couplers_ARREGION,xbar_to_m00_couplers_ARREGION}),
        .m_axi_arsize({xbar_to_m05_couplers_ARSIZE,xbar_to_m04_couplers_ARSIZE,xbar_to_m03_couplers_ARSIZE,xbar_to_m02_couplers_ARSIZE,xbar_to_m01_couplers_ARSIZE,xbar_to_m00_couplers_ARSIZE}),
        .m_axi_arvalid({xbar_to_m05_couplers_ARVALID,xbar_to_m04_couplers_ARVALID,xbar_to_m03_couplers_ARVALID,xbar_to_m02_couplers_ARVALID,xbar_to_m01_couplers_ARVALID,xbar_to_m00_couplers_ARVALID}),
        .m_axi_awaddr({xbar_to_m05_couplers_AWADDR,xbar_to_m04_couplers_AWADDR,xbar_to_m03_couplers_AWADDR,xbar_to_m02_couplers_AWADDR,xbar_to_m01_couplers_AWADDR,xbar_to_m00_couplers_AWADDR}),
        .m_axi_awburst({xbar_to_m05_couplers_AWBURST,xbar_to_m04_couplers_AWBURST,xbar_to_m03_couplers_AWBURST,xbar_to_m02_couplers_AWBURST,xbar_to_m01_couplers_AWBURST,xbar_to_m00_couplers_AWBURST}),
        .m_axi_awcache({xbar_to_m05_couplers_AWCACHE,xbar_to_m04_couplers_AWCACHE,xbar_to_m03_couplers_AWCACHE,xbar_to_m02_couplers_AWCACHE,xbar_to_m01_couplers_AWCACHE,xbar_to_m00_couplers_AWCACHE}),
        .m_axi_awid({xbar_to_m05_couplers_AWID,xbar_to_m04_couplers_AWID,xbar_to_m03_couplers_AWID,xbar_to_m02_couplers_AWID,xbar_to_m01_couplers_AWID,xbar_to_m00_couplers_AWID}),
        .m_axi_awlen({xbar_to_m05_couplers_AWLEN,xbar_to_m04_couplers_AWLEN,xbar_to_m03_couplers_AWLEN,xbar_to_m02_couplers_AWLEN,xbar_to_m01_couplers_AWLEN,xbar_to_m00_couplers_AWLEN}),
        .m_axi_awlock({xbar_to_m05_couplers_AWLOCK,xbar_to_m04_couplers_AWLOCK,xbar_to_m03_couplers_AWLOCK,xbar_to_m02_couplers_AWLOCK,xbar_to_m01_couplers_AWLOCK,xbar_to_m00_couplers_AWLOCK}),
        .m_axi_awprot({xbar_to_m05_couplers_AWPROT,xbar_to_m04_couplers_AWPROT,xbar_to_m03_couplers_AWPROT,xbar_to_m02_couplers_AWPROT,xbar_to_m01_couplers_AWPROT,xbar_to_m00_couplers_AWPROT}),
        .m_axi_awqos({xbar_to_m01_couplers_AWQOS,xbar_to_m00_couplers_AWQOS}),
        .m_axi_awready({xbar_to_m05_couplers_AWREADY,xbar_to_m04_couplers_AWREADY,xbar_to_m03_couplers_AWREADY,xbar_to_m02_couplers_AWREADY,xbar_to_m01_couplers_AWREADY,xbar_to_m00_couplers_AWREADY}),
        .m_axi_awregion({xbar_to_m01_couplers_AWREGION,xbar_to_m00_couplers_AWREGION}),
        .m_axi_awsize({xbar_to_m05_couplers_AWSIZE,xbar_to_m04_couplers_AWSIZE,xbar_to_m03_couplers_AWSIZE,xbar_to_m02_couplers_AWSIZE,xbar_to_m01_couplers_AWSIZE,xbar_to_m00_couplers_AWSIZE}),
        .m_axi_awvalid({xbar_to_m05_couplers_AWVALID,xbar_to_m04_couplers_AWVALID,xbar_to_m03_couplers_AWVALID,xbar_to_m02_couplers_AWVALID,xbar_to_m01_couplers_AWVALID,xbar_to_m00_couplers_AWVALID}),
        .m_axi_bid({xbar_to_m05_couplers_BID,xbar_to_m04_couplers_BID,xbar_to_m03_couplers_BID,xbar_to_m02_couplers_BID,xbar_to_m01_couplers_BID,xbar_to_m00_couplers_BID}),
        .m_axi_bready({xbar_to_m05_couplers_BREADY,xbar_to_m04_couplers_BREADY,xbar_to_m03_couplers_BREADY,xbar_to_m02_couplers_BREADY,xbar_to_m01_couplers_BREADY,xbar_to_m00_couplers_BREADY}),
        .m_axi_bresp({xbar_to_m05_couplers_BRESP,xbar_to_m04_couplers_BRESP,xbar_to_m03_couplers_BRESP,xbar_to_m02_couplers_BRESP,xbar_to_m01_couplers_BRESP,xbar_to_m00_couplers_BRESP}),
        .m_axi_bvalid({xbar_to_m05_couplers_BVALID,xbar_to_m04_couplers_BVALID,xbar_to_m03_couplers_BVALID,xbar_to_m02_couplers_BVALID,xbar_to_m01_couplers_BVALID,xbar_to_m00_couplers_BVALID}),
        .m_axi_rdata({xbar_to_m05_couplers_RDATA,xbar_to_m04_couplers_RDATA,xbar_to_m03_couplers_RDATA,xbar_to_m02_couplers_RDATA,xbar_to_m01_couplers_RDATA,xbar_to_m00_couplers_RDATA}),
        .m_axi_rid({xbar_to_m05_couplers_RID,xbar_to_m04_couplers_RID,xbar_to_m03_couplers_RID,xbar_to_m02_couplers_RID,xbar_to_m01_couplers_RID,xbar_to_m00_couplers_RID}),
        .m_axi_rlast({xbar_to_m05_couplers_RLAST,xbar_to_m04_couplers_RLAST,xbar_to_m03_couplers_RLAST,xbar_to_m02_couplers_RLAST,xbar_to_m01_couplers_RLAST,xbar_to_m00_couplers_RLAST}),
        .m_axi_rready({xbar_to_m05_couplers_RREADY,xbar_to_m04_couplers_RREADY,xbar_to_m03_couplers_RREADY,xbar_to_m02_couplers_RREADY,xbar_to_m01_couplers_RREADY,xbar_to_m00_couplers_RREADY}),
        .m_axi_rresp({xbar_to_m05_couplers_RRESP,xbar_to_m04_couplers_RRESP,xbar_to_m03_couplers_RRESP,xbar_to_m02_couplers_RRESP,xbar_to_m01_couplers_RRESP,xbar_to_m00_couplers_RRESP}),
        .m_axi_rvalid({xbar_to_m05_couplers_RVALID,xbar_to_m04_couplers_RVALID,xbar_to_m03_couplers_RVALID,xbar_to_m02_couplers_RVALID,xbar_to_m01_couplers_RVALID,xbar_to_m00_couplers_RVALID}),
        .m_axi_wdata({xbar_to_m05_couplers_WDATA,xbar_to_m04_couplers_WDATA,xbar_to_m03_couplers_WDATA,xbar_to_m02_couplers_WDATA,xbar_to_m01_couplers_WDATA,xbar_to_m00_couplers_WDATA}),
        .m_axi_wlast({xbar_to_m05_couplers_WLAST,xbar_to_m04_couplers_WLAST,xbar_to_m03_couplers_WLAST,xbar_to_m02_couplers_WLAST,xbar_to_m01_couplers_WLAST,xbar_to_m00_couplers_WLAST}),
        .m_axi_wready({xbar_to_m05_couplers_WREADY,xbar_to_m04_couplers_WREADY,xbar_to_m03_couplers_WREADY,xbar_to_m02_couplers_WREADY,xbar_to_m01_couplers_WREADY,xbar_to_m00_couplers_WREADY}),
        .m_axi_wstrb({xbar_to_m05_couplers_WSTRB,xbar_to_m04_couplers_WSTRB,xbar_to_m03_couplers_WSTRB,xbar_to_m02_couplers_WSTRB,xbar_to_m01_couplers_WSTRB,xbar_to_m00_couplers_WSTRB}),
        .m_axi_wvalid({xbar_to_m05_couplers_WVALID,xbar_to_m04_couplers_WVALID,xbar_to_m03_couplers_WVALID,xbar_to_m02_couplers_WVALID,xbar_to_m01_couplers_WVALID,xbar_to_m00_couplers_WVALID}),
        .s_axi_araddr(s00_couplers_to_xbar_ARADDR),
        .s_axi_arburst(s00_couplers_to_xbar_ARBURST),
        .s_axi_arcache(s00_couplers_to_xbar_ARCACHE),
        .s_axi_arid(s00_couplers_to_xbar_ARID),
        .s_axi_arlen(s00_couplers_to_xbar_ARLEN),
        .s_axi_arlock(s00_couplers_to_xbar_ARLOCK),
        .s_axi_arprot(s00_couplers_to_xbar_ARPROT),
        .s_axi_arqos(s00_couplers_to_xbar_ARQOS),
        .s_axi_arready(s00_couplers_to_xbar_ARREADY),
        .s_axi_arsize(s00_couplers_to_xbar_ARSIZE),
        .s_axi_arvalid(s00_couplers_to_xbar_ARVALID),
        .s_axi_awaddr(s00_couplers_to_xbar_AWADDR),
        .s_axi_awburst(s00_couplers_to_xbar_AWBURST),
        .s_axi_awcache(s00_couplers_to_xbar_AWCACHE),
        .s_axi_awid(s00_couplers_to_xbar_AWID),
        .s_axi_awlen(s00_couplers_to_xbar_AWLEN),
        .s_axi_awlock(s00_couplers_to_xbar_AWLOCK),
        .s_axi_awprot(s00_couplers_to_xbar_AWPROT),
        .s_axi_awqos(s00_couplers_to_xbar_AWQOS),
        .s_axi_awready(s00_couplers_to_xbar_AWREADY),
        .s_axi_awsize(s00_couplers_to_xbar_AWSIZE),
        .s_axi_awvalid(s00_couplers_to_xbar_AWVALID),
        .s_axi_bid(s00_couplers_to_xbar_BID),
        .s_axi_bready(s00_couplers_to_xbar_BREADY),
        .s_axi_bresp(s00_couplers_to_xbar_BRESP),
        .s_axi_bvalid(s00_couplers_to_xbar_BVALID),
        .s_axi_rdata(s00_couplers_to_xbar_RDATA),
        .s_axi_rid(s00_couplers_to_xbar_RID),
        .s_axi_rlast(s00_couplers_to_xbar_RLAST),
        .s_axi_rready(s00_couplers_to_xbar_RREADY),
        .s_axi_rresp(s00_couplers_to_xbar_RRESP),
        .s_axi_rvalid(s00_couplers_to_xbar_RVALID),
        .s_axi_wdata(s00_couplers_to_xbar_WDATA),
        .s_axi_wlast(s00_couplers_to_xbar_WLAST),
        .s_axi_wready(s00_couplers_to_xbar_WREADY),
        .s_axi_wstrb(s00_couplers_to_xbar_WSTRB),
        .s_axi_wvalid(s00_couplers_to_xbar_WVALID));
endmodule

module cerberus_psDramAxiInterconnect_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arburst,
    M00_AXI_arcache,
    M00_AXI_arlen,
    M00_AXI_arlock,
    M00_AXI_arprot,
    M00_AXI_arqos,
    M00_AXI_arready,
    M00_AXI_arsize,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awburst,
    M00_AXI_awcache,
    M00_AXI_awlen,
    M00_AXI_awlock,
    M00_AXI_awprot,
    M00_AXI_awqos,
    M00_AXI_awready,
    M00_AXI_awsize,
    M00_AXI_awvalid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rlast,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wlast,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arprot,
    S00_AXI_arready,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awprot,
    S00_AXI_awready,
    S00_AXI_awvalid,
    S00_AXI_bready,
    S00_AXI_bresp,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rready,
    S00_AXI_rresp,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid,
    S01_ACLK,
    S01_ARESETN,
    S01_AXI_araddr,
    S01_AXI_arprot,
    S01_AXI_arready,
    S01_AXI_arvalid,
    S01_AXI_awaddr,
    S01_AXI_awprot,
    S01_AXI_awready,
    S01_AXI_awvalid,
    S01_AXI_bready,
    S01_AXI_bresp,
    S01_AXI_bvalid,
    S01_AXI_rdata,
    S01_AXI_rready,
    S01_AXI_rresp,
    S01_AXI_rvalid,
    S01_AXI_wdata,
    S01_AXI_wready,
    S01_AXI_wstrb,
    S01_AXI_wvalid,
    S02_ACLK,
    S02_ARESETN,
    S02_AXI_araddr,
    S02_AXI_arprot,
    S02_AXI_arready,
    S02_AXI_arvalid,
    S02_AXI_awaddr,
    S02_AXI_awprot,
    S02_AXI_awready,
    S02_AXI_awvalid,
    S02_AXI_bready,
    S02_AXI_bresp,
    S02_AXI_bvalid,
    S02_AXI_rdata,
    S02_AXI_rready,
    S02_AXI_rresp,
    S02_AXI_rvalid,
    S02_AXI_wdata,
    S02_AXI_wready,
    S02_AXI_wstrb,
    S02_AXI_wvalid,
    S03_ACLK,
    S03_ARESETN,
    S03_AXI_araddr,
    S03_AXI_arprot,
    S03_AXI_arready,
    S03_AXI_arvalid,
    S03_AXI_awaddr,
    S03_AXI_awprot,
    S03_AXI_awready,
    S03_AXI_awvalid,
    S03_AXI_bready,
    S03_AXI_bresp,
    S03_AXI_bvalid,
    S03_AXI_rdata,
    S03_AXI_rready,
    S03_AXI_rresp,
    S03_AXI_rvalid,
    S03_AXI_wdata,
    S03_AXI_wready,
    S03_AXI_wstrb,
    S03_AXI_wvalid);
  input ACLK;
  input ARESETN;
  input M00_ACLK;
  input M00_ARESETN;
  output [31:0]M00_AXI_araddr;
  output [1:0]M00_AXI_arburst;
  output [3:0]M00_AXI_arcache;
  output [3:0]M00_AXI_arlen;
  output [1:0]M00_AXI_arlock;
  output [2:0]M00_AXI_arprot;
  output [3:0]M00_AXI_arqos;
  input M00_AXI_arready;
  output [2:0]M00_AXI_arsize;
  output M00_AXI_arvalid;
  output [31:0]M00_AXI_awaddr;
  output [1:0]M00_AXI_awburst;
  output [3:0]M00_AXI_awcache;
  output [3:0]M00_AXI_awlen;
  output [1:0]M00_AXI_awlock;
  output [2:0]M00_AXI_awprot;
  output [3:0]M00_AXI_awqos;
  input M00_AXI_awready;
  output [2:0]M00_AXI_awsize;
  output M00_AXI_awvalid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [63:0]M00_AXI_rdata;
  input M00_AXI_rlast;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [63:0]M00_AXI_wdata;
  output M00_AXI_wlast;
  input M00_AXI_wready;
  output [7:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  input S00_ACLK;
  input S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [2:0]S00_AXI_arprot;
  output [0:0]S00_AXI_arready;
  input [0:0]S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [2:0]S00_AXI_awprot;
  output [0:0]S00_AXI_awready;
  input [0:0]S00_AXI_awvalid;
  input [0:0]S00_AXI_bready;
  output [1:0]S00_AXI_bresp;
  output [0:0]S00_AXI_bvalid;
  output [63:0]S00_AXI_rdata;
  input [0:0]S00_AXI_rready;
  output [1:0]S00_AXI_rresp;
  output [0:0]S00_AXI_rvalid;
  input [63:0]S00_AXI_wdata;
  output [0:0]S00_AXI_wready;
  input [7:0]S00_AXI_wstrb;
  input [0:0]S00_AXI_wvalid;
  input S01_ACLK;
  input S01_ARESETN;
  input [31:0]S01_AXI_araddr;
  input [2:0]S01_AXI_arprot;
  output [0:0]S01_AXI_arready;
  input [0:0]S01_AXI_arvalid;
  input [31:0]S01_AXI_awaddr;
  input [2:0]S01_AXI_awprot;
  output [0:0]S01_AXI_awready;
  input [0:0]S01_AXI_awvalid;
  input [0:0]S01_AXI_bready;
  output [1:0]S01_AXI_bresp;
  output [0:0]S01_AXI_bvalid;
  output [63:0]S01_AXI_rdata;
  input [0:0]S01_AXI_rready;
  output [1:0]S01_AXI_rresp;
  output [0:0]S01_AXI_rvalid;
  input [63:0]S01_AXI_wdata;
  output [0:0]S01_AXI_wready;
  input [7:0]S01_AXI_wstrb;
  input [0:0]S01_AXI_wvalid;
  input S02_ACLK;
  input S02_ARESETN;
  input [31:0]S02_AXI_araddr;
  input [2:0]S02_AXI_arprot;
  output [0:0]S02_AXI_arready;
  input [0:0]S02_AXI_arvalid;
  input [31:0]S02_AXI_awaddr;
  input [2:0]S02_AXI_awprot;
  output [0:0]S02_AXI_awready;
  input [0:0]S02_AXI_awvalid;
  input [0:0]S02_AXI_bready;
  output [1:0]S02_AXI_bresp;
  output [0:0]S02_AXI_bvalid;
  output [63:0]S02_AXI_rdata;
  input [0:0]S02_AXI_rready;
  output [1:0]S02_AXI_rresp;
  output [0:0]S02_AXI_rvalid;
  input [63:0]S02_AXI_wdata;
  output [0:0]S02_AXI_wready;
  input [7:0]S02_AXI_wstrb;
  input [0:0]S02_AXI_wvalid;
  input S03_ACLK;
  input S03_ARESETN;
  input [31:0]S03_AXI_araddr;
  input [2:0]S03_AXI_arprot;
  output [0:0]S03_AXI_arready;
  input [0:0]S03_AXI_arvalid;
  input [31:0]S03_AXI_awaddr;
  input [2:0]S03_AXI_awprot;
  output [0:0]S03_AXI_awready;
  input [0:0]S03_AXI_awvalid;
  input [0:0]S03_AXI_bready;
  output [1:0]S03_AXI_bresp;
  output [0:0]S03_AXI_bvalid;
  output [63:0]S03_AXI_rdata;
  input [0:0]S03_AXI_rready;
  output [1:0]S03_AXI_rresp;
  output [0:0]S03_AXI_rvalid;
  input [63:0]S03_AXI_wdata;
  output [0:0]S03_AXI_wready;
  input [7:0]S03_AXI_wstrb;
  input [0:0]S03_AXI_wvalid;

  wire [31:0]m00_couplers_to_psDramAxiInterconnect_ARADDR;
  wire [1:0]m00_couplers_to_psDramAxiInterconnect_ARBURST;
  wire [3:0]m00_couplers_to_psDramAxiInterconnect_ARCACHE;
  wire [3:0]m00_couplers_to_psDramAxiInterconnect_ARLEN;
  wire [1:0]m00_couplers_to_psDramAxiInterconnect_ARLOCK;
  wire [2:0]m00_couplers_to_psDramAxiInterconnect_ARPROT;
  wire [3:0]m00_couplers_to_psDramAxiInterconnect_ARQOS;
  wire m00_couplers_to_psDramAxiInterconnect_ARREADY;
  wire [2:0]m00_couplers_to_psDramAxiInterconnect_ARSIZE;
  wire m00_couplers_to_psDramAxiInterconnect_ARVALID;
  wire [31:0]m00_couplers_to_psDramAxiInterconnect_AWADDR;
  wire [1:0]m00_couplers_to_psDramAxiInterconnect_AWBURST;
  wire [3:0]m00_couplers_to_psDramAxiInterconnect_AWCACHE;
  wire [3:0]m00_couplers_to_psDramAxiInterconnect_AWLEN;
  wire [1:0]m00_couplers_to_psDramAxiInterconnect_AWLOCK;
  wire [2:0]m00_couplers_to_psDramAxiInterconnect_AWPROT;
  wire [3:0]m00_couplers_to_psDramAxiInterconnect_AWQOS;
  wire m00_couplers_to_psDramAxiInterconnect_AWREADY;
  wire [2:0]m00_couplers_to_psDramAxiInterconnect_AWSIZE;
  wire m00_couplers_to_psDramAxiInterconnect_AWVALID;
  wire m00_couplers_to_psDramAxiInterconnect_BREADY;
  wire [1:0]m00_couplers_to_psDramAxiInterconnect_BRESP;
  wire m00_couplers_to_psDramAxiInterconnect_BVALID;
  wire [63:0]m00_couplers_to_psDramAxiInterconnect_RDATA;
  wire m00_couplers_to_psDramAxiInterconnect_RLAST;
  wire m00_couplers_to_psDramAxiInterconnect_RREADY;
  wire [1:0]m00_couplers_to_psDramAxiInterconnect_RRESP;
  wire m00_couplers_to_psDramAxiInterconnect_RVALID;
  wire [63:0]m00_couplers_to_psDramAxiInterconnect_WDATA;
  wire m00_couplers_to_psDramAxiInterconnect_WLAST;
  wire m00_couplers_to_psDramAxiInterconnect_WREADY;
  wire [7:0]m00_couplers_to_psDramAxiInterconnect_WSTRB;
  wire m00_couplers_to_psDramAxiInterconnect_WVALID;
  wire psDramAxiInterconnect_ACLK_net;
  wire psDramAxiInterconnect_ARESETN_net;
  wire [31:0]psDramAxiInterconnect_to_s00_couplers_ARADDR;
  wire [2:0]psDramAxiInterconnect_to_s00_couplers_ARPROT;
  wire [0:0]psDramAxiInterconnect_to_s00_couplers_ARREADY;
  wire [0:0]psDramAxiInterconnect_to_s00_couplers_ARVALID;
  wire [31:0]psDramAxiInterconnect_to_s00_couplers_AWADDR;
  wire [2:0]psDramAxiInterconnect_to_s00_couplers_AWPROT;
  wire [0:0]psDramAxiInterconnect_to_s00_couplers_AWREADY;
  wire [0:0]psDramAxiInterconnect_to_s00_couplers_AWVALID;
  wire [0:0]psDramAxiInterconnect_to_s00_couplers_BREADY;
  wire [1:0]psDramAxiInterconnect_to_s00_couplers_BRESP;
  wire [0:0]psDramAxiInterconnect_to_s00_couplers_BVALID;
  wire [63:0]psDramAxiInterconnect_to_s00_couplers_RDATA;
  wire [0:0]psDramAxiInterconnect_to_s00_couplers_RREADY;
  wire [1:0]psDramAxiInterconnect_to_s00_couplers_RRESP;
  wire [0:0]psDramAxiInterconnect_to_s00_couplers_RVALID;
  wire [63:0]psDramAxiInterconnect_to_s00_couplers_WDATA;
  wire [0:0]psDramAxiInterconnect_to_s00_couplers_WREADY;
  wire [7:0]psDramAxiInterconnect_to_s00_couplers_WSTRB;
  wire [0:0]psDramAxiInterconnect_to_s00_couplers_WVALID;
  wire [31:0]psDramAxiInterconnect_to_s01_couplers_ARADDR;
  wire [2:0]psDramAxiInterconnect_to_s01_couplers_ARPROT;
  wire [0:0]psDramAxiInterconnect_to_s01_couplers_ARREADY;
  wire [0:0]psDramAxiInterconnect_to_s01_couplers_ARVALID;
  wire [31:0]psDramAxiInterconnect_to_s01_couplers_AWADDR;
  wire [2:0]psDramAxiInterconnect_to_s01_couplers_AWPROT;
  wire [0:0]psDramAxiInterconnect_to_s01_couplers_AWREADY;
  wire [0:0]psDramAxiInterconnect_to_s01_couplers_AWVALID;
  wire [0:0]psDramAxiInterconnect_to_s01_couplers_BREADY;
  wire [1:0]psDramAxiInterconnect_to_s01_couplers_BRESP;
  wire [0:0]psDramAxiInterconnect_to_s01_couplers_BVALID;
  wire [63:0]psDramAxiInterconnect_to_s01_couplers_RDATA;
  wire [0:0]psDramAxiInterconnect_to_s01_couplers_RREADY;
  wire [1:0]psDramAxiInterconnect_to_s01_couplers_RRESP;
  wire [0:0]psDramAxiInterconnect_to_s01_couplers_RVALID;
  wire [63:0]psDramAxiInterconnect_to_s01_couplers_WDATA;
  wire [0:0]psDramAxiInterconnect_to_s01_couplers_WREADY;
  wire [7:0]psDramAxiInterconnect_to_s01_couplers_WSTRB;
  wire [0:0]psDramAxiInterconnect_to_s01_couplers_WVALID;
  wire [31:0]psDramAxiInterconnect_to_s02_couplers_ARADDR;
  wire [2:0]psDramAxiInterconnect_to_s02_couplers_ARPROT;
  wire [0:0]psDramAxiInterconnect_to_s02_couplers_ARREADY;
  wire [0:0]psDramAxiInterconnect_to_s02_couplers_ARVALID;
  wire [31:0]psDramAxiInterconnect_to_s02_couplers_AWADDR;
  wire [2:0]psDramAxiInterconnect_to_s02_couplers_AWPROT;
  wire [0:0]psDramAxiInterconnect_to_s02_couplers_AWREADY;
  wire [0:0]psDramAxiInterconnect_to_s02_couplers_AWVALID;
  wire [0:0]psDramAxiInterconnect_to_s02_couplers_BREADY;
  wire [1:0]psDramAxiInterconnect_to_s02_couplers_BRESP;
  wire [0:0]psDramAxiInterconnect_to_s02_couplers_BVALID;
  wire [63:0]psDramAxiInterconnect_to_s02_couplers_RDATA;
  wire [0:0]psDramAxiInterconnect_to_s02_couplers_RREADY;
  wire [1:0]psDramAxiInterconnect_to_s02_couplers_RRESP;
  wire [0:0]psDramAxiInterconnect_to_s02_couplers_RVALID;
  wire [63:0]psDramAxiInterconnect_to_s02_couplers_WDATA;
  wire [0:0]psDramAxiInterconnect_to_s02_couplers_WREADY;
  wire [7:0]psDramAxiInterconnect_to_s02_couplers_WSTRB;
  wire [0:0]psDramAxiInterconnect_to_s02_couplers_WVALID;
  wire [31:0]psDramAxiInterconnect_to_s03_couplers_ARADDR;
  wire [2:0]psDramAxiInterconnect_to_s03_couplers_ARPROT;
  wire [0:0]psDramAxiInterconnect_to_s03_couplers_ARREADY;
  wire [0:0]psDramAxiInterconnect_to_s03_couplers_ARVALID;
  wire [31:0]psDramAxiInterconnect_to_s03_couplers_AWADDR;
  wire [2:0]psDramAxiInterconnect_to_s03_couplers_AWPROT;
  wire [0:0]psDramAxiInterconnect_to_s03_couplers_AWREADY;
  wire [0:0]psDramAxiInterconnect_to_s03_couplers_AWVALID;
  wire [0:0]psDramAxiInterconnect_to_s03_couplers_BREADY;
  wire [1:0]psDramAxiInterconnect_to_s03_couplers_BRESP;
  wire [0:0]psDramAxiInterconnect_to_s03_couplers_BVALID;
  wire [63:0]psDramAxiInterconnect_to_s03_couplers_RDATA;
  wire [0:0]psDramAxiInterconnect_to_s03_couplers_RREADY;
  wire [1:0]psDramAxiInterconnect_to_s03_couplers_RRESP;
  wire [0:0]psDramAxiInterconnect_to_s03_couplers_RVALID;
  wire [63:0]psDramAxiInterconnect_to_s03_couplers_WDATA;
  wire [0:0]psDramAxiInterconnect_to_s03_couplers_WREADY;
  wire [7:0]psDramAxiInterconnect_to_s03_couplers_WSTRB;
  wire [0:0]psDramAxiInterconnect_to_s03_couplers_WVALID;
  wire [31:0]s00_couplers_to_xbar_ARADDR;
  wire [2:0]s00_couplers_to_xbar_ARPROT;
  wire [0:0]s00_couplers_to_xbar_ARREADY;
  wire [0:0]s00_couplers_to_xbar_ARVALID;
  wire [31:0]s00_couplers_to_xbar_AWADDR;
  wire [2:0]s00_couplers_to_xbar_AWPROT;
  wire [0:0]s00_couplers_to_xbar_AWREADY;
  wire [0:0]s00_couplers_to_xbar_AWVALID;
  wire [0:0]s00_couplers_to_xbar_BREADY;
  wire [1:0]s00_couplers_to_xbar_BRESP;
  wire [0:0]s00_couplers_to_xbar_BVALID;
  wire [63:0]s00_couplers_to_xbar_RDATA;
  wire [0:0]s00_couplers_to_xbar_RREADY;
  wire [1:0]s00_couplers_to_xbar_RRESP;
  wire [0:0]s00_couplers_to_xbar_RVALID;
  wire [63:0]s00_couplers_to_xbar_WDATA;
  wire [0:0]s00_couplers_to_xbar_WREADY;
  wire [7:0]s00_couplers_to_xbar_WSTRB;
  wire [0:0]s00_couplers_to_xbar_WVALID;
  wire [31:0]s01_couplers_to_xbar_ARADDR;
  wire [2:0]s01_couplers_to_xbar_ARPROT;
  wire [1:1]s01_couplers_to_xbar_ARREADY;
  wire [0:0]s01_couplers_to_xbar_ARVALID;
  wire [31:0]s01_couplers_to_xbar_AWADDR;
  wire [2:0]s01_couplers_to_xbar_AWPROT;
  wire [1:1]s01_couplers_to_xbar_AWREADY;
  wire [0:0]s01_couplers_to_xbar_AWVALID;
  wire [0:0]s01_couplers_to_xbar_BREADY;
  wire [3:2]s01_couplers_to_xbar_BRESP;
  wire [1:1]s01_couplers_to_xbar_BVALID;
  wire [127:64]s01_couplers_to_xbar_RDATA;
  wire [0:0]s01_couplers_to_xbar_RREADY;
  wire [3:2]s01_couplers_to_xbar_RRESP;
  wire [1:1]s01_couplers_to_xbar_RVALID;
  wire [63:0]s01_couplers_to_xbar_WDATA;
  wire [1:1]s01_couplers_to_xbar_WREADY;
  wire [7:0]s01_couplers_to_xbar_WSTRB;
  wire [0:0]s01_couplers_to_xbar_WVALID;
  wire [31:0]s02_couplers_to_xbar_ARADDR;
  wire [2:0]s02_couplers_to_xbar_ARPROT;
  wire [2:2]s02_couplers_to_xbar_ARREADY;
  wire [0:0]s02_couplers_to_xbar_ARVALID;
  wire [31:0]s02_couplers_to_xbar_AWADDR;
  wire [2:0]s02_couplers_to_xbar_AWPROT;
  wire [2:2]s02_couplers_to_xbar_AWREADY;
  wire [0:0]s02_couplers_to_xbar_AWVALID;
  wire [0:0]s02_couplers_to_xbar_BREADY;
  wire [5:4]s02_couplers_to_xbar_BRESP;
  wire [2:2]s02_couplers_to_xbar_BVALID;
  wire [191:128]s02_couplers_to_xbar_RDATA;
  wire [0:0]s02_couplers_to_xbar_RREADY;
  wire [5:4]s02_couplers_to_xbar_RRESP;
  wire [2:2]s02_couplers_to_xbar_RVALID;
  wire [63:0]s02_couplers_to_xbar_WDATA;
  wire [2:2]s02_couplers_to_xbar_WREADY;
  wire [7:0]s02_couplers_to_xbar_WSTRB;
  wire [0:0]s02_couplers_to_xbar_WVALID;
  wire [31:0]s03_couplers_to_xbar_ARADDR;
  wire [2:0]s03_couplers_to_xbar_ARPROT;
  wire [3:3]s03_couplers_to_xbar_ARREADY;
  wire [0:0]s03_couplers_to_xbar_ARVALID;
  wire [31:0]s03_couplers_to_xbar_AWADDR;
  wire [2:0]s03_couplers_to_xbar_AWPROT;
  wire [3:3]s03_couplers_to_xbar_AWREADY;
  wire [0:0]s03_couplers_to_xbar_AWVALID;
  wire [0:0]s03_couplers_to_xbar_BREADY;
  wire [7:6]s03_couplers_to_xbar_BRESP;
  wire [3:3]s03_couplers_to_xbar_BVALID;
  wire [255:192]s03_couplers_to_xbar_RDATA;
  wire [0:0]s03_couplers_to_xbar_RREADY;
  wire [7:6]s03_couplers_to_xbar_RRESP;
  wire [3:3]s03_couplers_to_xbar_RVALID;
  wire [63:0]s03_couplers_to_xbar_WDATA;
  wire [3:3]s03_couplers_to_xbar_WREADY;
  wire [7:0]s03_couplers_to_xbar_WSTRB;
  wire [0:0]s03_couplers_to_xbar_WVALID;
  wire [31:0]xbar_to_m00_couplers_ARADDR;
  wire [2:0]xbar_to_m00_couplers_ARPROT;
  wire xbar_to_m00_couplers_ARREADY;
  wire [0:0]xbar_to_m00_couplers_ARVALID;
  wire [31:0]xbar_to_m00_couplers_AWADDR;
  wire [2:0]xbar_to_m00_couplers_AWPROT;
  wire xbar_to_m00_couplers_AWREADY;
  wire [0:0]xbar_to_m00_couplers_AWVALID;
  wire [0:0]xbar_to_m00_couplers_BREADY;
  wire [1:0]xbar_to_m00_couplers_BRESP;
  wire xbar_to_m00_couplers_BVALID;
  wire [63:0]xbar_to_m00_couplers_RDATA;
  wire [0:0]xbar_to_m00_couplers_RREADY;
  wire [1:0]xbar_to_m00_couplers_RRESP;
  wire xbar_to_m00_couplers_RVALID;
  wire [63:0]xbar_to_m00_couplers_WDATA;
  wire xbar_to_m00_couplers_WREADY;
  wire [7:0]xbar_to_m00_couplers_WSTRB;
  wire [0:0]xbar_to_m00_couplers_WVALID;

  assign M00_AXI_araddr[31:0] = m00_couplers_to_psDramAxiInterconnect_ARADDR;
  assign M00_AXI_arburst[1:0] = m00_couplers_to_psDramAxiInterconnect_ARBURST;
  assign M00_AXI_arcache[3:0] = m00_couplers_to_psDramAxiInterconnect_ARCACHE;
  assign M00_AXI_arlen[3:0] = m00_couplers_to_psDramAxiInterconnect_ARLEN;
  assign M00_AXI_arlock[1:0] = m00_couplers_to_psDramAxiInterconnect_ARLOCK;
  assign M00_AXI_arprot[2:0] = m00_couplers_to_psDramAxiInterconnect_ARPROT;
  assign M00_AXI_arqos[3:0] = m00_couplers_to_psDramAxiInterconnect_ARQOS;
  assign M00_AXI_arsize[2:0] = m00_couplers_to_psDramAxiInterconnect_ARSIZE;
  assign M00_AXI_arvalid = m00_couplers_to_psDramAxiInterconnect_ARVALID;
  assign M00_AXI_awaddr[31:0] = m00_couplers_to_psDramAxiInterconnect_AWADDR;
  assign M00_AXI_awburst[1:0] = m00_couplers_to_psDramAxiInterconnect_AWBURST;
  assign M00_AXI_awcache[3:0] = m00_couplers_to_psDramAxiInterconnect_AWCACHE;
  assign M00_AXI_awlen[3:0] = m00_couplers_to_psDramAxiInterconnect_AWLEN;
  assign M00_AXI_awlock[1:0] = m00_couplers_to_psDramAxiInterconnect_AWLOCK;
  assign M00_AXI_awprot[2:0] = m00_couplers_to_psDramAxiInterconnect_AWPROT;
  assign M00_AXI_awqos[3:0] = m00_couplers_to_psDramAxiInterconnect_AWQOS;
  assign M00_AXI_awsize[2:0] = m00_couplers_to_psDramAxiInterconnect_AWSIZE;
  assign M00_AXI_awvalid = m00_couplers_to_psDramAxiInterconnect_AWVALID;
  assign M00_AXI_bready = m00_couplers_to_psDramAxiInterconnect_BREADY;
  assign M00_AXI_rready = m00_couplers_to_psDramAxiInterconnect_RREADY;
  assign M00_AXI_wdata[63:0] = m00_couplers_to_psDramAxiInterconnect_WDATA;
  assign M00_AXI_wlast = m00_couplers_to_psDramAxiInterconnect_WLAST;
  assign M00_AXI_wstrb[7:0] = m00_couplers_to_psDramAxiInterconnect_WSTRB;
  assign M00_AXI_wvalid = m00_couplers_to_psDramAxiInterconnect_WVALID;
  assign S00_AXI_arready[0] = psDramAxiInterconnect_to_s00_couplers_ARREADY;
  assign S00_AXI_awready[0] = psDramAxiInterconnect_to_s00_couplers_AWREADY;
  assign S00_AXI_bresp[1:0] = psDramAxiInterconnect_to_s00_couplers_BRESP;
  assign S00_AXI_bvalid[0] = psDramAxiInterconnect_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[63:0] = psDramAxiInterconnect_to_s00_couplers_RDATA;
  assign S00_AXI_rresp[1:0] = psDramAxiInterconnect_to_s00_couplers_RRESP;
  assign S00_AXI_rvalid[0] = psDramAxiInterconnect_to_s00_couplers_RVALID;
  assign S00_AXI_wready[0] = psDramAxiInterconnect_to_s00_couplers_WREADY;
  assign S01_AXI_arready[0] = psDramAxiInterconnect_to_s01_couplers_ARREADY;
  assign S01_AXI_awready[0] = psDramAxiInterconnect_to_s01_couplers_AWREADY;
  assign S01_AXI_bresp[1:0] = psDramAxiInterconnect_to_s01_couplers_BRESP;
  assign S01_AXI_bvalid[0] = psDramAxiInterconnect_to_s01_couplers_BVALID;
  assign S01_AXI_rdata[63:0] = psDramAxiInterconnect_to_s01_couplers_RDATA;
  assign S01_AXI_rresp[1:0] = psDramAxiInterconnect_to_s01_couplers_RRESP;
  assign S01_AXI_rvalid[0] = psDramAxiInterconnect_to_s01_couplers_RVALID;
  assign S01_AXI_wready[0] = psDramAxiInterconnect_to_s01_couplers_WREADY;
  assign S02_AXI_arready[0] = psDramAxiInterconnect_to_s02_couplers_ARREADY;
  assign S02_AXI_awready[0] = psDramAxiInterconnect_to_s02_couplers_AWREADY;
  assign S02_AXI_bresp[1:0] = psDramAxiInterconnect_to_s02_couplers_BRESP;
  assign S02_AXI_bvalid[0] = psDramAxiInterconnect_to_s02_couplers_BVALID;
  assign S02_AXI_rdata[63:0] = psDramAxiInterconnect_to_s02_couplers_RDATA;
  assign S02_AXI_rresp[1:0] = psDramAxiInterconnect_to_s02_couplers_RRESP;
  assign S02_AXI_rvalid[0] = psDramAxiInterconnect_to_s02_couplers_RVALID;
  assign S02_AXI_wready[0] = psDramAxiInterconnect_to_s02_couplers_WREADY;
  assign S03_AXI_arready[0] = psDramAxiInterconnect_to_s03_couplers_ARREADY;
  assign S03_AXI_awready[0] = psDramAxiInterconnect_to_s03_couplers_AWREADY;
  assign S03_AXI_bresp[1:0] = psDramAxiInterconnect_to_s03_couplers_BRESP;
  assign S03_AXI_bvalid[0] = psDramAxiInterconnect_to_s03_couplers_BVALID;
  assign S03_AXI_rdata[63:0] = psDramAxiInterconnect_to_s03_couplers_RDATA;
  assign S03_AXI_rresp[1:0] = psDramAxiInterconnect_to_s03_couplers_RRESP;
  assign S03_AXI_rvalid[0] = psDramAxiInterconnect_to_s03_couplers_RVALID;
  assign S03_AXI_wready[0] = psDramAxiInterconnect_to_s03_couplers_WREADY;
  assign m00_couplers_to_psDramAxiInterconnect_ARREADY = M00_AXI_arready;
  assign m00_couplers_to_psDramAxiInterconnect_AWREADY = M00_AXI_awready;
  assign m00_couplers_to_psDramAxiInterconnect_BRESP = M00_AXI_bresp[1:0];
  assign m00_couplers_to_psDramAxiInterconnect_BVALID = M00_AXI_bvalid;
  assign m00_couplers_to_psDramAxiInterconnect_RDATA = M00_AXI_rdata[63:0];
  assign m00_couplers_to_psDramAxiInterconnect_RLAST = M00_AXI_rlast;
  assign m00_couplers_to_psDramAxiInterconnect_RRESP = M00_AXI_rresp[1:0];
  assign m00_couplers_to_psDramAxiInterconnect_RVALID = M00_AXI_rvalid;
  assign m00_couplers_to_psDramAxiInterconnect_WREADY = M00_AXI_wready;
  assign psDramAxiInterconnect_ACLK_net = ACLK;
  assign psDramAxiInterconnect_ARESETN_net = ARESETN;
  assign psDramAxiInterconnect_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign psDramAxiInterconnect_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign psDramAxiInterconnect_to_s00_couplers_ARVALID = S00_AXI_arvalid[0];
  assign psDramAxiInterconnect_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign psDramAxiInterconnect_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign psDramAxiInterconnect_to_s00_couplers_AWVALID = S00_AXI_awvalid[0];
  assign psDramAxiInterconnect_to_s00_couplers_BREADY = S00_AXI_bready[0];
  assign psDramAxiInterconnect_to_s00_couplers_RREADY = S00_AXI_rready[0];
  assign psDramAxiInterconnect_to_s00_couplers_WDATA = S00_AXI_wdata[63:0];
  assign psDramAxiInterconnect_to_s00_couplers_WSTRB = S00_AXI_wstrb[7:0];
  assign psDramAxiInterconnect_to_s00_couplers_WVALID = S00_AXI_wvalid[0];
  assign psDramAxiInterconnect_to_s01_couplers_ARADDR = S01_AXI_araddr[31:0];
  assign psDramAxiInterconnect_to_s01_couplers_ARPROT = S01_AXI_arprot[2:0];
  assign psDramAxiInterconnect_to_s01_couplers_ARVALID = S01_AXI_arvalid[0];
  assign psDramAxiInterconnect_to_s01_couplers_AWADDR = S01_AXI_awaddr[31:0];
  assign psDramAxiInterconnect_to_s01_couplers_AWPROT = S01_AXI_awprot[2:0];
  assign psDramAxiInterconnect_to_s01_couplers_AWVALID = S01_AXI_awvalid[0];
  assign psDramAxiInterconnect_to_s01_couplers_BREADY = S01_AXI_bready[0];
  assign psDramAxiInterconnect_to_s01_couplers_RREADY = S01_AXI_rready[0];
  assign psDramAxiInterconnect_to_s01_couplers_WDATA = S01_AXI_wdata[63:0];
  assign psDramAxiInterconnect_to_s01_couplers_WSTRB = S01_AXI_wstrb[7:0];
  assign psDramAxiInterconnect_to_s01_couplers_WVALID = S01_AXI_wvalid[0];
  assign psDramAxiInterconnect_to_s02_couplers_ARADDR = S02_AXI_araddr[31:0];
  assign psDramAxiInterconnect_to_s02_couplers_ARPROT = S02_AXI_arprot[2:0];
  assign psDramAxiInterconnect_to_s02_couplers_ARVALID = S02_AXI_arvalid[0];
  assign psDramAxiInterconnect_to_s02_couplers_AWADDR = S02_AXI_awaddr[31:0];
  assign psDramAxiInterconnect_to_s02_couplers_AWPROT = S02_AXI_awprot[2:0];
  assign psDramAxiInterconnect_to_s02_couplers_AWVALID = S02_AXI_awvalid[0];
  assign psDramAxiInterconnect_to_s02_couplers_BREADY = S02_AXI_bready[0];
  assign psDramAxiInterconnect_to_s02_couplers_RREADY = S02_AXI_rready[0];
  assign psDramAxiInterconnect_to_s02_couplers_WDATA = S02_AXI_wdata[63:0];
  assign psDramAxiInterconnect_to_s02_couplers_WSTRB = S02_AXI_wstrb[7:0];
  assign psDramAxiInterconnect_to_s02_couplers_WVALID = S02_AXI_wvalid[0];
  assign psDramAxiInterconnect_to_s03_couplers_ARADDR = S03_AXI_araddr[31:0];
  assign psDramAxiInterconnect_to_s03_couplers_ARPROT = S03_AXI_arprot[2:0];
  assign psDramAxiInterconnect_to_s03_couplers_ARVALID = S03_AXI_arvalid[0];
  assign psDramAxiInterconnect_to_s03_couplers_AWADDR = S03_AXI_awaddr[31:0];
  assign psDramAxiInterconnect_to_s03_couplers_AWPROT = S03_AXI_awprot[2:0];
  assign psDramAxiInterconnect_to_s03_couplers_AWVALID = S03_AXI_awvalid[0];
  assign psDramAxiInterconnect_to_s03_couplers_BREADY = S03_AXI_bready[0];
  assign psDramAxiInterconnect_to_s03_couplers_RREADY = S03_AXI_rready[0];
  assign psDramAxiInterconnect_to_s03_couplers_WDATA = S03_AXI_wdata[63:0];
  assign psDramAxiInterconnect_to_s03_couplers_WSTRB = S03_AXI_wstrb[7:0];
  assign psDramAxiInterconnect_to_s03_couplers_WVALID = S03_AXI_wvalid[0];
  m00_couplers_imp_LTHPB m00_couplers
       (.M_ACLK(psDramAxiInterconnect_ACLK_net),
        .M_ARESETN(psDramAxiInterconnect_ARESETN_net),
        .M_AXI_araddr(m00_couplers_to_psDramAxiInterconnect_ARADDR),
        .M_AXI_arburst(m00_couplers_to_psDramAxiInterconnect_ARBURST),
        .M_AXI_arcache(m00_couplers_to_psDramAxiInterconnect_ARCACHE),
        .M_AXI_arlen(m00_couplers_to_psDramAxiInterconnect_ARLEN),
        .M_AXI_arlock(m00_couplers_to_psDramAxiInterconnect_ARLOCK),
        .M_AXI_arprot(m00_couplers_to_psDramAxiInterconnect_ARPROT),
        .M_AXI_arqos(m00_couplers_to_psDramAxiInterconnect_ARQOS),
        .M_AXI_arready(m00_couplers_to_psDramAxiInterconnect_ARREADY),
        .M_AXI_arsize(m00_couplers_to_psDramAxiInterconnect_ARSIZE),
        .M_AXI_arvalid(m00_couplers_to_psDramAxiInterconnect_ARVALID),
        .M_AXI_awaddr(m00_couplers_to_psDramAxiInterconnect_AWADDR),
        .M_AXI_awburst(m00_couplers_to_psDramAxiInterconnect_AWBURST),
        .M_AXI_awcache(m00_couplers_to_psDramAxiInterconnect_AWCACHE),
        .M_AXI_awlen(m00_couplers_to_psDramAxiInterconnect_AWLEN),
        .M_AXI_awlock(m00_couplers_to_psDramAxiInterconnect_AWLOCK),
        .M_AXI_awprot(m00_couplers_to_psDramAxiInterconnect_AWPROT),
        .M_AXI_awqos(m00_couplers_to_psDramAxiInterconnect_AWQOS),
        .M_AXI_awready(m00_couplers_to_psDramAxiInterconnect_AWREADY),
        .M_AXI_awsize(m00_couplers_to_psDramAxiInterconnect_AWSIZE),
        .M_AXI_awvalid(m00_couplers_to_psDramAxiInterconnect_AWVALID),
        .M_AXI_bready(m00_couplers_to_psDramAxiInterconnect_BREADY),
        .M_AXI_bresp(m00_couplers_to_psDramAxiInterconnect_BRESP),
        .M_AXI_bvalid(m00_couplers_to_psDramAxiInterconnect_BVALID),
        .M_AXI_rdata(m00_couplers_to_psDramAxiInterconnect_RDATA),
        .M_AXI_rlast(m00_couplers_to_psDramAxiInterconnect_RLAST),
        .M_AXI_rready(m00_couplers_to_psDramAxiInterconnect_RREADY),
        .M_AXI_rresp(m00_couplers_to_psDramAxiInterconnect_RRESP),
        .M_AXI_rvalid(m00_couplers_to_psDramAxiInterconnect_RVALID),
        .M_AXI_wdata(m00_couplers_to_psDramAxiInterconnect_WDATA),
        .M_AXI_wlast(m00_couplers_to_psDramAxiInterconnect_WLAST),
        .M_AXI_wready(m00_couplers_to_psDramAxiInterconnect_WREADY),
        .M_AXI_wstrb(m00_couplers_to_psDramAxiInterconnect_WSTRB),
        .M_AXI_wvalid(m00_couplers_to_psDramAxiInterconnect_WVALID),
        .S_ACLK(psDramAxiInterconnect_ACLK_net),
        .S_ARESETN(psDramAxiInterconnect_ARESETN_net),
        .S_AXI_araddr(xbar_to_m00_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m00_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m00_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m00_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m00_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m00_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m00_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m00_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m00_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m00_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m00_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m00_couplers_RDATA),
        .S_AXI_rready(xbar_to_m00_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m00_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m00_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m00_couplers_WDATA),
        .S_AXI_wready(xbar_to_m00_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m00_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m00_couplers_WVALID));
  s00_couplers_imp_1XNV4IL s00_couplers
       (.M_ACLK(psDramAxiInterconnect_ACLK_net),
        .M_ARESETN(psDramAxiInterconnect_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_xbar_ARADDR),
        .M_AXI_arprot(s00_couplers_to_xbar_ARPROT),
        .M_AXI_arready(s00_couplers_to_xbar_ARREADY),
        .M_AXI_arvalid(s00_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_xbar_AWADDR),
        .M_AXI_awprot(s00_couplers_to_xbar_AWPROT),
        .M_AXI_awready(s00_couplers_to_xbar_AWREADY),
        .M_AXI_awvalid(s00_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s00_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s00_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s00_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s00_couplers_to_xbar_RDATA),
        .M_AXI_rready(s00_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s00_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s00_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s00_couplers_to_xbar_WDATA),
        .M_AXI_wready(s00_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s00_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_xbar_WVALID),
        .S_ACLK(psDramAxiInterconnect_ACLK_net),
        .S_ARESETN(psDramAxiInterconnect_ARESETN_net),
        .S_AXI_araddr(psDramAxiInterconnect_to_s00_couplers_ARADDR),
        .S_AXI_arprot(psDramAxiInterconnect_to_s00_couplers_ARPROT),
        .S_AXI_arready(psDramAxiInterconnect_to_s00_couplers_ARREADY),
        .S_AXI_arvalid(psDramAxiInterconnect_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(psDramAxiInterconnect_to_s00_couplers_AWADDR),
        .S_AXI_awprot(psDramAxiInterconnect_to_s00_couplers_AWPROT),
        .S_AXI_awready(psDramAxiInterconnect_to_s00_couplers_AWREADY),
        .S_AXI_awvalid(psDramAxiInterconnect_to_s00_couplers_AWVALID),
        .S_AXI_bready(psDramAxiInterconnect_to_s00_couplers_BREADY),
        .S_AXI_bresp(psDramAxiInterconnect_to_s00_couplers_BRESP),
        .S_AXI_bvalid(psDramAxiInterconnect_to_s00_couplers_BVALID),
        .S_AXI_rdata(psDramAxiInterconnect_to_s00_couplers_RDATA),
        .S_AXI_rready(psDramAxiInterconnect_to_s00_couplers_RREADY),
        .S_AXI_rresp(psDramAxiInterconnect_to_s00_couplers_RRESP),
        .S_AXI_rvalid(psDramAxiInterconnect_to_s00_couplers_RVALID),
        .S_AXI_wdata(psDramAxiInterconnect_to_s00_couplers_WDATA),
        .S_AXI_wready(psDramAxiInterconnect_to_s00_couplers_WREADY),
        .S_AXI_wstrb(psDramAxiInterconnect_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(psDramAxiInterconnect_to_s00_couplers_WVALID));
  s01_couplers_imp_5MEJR0 s01_couplers
       (.M_ACLK(psDramAxiInterconnect_ACLK_net),
        .M_ARESETN(psDramAxiInterconnect_ARESETN_net),
        .M_AXI_araddr(s01_couplers_to_xbar_ARADDR),
        .M_AXI_arprot(s01_couplers_to_xbar_ARPROT),
        .M_AXI_arready(s01_couplers_to_xbar_ARREADY),
        .M_AXI_arvalid(s01_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s01_couplers_to_xbar_AWADDR),
        .M_AXI_awprot(s01_couplers_to_xbar_AWPROT),
        .M_AXI_awready(s01_couplers_to_xbar_AWREADY),
        .M_AXI_awvalid(s01_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s01_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s01_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s01_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s01_couplers_to_xbar_RDATA),
        .M_AXI_rready(s01_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s01_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s01_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s01_couplers_to_xbar_WDATA),
        .M_AXI_wready(s01_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s01_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s01_couplers_to_xbar_WVALID),
        .S_ACLK(psDramAxiInterconnect_ACLK_net),
        .S_ARESETN(psDramAxiInterconnect_ARESETN_net),
        .S_AXI_araddr(psDramAxiInterconnect_to_s01_couplers_ARADDR),
        .S_AXI_arprot(psDramAxiInterconnect_to_s01_couplers_ARPROT),
        .S_AXI_arready(psDramAxiInterconnect_to_s01_couplers_ARREADY),
        .S_AXI_arvalid(psDramAxiInterconnect_to_s01_couplers_ARVALID),
        .S_AXI_awaddr(psDramAxiInterconnect_to_s01_couplers_AWADDR),
        .S_AXI_awprot(psDramAxiInterconnect_to_s01_couplers_AWPROT),
        .S_AXI_awready(psDramAxiInterconnect_to_s01_couplers_AWREADY),
        .S_AXI_awvalid(psDramAxiInterconnect_to_s01_couplers_AWVALID),
        .S_AXI_bready(psDramAxiInterconnect_to_s01_couplers_BREADY),
        .S_AXI_bresp(psDramAxiInterconnect_to_s01_couplers_BRESP),
        .S_AXI_bvalid(psDramAxiInterconnect_to_s01_couplers_BVALID),
        .S_AXI_rdata(psDramAxiInterconnect_to_s01_couplers_RDATA),
        .S_AXI_rready(psDramAxiInterconnect_to_s01_couplers_RREADY),
        .S_AXI_rresp(psDramAxiInterconnect_to_s01_couplers_RRESP),
        .S_AXI_rvalid(psDramAxiInterconnect_to_s01_couplers_RVALID),
        .S_AXI_wdata(psDramAxiInterconnect_to_s01_couplers_WDATA),
        .S_AXI_wready(psDramAxiInterconnect_to_s01_couplers_WREADY),
        .S_AXI_wstrb(psDramAxiInterconnect_to_s01_couplers_WSTRB),
        .S_AXI_wvalid(psDramAxiInterconnect_to_s01_couplers_WVALID));
  s02_couplers_imp_1YOULGE s02_couplers
       (.M_ACLK(psDramAxiInterconnect_ACLK_net),
        .M_ARESETN(psDramAxiInterconnect_ARESETN_net),
        .M_AXI_araddr(s02_couplers_to_xbar_ARADDR),
        .M_AXI_arprot(s02_couplers_to_xbar_ARPROT),
        .M_AXI_arready(s02_couplers_to_xbar_ARREADY),
        .M_AXI_arvalid(s02_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s02_couplers_to_xbar_AWADDR),
        .M_AXI_awprot(s02_couplers_to_xbar_AWPROT),
        .M_AXI_awready(s02_couplers_to_xbar_AWREADY),
        .M_AXI_awvalid(s02_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s02_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s02_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s02_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s02_couplers_to_xbar_RDATA),
        .M_AXI_rready(s02_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s02_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s02_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s02_couplers_to_xbar_WDATA),
        .M_AXI_wready(s02_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s02_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s02_couplers_to_xbar_WVALID),
        .S_ACLK(psDramAxiInterconnect_ACLK_net),
        .S_ARESETN(psDramAxiInterconnect_ARESETN_net),
        .S_AXI_araddr(psDramAxiInterconnect_to_s02_couplers_ARADDR),
        .S_AXI_arprot(psDramAxiInterconnect_to_s02_couplers_ARPROT),
        .S_AXI_arready(psDramAxiInterconnect_to_s02_couplers_ARREADY),
        .S_AXI_arvalid(psDramAxiInterconnect_to_s02_couplers_ARVALID),
        .S_AXI_awaddr(psDramAxiInterconnect_to_s02_couplers_AWADDR),
        .S_AXI_awprot(psDramAxiInterconnect_to_s02_couplers_AWPROT),
        .S_AXI_awready(psDramAxiInterconnect_to_s02_couplers_AWREADY),
        .S_AXI_awvalid(psDramAxiInterconnect_to_s02_couplers_AWVALID),
        .S_AXI_bready(psDramAxiInterconnect_to_s02_couplers_BREADY),
        .S_AXI_bresp(psDramAxiInterconnect_to_s02_couplers_BRESP),
        .S_AXI_bvalid(psDramAxiInterconnect_to_s02_couplers_BVALID),
        .S_AXI_rdata(psDramAxiInterconnect_to_s02_couplers_RDATA),
        .S_AXI_rready(psDramAxiInterconnect_to_s02_couplers_RREADY),
        .S_AXI_rresp(psDramAxiInterconnect_to_s02_couplers_RRESP),
        .S_AXI_rvalid(psDramAxiInterconnect_to_s02_couplers_RVALID),
        .S_AXI_wdata(psDramAxiInterconnect_to_s02_couplers_WDATA),
        .S_AXI_wready(psDramAxiInterconnect_to_s02_couplers_WREADY),
        .S_AXI_wstrb(psDramAxiInterconnect_to_s02_couplers_WSTRB),
        .S_AXI_wvalid(psDramAxiInterconnect_to_s02_couplers_WVALID));
  s03_couplers_imp_4VPA7Z s03_couplers
       (.M_ACLK(psDramAxiInterconnect_ACLK_net),
        .M_ARESETN(psDramAxiInterconnect_ARESETN_net),
        .M_AXI_araddr(s03_couplers_to_xbar_ARADDR),
        .M_AXI_arprot(s03_couplers_to_xbar_ARPROT),
        .M_AXI_arready(s03_couplers_to_xbar_ARREADY),
        .M_AXI_arvalid(s03_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s03_couplers_to_xbar_AWADDR),
        .M_AXI_awprot(s03_couplers_to_xbar_AWPROT),
        .M_AXI_awready(s03_couplers_to_xbar_AWREADY),
        .M_AXI_awvalid(s03_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s03_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s03_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s03_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s03_couplers_to_xbar_RDATA),
        .M_AXI_rready(s03_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s03_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s03_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s03_couplers_to_xbar_WDATA),
        .M_AXI_wready(s03_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s03_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s03_couplers_to_xbar_WVALID),
        .S_ACLK(psDramAxiInterconnect_ACLK_net),
        .S_ARESETN(psDramAxiInterconnect_ARESETN_net),
        .S_AXI_araddr(psDramAxiInterconnect_to_s03_couplers_ARADDR),
        .S_AXI_arprot(psDramAxiInterconnect_to_s03_couplers_ARPROT),
        .S_AXI_arready(psDramAxiInterconnect_to_s03_couplers_ARREADY),
        .S_AXI_arvalid(psDramAxiInterconnect_to_s03_couplers_ARVALID),
        .S_AXI_awaddr(psDramAxiInterconnect_to_s03_couplers_AWADDR),
        .S_AXI_awprot(psDramAxiInterconnect_to_s03_couplers_AWPROT),
        .S_AXI_awready(psDramAxiInterconnect_to_s03_couplers_AWREADY),
        .S_AXI_awvalid(psDramAxiInterconnect_to_s03_couplers_AWVALID),
        .S_AXI_bready(psDramAxiInterconnect_to_s03_couplers_BREADY),
        .S_AXI_bresp(psDramAxiInterconnect_to_s03_couplers_BRESP),
        .S_AXI_bvalid(psDramAxiInterconnect_to_s03_couplers_BVALID),
        .S_AXI_rdata(psDramAxiInterconnect_to_s03_couplers_RDATA),
        .S_AXI_rready(psDramAxiInterconnect_to_s03_couplers_RREADY),
        .S_AXI_rresp(psDramAxiInterconnect_to_s03_couplers_RRESP),
        .S_AXI_rvalid(psDramAxiInterconnect_to_s03_couplers_RVALID),
        .S_AXI_wdata(psDramAxiInterconnect_to_s03_couplers_WDATA),
        .S_AXI_wready(psDramAxiInterconnect_to_s03_couplers_WREADY),
        .S_AXI_wstrb(psDramAxiInterconnect_to_s03_couplers_WSTRB),
        .S_AXI_wvalid(psDramAxiInterconnect_to_s03_couplers_WVALID));
  cerberus_xbar_1 xbar
       (.aclk(psDramAxiInterconnect_ACLK_net),
        .aresetn(psDramAxiInterconnect_ARESETN_net),
        .m_axi_araddr(xbar_to_m00_couplers_ARADDR),
        .m_axi_arprot(xbar_to_m00_couplers_ARPROT),
        .m_axi_arready(xbar_to_m00_couplers_ARREADY),
        .m_axi_arvalid(xbar_to_m00_couplers_ARVALID),
        .m_axi_awaddr(xbar_to_m00_couplers_AWADDR),
        .m_axi_awprot(xbar_to_m00_couplers_AWPROT),
        .m_axi_awready(xbar_to_m00_couplers_AWREADY),
        .m_axi_awvalid(xbar_to_m00_couplers_AWVALID),
        .m_axi_bready(xbar_to_m00_couplers_BREADY),
        .m_axi_bresp(xbar_to_m00_couplers_BRESP),
        .m_axi_bvalid(xbar_to_m00_couplers_BVALID),
        .m_axi_rdata(xbar_to_m00_couplers_RDATA),
        .m_axi_rready(xbar_to_m00_couplers_RREADY),
        .m_axi_rresp(xbar_to_m00_couplers_RRESP),
        .m_axi_rvalid(xbar_to_m00_couplers_RVALID),
        .m_axi_wdata(xbar_to_m00_couplers_WDATA),
        .m_axi_wready(xbar_to_m00_couplers_WREADY),
        .m_axi_wstrb(xbar_to_m00_couplers_WSTRB),
        .m_axi_wvalid(xbar_to_m00_couplers_WVALID),
        .s_axi_araddr({s03_couplers_to_xbar_ARADDR,s02_couplers_to_xbar_ARADDR,s01_couplers_to_xbar_ARADDR,s00_couplers_to_xbar_ARADDR}),
        .s_axi_arprot({s03_couplers_to_xbar_ARPROT,s02_couplers_to_xbar_ARPROT,s01_couplers_to_xbar_ARPROT,s00_couplers_to_xbar_ARPROT}),
        .s_axi_arready({s03_couplers_to_xbar_ARREADY,s02_couplers_to_xbar_ARREADY,s01_couplers_to_xbar_ARREADY,s00_couplers_to_xbar_ARREADY}),
        .s_axi_arvalid({s03_couplers_to_xbar_ARVALID,s02_couplers_to_xbar_ARVALID,s01_couplers_to_xbar_ARVALID,s00_couplers_to_xbar_ARVALID}),
        .s_axi_awaddr({s03_couplers_to_xbar_AWADDR,s02_couplers_to_xbar_AWADDR,s01_couplers_to_xbar_AWADDR,s00_couplers_to_xbar_AWADDR}),
        .s_axi_awprot({s03_couplers_to_xbar_AWPROT,s02_couplers_to_xbar_AWPROT,s01_couplers_to_xbar_AWPROT,s00_couplers_to_xbar_AWPROT}),
        .s_axi_awready({s03_couplers_to_xbar_AWREADY,s02_couplers_to_xbar_AWREADY,s01_couplers_to_xbar_AWREADY,s00_couplers_to_xbar_AWREADY}),
        .s_axi_awvalid({s03_couplers_to_xbar_AWVALID,s02_couplers_to_xbar_AWVALID,s01_couplers_to_xbar_AWVALID,s00_couplers_to_xbar_AWVALID}),
        .s_axi_bready({s03_couplers_to_xbar_BREADY,s02_couplers_to_xbar_BREADY,s01_couplers_to_xbar_BREADY,s00_couplers_to_xbar_BREADY}),
        .s_axi_bresp({s03_couplers_to_xbar_BRESP,s02_couplers_to_xbar_BRESP,s01_couplers_to_xbar_BRESP,s00_couplers_to_xbar_BRESP}),
        .s_axi_bvalid({s03_couplers_to_xbar_BVALID,s02_couplers_to_xbar_BVALID,s01_couplers_to_xbar_BVALID,s00_couplers_to_xbar_BVALID}),
        .s_axi_rdata({s03_couplers_to_xbar_RDATA,s02_couplers_to_xbar_RDATA,s01_couplers_to_xbar_RDATA,s00_couplers_to_xbar_RDATA}),
        .s_axi_rready({s03_couplers_to_xbar_RREADY,s02_couplers_to_xbar_RREADY,s01_couplers_to_xbar_RREADY,s00_couplers_to_xbar_RREADY}),
        .s_axi_rresp({s03_couplers_to_xbar_RRESP,s02_couplers_to_xbar_RRESP,s01_couplers_to_xbar_RRESP,s00_couplers_to_xbar_RRESP}),
        .s_axi_rvalid({s03_couplers_to_xbar_RVALID,s02_couplers_to_xbar_RVALID,s01_couplers_to_xbar_RVALID,s00_couplers_to_xbar_RVALID}),
        .s_axi_wdata({s03_couplers_to_xbar_WDATA,s02_couplers_to_xbar_WDATA,s01_couplers_to_xbar_WDATA,s00_couplers_to_xbar_WDATA}),
        .s_axi_wready({s03_couplers_to_xbar_WREADY,s02_couplers_to_xbar_WREADY,s01_couplers_to_xbar_WREADY,s00_couplers_to_xbar_WREADY}),
        .s_axi_wstrb({s03_couplers_to_xbar_WSTRB,s02_couplers_to_xbar_WSTRB,s01_couplers_to_xbar_WSTRB,s00_couplers_to_xbar_WSTRB}),
        .s_axi_wvalid({s03_couplers_to_xbar_WVALID,s02_couplers_to_xbar_WVALID,s01_couplers_to_xbar_WVALID,s00_couplers_to_xbar_WVALID}));
endmodule

module cerberus_psDramInterface_0
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arprot,
    M00_AXI_arready,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awprot,
    M00_AXI_awready,
    M00_AXI_awvalid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    M01_ACLK,
    M01_ARESETN,
    M01_AXI_araddr,
    M01_AXI_arprot,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awprot,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wstrb,
    M01_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arprot,
    S00_AXI_arready,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awprot,
    S00_AXI_awready,
    S00_AXI_awvalid,
    S00_AXI_bready,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rready,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input ARESETN;
  input M00_ACLK;
  input M00_ARESETN;
  output [16:0]M00_AXI_araddr;
  output [2:0]M00_AXI_arprot;
  input M00_AXI_arready;
  output M00_AXI_arvalid;
  output [16:0]M00_AXI_awaddr;
  output [2:0]M00_AXI_awprot;
  input M00_AXI_awready;
  output M00_AXI_awvalid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  input M01_ACLK;
  input M01_ARESETN;
  output [31:0]M01_AXI_araddr;
  output [2:0]M01_AXI_arprot;
  input [0:0]M01_AXI_arready;
  output [0:0]M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  output [2:0]M01_AXI_awprot;
  input [0:0]M01_AXI_awready;
  output [0:0]M01_AXI_awvalid;
  output [0:0]M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input [0:0]M01_AXI_bvalid;
  input [63:0]M01_AXI_rdata;
  output [0:0]M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input [0:0]M01_AXI_rvalid;
  output [63:0]M01_AXI_wdata;
  input [0:0]M01_AXI_wready;
  output [7:0]M01_AXI_wstrb;
  output [0:0]M01_AXI_wvalid;
  input S00_ACLK;
  input S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [2:0]S00_AXI_arprot;
  output S00_AXI_arready;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [2:0]S00_AXI_awprot;
  output S00_AXI_awready;
  input S00_AXI_awvalid;
  input S00_AXI_bready;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  input S00_AXI_rready;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire [16:0]m00_couplers_to_psDramInterface_ARADDR;
  wire [2:0]m00_couplers_to_psDramInterface_ARPROT;
  wire m00_couplers_to_psDramInterface_ARREADY;
  wire m00_couplers_to_psDramInterface_ARVALID;
  wire [16:0]m00_couplers_to_psDramInterface_AWADDR;
  wire [2:0]m00_couplers_to_psDramInterface_AWPROT;
  wire m00_couplers_to_psDramInterface_AWREADY;
  wire m00_couplers_to_psDramInterface_AWVALID;
  wire m00_couplers_to_psDramInterface_BREADY;
  wire [1:0]m00_couplers_to_psDramInterface_BRESP;
  wire m00_couplers_to_psDramInterface_BVALID;
  wire [31:0]m00_couplers_to_psDramInterface_RDATA;
  wire m00_couplers_to_psDramInterface_RREADY;
  wire [1:0]m00_couplers_to_psDramInterface_RRESP;
  wire m00_couplers_to_psDramInterface_RVALID;
  wire [31:0]m00_couplers_to_psDramInterface_WDATA;
  wire m00_couplers_to_psDramInterface_WREADY;
  wire [3:0]m00_couplers_to_psDramInterface_WSTRB;
  wire m00_couplers_to_psDramInterface_WVALID;
  wire [31:0]m01_couplers_to_psDramInterface_ARADDR;
  wire [2:0]m01_couplers_to_psDramInterface_ARPROT;
  wire [0:0]m01_couplers_to_psDramInterface_ARREADY;
  wire [0:0]m01_couplers_to_psDramInterface_ARVALID;
  wire [31:0]m01_couplers_to_psDramInterface_AWADDR;
  wire [2:0]m01_couplers_to_psDramInterface_AWPROT;
  wire [0:0]m01_couplers_to_psDramInterface_AWREADY;
  wire [0:0]m01_couplers_to_psDramInterface_AWVALID;
  wire [0:0]m01_couplers_to_psDramInterface_BREADY;
  wire [1:0]m01_couplers_to_psDramInterface_BRESP;
  wire [0:0]m01_couplers_to_psDramInterface_BVALID;
  wire [63:0]m01_couplers_to_psDramInterface_RDATA;
  wire [0:0]m01_couplers_to_psDramInterface_RREADY;
  wire [1:0]m01_couplers_to_psDramInterface_RRESP;
  wire [0:0]m01_couplers_to_psDramInterface_RVALID;
  wire [63:0]m01_couplers_to_psDramInterface_WDATA;
  wire [0:0]m01_couplers_to_psDramInterface_WREADY;
  wire [7:0]m01_couplers_to_psDramInterface_WSTRB;
  wire [0:0]m01_couplers_to_psDramInterface_WVALID;
  wire psDramInterface_ACLK_net;
  wire psDramInterface_ARESETN_net;
  wire [31:0]psDramInterface_to_s00_couplers_ARADDR;
  wire [2:0]psDramInterface_to_s00_couplers_ARPROT;
  wire psDramInterface_to_s00_couplers_ARREADY;
  wire psDramInterface_to_s00_couplers_ARVALID;
  wire [31:0]psDramInterface_to_s00_couplers_AWADDR;
  wire [2:0]psDramInterface_to_s00_couplers_AWPROT;
  wire psDramInterface_to_s00_couplers_AWREADY;
  wire psDramInterface_to_s00_couplers_AWVALID;
  wire psDramInterface_to_s00_couplers_BREADY;
  wire psDramInterface_to_s00_couplers_BVALID;
  wire [31:0]psDramInterface_to_s00_couplers_RDATA;
  wire psDramInterface_to_s00_couplers_RREADY;
  wire psDramInterface_to_s00_couplers_RVALID;
  wire [31:0]psDramInterface_to_s00_couplers_WDATA;
  wire psDramInterface_to_s00_couplers_WREADY;
  wire [3:0]psDramInterface_to_s00_couplers_WSTRB;
  wire psDramInterface_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_xbar_ARADDR;
  wire [2:0]s00_couplers_to_xbar_ARPROT;
  wire [0:0]s00_couplers_to_xbar_ARREADY;
  wire s00_couplers_to_xbar_ARVALID;
  wire [31:0]s00_couplers_to_xbar_AWADDR;
  wire [2:0]s00_couplers_to_xbar_AWPROT;
  wire [0:0]s00_couplers_to_xbar_AWREADY;
  wire s00_couplers_to_xbar_AWVALID;
  wire s00_couplers_to_xbar_BREADY;
  wire [1:0]s00_couplers_to_xbar_BRESP;
  wire [0:0]s00_couplers_to_xbar_BVALID;
  wire [63:0]s00_couplers_to_xbar_RDATA;
  wire s00_couplers_to_xbar_RREADY;
  wire [1:0]s00_couplers_to_xbar_RRESP;
  wire [0:0]s00_couplers_to_xbar_RVALID;
  wire [63:0]s00_couplers_to_xbar_WDATA;
  wire [0:0]s00_couplers_to_xbar_WREADY;
  wire [7:0]s00_couplers_to_xbar_WSTRB;
  wire s00_couplers_to_xbar_WVALID;
  wire [31:0]xbar_to_m00_couplers_ARADDR;
  wire [2:0]xbar_to_m00_couplers_ARPROT;
  wire xbar_to_m00_couplers_ARREADY;
  wire [0:0]xbar_to_m00_couplers_ARVALID;
  wire [31:0]xbar_to_m00_couplers_AWADDR;
  wire [2:0]xbar_to_m00_couplers_AWPROT;
  wire xbar_to_m00_couplers_AWREADY;
  wire [0:0]xbar_to_m00_couplers_AWVALID;
  wire [0:0]xbar_to_m00_couplers_BREADY;
  wire [1:0]xbar_to_m00_couplers_BRESP;
  wire xbar_to_m00_couplers_BVALID;
  wire [63:0]xbar_to_m00_couplers_RDATA;
  wire [0:0]xbar_to_m00_couplers_RREADY;
  wire [1:0]xbar_to_m00_couplers_RRESP;
  wire xbar_to_m00_couplers_RVALID;
  wire [63:0]xbar_to_m00_couplers_WDATA;
  wire xbar_to_m00_couplers_WREADY;
  wire [7:0]xbar_to_m00_couplers_WSTRB;
  wire [0:0]xbar_to_m00_couplers_WVALID;
  wire [63:32]xbar_to_m01_couplers_ARADDR;
  wire [5:3]xbar_to_m01_couplers_ARPROT;
  wire [0:0]xbar_to_m01_couplers_ARREADY;
  wire [1:1]xbar_to_m01_couplers_ARVALID;
  wire [63:32]xbar_to_m01_couplers_AWADDR;
  wire [5:3]xbar_to_m01_couplers_AWPROT;
  wire [0:0]xbar_to_m01_couplers_AWREADY;
  wire [1:1]xbar_to_m01_couplers_AWVALID;
  wire [1:1]xbar_to_m01_couplers_BREADY;
  wire [1:0]xbar_to_m01_couplers_BRESP;
  wire [0:0]xbar_to_m01_couplers_BVALID;
  wire [63:0]xbar_to_m01_couplers_RDATA;
  wire [1:1]xbar_to_m01_couplers_RREADY;
  wire [1:0]xbar_to_m01_couplers_RRESP;
  wire [0:0]xbar_to_m01_couplers_RVALID;
  wire [127:64]xbar_to_m01_couplers_WDATA;
  wire [0:0]xbar_to_m01_couplers_WREADY;
  wire [15:8]xbar_to_m01_couplers_WSTRB;
  wire [1:1]xbar_to_m01_couplers_WVALID;

  assign M00_AXI_araddr[16:0] = m00_couplers_to_psDramInterface_ARADDR;
  assign M00_AXI_arprot[2:0] = m00_couplers_to_psDramInterface_ARPROT;
  assign M00_AXI_arvalid = m00_couplers_to_psDramInterface_ARVALID;
  assign M00_AXI_awaddr[16:0] = m00_couplers_to_psDramInterface_AWADDR;
  assign M00_AXI_awprot[2:0] = m00_couplers_to_psDramInterface_AWPROT;
  assign M00_AXI_awvalid = m00_couplers_to_psDramInterface_AWVALID;
  assign M00_AXI_bready = m00_couplers_to_psDramInterface_BREADY;
  assign M00_AXI_rready = m00_couplers_to_psDramInterface_RREADY;
  assign M00_AXI_wdata[31:0] = m00_couplers_to_psDramInterface_WDATA;
  assign M00_AXI_wstrb[3:0] = m00_couplers_to_psDramInterface_WSTRB;
  assign M00_AXI_wvalid = m00_couplers_to_psDramInterface_WVALID;
  assign M01_AXI_araddr[31:0] = m01_couplers_to_psDramInterface_ARADDR;
  assign M01_AXI_arprot[2:0] = m01_couplers_to_psDramInterface_ARPROT;
  assign M01_AXI_arvalid[0] = m01_couplers_to_psDramInterface_ARVALID;
  assign M01_AXI_awaddr[31:0] = m01_couplers_to_psDramInterface_AWADDR;
  assign M01_AXI_awprot[2:0] = m01_couplers_to_psDramInterface_AWPROT;
  assign M01_AXI_awvalid[0] = m01_couplers_to_psDramInterface_AWVALID;
  assign M01_AXI_bready[0] = m01_couplers_to_psDramInterface_BREADY;
  assign M01_AXI_rready[0] = m01_couplers_to_psDramInterface_RREADY;
  assign M01_AXI_wdata[63:0] = m01_couplers_to_psDramInterface_WDATA;
  assign M01_AXI_wstrb[7:0] = m01_couplers_to_psDramInterface_WSTRB;
  assign M01_AXI_wvalid[0] = m01_couplers_to_psDramInterface_WVALID;
  assign S00_AXI_arready = psDramInterface_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = psDramInterface_to_s00_couplers_AWREADY;
  assign S00_AXI_bvalid = psDramInterface_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = psDramInterface_to_s00_couplers_RDATA;
  assign S00_AXI_rvalid = psDramInterface_to_s00_couplers_RVALID;
  assign S00_AXI_wready = psDramInterface_to_s00_couplers_WREADY;
  assign m00_couplers_to_psDramInterface_ARREADY = M00_AXI_arready;
  assign m00_couplers_to_psDramInterface_AWREADY = M00_AXI_awready;
  assign m00_couplers_to_psDramInterface_BRESP = M00_AXI_bresp[1:0];
  assign m00_couplers_to_psDramInterface_BVALID = M00_AXI_bvalid;
  assign m00_couplers_to_psDramInterface_RDATA = M00_AXI_rdata[31:0];
  assign m00_couplers_to_psDramInterface_RRESP = M00_AXI_rresp[1:0];
  assign m00_couplers_to_psDramInterface_RVALID = M00_AXI_rvalid;
  assign m00_couplers_to_psDramInterface_WREADY = M00_AXI_wready;
  assign m01_couplers_to_psDramInterface_ARREADY = M01_AXI_arready[0];
  assign m01_couplers_to_psDramInterface_AWREADY = M01_AXI_awready[0];
  assign m01_couplers_to_psDramInterface_BRESP = M01_AXI_bresp[1:0];
  assign m01_couplers_to_psDramInterface_BVALID = M01_AXI_bvalid[0];
  assign m01_couplers_to_psDramInterface_RDATA = M01_AXI_rdata[63:0];
  assign m01_couplers_to_psDramInterface_RRESP = M01_AXI_rresp[1:0];
  assign m01_couplers_to_psDramInterface_RVALID = M01_AXI_rvalid[0];
  assign m01_couplers_to_psDramInterface_WREADY = M01_AXI_wready[0];
  assign psDramInterface_ACLK_net = ACLK;
  assign psDramInterface_ARESETN_net = ARESETN;
  assign psDramInterface_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign psDramInterface_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign psDramInterface_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign psDramInterface_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign psDramInterface_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign psDramInterface_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign psDramInterface_to_s00_couplers_BREADY = S00_AXI_bready;
  assign psDramInterface_to_s00_couplers_RREADY = S00_AXI_rready;
  assign psDramInterface_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign psDramInterface_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign psDramInterface_to_s00_couplers_WVALID = S00_AXI_wvalid;
  m00_couplers_imp_4W37PH m00_couplers
       (.M_ACLK(psDramInterface_ACLK_net),
        .M_ARESETN(psDramInterface_ARESETN_net),
        .M_AXI_araddr(m00_couplers_to_psDramInterface_ARADDR),
        .M_AXI_arprot(m00_couplers_to_psDramInterface_ARPROT),
        .M_AXI_arready(m00_couplers_to_psDramInterface_ARREADY),
        .M_AXI_arvalid(m00_couplers_to_psDramInterface_ARVALID),
        .M_AXI_awaddr(m00_couplers_to_psDramInterface_AWADDR),
        .M_AXI_awprot(m00_couplers_to_psDramInterface_AWPROT),
        .M_AXI_awready(m00_couplers_to_psDramInterface_AWREADY),
        .M_AXI_awvalid(m00_couplers_to_psDramInterface_AWVALID),
        .M_AXI_bready(m00_couplers_to_psDramInterface_BREADY),
        .M_AXI_bresp(m00_couplers_to_psDramInterface_BRESP),
        .M_AXI_bvalid(m00_couplers_to_psDramInterface_BVALID),
        .M_AXI_rdata(m00_couplers_to_psDramInterface_RDATA),
        .M_AXI_rready(m00_couplers_to_psDramInterface_RREADY),
        .M_AXI_rresp(m00_couplers_to_psDramInterface_RRESP),
        .M_AXI_rvalid(m00_couplers_to_psDramInterface_RVALID),
        .M_AXI_wdata(m00_couplers_to_psDramInterface_WDATA),
        .M_AXI_wready(m00_couplers_to_psDramInterface_WREADY),
        .M_AXI_wstrb(m00_couplers_to_psDramInterface_WSTRB),
        .M_AXI_wvalid(m00_couplers_to_psDramInterface_WVALID),
        .S_ACLK(psDramInterface_ACLK_net),
        .S_ARESETN(psDramInterface_ARESETN_net),
        .S_AXI_araddr(xbar_to_m00_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m00_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m00_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m00_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m00_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m00_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m00_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m00_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m00_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m00_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m00_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m00_couplers_RDATA),
        .S_AXI_rready(xbar_to_m00_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m00_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m00_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m00_couplers_WDATA),
        .S_AXI_wready(xbar_to_m00_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m00_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m00_couplers_WVALID));
  m01_couplers_imp_1YQL89W m01_couplers
       (.M_ACLK(psDramInterface_ACLK_net),
        .M_ARESETN(psDramInterface_ARESETN_net),
        .M_AXI_araddr(m01_couplers_to_psDramInterface_ARADDR),
        .M_AXI_arprot(m01_couplers_to_psDramInterface_ARPROT),
        .M_AXI_arready(m01_couplers_to_psDramInterface_ARREADY),
        .M_AXI_arvalid(m01_couplers_to_psDramInterface_ARVALID),
        .M_AXI_awaddr(m01_couplers_to_psDramInterface_AWADDR),
        .M_AXI_awprot(m01_couplers_to_psDramInterface_AWPROT),
        .M_AXI_awready(m01_couplers_to_psDramInterface_AWREADY),
        .M_AXI_awvalid(m01_couplers_to_psDramInterface_AWVALID),
        .M_AXI_bready(m01_couplers_to_psDramInterface_BREADY),
        .M_AXI_bresp(m01_couplers_to_psDramInterface_BRESP),
        .M_AXI_bvalid(m01_couplers_to_psDramInterface_BVALID),
        .M_AXI_rdata(m01_couplers_to_psDramInterface_RDATA),
        .M_AXI_rready(m01_couplers_to_psDramInterface_RREADY),
        .M_AXI_rresp(m01_couplers_to_psDramInterface_RRESP),
        .M_AXI_rvalid(m01_couplers_to_psDramInterface_RVALID),
        .M_AXI_wdata(m01_couplers_to_psDramInterface_WDATA),
        .M_AXI_wready(m01_couplers_to_psDramInterface_WREADY),
        .M_AXI_wstrb(m01_couplers_to_psDramInterface_WSTRB),
        .M_AXI_wvalid(m01_couplers_to_psDramInterface_WVALID),
        .S_ACLK(psDramInterface_ACLK_net),
        .S_ARESETN(psDramInterface_ARESETN_net),
        .S_AXI_araddr(xbar_to_m01_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m01_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m01_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m01_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m01_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m01_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m01_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m01_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m01_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m01_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m01_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m01_couplers_RDATA),
        .S_AXI_rready(xbar_to_m01_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m01_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m01_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m01_couplers_WDATA),
        .S_AXI_wready(xbar_to_m01_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m01_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m01_couplers_WVALID));
  s00_couplers_imp_1SJJVK7 s00_couplers
       (.M_ACLK(psDramInterface_ACLK_net),
        .M_ARESETN(psDramInterface_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_xbar_ARADDR),
        .M_AXI_arprot(s00_couplers_to_xbar_ARPROT),
        .M_AXI_arready(s00_couplers_to_xbar_ARREADY),
        .M_AXI_arvalid(s00_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_xbar_AWADDR),
        .M_AXI_awprot(s00_couplers_to_xbar_AWPROT),
        .M_AXI_awready(s00_couplers_to_xbar_AWREADY),
        .M_AXI_awvalid(s00_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s00_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s00_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s00_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s00_couplers_to_xbar_RDATA),
        .M_AXI_rready(s00_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s00_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s00_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s00_couplers_to_xbar_WDATA),
        .M_AXI_wready(s00_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s00_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_xbar_WVALID),
        .S_ACLK(psDramInterface_ACLK_net),
        .S_ARESETN(psDramInterface_ARESETN_net),
        .S_AXI_araddr(psDramInterface_to_s00_couplers_ARADDR),
        .S_AXI_arprot(psDramInterface_to_s00_couplers_ARPROT),
        .S_AXI_arready(psDramInterface_to_s00_couplers_ARREADY),
        .S_AXI_arvalid(psDramInterface_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(psDramInterface_to_s00_couplers_AWADDR),
        .S_AXI_awprot(psDramInterface_to_s00_couplers_AWPROT),
        .S_AXI_awready(psDramInterface_to_s00_couplers_AWREADY),
        .S_AXI_awvalid(psDramInterface_to_s00_couplers_AWVALID),
        .S_AXI_bready(psDramInterface_to_s00_couplers_BREADY),
        .S_AXI_bvalid(psDramInterface_to_s00_couplers_BVALID),
        .S_AXI_rdata(psDramInterface_to_s00_couplers_RDATA),
        .S_AXI_rready(psDramInterface_to_s00_couplers_RREADY),
        .S_AXI_rvalid(psDramInterface_to_s00_couplers_RVALID),
        .S_AXI_wdata(psDramInterface_to_s00_couplers_WDATA),
        .S_AXI_wready(psDramInterface_to_s00_couplers_WREADY),
        .S_AXI_wstrb(psDramInterface_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(psDramInterface_to_s00_couplers_WVALID));
  cerberus_xbar_2 xbar
       (.aclk(psDramInterface_ACLK_net),
        .aresetn(psDramInterface_ARESETN_net),
        .m_axi_araddr({xbar_to_m01_couplers_ARADDR,xbar_to_m00_couplers_ARADDR}),
        .m_axi_arprot({xbar_to_m01_couplers_ARPROT,xbar_to_m00_couplers_ARPROT}),
        .m_axi_arready({xbar_to_m01_couplers_ARREADY,xbar_to_m00_couplers_ARREADY}),
        .m_axi_arvalid({xbar_to_m01_couplers_ARVALID,xbar_to_m00_couplers_ARVALID}),
        .m_axi_awaddr({xbar_to_m01_couplers_AWADDR,xbar_to_m00_couplers_AWADDR}),
        .m_axi_awprot({xbar_to_m01_couplers_AWPROT,xbar_to_m00_couplers_AWPROT}),
        .m_axi_awready({xbar_to_m01_couplers_AWREADY,xbar_to_m00_couplers_AWREADY}),
        .m_axi_awvalid({xbar_to_m01_couplers_AWVALID,xbar_to_m00_couplers_AWVALID}),
        .m_axi_bready({xbar_to_m01_couplers_BREADY,xbar_to_m00_couplers_BREADY}),
        .m_axi_bresp({xbar_to_m01_couplers_BRESP,xbar_to_m00_couplers_BRESP}),
        .m_axi_bvalid({xbar_to_m01_couplers_BVALID,xbar_to_m00_couplers_BVALID}),
        .m_axi_rdata({xbar_to_m01_couplers_RDATA,xbar_to_m00_couplers_RDATA}),
        .m_axi_rready({xbar_to_m01_couplers_RREADY,xbar_to_m00_couplers_RREADY}),
        .m_axi_rresp({xbar_to_m01_couplers_RRESP,xbar_to_m00_couplers_RRESP}),
        .m_axi_rvalid({xbar_to_m01_couplers_RVALID,xbar_to_m00_couplers_RVALID}),
        .m_axi_wdata({xbar_to_m01_couplers_WDATA,xbar_to_m00_couplers_WDATA}),
        .m_axi_wready({xbar_to_m01_couplers_WREADY,xbar_to_m00_couplers_WREADY}),
        .m_axi_wstrb({xbar_to_m01_couplers_WSTRB,xbar_to_m00_couplers_WSTRB}),
        .m_axi_wvalid({xbar_to_m01_couplers_WVALID,xbar_to_m00_couplers_WVALID}),
        .s_axi_araddr(s00_couplers_to_xbar_ARADDR),
        .s_axi_arprot(s00_couplers_to_xbar_ARPROT),
        .s_axi_arready(s00_couplers_to_xbar_ARREADY),
        .s_axi_arvalid(s00_couplers_to_xbar_ARVALID),
        .s_axi_awaddr(s00_couplers_to_xbar_AWADDR),
        .s_axi_awprot(s00_couplers_to_xbar_AWPROT),
        .s_axi_awready(s00_couplers_to_xbar_AWREADY),
        .s_axi_awvalid(s00_couplers_to_xbar_AWVALID),
        .s_axi_bready(s00_couplers_to_xbar_BREADY),
        .s_axi_bresp(s00_couplers_to_xbar_BRESP),
        .s_axi_bvalid(s00_couplers_to_xbar_BVALID),
        .s_axi_rdata(s00_couplers_to_xbar_RDATA),
        .s_axi_rready(s00_couplers_to_xbar_RREADY),
        .s_axi_rresp(s00_couplers_to_xbar_RRESP),
        .s_axi_rvalid(s00_couplers_to_xbar_RVALID),
        .s_axi_wdata(s00_couplers_to_xbar_WDATA),
        .s_axi_wready(s00_couplers_to_xbar_WREADY),
        .s_axi_wstrb(s00_couplers_to_xbar_WSTRB),
        .s_axi_wvalid(s00_couplers_to_xbar_WVALID));
endmodule

module cerberus_psDramInterface_1
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arprot,
    M00_AXI_arready,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awprot,
    M00_AXI_awready,
    M00_AXI_awvalid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    M01_ACLK,
    M01_ARESETN,
    M01_AXI_araddr,
    M01_AXI_arprot,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awprot,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wstrb,
    M01_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arprot,
    S00_AXI_arready,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awprot,
    S00_AXI_awready,
    S00_AXI_awvalid,
    S00_AXI_bready,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rready,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input ARESETN;
  input M00_ACLK;
  input M00_ARESETN;
  output [16:0]M00_AXI_araddr;
  output [2:0]M00_AXI_arprot;
  input M00_AXI_arready;
  output M00_AXI_arvalid;
  output [16:0]M00_AXI_awaddr;
  output [2:0]M00_AXI_awprot;
  input M00_AXI_awready;
  output M00_AXI_awvalid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  input M01_ACLK;
  input M01_ARESETN;
  output [31:0]M01_AXI_araddr;
  output [2:0]M01_AXI_arprot;
  input [0:0]M01_AXI_arready;
  output [0:0]M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  output [2:0]M01_AXI_awprot;
  input [0:0]M01_AXI_awready;
  output [0:0]M01_AXI_awvalid;
  output [0:0]M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input [0:0]M01_AXI_bvalid;
  input [63:0]M01_AXI_rdata;
  output [0:0]M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input [0:0]M01_AXI_rvalid;
  output [63:0]M01_AXI_wdata;
  input [0:0]M01_AXI_wready;
  output [7:0]M01_AXI_wstrb;
  output [0:0]M01_AXI_wvalid;
  input S00_ACLK;
  input S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [2:0]S00_AXI_arprot;
  output S00_AXI_arready;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [2:0]S00_AXI_awprot;
  output S00_AXI_awready;
  input S00_AXI_awvalid;
  input S00_AXI_bready;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  input S00_AXI_rready;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire [16:0]m00_couplers_to_psDramInterface_ARADDR;
  wire [2:0]m00_couplers_to_psDramInterface_ARPROT;
  wire m00_couplers_to_psDramInterface_ARREADY;
  wire m00_couplers_to_psDramInterface_ARVALID;
  wire [16:0]m00_couplers_to_psDramInterface_AWADDR;
  wire [2:0]m00_couplers_to_psDramInterface_AWPROT;
  wire m00_couplers_to_psDramInterface_AWREADY;
  wire m00_couplers_to_psDramInterface_AWVALID;
  wire m00_couplers_to_psDramInterface_BREADY;
  wire [1:0]m00_couplers_to_psDramInterface_BRESP;
  wire m00_couplers_to_psDramInterface_BVALID;
  wire [31:0]m00_couplers_to_psDramInterface_RDATA;
  wire m00_couplers_to_psDramInterface_RREADY;
  wire [1:0]m00_couplers_to_psDramInterface_RRESP;
  wire m00_couplers_to_psDramInterface_RVALID;
  wire [31:0]m00_couplers_to_psDramInterface_WDATA;
  wire m00_couplers_to_psDramInterface_WREADY;
  wire [3:0]m00_couplers_to_psDramInterface_WSTRB;
  wire m00_couplers_to_psDramInterface_WVALID;
  wire [31:0]m01_couplers_to_psDramInterface_ARADDR;
  wire [2:0]m01_couplers_to_psDramInterface_ARPROT;
  wire [0:0]m01_couplers_to_psDramInterface_ARREADY;
  wire [0:0]m01_couplers_to_psDramInterface_ARVALID;
  wire [31:0]m01_couplers_to_psDramInterface_AWADDR;
  wire [2:0]m01_couplers_to_psDramInterface_AWPROT;
  wire [0:0]m01_couplers_to_psDramInterface_AWREADY;
  wire [0:0]m01_couplers_to_psDramInterface_AWVALID;
  wire [0:0]m01_couplers_to_psDramInterface_BREADY;
  wire [1:0]m01_couplers_to_psDramInterface_BRESP;
  wire [0:0]m01_couplers_to_psDramInterface_BVALID;
  wire [63:0]m01_couplers_to_psDramInterface_RDATA;
  wire [0:0]m01_couplers_to_psDramInterface_RREADY;
  wire [1:0]m01_couplers_to_psDramInterface_RRESP;
  wire [0:0]m01_couplers_to_psDramInterface_RVALID;
  wire [63:0]m01_couplers_to_psDramInterface_WDATA;
  wire [0:0]m01_couplers_to_psDramInterface_WREADY;
  wire [7:0]m01_couplers_to_psDramInterface_WSTRB;
  wire [0:0]m01_couplers_to_psDramInterface_WVALID;
  wire psDramInterface_ACLK_net;
  wire psDramInterface_ARESETN_net;
  wire [31:0]psDramInterface_to_s00_couplers_ARADDR;
  wire [2:0]psDramInterface_to_s00_couplers_ARPROT;
  wire psDramInterface_to_s00_couplers_ARREADY;
  wire psDramInterface_to_s00_couplers_ARVALID;
  wire [31:0]psDramInterface_to_s00_couplers_AWADDR;
  wire [2:0]psDramInterface_to_s00_couplers_AWPROT;
  wire psDramInterface_to_s00_couplers_AWREADY;
  wire psDramInterface_to_s00_couplers_AWVALID;
  wire psDramInterface_to_s00_couplers_BREADY;
  wire psDramInterface_to_s00_couplers_BVALID;
  wire [31:0]psDramInterface_to_s00_couplers_RDATA;
  wire psDramInterface_to_s00_couplers_RREADY;
  wire psDramInterface_to_s00_couplers_RVALID;
  wire [31:0]psDramInterface_to_s00_couplers_WDATA;
  wire psDramInterface_to_s00_couplers_WREADY;
  wire [3:0]psDramInterface_to_s00_couplers_WSTRB;
  wire psDramInterface_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_xbar_ARADDR;
  wire [2:0]s00_couplers_to_xbar_ARPROT;
  wire [0:0]s00_couplers_to_xbar_ARREADY;
  wire s00_couplers_to_xbar_ARVALID;
  wire [31:0]s00_couplers_to_xbar_AWADDR;
  wire [2:0]s00_couplers_to_xbar_AWPROT;
  wire [0:0]s00_couplers_to_xbar_AWREADY;
  wire s00_couplers_to_xbar_AWVALID;
  wire s00_couplers_to_xbar_BREADY;
  wire [1:0]s00_couplers_to_xbar_BRESP;
  wire [0:0]s00_couplers_to_xbar_BVALID;
  wire [63:0]s00_couplers_to_xbar_RDATA;
  wire s00_couplers_to_xbar_RREADY;
  wire [1:0]s00_couplers_to_xbar_RRESP;
  wire [0:0]s00_couplers_to_xbar_RVALID;
  wire [63:0]s00_couplers_to_xbar_WDATA;
  wire [0:0]s00_couplers_to_xbar_WREADY;
  wire [7:0]s00_couplers_to_xbar_WSTRB;
  wire s00_couplers_to_xbar_WVALID;
  wire [31:0]xbar_to_m00_couplers_ARADDR;
  wire [2:0]xbar_to_m00_couplers_ARPROT;
  wire xbar_to_m00_couplers_ARREADY;
  wire [0:0]xbar_to_m00_couplers_ARVALID;
  wire [31:0]xbar_to_m00_couplers_AWADDR;
  wire [2:0]xbar_to_m00_couplers_AWPROT;
  wire xbar_to_m00_couplers_AWREADY;
  wire [0:0]xbar_to_m00_couplers_AWVALID;
  wire [0:0]xbar_to_m00_couplers_BREADY;
  wire [1:0]xbar_to_m00_couplers_BRESP;
  wire xbar_to_m00_couplers_BVALID;
  wire [63:0]xbar_to_m00_couplers_RDATA;
  wire [0:0]xbar_to_m00_couplers_RREADY;
  wire [1:0]xbar_to_m00_couplers_RRESP;
  wire xbar_to_m00_couplers_RVALID;
  wire [63:0]xbar_to_m00_couplers_WDATA;
  wire xbar_to_m00_couplers_WREADY;
  wire [7:0]xbar_to_m00_couplers_WSTRB;
  wire [0:0]xbar_to_m00_couplers_WVALID;
  wire [63:32]xbar_to_m01_couplers_ARADDR;
  wire [5:3]xbar_to_m01_couplers_ARPROT;
  wire [0:0]xbar_to_m01_couplers_ARREADY;
  wire [1:1]xbar_to_m01_couplers_ARVALID;
  wire [63:32]xbar_to_m01_couplers_AWADDR;
  wire [5:3]xbar_to_m01_couplers_AWPROT;
  wire [0:0]xbar_to_m01_couplers_AWREADY;
  wire [1:1]xbar_to_m01_couplers_AWVALID;
  wire [1:1]xbar_to_m01_couplers_BREADY;
  wire [1:0]xbar_to_m01_couplers_BRESP;
  wire [0:0]xbar_to_m01_couplers_BVALID;
  wire [63:0]xbar_to_m01_couplers_RDATA;
  wire [1:1]xbar_to_m01_couplers_RREADY;
  wire [1:0]xbar_to_m01_couplers_RRESP;
  wire [0:0]xbar_to_m01_couplers_RVALID;
  wire [127:64]xbar_to_m01_couplers_WDATA;
  wire [0:0]xbar_to_m01_couplers_WREADY;
  wire [15:8]xbar_to_m01_couplers_WSTRB;
  wire [1:1]xbar_to_m01_couplers_WVALID;

  assign M00_AXI_araddr[16:0] = m00_couplers_to_psDramInterface_ARADDR;
  assign M00_AXI_arprot[2:0] = m00_couplers_to_psDramInterface_ARPROT;
  assign M00_AXI_arvalid = m00_couplers_to_psDramInterface_ARVALID;
  assign M00_AXI_awaddr[16:0] = m00_couplers_to_psDramInterface_AWADDR;
  assign M00_AXI_awprot[2:0] = m00_couplers_to_psDramInterface_AWPROT;
  assign M00_AXI_awvalid = m00_couplers_to_psDramInterface_AWVALID;
  assign M00_AXI_bready = m00_couplers_to_psDramInterface_BREADY;
  assign M00_AXI_rready = m00_couplers_to_psDramInterface_RREADY;
  assign M00_AXI_wdata[31:0] = m00_couplers_to_psDramInterface_WDATA;
  assign M00_AXI_wstrb[3:0] = m00_couplers_to_psDramInterface_WSTRB;
  assign M00_AXI_wvalid = m00_couplers_to_psDramInterface_WVALID;
  assign M01_AXI_araddr[31:0] = m01_couplers_to_psDramInterface_ARADDR;
  assign M01_AXI_arprot[2:0] = m01_couplers_to_psDramInterface_ARPROT;
  assign M01_AXI_arvalid[0] = m01_couplers_to_psDramInterface_ARVALID;
  assign M01_AXI_awaddr[31:0] = m01_couplers_to_psDramInterface_AWADDR;
  assign M01_AXI_awprot[2:0] = m01_couplers_to_psDramInterface_AWPROT;
  assign M01_AXI_awvalid[0] = m01_couplers_to_psDramInterface_AWVALID;
  assign M01_AXI_bready[0] = m01_couplers_to_psDramInterface_BREADY;
  assign M01_AXI_rready[0] = m01_couplers_to_psDramInterface_RREADY;
  assign M01_AXI_wdata[63:0] = m01_couplers_to_psDramInterface_WDATA;
  assign M01_AXI_wstrb[7:0] = m01_couplers_to_psDramInterface_WSTRB;
  assign M01_AXI_wvalid[0] = m01_couplers_to_psDramInterface_WVALID;
  assign S00_AXI_arready = psDramInterface_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = psDramInterface_to_s00_couplers_AWREADY;
  assign S00_AXI_bvalid = psDramInterface_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = psDramInterface_to_s00_couplers_RDATA;
  assign S00_AXI_rvalid = psDramInterface_to_s00_couplers_RVALID;
  assign S00_AXI_wready = psDramInterface_to_s00_couplers_WREADY;
  assign m00_couplers_to_psDramInterface_ARREADY = M00_AXI_arready;
  assign m00_couplers_to_psDramInterface_AWREADY = M00_AXI_awready;
  assign m00_couplers_to_psDramInterface_BRESP = M00_AXI_bresp[1:0];
  assign m00_couplers_to_psDramInterface_BVALID = M00_AXI_bvalid;
  assign m00_couplers_to_psDramInterface_RDATA = M00_AXI_rdata[31:0];
  assign m00_couplers_to_psDramInterface_RRESP = M00_AXI_rresp[1:0];
  assign m00_couplers_to_psDramInterface_RVALID = M00_AXI_rvalid;
  assign m00_couplers_to_psDramInterface_WREADY = M00_AXI_wready;
  assign m01_couplers_to_psDramInterface_ARREADY = M01_AXI_arready[0];
  assign m01_couplers_to_psDramInterface_AWREADY = M01_AXI_awready[0];
  assign m01_couplers_to_psDramInterface_BRESP = M01_AXI_bresp[1:0];
  assign m01_couplers_to_psDramInterface_BVALID = M01_AXI_bvalid[0];
  assign m01_couplers_to_psDramInterface_RDATA = M01_AXI_rdata[63:0];
  assign m01_couplers_to_psDramInterface_RRESP = M01_AXI_rresp[1:0];
  assign m01_couplers_to_psDramInterface_RVALID = M01_AXI_rvalid[0];
  assign m01_couplers_to_psDramInterface_WREADY = M01_AXI_wready[0];
  assign psDramInterface_ACLK_net = ACLK;
  assign psDramInterface_ARESETN_net = ARESETN;
  assign psDramInterface_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign psDramInterface_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign psDramInterface_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign psDramInterface_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign psDramInterface_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign psDramInterface_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign psDramInterface_to_s00_couplers_BREADY = S00_AXI_bready;
  assign psDramInterface_to_s00_couplers_RREADY = S00_AXI_rready;
  assign psDramInterface_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign psDramInterface_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign psDramInterface_to_s00_couplers_WVALID = S00_AXI_wvalid;
  m00_couplers_imp_1M74UV4 m00_couplers
       (.M_ACLK(psDramInterface_ACLK_net),
        .M_ARESETN(psDramInterface_ARESETN_net),
        .M_AXI_araddr(m00_couplers_to_psDramInterface_ARADDR),
        .M_AXI_arprot(m00_couplers_to_psDramInterface_ARPROT),
        .M_AXI_arready(m00_couplers_to_psDramInterface_ARREADY),
        .M_AXI_arvalid(m00_couplers_to_psDramInterface_ARVALID),
        .M_AXI_awaddr(m00_couplers_to_psDramInterface_AWADDR),
        .M_AXI_awprot(m00_couplers_to_psDramInterface_AWPROT),
        .M_AXI_awready(m00_couplers_to_psDramInterface_AWREADY),
        .M_AXI_awvalid(m00_couplers_to_psDramInterface_AWVALID),
        .M_AXI_bready(m00_couplers_to_psDramInterface_BREADY),
        .M_AXI_bresp(m00_couplers_to_psDramInterface_BRESP),
        .M_AXI_bvalid(m00_couplers_to_psDramInterface_BVALID),
        .M_AXI_rdata(m00_couplers_to_psDramInterface_RDATA),
        .M_AXI_rready(m00_couplers_to_psDramInterface_RREADY),
        .M_AXI_rresp(m00_couplers_to_psDramInterface_RRESP),
        .M_AXI_rvalid(m00_couplers_to_psDramInterface_RVALID),
        .M_AXI_wdata(m00_couplers_to_psDramInterface_WDATA),
        .M_AXI_wready(m00_couplers_to_psDramInterface_WREADY),
        .M_AXI_wstrb(m00_couplers_to_psDramInterface_WSTRB),
        .M_AXI_wvalid(m00_couplers_to_psDramInterface_WVALID),
        .S_ACLK(psDramInterface_ACLK_net),
        .S_ARESETN(psDramInterface_ARESETN_net),
        .S_AXI_araddr(xbar_to_m00_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m00_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m00_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m00_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m00_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m00_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m00_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m00_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m00_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m00_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m00_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m00_couplers_RDATA),
        .S_AXI_rready(xbar_to_m00_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m00_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m00_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m00_couplers_WDATA),
        .S_AXI_wready(xbar_to_m00_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m00_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m00_couplers_WVALID));
  m01_couplers_imp_HD0VXD m01_couplers
       (.M_ACLK(psDramInterface_ACLK_net),
        .M_ARESETN(psDramInterface_ARESETN_net),
        .M_AXI_araddr(m01_couplers_to_psDramInterface_ARADDR),
        .M_AXI_arprot(m01_couplers_to_psDramInterface_ARPROT),
        .M_AXI_arready(m01_couplers_to_psDramInterface_ARREADY),
        .M_AXI_arvalid(m01_couplers_to_psDramInterface_ARVALID),
        .M_AXI_awaddr(m01_couplers_to_psDramInterface_AWADDR),
        .M_AXI_awprot(m01_couplers_to_psDramInterface_AWPROT),
        .M_AXI_awready(m01_couplers_to_psDramInterface_AWREADY),
        .M_AXI_awvalid(m01_couplers_to_psDramInterface_AWVALID),
        .M_AXI_bready(m01_couplers_to_psDramInterface_BREADY),
        .M_AXI_bresp(m01_couplers_to_psDramInterface_BRESP),
        .M_AXI_bvalid(m01_couplers_to_psDramInterface_BVALID),
        .M_AXI_rdata(m01_couplers_to_psDramInterface_RDATA),
        .M_AXI_rready(m01_couplers_to_psDramInterface_RREADY),
        .M_AXI_rresp(m01_couplers_to_psDramInterface_RRESP),
        .M_AXI_rvalid(m01_couplers_to_psDramInterface_RVALID),
        .M_AXI_wdata(m01_couplers_to_psDramInterface_WDATA),
        .M_AXI_wready(m01_couplers_to_psDramInterface_WREADY),
        .M_AXI_wstrb(m01_couplers_to_psDramInterface_WSTRB),
        .M_AXI_wvalid(m01_couplers_to_psDramInterface_WVALID),
        .S_ACLK(psDramInterface_ACLK_net),
        .S_ARESETN(psDramInterface_ARESETN_net),
        .S_AXI_araddr(xbar_to_m01_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m01_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m01_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m01_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m01_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m01_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m01_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m01_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m01_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m01_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m01_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m01_couplers_RDATA),
        .S_AXI_rready(xbar_to_m01_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m01_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m01_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m01_couplers_WDATA),
        .S_AXI_wready(xbar_to_m01_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m01_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m01_couplers_WVALID));
  s00_couplers_imp_BDHSLE s00_couplers
       (.M_ACLK(psDramInterface_ACLK_net),
        .M_ARESETN(psDramInterface_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_xbar_ARADDR),
        .M_AXI_arprot(s00_couplers_to_xbar_ARPROT),
        .M_AXI_arready(s00_couplers_to_xbar_ARREADY),
        .M_AXI_arvalid(s00_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_xbar_AWADDR),
        .M_AXI_awprot(s00_couplers_to_xbar_AWPROT),
        .M_AXI_awready(s00_couplers_to_xbar_AWREADY),
        .M_AXI_awvalid(s00_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s00_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s00_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s00_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s00_couplers_to_xbar_RDATA),
        .M_AXI_rready(s00_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s00_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s00_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s00_couplers_to_xbar_WDATA),
        .M_AXI_wready(s00_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s00_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_xbar_WVALID),
        .S_ACLK(psDramInterface_ACLK_net),
        .S_ARESETN(psDramInterface_ARESETN_net),
        .S_AXI_araddr(psDramInterface_to_s00_couplers_ARADDR),
        .S_AXI_arprot(psDramInterface_to_s00_couplers_ARPROT),
        .S_AXI_arready(psDramInterface_to_s00_couplers_ARREADY),
        .S_AXI_arvalid(psDramInterface_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(psDramInterface_to_s00_couplers_AWADDR),
        .S_AXI_awprot(psDramInterface_to_s00_couplers_AWPROT),
        .S_AXI_awready(psDramInterface_to_s00_couplers_AWREADY),
        .S_AXI_awvalid(psDramInterface_to_s00_couplers_AWVALID),
        .S_AXI_bready(psDramInterface_to_s00_couplers_BREADY),
        .S_AXI_bvalid(psDramInterface_to_s00_couplers_BVALID),
        .S_AXI_rdata(psDramInterface_to_s00_couplers_RDATA),
        .S_AXI_rready(psDramInterface_to_s00_couplers_RREADY),
        .S_AXI_rvalid(psDramInterface_to_s00_couplers_RVALID),
        .S_AXI_wdata(psDramInterface_to_s00_couplers_WDATA),
        .S_AXI_wready(psDramInterface_to_s00_couplers_WREADY),
        .S_AXI_wstrb(psDramInterface_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(psDramInterface_to_s00_couplers_WVALID));
  cerberus_xbar_4 xbar
       (.aclk(psDramInterface_ACLK_net),
        .aresetn(psDramInterface_ARESETN_net),
        .m_axi_araddr({xbar_to_m01_couplers_ARADDR,xbar_to_m00_couplers_ARADDR}),
        .m_axi_arprot({xbar_to_m01_couplers_ARPROT,xbar_to_m00_couplers_ARPROT}),
        .m_axi_arready({xbar_to_m01_couplers_ARREADY,xbar_to_m00_couplers_ARREADY}),
        .m_axi_arvalid({xbar_to_m01_couplers_ARVALID,xbar_to_m00_couplers_ARVALID}),
        .m_axi_awaddr({xbar_to_m01_couplers_AWADDR,xbar_to_m00_couplers_AWADDR}),
        .m_axi_awprot({xbar_to_m01_couplers_AWPROT,xbar_to_m00_couplers_AWPROT}),
        .m_axi_awready({xbar_to_m01_couplers_AWREADY,xbar_to_m00_couplers_AWREADY}),
        .m_axi_awvalid({xbar_to_m01_couplers_AWVALID,xbar_to_m00_couplers_AWVALID}),
        .m_axi_bready({xbar_to_m01_couplers_BREADY,xbar_to_m00_couplers_BREADY}),
        .m_axi_bresp({xbar_to_m01_couplers_BRESP,xbar_to_m00_couplers_BRESP}),
        .m_axi_bvalid({xbar_to_m01_couplers_BVALID,xbar_to_m00_couplers_BVALID}),
        .m_axi_rdata({xbar_to_m01_couplers_RDATA,xbar_to_m00_couplers_RDATA}),
        .m_axi_rready({xbar_to_m01_couplers_RREADY,xbar_to_m00_couplers_RREADY}),
        .m_axi_rresp({xbar_to_m01_couplers_RRESP,xbar_to_m00_couplers_RRESP}),
        .m_axi_rvalid({xbar_to_m01_couplers_RVALID,xbar_to_m00_couplers_RVALID}),
        .m_axi_wdata({xbar_to_m01_couplers_WDATA,xbar_to_m00_couplers_WDATA}),
        .m_axi_wready({xbar_to_m01_couplers_WREADY,xbar_to_m00_couplers_WREADY}),
        .m_axi_wstrb({xbar_to_m01_couplers_WSTRB,xbar_to_m00_couplers_WSTRB}),
        .m_axi_wvalid({xbar_to_m01_couplers_WVALID,xbar_to_m00_couplers_WVALID}),
        .s_axi_araddr(s00_couplers_to_xbar_ARADDR),
        .s_axi_arprot(s00_couplers_to_xbar_ARPROT),
        .s_axi_arready(s00_couplers_to_xbar_ARREADY),
        .s_axi_arvalid(s00_couplers_to_xbar_ARVALID),
        .s_axi_awaddr(s00_couplers_to_xbar_AWADDR),
        .s_axi_awprot(s00_couplers_to_xbar_AWPROT),
        .s_axi_awready(s00_couplers_to_xbar_AWREADY),
        .s_axi_awvalid(s00_couplers_to_xbar_AWVALID),
        .s_axi_bready(s00_couplers_to_xbar_BREADY),
        .s_axi_bresp(s00_couplers_to_xbar_BRESP),
        .s_axi_bvalid(s00_couplers_to_xbar_BVALID),
        .s_axi_rdata(s00_couplers_to_xbar_RDATA),
        .s_axi_rready(s00_couplers_to_xbar_RREADY),
        .s_axi_rresp(s00_couplers_to_xbar_RRESP),
        .s_axi_rvalid(s00_couplers_to_xbar_RVALID),
        .s_axi_wdata(s00_couplers_to_xbar_WDATA),
        .s_axi_wready(s00_couplers_to_xbar_WREADY),
        .s_axi_wstrb(s00_couplers_to_xbar_WSTRB),
        .s_axi_wvalid(s00_couplers_to_xbar_WVALID));
endmodule

module cerberus_psDramInterface_2
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arprot,
    M00_AXI_arready,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awprot,
    M00_AXI_awready,
    M00_AXI_awvalid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    M01_ACLK,
    M01_ARESETN,
    M01_AXI_araddr,
    M01_AXI_arprot,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awprot,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wstrb,
    M01_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arprot,
    S00_AXI_arready,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awprot,
    S00_AXI_awready,
    S00_AXI_awvalid,
    S00_AXI_bready,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rready,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input ARESETN;
  input M00_ACLK;
  input M00_ARESETN;
  output [16:0]M00_AXI_araddr;
  output [2:0]M00_AXI_arprot;
  input M00_AXI_arready;
  output M00_AXI_arvalid;
  output [16:0]M00_AXI_awaddr;
  output [2:0]M00_AXI_awprot;
  input M00_AXI_awready;
  output M00_AXI_awvalid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  input M01_ACLK;
  input M01_ARESETN;
  output [31:0]M01_AXI_araddr;
  output [2:0]M01_AXI_arprot;
  input [0:0]M01_AXI_arready;
  output [0:0]M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  output [2:0]M01_AXI_awprot;
  input [0:0]M01_AXI_awready;
  output [0:0]M01_AXI_awvalid;
  output [0:0]M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input [0:0]M01_AXI_bvalid;
  input [63:0]M01_AXI_rdata;
  output [0:0]M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input [0:0]M01_AXI_rvalid;
  output [63:0]M01_AXI_wdata;
  input [0:0]M01_AXI_wready;
  output [7:0]M01_AXI_wstrb;
  output [0:0]M01_AXI_wvalid;
  input S00_ACLK;
  input S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [2:0]S00_AXI_arprot;
  output S00_AXI_arready;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [2:0]S00_AXI_awprot;
  output S00_AXI_awready;
  input S00_AXI_awvalid;
  input S00_AXI_bready;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  input S00_AXI_rready;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire [16:0]m00_couplers_to_psDramInterface_ARADDR;
  wire [2:0]m00_couplers_to_psDramInterface_ARPROT;
  wire m00_couplers_to_psDramInterface_ARREADY;
  wire m00_couplers_to_psDramInterface_ARVALID;
  wire [16:0]m00_couplers_to_psDramInterface_AWADDR;
  wire [2:0]m00_couplers_to_psDramInterface_AWPROT;
  wire m00_couplers_to_psDramInterface_AWREADY;
  wire m00_couplers_to_psDramInterface_AWVALID;
  wire m00_couplers_to_psDramInterface_BREADY;
  wire [1:0]m00_couplers_to_psDramInterface_BRESP;
  wire m00_couplers_to_psDramInterface_BVALID;
  wire [31:0]m00_couplers_to_psDramInterface_RDATA;
  wire m00_couplers_to_psDramInterface_RREADY;
  wire [1:0]m00_couplers_to_psDramInterface_RRESP;
  wire m00_couplers_to_psDramInterface_RVALID;
  wire [31:0]m00_couplers_to_psDramInterface_WDATA;
  wire m00_couplers_to_psDramInterface_WREADY;
  wire [3:0]m00_couplers_to_psDramInterface_WSTRB;
  wire m00_couplers_to_psDramInterface_WVALID;
  wire [31:0]m01_couplers_to_psDramInterface_ARADDR;
  wire [2:0]m01_couplers_to_psDramInterface_ARPROT;
  wire [0:0]m01_couplers_to_psDramInterface_ARREADY;
  wire [0:0]m01_couplers_to_psDramInterface_ARVALID;
  wire [31:0]m01_couplers_to_psDramInterface_AWADDR;
  wire [2:0]m01_couplers_to_psDramInterface_AWPROT;
  wire [0:0]m01_couplers_to_psDramInterface_AWREADY;
  wire [0:0]m01_couplers_to_psDramInterface_AWVALID;
  wire [0:0]m01_couplers_to_psDramInterface_BREADY;
  wire [1:0]m01_couplers_to_psDramInterface_BRESP;
  wire [0:0]m01_couplers_to_psDramInterface_BVALID;
  wire [63:0]m01_couplers_to_psDramInterface_RDATA;
  wire [0:0]m01_couplers_to_psDramInterface_RREADY;
  wire [1:0]m01_couplers_to_psDramInterface_RRESP;
  wire [0:0]m01_couplers_to_psDramInterface_RVALID;
  wire [63:0]m01_couplers_to_psDramInterface_WDATA;
  wire [0:0]m01_couplers_to_psDramInterface_WREADY;
  wire [7:0]m01_couplers_to_psDramInterface_WSTRB;
  wire [0:0]m01_couplers_to_psDramInterface_WVALID;
  wire psDramInterface_ACLK_net;
  wire psDramInterface_ARESETN_net;
  wire [31:0]psDramInterface_to_s00_couplers_ARADDR;
  wire [2:0]psDramInterface_to_s00_couplers_ARPROT;
  wire psDramInterface_to_s00_couplers_ARREADY;
  wire psDramInterface_to_s00_couplers_ARVALID;
  wire [31:0]psDramInterface_to_s00_couplers_AWADDR;
  wire [2:0]psDramInterface_to_s00_couplers_AWPROT;
  wire psDramInterface_to_s00_couplers_AWREADY;
  wire psDramInterface_to_s00_couplers_AWVALID;
  wire psDramInterface_to_s00_couplers_BREADY;
  wire psDramInterface_to_s00_couplers_BVALID;
  wire [31:0]psDramInterface_to_s00_couplers_RDATA;
  wire psDramInterface_to_s00_couplers_RREADY;
  wire psDramInterface_to_s00_couplers_RVALID;
  wire [31:0]psDramInterface_to_s00_couplers_WDATA;
  wire psDramInterface_to_s00_couplers_WREADY;
  wire [3:0]psDramInterface_to_s00_couplers_WSTRB;
  wire psDramInterface_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_xbar_ARADDR;
  wire [2:0]s00_couplers_to_xbar_ARPROT;
  wire [0:0]s00_couplers_to_xbar_ARREADY;
  wire s00_couplers_to_xbar_ARVALID;
  wire [31:0]s00_couplers_to_xbar_AWADDR;
  wire [2:0]s00_couplers_to_xbar_AWPROT;
  wire [0:0]s00_couplers_to_xbar_AWREADY;
  wire s00_couplers_to_xbar_AWVALID;
  wire s00_couplers_to_xbar_BREADY;
  wire [1:0]s00_couplers_to_xbar_BRESP;
  wire [0:0]s00_couplers_to_xbar_BVALID;
  wire [63:0]s00_couplers_to_xbar_RDATA;
  wire s00_couplers_to_xbar_RREADY;
  wire [1:0]s00_couplers_to_xbar_RRESP;
  wire [0:0]s00_couplers_to_xbar_RVALID;
  wire [63:0]s00_couplers_to_xbar_WDATA;
  wire [0:0]s00_couplers_to_xbar_WREADY;
  wire [7:0]s00_couplers_to_xbar_WSTRB;
  wire s00_couplers_to_xbar_WVALID;
  wire [31:0]xbar_to_m00_couplers_ARADDR;
  wire [2:0]xbar_to_m00_couplers_ARPROT;
  wire xbar_to_m00_couplers_ARREADY;
  wire [0:0]xbar_to_m00_couplers_ARVALID;
  wire [31:0]xbar_to_m00_couplers_AWADDR;
  wire [2:0]xbar_to_m00_couplers_AWPROT;
  wire xbar_to_m00_couplers_AWREADY;
  wire [0:0]xbar_to_m00_couplers_AWVALID;
  wire [0:0]xbar_to_m00_couplers_BREADY;
  wire [1:0]xbar_to_m00_couplers_BRESP;
  wire xbar_to_m00_couplers_BVALID;
  wire [63:0]xbar_to_m00_couplers_RDATA;
  wire [0:0]xbar_to_m00_couplers_RREADY;
  wire [1:0]xbar_to_m00_couplers_RRESP;
  wire xbar_to_m00_couplers_RVALID;
  wire [63:0]xbar_to_m00_couplers_WDATA;
  wire xbar_to_m00_couplers_WREADY;
  wire [7:0]xbar_to_m00_couplers_WSTRB;
  wire [0:0]xbar_to_m00_couplers_WVALID;
  wire [63:32]xbar_to_m01_couplers_ARADDR;
  wire [5:3]xbar_to_m01_couplers_ARPROT;
  wire [0:0]xbar_to_m01_couplers_ARREADY;
  wire [1:1]xbar_to_m01_couplers_ARVALID;
  wire [63:32]xbar_to_m01_couplers_AWADDR;
  wire [5:3]xbar_to_m01_couplers_AWPROT;
  wire [0:0]xbar_to_m01_couplers_AWREADY;
  wire [1:1]xbar_to_m01_couplers_AWVALID;
  wire [1:1]xbar_to_m01_couplers_BREADY;
  wire [1:0]xbar_to_m01_couplers_BRESP;
  wire [0:0]xbar_to_m01_couplers_BVALID;
  wire [63:0]xbar_to_m01_couplers_RDATA;
  wire [1:1]xbar_to_m01_couplers_RREADY;
  wire [1:0]xbar_to_m01_couplers_RRESP;
  wire [0:0]xbar_to_m01_couplers_RVALID;
  wire [127:64]xbar_to_m01_couplers_WDATA;
  wire [0:0]xbar_to_m01_couplers_WREADY;
  wire [15:8]xbar_to_m01_couplers_WSTRB;
  wire [1:1]xbar_to_m01_couplers_WVALID;

  assign M00_AXI_araddr[16:0] = m00_couplers_to_psDramInterface_ARADDR;
  assign M00_AXI_arprot[2:0] = m00_couplers_to_psDramInterface_ARPROT;
  assign M00_AXI_arvalid = m00_couplers_to_psDramInterface_ARVALID;
  assign M00_AXI_awaddr[16:0] = m00_couplers_to_psDramInterface_AWADDR;
  assign M00_AXI_awprot[2:0] = m00_couplers_to_psDramInterface_AWPROT;
  assign M00_AXI_awvalid = m00_couplers_to_psDramInterface_AWVALID;
  assign M00_AXI_bready = m00_couplers_to_psDramInterface_BREADY;
  assign M00_AXI_rready = m00_couplers_to_psDramInterface_RREADY;
  assign M00_AXI_wdata[31:0] = m00_couplers_to_psDramInterface_WDATA;
  assign M00_AXI_wstrb[3:0] = m00_couplers_to_psDramInterface_WSTRB;
  assign M00_AXI_wvalid = m00_couplers_to_psDramInterface_WVALID;
  assign M01_AXI_araddr[31:0] = m01_couplers_to_psDramInterface_ARADDR;
  assign M01_AXI_arprot[2:0] = m01_couplers_to_psDramInterface_ARPROT;
  assign M01_AXI_arvalid[0] = m01_couplers_to_psDramInterface_ARVALID;
  assign M01_AXI_awaddr[31:0] = m01_couplers_to_psDramInterface_AWADDR;
  assign M01_AXI_awprot[2:0] = m01_couplers_to_psDramInterface_AWPROT;
  assign M01_AXI_awvalid[0] = m01_couplers_to_psDramInterface_AWVALID;
  assign M01_AXI_bready[0] = m01_couplers_to_psDramInterface_BREADY;
  assign M01_AXI_rready[0] = m01_couplers_to_psDramInterface_RREADY;
  assign M01_AXI_wdata[63:0] = m01_couplers_to_psDramInterface_WDATA;
  assign M01_AXI_wstrb[7:0] = m01_couplers_to_psDramInterface_WSTRB;
  assign M01_AXI_wvalid[0] = m01_couplers_to_psDramInterface_WVALID;
  assign S00_AXI_arready = psDramInterface_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = psDramInterface_to_s00_couplers_AWREADY;
  assign S00_AXI_bvalid = psDramInterface_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = psDramInterface_to_s00_couplers_RDATA;
  assign S00_AXI_rvalid = psDramInterface_to_s00_couplers_RVALID;
  assign S00_AXI_wready = psDramInterface_to_s00_couplers_WREADY;
  assign m00_couplers_to_psDramInterface_ARREADY = M00_AXI_arready;
  assign m00_couplers_to_psDramInterface_AWREADY = M00_AXI_awready;
  assign m00_couplers_to_psDramInterface_BRESP = M00_AXI_bresp[1:0];
  assign m00_couplers_to_psDramInterface_BVALID = M00_AXI_bvalid;
  assign m00_couplers_to_psDramInterface_RDATA = M00_AXI_rdata[31:0];
  assign m00_couplers_to_psDramInterface_RRESP = M00_AXI_rresp[1:0];
  assign m00_couplers_to_psDramInterface_RVALID = M00_AXI_rvalid;
  assign m00_couplers_to_psDramInterface_WREADY = M00_AXI_wready;
  assign m01_couplers_to_psDramInterface_ARREADY = M01_AXI_arready[0];
  assign m01_couplers_to_psDramInterface_AWREADY = M01_AXI_awready[0];
  assign m01_couplers_to_psDramInterface_BRESP = M01_AXI_bresp[1:0];
  assign m01_couplers_to_psDramInterface_BVALID = M01_AXI_bvalid[0];
  assign m01_couplers_to_psDramInterface_RDATA = M01_AXI_rdata[63:0];
  assign m01_couplers_to_psDramInterface_RRESP = M01_AXI_rresp[1:0];
  assign m01_couplers_to_psDramInterface_RVALID = M01_AXI_rvalid[0];
  assign m01_couplers_to_psDramInterface_WREADY = M01_AXI_wready[0];
  assign psDramInterface_ACLK_net = ACLK;
  assign psDramInterface_ARESETN_net = ARESETN;
  assign psDramInterface_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign psDramInterface_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign psDramInterface_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign psDramInterface_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign psDramInterface_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign psDramInterface_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign psDramInterface_to_s00_couplers_BREADY = S00_AXI_bready;
  assign psDramInterface_to_s00_couplers_RREADY = S00_AXI_rready;
  assign psDramInterface_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign psDramInterface_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign psDramInterface_to_s00_couplers_WVALID = S00_AXI_wvalid;
  m00_couplers_imp_1YR4BR9 m00_couplers
       (.M_ACLK(psDramInterface_ACLK_net),
        .M_ARESETN(psDramInterface_ARESETN_net),
        .M_AXI_araddr(m00_couplers_to_psDramInterface_ARADDR),
        .M_AXI_arprot(m00_couplers_to_psDramInterface_ARPROT),
        .M_AXI_arready(m00_couplers_to_psDramInterface_ARREADY),
        .M_AXI_arvalid(m00_couplers_to_psDramInterface_ARVALID),
        .M_AXI_awaddr(m00_couplers_to_psDramInterface_AWADDR),
        .M_AXI_awprot(m00_couplers_to_psDramInterface_AWPROT),
        .M_AXI_awready(m00_couplers_to_psDramInterface_AWREADY),
        .M_AXI_awvalid(m00_couplers_to_psDramInterface_AWVALID),
        .M_AXI_bready(m00_couplers_to_psDramInterface_BREADY),
        .M_AXI_bresp(m00_couplers_to_psDramInterface_BRESP),
        .M_AXI_bvalid(m00_couplers_to_psDramInterface_BVALID),
        .M_AXI_rdata(m00_couplers_to_psDramInterface_RDATA),
        .M_AXI_rready(m00_couplers_to_psDramInterface_RREADY),
        .M_AXI_rresp(m00_couplers_to_psDramInterface_RRESP),
        .M_AXI_rvalid(m00_couplers_to_psDramInterface_RVALID),
        .M_AXI_wdata(m00_couplers_to_psDramInterface_WDATA),
        .M_AXI_wready(m00_couplers_to_psDramInterface_WREADY),
        .M_AXI_wstrb(m00_couplers_to_psDramInterface_WSTRB),
        .M_AXI_wvalid(m00_couplers_to_psDramInterface_WVALID),
        .S_ACLK(psDramInterface_ACLK_net),
        .S_ARESETN(psDramInterface_ARESETN_net),
        .S_AXI_araddr(xbar_to_m00_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m00_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m00_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m00_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m00_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m00_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m00_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m00_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m00_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m00_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m00_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m00_couplers_RDATA),
        .S_AXI_rready(xbar_to_m00_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m00_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m00_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m00_couplers_WDATA),
        .S_AXI_wready(xbar_to_m00_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m00_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m00_couplers_WVALID));
  m01_couplers_imp_4WKJTW m01_couplers
       (.M_ACLK(psDramInterface_ACLK_net),
        .M_ARESETN(psDramInterface_ARESETN_net),
        .M_AXI_araddr(m01_couplers_to_psDramInterface_ARADDR),
        .M_AXI_arprot(m01_couplers_to_psDramInterface_ARPROT),
        .M_AXI_arready(m01_couplers_to_psDramInterface_ARREADY),
        .M_AXI_arvalid(m01_couplers_to_psDramInterface_ARVALID),
        .M_AXI_awaddr(m01_couplers_to_psDramInterface_AWADDR),
        .M_AXI_awprot(m01_couplers_to_psDramInterface_AWPROT),
        .M_AXI_awready(m01_couplers_to_psDramInterface_AWREADY),
        .M_AXI_awvalid(m01_couplers_to_psDramInterface_AWVALID),
        .M_AXI_bready(m01_couplers_to_psDramInterface_BREADY),
        .M_AXI_bresp(m01_couplers_to_psDramInterface_BRESP),
        .M_AXI_bvalid(m01_couplers_to_psDramInterface_BVALID),
        .M_AXI_rdata(m01_couplers_to_psDramInterface_RDATA),
        .M_AXI_rready(m01_couplers_to_psDramInterface_RREADY),
        .M_AXI_rresp(m01_couplers_to_psDramInterface_RRESP),
        .M_AXI_rvalid(m01_couplers_to_psDramInterface_RVALID),
        .M_AXI_wdata(m01_couplers_to_psDramInterface_WDATA),
        .M_AXI_wready(m01_couplers_to_psDramInterface_WREADY),
        .M_AXI_wstrb(m01_couplers_to_psDramInterface_WSTRB),
        .M_AXI_wvalid(m01_couplers_to_psDramInterface_WVALID),
        .S_ACLK(psDramInterface_ACLK_net),
        .S_ARESETN(psDramInterface_ARESETN_net),
        .S_AXI_araddr(xbar_to_m01_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m01_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m01_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m01_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m01_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m01_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m01_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m01_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m01_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m01_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m01_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m01_couplers_RDATA),
        .S_AXI_rready(xbar_to_m01_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m01_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m01_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m01_couplers_WDATA),
        .S_AXI_wready(xbar_to_m01_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m01_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m01_couplers_WVALID));
  s00_couplers_imp_25GATJ s00_couplers
       (.M_ACLK(psDramInterface_ACLK_net),
        .M_ARESETN(psDramInterface_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_xbar_ARADDR),
        .M_AXI_arprot(s00_couplers_to_xbar_ARPROT),
        .M_AXI_arready(s00_couplers_to_xbar_ARREADY),
        .M_AXI_arvalid(s00_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_xbar_AWADDR),
        .M_AXI_awprot(s00_couplers_to_xbar_AWPROT),
        .M_AXI_awready(s00_couplers_to_xbar_AWREADY),
        .M_AXI_awvalid(s00_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s00_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s00_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s00_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s00_couplers_to_xbar_RDATA),
        .M_AXI_rready(s00_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s00_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s00_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s00_couplers_to_xbar_WDATA),
        .M_AXI_wready(s00_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s00_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_xbar_WVALID),
        .S_ACLK(psDramInterface_ACLK_net),
        .S_ARESETN(psDramInterface_ARESETN_net),
        .S_AXI_araddr(psDramInterface_to_s00_couplers_ARADDR),
        .S_AXI_arprot(psDramInterface_to_s00_couplers_ARPROT),
        .S_AXI_arready(psDramInterface_to_s00_couplers_ARREADY),
        .S_AXI_arvalid(psDramInterface_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(psDramInterface_to_s00_couplers_AWADDR),
        .S_AXI_awprot(psDramInterface_to_s00_couplers_AWPROT),
        .S_AXI_awready(psDramInterface_to_s00_couplers_AWREADY),
        .S_AXI_awvalid(psDramInterface_to_s00_couplers_AWVALID),
        .S_AXI_bready(psDramInterface_to_s00_couplers_BREADY),
        .S_AXI_bvalid(psDramInterface_to_s00_couplers_BVALID),
        .S_AXI_rdata(psDramInterface_to_s00_couplers_RDATA),
        .S_AXI_rready(psDramInterface_to_s00_couplers_RREADY),
        .S_AXI_rvalid(psDramInterface_to_s00_couplers_RVALID),
        .S_AXI_wdata(psDramInterface_to_s00_couplers_WDATA),
        .S_AXI_wready(psDramInterface_to_s00_couplers_WREADY),
        .S_AXI_wstrb(psDramInterface_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(psDramInterface_to_s00_couplers_WVALID));
  cerberus_xbar_5 xbar
       (.aclk(psDramInterface_ACLK_net),
        .aresetn(psDramInterface_ARESETN_net),
        .m_axi_araddr({xbar_to_m01_couplers_ARADDR,xbar_to_m00_couplers_ARADDR}),
        .m_axi_arprot({xbar_to_m01_couplers_ARPROT,xbar_to_m00_couplers_ARPROT}),
        .m_axi_arready({xbar_to_m01_couplers_ARREADY,xbar_to_m00_couplers_ARREADY}),
        .m_axi_arvalid({xbar_to_m01_couplers_ARVALID,xbar_to_m00_couplers_ARVALID}),
        .m_axi_awaddr({xbar_to_m01_couplers_AWADDR,xbar_to_m00_couplers_AWADDR}),
        .m_axi_awprot({xbar_to_m01_couplers_AWPROT,xbar_to_m00_couplers_AWPROT}),
        .m_axi_awready({xbar_to_m01_couplers_AWREADY,xbar_to_m00_couplers_AWREADY}),
        .m_axi_awvalid({xbar_to_m01_couplers_AWVALID,xbar_to_m00_couplers_AWVALID}),
        .m_axi_bready({xbar_to_m01_couplers_BREADY,xbar_to_m00_couplers_BREADY}),
        .m_axi_bresp({xbar_to_m01_couplers_BRESP,xbar_to_m00_couplers_BRESP}),
        .m_axi_bvalid({xbar_to_m01_couplers_BVALID,xbar_to_m00_couplers_BVALID}),
        .m_axi_rdata({xbar_to_m01_couplers_RDATA,xbar_to_m00_couplers_RDATA}),
        .m_axi_rready({xbar_to_m01_couplers_RREADY,xbar_to_m00_couplers_RREADY}),
        .m_axi_rresp({xbar_to_m01_couplers_RRESP,xbar_to_m00_couplers_RRESP}),
        .m_axi_rvalid({xbar_to_m01_couplers_RVALID,xbar_to_m00_couplers_RVALID}),
        .m_axi_wdata({xbar_to_m01_couplers_WDATA,xbar_to_m00_couplers_WDATA}),
        .m_axi_wready({xbar_to_m01_couplers_WREADY,xbar_to_m00_couplers_WREADY}),
        .m_axi_wstrb({xbar_to_m01_couplers_WSTRB,xbar_to_m00_couplers_WSTRB}),
        .m_axi_wvalid({xbar_to_m01_couplers_WVALID,xbar_to_m00_couplers_WVALID}),
        .s_axi_araddr(s00_couplers_to_xbar_ARADDR),
        .s_axi_arprot(s00_couplers_to_xbar_ARPROT),
        .s_axi_arready(s00_couplers_to_xbar_ARREADY),
        .s_axi_arvalid(s00_couplers_to_xbar_ARVALID),
        .s_axi_awaddr(s00_couplers_to_xbar_AWADDR),
        .s_axi_awprot(s00_couplers_to_xbar_AWPROT),
        .s_axi_awready(s00_couplers_to_xbar_AWREADY),
        .s_axi_awvalid(s00_couplers_to_xbar_AWVALID),
        .s_axi_bready(s00_couplers_to_xbar_BREADY),
        .s_axi_bresp(s00_couplers_to_xbar_BRESP),
        .s_axi_bvalid(s00_couplers_to_xbar_BVALID),
        .s_axi_rdata(s00_couplers_to_xbar_RDATA),
        .s_axi_rready(s00_couplers_to_xbar_RREADY),
        .s_axi_rresp(s00_couplers_to_xbar_RRESP),
        .s_axi_rvalid(s00_couplers_to_xbar_RVALID),
        .s_axi_wdata(s00_couplers_to_xbar_WDATA),
        .s_axi_wready(s00_couplers_to_xbar_WREADY),
        .s_axi_wstrb(s00_couplers_to_xbar_WSTRB),
        .s_axi_wvalid(s00_couplers_to_xbar_WVALID));
endmodule

module cerberus_psDramInterface_3
   (ACLK,
    ARESETN,
    M00_ACLK,
    M00_ARESETN,
    M00_AXI_araddr,
    M00_AXI_arprot,
    M00_AXI_arready,
    M00_AXI_arvalid,
    M00_AXI_awaddr,
    M00_AXI_awprot,
    M00_AXI_awready,
    M00_AXI_awvalid,
    M00_AXI_bready,
    M00_AXI_bresp,
    M00_AXI_bvalid,
    M00_AXI_rdata,
    M00_AXI_rready,
    M00_AXI_rresp,
    M00_AXI_rvalid,
    M00_AXI_wdata,
    M00_AXI_wready,
    M00_AXI_wstrb,
    M00_AXI_wvalid,
    M01_ACLK,
    M01_ARESETN,
    M01_AXI_araddr,
    M01_AXI_arprot,
    M01_AXI_arready,
    M01_AXI_arvalid,
    M01_AXI_awaddr,
    M01_AXI_awprot,
    M01_AXI_awready,
    M01_AXI_awvalid,
    M01_AXI_bready,
    M01_AXI_bresp,
    M01_AXI_bvalid,
    M01_AXI_rdata,
    M01_AXI_rready,
    M01_AXI_rresp,
    M01_AXI_rvalid,
    M01_AXI_wdata,
    M01_AXI_wready,
    M01_AXI_wstrb,
    M01_AXI_wvalid,
    S00_ACLK,
    S00_ARESETN,
    S00_AXI_araddr,
    S00_AXI_arprot,
    S00_AXI_arready,
    S00_AXI_arvalid,
    S00_AXI_awaddr,
    S00_AXI_awprot,
    S00_AXI_awready,
    S00_AXI_awvalid,
    S00_AXI_bready,
    S00_AXI_bvalid,
    S00_AXI_rdata,
    S00_AXI_rready,
    S00_AXI_rvalid,
    S00_AXI_wdata,
    S00_AXI_wready,
    S00_AXI_wstrb,
    S00_AXI_wvalid);
  input ACLK;
  input ARESETN;
  input M00_ACLK;
  input M00_ARESETN;
  output [16:0]M00_AXI_araddr;
  output [2:0]M00_AXI_arprot;
  input M00_AXI_arready;
  output M00_AXI_arvalid;
  output [16:0]M00_AXI_awaddr;
  output [2:0]M00_AXI_awprot;
  input M00_AXI_awready;
  output M00_AXI_awvalid;
  output M00_AXI_bready;
  input [1:0]M00_AXI_bresp;
  input M00_AXI_bvalid;
  input [31:0]M00_AXI_rdata;
  output M00_AXI_rready;
  input [1:0]M00_AXI_rresp;
  input M00_AXI_rvalid;
  output [31:0]M00_AXI_wdata;
  input M00_AXI_wready;
  output [3:0]M00_AXI_wstrb;
  output M00_AXI_wvalid;
  input M01_ACLK;
  input M01_ARESETN;
  output [31:0]M01_AXI_araddr;
  output [2:0]M01_AXI_arprot;
  input [0:0]M01_AXI_arready;
  output [0:0]M01_AXI_arvalid;
  output [31:0]M01_AXI_awaddr;
  output [2:0]M01_AXI_awprot;
  input [0:0]M01_AXI_awready;
  output [0:0]M01_AXI_awvalid;
  output [0:0]M01_AXI_bready;
  input [1:0]M01_AXI_bresp;
  input [0:0]M01_AXI_bvalid;
  input [63:0]M01_AXI_rdata;
  output [0:0]M01_AXI_rready;
  input [1:0]M01_AXI_rresp;
  input [0:0]M01_AXI_rvalid;
  output [63:0]M01_AXI_wdata;
  input [0:0]M01_AXI_wready;
  output [7:0]M01_AXI_wstrb;
  output [0:0]M01_AXI_wvalid;
  input S00_ACLK;
  input S00_ARESETN;
  input [31:0]S00_AXI_araddr;
  input [2:0]S00_AXI_arprot;
  output S00_AXI_arready;
  input S00_AXI_arvalid;
  input [31:0]S00_AXI_awaddr;
  input [2:0]S00_AXI_awprot;
  output S00_AXI_awready;
  input S00_AXI_awvalid;
  input S00_AXI_bready;
  output S00_AXI_bvalid;
  output [31:0]S00_AXI_rdata;
  input S00_AXI_rready;
  output S00_AXI_rvalid;
  input [31:0]S00_AXI_wdata;
  output S00_AXI_wready;
  input [3:0]S00_AXI_wstrb;
  input S00_AXI_wvalid;

  wire [16:0]m00_couplers_to_psDramInterface_ARADDR;
  wire [2:0]m00_couplers_to_psDramInterface_ARPROT;
  wire m00_couplers_to_psDramInterface_ARREADY;
  wire m00_couplers_to_psDramInterface_ARVALID;
  wire [16:0]m00_couplers_to_psDramInterface_AWADDR;
  wire [2:0]m00_couplers_to_psDramInterface_AWPROT;
  wire m00_couplers_to_psDramInterface_AWREADY;
  wire m00_couplers_to_psDramInterface_AWVALID;
  wire m00_couplers_to_psDramInterface_BREADY;
  wire [1:0]m00_couplers_to_psDramInterface_BRESP;
  wire m00_couplers_to_psDramInterface_BVALID;
  wire [31:0]m00_couplers_to_psDramInterface_RDATA;
  wire m00_couplers_to_psDramInterface_RREADY;
  wire [1:0]m00_couplers_to_psDramInterface_RRESP;
  wire m00_couplers_to_psDramInterface_RVALID;
  wire [31:0]m00_couplers_to_psDramInterface_WDATA;
  wire m00_couplers_to_psDramInterface_WREADY;
  wire [3:0]m00_couplers_to_psDramInterface_WSTRB;
  wire m00_couplers_to_psDramInterface_WVALID;
  wire [31:0]m01_couplers_to_psDramInterface_ARADDR;
  wire [2:0]m01_couplers_to_psDramInterface_ARPROT;
  wire [0:0]m01_couplers_to_psDramInterface_ARREADY;
  wire [0:0]m01_couplers_to_psDramInterface_ARVALID;
  wire [31:0]m01_couplers_to_psDramInterface_AWADDR;
  wire [2:0]m01_couplers_to_psDramInterface_AWPROT;
  wire [0:0]m01_couplers_to_psDramInterface_AWREADY;
  wire [0:0]m01_couplers_to_psDramInterface_AWVALID;
  wire [0:0]m01_couplers_to_psDramInterface_BREADY;
  wire [1:0]m01_couplers_to_psDramInterface_BRESP;
  wire [0:0]m01_couplers_to_psDramInterface_BVALID;
  wire [63:0]m01_couplers_to_psDramInterface_RDATA;
  wire [0:0]m01_couplers_to_psDramInterface_RREADY;
  wire [1:0]m01_couplers_to_psDramInterface_RRESP;
  wire [0:0]m01_couplers_to_psDramInterface_RVALID;
  wire [63:0]m01_couplers_to_psDramInterface_WDATA;
  wire [0:0]m01_couplers_to_psDramInterface_WREADY;
  wire [7:0]m01_couplers_to_psDramInterface_WSTRB;
  wire [0:0]m01_couplers_to_psDramInterface_WVALID;
  wire psDramInterface_ACLK_net;
  wire psDramInterface_ARESETN_net;
  wire [31:0]psDramInterface_to_s00_couplers_ARADDR;
  wire [2:0]psDramInterface_to_s00_couplers_ARPROT;
  wire psDramInterface_to_s00_couplers_ARREADY;
  wire psDramInterface_to_s00_couplers_ARVALID;
  wire [31:0]psDramInterface_to_s00_couplers_AWADDR;
  wire [2:0]psDramInterface_to_s00_couplers_AWPROT;
  wire psDramInterface_to_s00_couplers_AWREADY;
  wire psDramInterface_to_s00_couplers_AWVALID;
  wire psDramInterface_to_s00_couplers_BREADY;
  wire psDramInterface_to_s00_couplers_BVALID;
  wire [31:0]psDramInterface_to_s00_couplers_RDATA;
  wire psDramInterface_to_s00_couplers_RREADY;
  wire psDramInterface_to_s00_couplers_RVALID;
  wire [31:0]psDramInterface_to_s00_couplers_WDATA;
  wire psDramInterface_to_s00_couplers_WREADY;
  wire [3:0]psDramInterface_to_s00_couplers_WSTRB;
  wire psDramInterface_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_xbar_ARADDR;
  wire [2:0]s00_couplers_to_xbar_ARPROT;
  wire [0:0]s00_couplers_to_xbar_ARREADY;
  wire s00_couplers_to_xbar_ARVALID;
  wire [31:0]s00_couplers_to_xbar_AWADDR;
  wire [2:0]s00_couplers_to_xbar_AWPROT;
  wire [0:0]s00_couplers_to_xbar_AWREADY;
  wire s00_couplers_to_xbar_AWVALID;
  wire s00_couplers_to_xbar_BREADY;
  wire [1:0]s00_couplers_to_xbar_BRESP;
  wire [0:0]s00_couplers_to_xbar_BVALID;
  wire [63:0]s00_couplers_to_xbar_RDATA;
  wire s00_couplers_to_xbar_RREADY;
  wire [1:0]s00_couplers_to_xbar_RRESP;
  wire [0:0]s00_couplers_to_xbar_RVALID;
  wire [63:0]s00_couplers_to_xbar_WDATA;
  wire [0:0]s00_couplers_to_xbar_WREADY;
  wire [7:0]s00_couplers_to_xbar_WSTRB;
  wire s00_couplers_to_xbar_WVALID;
  wire [31:0]xbar_to_m00_couplers_ARADDR;
  wire [2:0]xbar_to_m00_couplers_ARPROT;
  wire xbar_to_m00_couplers_ARREADY;
  wire [0:0]xbar_to_m00_couplers_ARVALID;
  wire [31:0]xbar_to_m00_couplers_AWADDR;
  wire [2:0]xbar_to_m00_couplers_AWPROT;
  wire xbar_to_m00_couplers_AWREADY;
  wire [0:0]xbar_to_m00_couplers_AWVALID;
  wire [0:0]xbar_to_m00_couplers_BREADY;
  wire [1:0]xbar_to_m00_couplers_BRESP;
  wire xbar_to_m00_couplers_BVALID;
  wire [63:0]xbar_to_m00_couplers_RDATA;
  wire [0:0]xbar_to_m00_couplers_RREADY;
  wire [1:0]xbar_to_m00_couplers_RRESP;
  wire xbar_to_m00_couplers_RVALID;
  wire [63:0]xbar_to_m00_couplers_WDATA;
  wire xbar_to_m00_couplers_WREADY;
  wire [7:0]xbar_to_m00_couplers_WSTRB;
  wire [0:0]xbar_to_m00_couplers_WVALID;
  wire [63:32]xbar_to_m01_couplers_ARADDR;
  wire [5:3]xbar_to_m01_couplers_ARPROT;
  wire [0:0]xbar_to_m01_couplers_ARREADY;
  wire [1:1]xbar_to_m01_couplers_ARVALID;
  wire [63:32]xbar_to_m01_couplers_AWADDR;
  wire [5:3]xbar_to_m01_couplers_AWPROT;
  wire [0:0]xbar_to_m01_couplers_AWREADY;
  wire [1:1]xbar_to_m01_couplers_AWVALID;
  wire [1:1]xbar_to_m01_couplers_BREADY;
  wire [1:0]xbar_to_m01_couplers_BRESP;
  wire [0:0]xbar_to_m01_couplers_BVALID;
  wire [63:0]xbar_to_m01_couplers_RDATA;
  wire [1:1]xbar_to_m01_couplers_RREADY;
  wire [1:0]xbar_to_m01_couplers_RRESP;
  wire [0:0]xbar_to_m01_couplers_RVALID;
  wire [127:64]xbar_to_m01_couplers_WDATA;
  wire [0:0]xbar_to_m01_couplers_WREADY;
  wire [15:8]xbar_to_m01_couplers_WSTRB;
  wire [1:1]xbar_to_m01_couplers_WVALID;

  assign M00_AXI_araddr[16:0] = m00_couplers_to_psDramInterface_ARADDR;
  assign M00_AXI_arprot[2:0] = m00_couplers_to_psDramInterface_ARPROT;
  assign M00_AXI_arvalid = m00_couplers_to_psDramInterface_ARVALID;
  assign M00_AXI_awaddr[16:0] = m00_couplers_to_psDramInterface_AWADDR;
  assign M00_AXI_awprot[2:0] = m00_couplers_to_psDramInterface_AWPROT;
  assign M00_AXI_awvalid = m00_couplers_to_psDramInterface_AWVALID;
  assign M00_AXI_bready = m00_couplers_to_psDramInterface_BREADY;
  assign M00_AXI_rready = m00_couplers_to_psDramInterface_RREADY;
  assign M00_AXI_wdata[31:0] = m00_couplers_to_psDramInterface_WDATA;
  assign M00_AXI_wstrb[3:0] = m00_couplers_to_psDramInterface_WSTRB;
  assign M00_AXI_wvalid = m00_couplers_to_psDramInterface_WVALID;
  assign M01_AXI_araddr[31:0] = m01_couplers_to_psDramInterface_ARADDR;
  assign M01_AXI_arprot[2:0] = m01_couplers_to_psDramInterface_ARPROT;
  assign M01_AXI_arvalid[0] = m01_couplers_to_psDramInterface_ARVALID;
  assign M01_AXI_awaddr[31:0] = m01_couplers_to_psDramInterface_AWADDR;
  assign M01_AXI_awprot[2:0] = m01_couplers_to_psDramInterface_AWPROT;
  assign M01_AXI_awvalid[0] = m01_couplers_to_psDramInterface_AWVALID;
  assign M01_AXI_bready[0] = m01_couplers_to_psDramInterface_BREADY;
  assign M01_AXI_rready[0] = m01_couplers_to_psDramInterface_RREADY;
  assign M01_AXI_wdata[63:0] = m01_couplers_to_psDramInterface_WDATA;
  assign M01_AXI_wstrb[7:0] = m01_couplers_to_psDramInterface_WSTRB;
  assign M01_AXI_wvalid[0] = m01_couplers_to_psDramInterface_WVALID;
  assign S00_AXI_arready = psDramInterface_to_s00_couplers_ARREADY;
  assign S00_AXI_awready = psDramInterface_to_s00_couplers_AWREADY;
  assign S00_AXI_bvalid = psDramInterface_to_s00_couplers_BVALID;
  assign S00_AXI_rdata[31:0] = psDramInterface_to_s00_couplers_RDATA;
  assign S00_AXI_rvalid = psDramInterface_to_s00_couplers_RVALID;
  assign S00_AXI_wready = psDramInterface_to_s00_couplers_WREADY;
  assign m00_couplers_to_psDramInterface_ARREADY = M00_AXI_arready;
  assign m00_couplers_to_psDramInterface_AWREADY = M00_AXI_awready;
  assign m00_couplers_to_psDramInterface_BRESP = M00_AXI_bresp[1:0];
  assign m00_couplers_to_psDramInterface_BVALID = M00_AXI_bvalid;
  assign m00_couplers_to_psDramInterface_RDATA = M00_AXI_rdata[31:0];
  assign m00_couplers_to_psDramInterface_RRESP = M00_AXI_rresp[1:0];
  assign m00_couplers_to_psDramInterface_RVALID = M00_AXI_rvalid;
  assign m00_couplers_to_psDramInterface_WREADY = M00_AXI_wready;
  assign m01_couplers_to_psDramInterface_ARREADY = M01_AXI_arready[0];
  assign m01_couplers_to_psDramInterface_AWREADY = M01_AXI_awready[0];
  assign m01_couplers_to_psDramInterface_BRESP = M01_AXI_bresp[1:0];
  assign m01_couplers_to_psDramInterface_BVALID = M01_AXI_bvalid[0];
  assign m01_couplers_to_psDramInterface_RDATA = M01_AXI_rdata[63:0];
  assign m01_couplers_to_psDramInterface_RRESP = M01_AXI_rresp[1:0];
  assign m01_couplers_to_psDramInterface_RVALID = M01_AXI_rvalid[0];
  assign m01_couplers_to_psDramInterface_WREADY = M01_AXI_wready[0];
  assign psDramInterface_ACLK_net = ACLK;
  assign psDramInterface_ARESETN_net = ARESETN;
  assign psDramInterface_to_s00_couplers_ARADDR = S00_AXI_araddr[31:0];
  assign psDramInterface_to_s00_couplers_ARPROT = S00_AXI_arprot[2:0];
  assign psDramInterface_to_s00_couplers_ARVALID = S00_AXI_arvalid;
  assign psDramInterface_to_s00_couplers_AWADDR = S00_AXI_awaddr[31:0];
  assign psDramInterface_to_s00_couplers_AWPROT = S00_AXI_awprot[2:0];
  assign psDramInterface_to_s00_couplers_AWVALID = S00_AXI_awvalid;
  assign psDramInterface_to_s00_couplers_BREADY = S00_AXI_bready;
  assign psDramInterface_to_s00_couplers_RREADY = S00_AXI_rready;
  assign psDramInterface_to_s00_couplers_WDATA = S00_AXI_wdata[31:0];
  assign psDramInterface_to_s00_couplers_WSTRB = S00_AXI_wstrb[3:0];
  assign psDramInterface_to_s00_couplers_WVALID = S00_AXI_wvalid;
  m00_couplers_imp_PLT5EQ m00_couplers
       (.M_ACLK(psDramInterface_ACLK_net),
        .M_ARESETN(psDramInterface_ARESETN_net),
        .M_AXI_araddr(m00_couplers_to_psDramInterface_ARADDR),
        .M_AXI_arprot(m00_couplers_to_psDramInterface_ARPROT),
        .M_AXI_arready(m00_couplers_to_psDramInterface_ARREADY),
        .M_AXI_arvalid(m00_couplers_to_psDramInterface_ARVALID),
        .M_AXI_awaddr(m00_couplers_to_psDramInterface_AWADDR),
        .M_AXI_awprot(m00_couplers_to_psDramInterface_AWPROT),
        .M_AXI_awready(m00_couplers_to_psDramInterface_AWREADY),
        .M_AXI_awvalid(m00_couplers_to_psDramInterface_AWVALID),
        .M_AXI_bready(m00_couplers_to_psDramInterface_BREADY),
        .M_AXI_bresp(m00_couplers_to_psDramInterface_BRESP),
        .M_AXI_bvalid(m00_couplers_to_psDramInterface_BVALID),
        .M_AXI_rdata(m00_couplers_to_psDramInterface_RDATA),
        .M_AXI_rready(m00_couplers_to_psDramInterface_RREADY),
        .M_AXI_rresp(m00_couplers_to_psDramInterface_RRESP),
        .M_AXI_rvalid(m00_couplers_to_psDramInterface_RVALID),
        .M_AXI_wdata(m00_couplers_to_psDramInterface_WDATA),
        .M_AXI_wready(m00_couplers_to_psDramInterface_WREADY),
        .M_AXI_wstrb(m00_couplers_to_psDramInterface_WSTRB),
        .M_AXI_wvalid(m00_couplers_to_psDramInterface_WVALID),
        .S_ACLK(psDramInterface_ACLK_net),
        .S_ARESETN(psDramInterface_ARESETN_net),
        .S_AXI_araddr(xbar_to_m00_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m00_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m00_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m00_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m00_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m00_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m00_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m00_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m00_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m00_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m00_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m00_couplers_RDATA),
        .S_AXI_rready(xbar_to_m00_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m00_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m00_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m00_couplers_WDATA),
        .S_AXI_wready(xbar_to_m00_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m00_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m00_couplers_WVALID));
  m01_couplers_imp_1DP09XF m01_couplers
       (.M_ACLK(psDramInterface_ACLK_net),
        .M_ARESETN(psDramInterface_ARESETN_net),
        .M_AXI_araddr(m01_couplers_to_psDramInterface_ARADDR),
        .M_AXI_arprot(m01_couplers_to_psDramInterface_ARPROT),
        .M_AXI_arready(m01_couplers_to_psDramInterface_ARREADY),
        .M_AXI_arvalid(m01_couplers_to_psDramInterface_ARVALID),
        .M_AXI_awaddr(m01_couplers_to_psDramInterface_AWADDR),
        .M_AXI_awprot(m01_couplers_to_psDramInterface_AWPROT),
        .M_AXI_awready(m01_couplers_to_psDramInterface_AWREADY),
        .M_AXI_awvalid(m01_couplers_to_psDramInterface_AWVALID),
        .M_AXI_bready(m01_couplers_to_psDramInterface_BREADY),
        .M_AXI_bresp(m01_couplers_to_psDramInterface_BRESP),
        .M_AXI_bvalid(m01_couplers_to_psDramInterface_BVALID),
        .M_AXI_rdata(m01_couplers_to_psDramInterface_RDATA),
        .M_AXI_rready(m01_couplers_to_psDramInterface_RREADY),
        .M_AXI_rresp(m01_couplers_to_psDramInterface_RRESP),
        .M_AXI_rvalid(m01_couplers_to_psDramInterface_RVALID),
        .M_AXI_wdata(m01_couplers_to_psDramInterface_WDATA),
        .M_AXI_wready(m01_couplers_to_psDramInterface_WREADY),
        .M_AXI_wstrb(m01_couplers_to_psDramInterface_WSTRB),
        .M_AXI_wvalid(m01_couplers_to_psDramInterface_WVALID),
        .S_ACLK(psDramInterface_ACLK_net),
        .S_ARESETN(psDramInterface_ARESETN_net),
        .S_AXI_araddr(xbar_to_m01_couplers_ARADDR),
        .S_AXI_arprot(xbar_to_m01_couplers_ARPROT),
        .S_AXI_arready(xbar_to_m01_couplers_ARREADY),
        .S_AXI_arvalid(xbar_to_m01_couplers_ARVALID),
        .S_AXI_awaddr(xbar_to_m01_couplers_AWADDR),
        .S_AXI_awprot(xbar_to_m01_couplers_AWPROT),
        .S_AXI_awready(xbar_to_m01_couplers_AWREADY),
        .S_AXI_awvalid(xbar_to_m01_couplers_AWVALID),
        .S_AXI_bready(xbar_to_m01_couplers_BREADY),
        .S_AXI_bresp(xbar_to_m01_couplers_BRESP),
        .S_AXI_bvalid(xbar_to_m01_couplers_BVALID),
        .S_AXI_rdata(xbar_to_m01_couplers_RDATA),
        .S_AXI_rready(xbar_to_m01_couplers_RREADY),
        .S_AXI_rresp(xbar_to_m01_couplers_RRESP),
        .S_AXI_rvalid(xbar_to_m01_couplers_RVALID),
        .S_AXI_wdata(xbar_to_m01_couplers_WDATA),
        .S_AXI_wready(xbar_to_m01_couplers_WREADY),
        .S_AXI_wstrb(xbar_to_m01_couplers_WSTRB),
        .S_AXI_wvalid(xbar_to_m01_couplers_WVALID));
  s00_couplers_imp_19WQW9S s00_couplers
       (.M_ACLK(psDramInterface_ACLK_net),
        .M_ARESETN(psDramInterface_ARESETN_net),
        .M_AXI_araddr(s00_couplers_to_xbar_ARADDR),
        .M_AXI_arprot(s00_couplers_to_xbar_ARPROT),
        .M_AXI_arready(s00_couplers_to_xbar_ARREADY),
        .M_AXI_arvalid(s00_couplers_to_xbar_ARVALID),
        .M_AXI_awaddr(s00_couplers_to_xbar_AWADDR),
        .M_AXI_awprot(s00_couplers_to_xbar_AWPROT),
        .M_AXI_awready(s00_couplers_to_xbar_AWREADY),
        .M_AXI_awvalid(s00_couplers_to_xbar_AWVALID),
        .M_AXI_bready(s00_couplers_to_xbar_BREADY),
        .M_AXI_bresp(s00_couplers_to_xbar_BRESP),
        .M_AXI_bvalid(s00_couplers_to_xbar_BVALID),
        .M_AXI_rdata(s00_couplers_to_xbar_RDATA),
        .M_AXI_rready(s00_couplers_to_xbar_RREADY),
        .M_AXI_rresp(s00_couplers_to_xbar_RRESP),
        .M_AXI_rvalid(s00_couplers_to_xbar_RVALID),
        .M_AXI_wdata(s00_couplers_to_xbar_WDATA),
        .M_AXI_wready(s00_couplers_to_xbar_WREADY),
        .M_AXI_wstrb(s00_couplers_to_xbar_WSTRB),
        .M_AXI_wvalid(s00_couplers_to_xbar_WVALID),
        .S_ACLK(psDramInterface_ACLK_net),
        .S_ARESETN(psDramInterface_ARESETN_net),
        .S_AXI_araddr(psDramInterface_to_s00_couplers_ARADDR),
        .S_AXI_arprot(psDramInterface_to_s00_couplers_ARPROT),
        .S_AXI_arready(psDramInterface_to_s00_couplers_ARREADY),
        .S_AXI_arvalid(psDramInterface_to_s00_couplers_ARVALID),
        .S_AXI_awaddr(psDramInterface_to_s00_couplers_AWADDR),
        .S_AXI_awprot(psDramInterface_to_s00_couplers_AWPROT),
        .S_AXI_awready(psDramInterface_to_s00_couplers_AWREADY),
        .S_AXI_awvalid(psDramInterface_to_s00_couplers_AWVALID),
        .S_AXI_bready(psDramInterface_to_s00_couplers_BREADY),
        .S_AXI_bvalid(psDramInterface_to_s00_couplers_BVALID),
        .S_AXI_rdata(psDramInterface_to_s00_couplers_RDATA),
        .S_AXI_rready(psDramInterface_to_s00_couplers_RREADY),
        .S_AXI_rvalid(psDramInterface_to_s00_couplers_RVALID),
        .S_AXI_wdata(psDramInterface_to_s00_couplers_WDATA),
        .S_AXI_wready(psDramInterface_to_s00_couplers_WREADY),
        .S_AXI_wstrb(psDramInterface_to_s00_couplers_WSTRB),
        .S_AXI_wvalid(psDramInterface_to_s00_couplers_WVALID));
  cerberus_xbar_6 xbar
       (.aclk(psDramInterface_ACLK_net),
        .aresetn(psDramInterface_ARESETN_net),
        .m_axi_araddr({xbar_to_m01_couplers_ARADDR,xbar_to_m00_couplers_ARADDR}),
        .m_axi_arprot({xbar_to_m01_couplers_ARPROT,xbar_to_m00_couplers_ARPROT}),
        .m_axi_arready({xbar_to_m01_couplers_ARREADY,xbar_to_m00_couplers_ARREADY}),
        .m_axi_arvalid({xbar_to_m01_couplers_ARVALID,xbar_to_m00_couplers_ARVALID}),
        .m_axi_awaddr({xbar_to_m01_couplers_AWADDR,xbar_to_m00_couplers_AWADDR}),
        .m_axi_awprot({xbar_to_m01_couplers_AWPROT,xbar_to_m00_couplers_AWPROT}),
        .m_axi_awready({xbar_to_m01_couplers_AWREADY,xbar_to_m00_couplers_AWREADY}),
        .m_axi_awvalid({xbar_to_m01_couplers_AWVALID,xbar_to_m00_couplers_AWVALID}),
        .m_axi_bready({xbar_to_m01_couplers_BREADY,xbar_to_m00_couplers_BREADY}),
        .m_axi_bresp({xbar_to_m01_couplers_BRESP,xbar_to_m00_couplers_BRESP}),
        .m_axi_bvalid({xbar_to_m01_couplers_BVALID,xbar_to_m00_couplers_BVALID}),
        .m_axi_rdata({xbar_to_m01_couplers_RDATA,xbar_to_m00_couplers_RDATA}),
        .m_axi_rready({xbar_to_m01_couplers_RREADY,xbar_to_m00_couplers_RREADY}),
        .m_axi_rresp({xbar_to_m01_couplers_RRESP,xbar_to_m00_couplers_RRESP}),
        .m_axi_rvalid({xbar_to_m01_couplers_RVALID,xbar_to_m00_couplers_RVALID}),
        .m_axi_wdata({xbar_to_m01_couplers_WDATA,xbar_to_m00_couplers_WDATA}),
        .m_axi_wready({xbar_to_m01_couplers_WREADY,xbar_to_m00_couplers_WREADY}),
        .m_axi_wstrb({xbar_to_m01_couplers_WSTRB,xbar_to_m00_couplers_WSTRB}),
        .m_axi_wvalid({xbar_to_m01_couplers_WVALID,xbar_to_m00_couplers_WVALID}),
        .s_axi_araddr(s00_couplers_to_xbar_ARADDR),
        .s_axi_arprot(s00_couplers_to_xbar_ARPROT),
        .s_axi_arready(s00_couplers_to_xbar_ARREADY),
        .s_axi_arvalid(s00_couplers_to_xbar_ARVALID),
        .s_axi_awaddr(s00_couplers_to_xbar_AWADDR),
        .s_axi_awprot(s00_couplers_to_xbar_AWPROT),
        .s_axi_awready(s00_couplers_to_xbar_AWREADY),
        .s_axi_awvalid(s00_couplers_to_xbar_AWVALID),
        .s_axi_bready(s00_couplers_to_xbar_BREADY),
        .s_axi_bresp(s00_couplers_to_xbar_BRESP),
        .s_axi_bvalid(s00_couplers_to_xbar_BVALID),
        .s_axi_rdata(s00_couplers_to_xbar_RDATA),
        .s_axi_rready(s00_couplers_to_xbar_RREADY),
        .s_axi_rresp(s00_couplers_to_xbar_RRESP),
        .s_axi_rvalid(s00_couplers_to_xbar_RVALID),
        .s_axi_wdata(s00_couplers_to_xbar_WDATA),
        .s_axi_wready(s00_couplers_to_xbar_WREADY),
        .s_axi_wstrb(s00_couplers_to_xbar_WSTRB),
        .s_axi_wvalid(s00_couplers_to_xbar_WVALID));
endmodule

module m00_couplers_imp_1AHJTCP
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arregion,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awregion,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input [0:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [3:0]S_AXI_arregion;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input [0:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [3:0]S_AXI_awregion;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_pc_to_m00_couplers_ARADDR;
  wire auto_pc_to_m00_couplers_ARREADY;
  wire auto_pc_to_m00_couplers_ARVALID;
  wire [31:0]auto_pc_to_m00_couplers_AWADDR;
  wire auto_pc_to_m00_couplers_AWREADY;
  wire auto_pc_to_m00_couplers_AWVALID;
  wire auto_pc_to_m00_couplers_BREADY;
  wire [1:0]auto_pc_to_m00_couplers_BRESP;
  wire auto_pc_to_m00_couplers_BVALID;
  wire [31:0]auto_pc_to_m00_couplers_RDATA;
  wire auto_pc_to_m00_couplers_RREADY;
  wire [1:0]auto_pc_to_m00_couplers_RRESP;
  wire auto_pc_to_m00_couplers_RVALID;
  wire [31:0]auto_pc_to_m00_couplers_WDATA;
  wire auto_pc_to_m00_couplers_WREADY;
  wire [3:0]auto_pc_to_m00_couplers_WSTRB;
  wire auto_pc_to_m00_couplers_WVALID;
  wire [31:0]m00_couplers_to_auto_pc_ARADDR;
  wire [1:0]m00_couplers_to_auto_pc_ARBURST;
  wire [3:0]m00_couplers_to_auto_pc_ARCACHE;
  wire [11:0]m00_couplers_to_auto_pc_ARID;
  wire [7:0]m00_couplers_to_auto_pc_ARLEN;
  wire [0:0]m00_couplers_to_auto_pc_ARLOCK;
  wire [2:0]m00_couplers_to_auto_pc_ARPROT;
  wire [3:0]m00_couplers_to_auto_pc_ARQOS;
  wire m00_couplers_to_auto_pc_ARREADY;
  wire [3:0]m00_couplers_to_auto_pc_ARREGION;
  wire [2:0]m00_couplers_to_auto_pc_ARSIZE;
  wire m00_couplers_to_auto_pc_ARVALID;
  wire [31:0]m00_couplers_to_auto_pc_AWADDR;
  wire [1:0]m00_couplers_to_auto_pc_AWBURST;
  wire [3:0]m00_couplers_to_auto_pc_AWCACHE;
  wire [11:0]m00_couplers_to_auto_pc_AWID;
  wire [7:0]m00_couplers_to_auto_pc_AWLEN;
  wire [0:0]m00_couplers_to_auto_pc_AWLOCK;
  wire [2:0]m00_couplers_to_auto_pc_AWPROT;
  wire [3:0]m00_couplers_to_auto_pc_AWQOS;
  wire m00_couplers_to_auto_pc_AWREADY;
  wire [3:0]m00_couplers_to_auto_pc_AWREGION;
  wire [2:0]m00_couplers_to_auto_pc_AWSIZE;
  wire m00_couplers_to_auto_pc_AWVALID;
  wire [11:0]m00_couplers_to_auto_pc_BID;
  wire m00_couplers_to_auto_pc_BREADY;
  wire [1:0]m00_couplers_to_auto_pc_BRESP;
  wire m00_couplers_to_auto_pc_BVALID;
  wire [31:0]m00_couplers_to_auto_pc_RDATA;
  wire [11:0]m00_couplers_to_auto_pc_RID;
  wire m00_couplers_to_auto_pc_RLAST;
  wire m00_couplers_to_auto_pc_RREADY;
  wire [1:0]m00_couplers_to_auto_pc_RRESP;
  wire m00_couplers_to_auto_pc_RVALID;
  wire [31:0]m00_couplers_to_auto_pc_WDATA;
  wire m00_couplers_to_auto_pc_WLAST;
  wire m00_couplers_to_auto_pc_WREADY;
  wire [3:0]m00_couplers_to_auto_pc_WSTRB;
  wire m00_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[31:0] = auto_pc_to_m00_couplers_ARADDR;
  assign M_AXI_arvalid = auto_pc_to_m00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_pc_to_m00_couplers_AWADDR;
  assign M_AXI_awvalid = auto_pc_to_m00_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_m00_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_m00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_m00_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = auto_pc_to_m00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_m00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = m00_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = m00_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[11:0] = m00_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = m00_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = m00_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = m00_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[11:0] = m00_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = m00_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = m00_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = m00_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = m00_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_m00_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_m00_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_m00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_m00_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_m00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_m00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_m00_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_m00_couplers_WREADY = M_AXI_wready;
  assign m00_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign m00_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign m00_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign m00_couplers_to_auto_pc_ARID = S_AXI_arid[11:0];
  assign m00_couplers_to_auto_pc_ARLEN = S_AXI_arlen[7:0];
  assign m00_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[0];
  assign m00_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign m00_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign m00_couplers_to_auto_pc_ARREGION = S_AXI_arregion[3:0];
  assign m00_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign m00_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign m00_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign m00_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign m00_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign m00_couplers_to_auto_pc_AWID = S_AXI_awid[11:0];
  assign m00_couplers_to_auto_pc_AWLEN = S_AXI_awlen[7:0];
  assign m00_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[0];
  assign m00_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign m00_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign m00_couplers_to_auto_pc_AWREGION = S_AXI_awregion[3:0];
  assign m00_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign m00_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign m00_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign m00_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign m00_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign m00_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign m00_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign m00_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
  cerberus_auto_pc_0 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_m00_couplers_ARADDR),
        .m_axi_arready(auto_pc_to_m00_couplers_ARREADY),
        .m_axi_arvalid(auto_pc_to_m00_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_m00_couplers_AWADDR),
        .m_axi_awready(auto_pc_to_m00_couplers_AWREADY),
        .m_axi_awvalid(auto_pc_to_m00_couplers_AWVALID),
        .m_axi_bready(auto_pc_to_m00_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_m00_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_m00_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_m00_couplers_RDATA),
        .m_axi_rready(auto_pc_to_m00_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_m00_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_m00_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_m00_couplers_WDATA),
        .m_axi_wready(auto_pc_to_m00_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_m00_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_m00_couplers_WVALID),
        .s_axi_araddr(m00_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(m00_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(m00_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(m00_couplers_to_auto_pc_ARID),
        .s_axi_arlen(m00_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(m00_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(m00_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(m00_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(m00_couplers_to_auto_pc_ARREADY),
        .s_axi_arregion(m00_couplers_to_auto_pc_ARREGION),
        .s_axi_arsize(m00_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(m00_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(m00_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(m00_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(m00_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(m00_couplers_to_auto_pc_AWID),
        .s_axi_awlen(m00_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(m00_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(m00_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(m00_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(m00_couplers_to_auto_pc_AWREADY),
        .s_axi_awregion(m00_couplers_to_auto_pc_AWREGION),
        .s_axi_awsize(m00_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(m00_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(m00_couplers_to_auto_pc_BID),
        .s_axi_bready(m00_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(m00_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(m00_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(m00_couplers_to_auto_pc_RDATA),
        .s_axi_rid(m00_couplers_to_auto_pc_RID),
        .s_axi_rlast(m00_couplers_to_auto_pc_RLAST),
        .s_axi_rready(m00_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(m00_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(m00_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(m00_couplers_to_auto_pc_WDATA),
        .s_axi_wlast(m00_couplers_to_auto_pc_WLAST),
        .s_axi_wready(m00_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(m00_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(m00_couplers_to_auto_pc_WVALID));
endmodule

module m00_couplers_imp_1M74UV4
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [16:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [16:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [63:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [63:0]S_AXI_wdata;
  output S_AXI_wready;
  input [7:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [16:0]auto_ds_to_m00_couplers_ARADDR;
  wire [2:0]auto_ds_to_m00_couplers_ARPROT;
  wire auto_ds_to_m00_couplers_ARREADY;
  wire auto_ds_to_m00_couplers_ARVALID;
  wire [16:0]auto_ds_to_m00_couplers_AWADDR;
  wire [2:0]auto_ds_to_m00_couplers_AWPROT;
  wire auto_ds_to_m00_couplers_AWREADY;
  wire auto_ds_to_m00_couplers_AWVALID;
  wire auto_ds_to_m00_couplers_BREADY;
  wire [1:0]auto_ds_to_m00_couplers_BRESP;
  wire auto_ds_to_m00_couplers_BVALID;
  wire [31:0]auto_ds_to_m00_couplers_RDATA;
  wire auto_ds_to_m00_couplers_RREADY;
  wire [1:0]auto_ds_to_m00_couplers_RRESP;
  wire auto_ds_to_m00_couplers_RVALID;
  wire [31:0]auto_ds_to_m00_couplers_WDATA;
  wire auto_ds_to_m00_couplers_WREADY;
  wire [3:0]auto_ds_to_m00_couplers_WSTRB;
  wire auto_ds_to_m00_couplers_WVALID;
  wire [31:0]m00_couplers_to_auto_ds_ARADDR;
  wire [2:0]m00_couplers_to_auto_ds_ARPROT;
  wire m00_couplers_to_auto_ds_ARREADY;
  wire m00_couplers_to_auto_ds_ARVALID;
  wire [31:0]m00_couplers_to_auto_ds_AWADDR;
  wire [2:0]m00_couplers_to_auto_ds_AWPROT;
  wire m00_couplers_to_auto_ds_AWREADY;
  wire m00_couplers_to_auto_ds_AWVALID;
  wire m00_couplers_to_auto_ds_BREADY;
  wire [1:0]m00_couplers_to_auto_ds_BRESP;
  wire m00_couplers_to_auto_ds_BVALID;
  wire [63:0]m00_couplers_to_auto_ds_RDATA;
  wire m00_couplers_to_auto_ds_RREADY;
  wire [1:0]m00_couplers_to_auto_ds_RRESP;
  wire m00_couplers_to_auto_ds_RVALID;
  wire [63:0]m00_couplers_to_auto_ds_WDATA;
  wire m00_couplers_to_auto_ds_WREADY;
  wire [7:0]m00_couplers_to_auto_ds_WSTRB;
  wire m00_couplers_to_auto_ds_WVALID;

  assign M_AXI_araddr[16:0] = auto_ds_to_m00_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = auto_ds_to_m00_couplers_ARPROT;
  assign M_AXI_arvalid = auto_ds_to_m00_couplers_ARVALID;
  assign M_AXI_awaddr[16:0] = auto_ds_to_m00_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = auto_ds_to_m00_couplers_AWPROT;
  assign M_AXI_awvalid = auto_ds_to_m00_couplers_AWVALID;
  assign M_AXI_bready = auto_ds_to_m00_couplers_BREADY;
  assign M_AXI_rready = auto_ds_to_m00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_ds_to_m00_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = auto_ds_to_m00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_ds_to_m00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = m00_couplers_to_auto_ds_ARREADY;
  assign S_AXI_awready = m00_couplers_to_auto_ds_AWREADY;
  assign S_AXI_bresp[1:0] = m00_couplers_to_auto_ds_BRESP;
  assign S_AXI_bvalid = m00_couplers_to_auto_ds_BVALID;
  assign S_AXI_rdata[63:0] = m00_couplers_to_auto_ds_RDATA;
  assign S_AXI_rresp[1:0] = m00_couplers_to_auto_ds_RRESP;
  assign S_AXI_rvalid = m00_couplers_to_auto_ds_RVALID;
  assign S_AXI_wready = m00_couplers_to_auto_ds_WREADY;
  assign auto_ds_to_m00_couplers_ARREADY = M_AXI_arready;
  assign auto_ds_to_m00_couplers_AWREADY = M_AXI_awready;
  assign auto_ds_to_m00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_ds_to_m00_couplers_BVALID = M_AXI_bvalid;
  assign auto_ds_to_m00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_ds_to_m00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_ds_to_m00_couplers_RVALID = M_AXI_rvalid;
  assign auto_ds_to_m00_couplers_WREADY = M_AXI_wready;
  assign m00_couplers_to_auto_ds_ARADDR = S_AXI_araddr[31:0];
  assign m00_couplers_to_auto_ds_ARPROT = S_AXI_arprot[2:0];
  assign m00_couplers_to_auto_ds_ARVALID = S_AXI_arvalid;
  assign m00_couplers_to_auto_ds_AWADDR = S_AXI_awaddr[31:0];
  assign m00_couplers_to_auto_ds_AWPROT = S_AXI_awprot[2:0];
  assign m00_couplers_to_auto_ds_AWVALID = S_AXI_awvalid;
  assign m00_couplers_to_auto_ds_BREADY = S_AXI_bready;
  assign m00_couplers_to_auto_ds_RREADY = S_AXI_rready;
  assign m00_couplers_to_auto_ds_WDATA = S_AXI_wdata[63:0];
  assign m00_couplers_to_auto_ds_WSTRB = S_AXI_wstrb[7:0];
  assign m00_couplers_to_auto_ds_WVALID = S_AXI_wvalid;
  cerberus_auto_ds_1 auto_ds
       (.m_axi_araddr(auto_ds_to_m00_couplers_ARADDR),
        .m_axi_arprot(auto_ds_to_m00_couplers_ARPROT),
        .m_axi_arready(auto_ds_to_m00_couplers_ARREADY),
        .m_axi_arvalid(auto_ds_to_m00_couplers_ARVALID),
        .m_axi_awaddr(auto_ds_to_m00_couplers_AWADDR),
        .m_axi_awprot(auto_ds_to_m00_couplers_AWPROT),
        .m_axi_awready(auto_ds_to_m00_couplers_AWREADY),
        .m_axi_awvalid(auto_ds_to_m00_couplers_AWVALID),
        .m_axi_bready(auto_ds_to_m00_couplers_BREADY),
        .m_axi_bresp(auto_ds_to_m00_couplers_BRESP),
        .m_axi_bvalid(auto_ds_to_m00_couplers_BVALID),
        .m_axi_rdata(auto_ds_to_m00_couplers_RDATA),
        .m_axi_rready(auto_ds_to_m00_couplers_RREADY),
        .m_axi_rresp(auto_ds_to_m00_couplers_RRESP),
        .m_axi_rvalid(auto_ds_to_m00_couplers_RVALID),
        .m_axi_wdata(auto_ds_to_m00_couplers_WDATA),
        .m_axi_wready(auto_ds_to_m00_couplers_WREADY),
        .m_axi_wstrb(auto_ds_to_m00_couplers_WSTRB),
        .m_axi_wvalid(auto_ds_to_m00_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(m00_couplers_to_auto_ds_ARADDR[16:0]),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arprot(m00_couplers_to_auto_ds_ARPROT),
        .s_axi_arready(m00_couplers_to_auto_ds_ARREADY),
        .s_axi_arvalid(m00_couplers_to_auto_ds_ARVALID),
        .s_axi_awaddr(m00_couplers_to_auto_ds_AWADDR[16:0]),
        .s_axi_awprot(m00_couplers_to_auto_ds_AWPROT),
        .s_axi_awready(m00_couplers_to_auto_ds_AWREADY),
        .s_axi_awvalid(m00_couplers_to_auto_ds_AWVALID),
        .s_axi_bready(m00_couplers_to_auto_ds_BREADY),
        .s_axi_bresp(m00_couplers_to_auto_ds_BRESP),
        .s_axi_bvalid(m00_couplers_to_auto_ds_BVALID),
        .s_axi_rdata(m00_couplers_to_auto_ds_RDATA),
        .s_axi_rready(m00_couplers_to_auto_ds_RREADY),
        .s_axi_rresp(m00_couplers_to_auto_ds_RRESP),
        .s_axi_rvalid(m00_couplers_to_auto_ds_RVALID),
        .s_axi_wdata(m00_couplers_to_auto_ds_WDATA),
        .s_axi_wready(m00_couplers_to_auto_ds_WREADY),
        .s_axi_wstrb(m00_couplers_to_auto_ds_WSTRB),
        .s_axi_wvalid(m00_couplers_to_auto_ds_WVALID));
endmodule

module m00_couplers_imp_1YR4BR9
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [16:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [16:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [63:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [63:0]S_AXI_wdata;
  output S_AXI_wready;
  input [7:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [16:0]auto_ds_to_m00_couplers_ARADDR;
  wire [2:0]auto_ds_to_m00_couplers_ARPROT;
  wire auto_ds_to_m00_couplers_ARREADY;
  wire auto_ds_to_m00_couplers_ARVALID;
  wire [16:0]auto_ds_to_m00_couplers_AWADDR;
  wire [2:0]auto_ds_to_m00_couplers_AWPROT;
  wire auto_ds_to_m00_couplers_AWREADY;
  wire auto_ds_to_m00_couplers_AWVALID;
  wire auto_ds_to_m00_couplers_BREADY;
  wire [1:0]auto_ds_to_m00_couplers_BRESP;
  wire auto_ds_to_m00_couplers_BVALID;
  wire [31:0]auto_ds_to_m00_couplers_RDATA;
  wire auto_ds_to_m00_couplers_RREADY;
  wire [1:0]auto_ds_to_m00_couplers_RRESP;
  wire auto_ds_to_m00_couplers_RVALID;
  wire [31:0]auto_ds_to_m00_couplers_WDATA;
  wire auto_ds_to_m00_couplers_WREADY;
  wire [3:0]auto_ds_to_m00_couplers_WSTRB;
  wire auto_ds_to_m00_couplers_WVALID;
  wire [31:0]m00_couplers_to_auto_ds_ARADDR;
  wire [2:0]m00_couplers_to_auto_ds_ARPROT;
  wire m00_couplers_to_auto_ds_ARREADY;
  wire m00_couplers_to_auto_ds_ARVALID;
  wire [31:0]m00_couplers_to_auto_ds_AWADDR;
  wire [2:0]m00_couplers_to_auto_ds_AWPROT;
  wire m00_couplers_to_auto_ds_AWREADY;
  wire m00_couplers_to_auto_ds_AWVALID;
  wire m00_couplers_to_auto_ds_BREADY;
  wire [1:0]m00_couplers_to_auto_ds_BRESP;
  wire m00_couplers_to_auto_ds_BVALID;
  wire [63:0]m00_couplers_to_auto_ds_RDATA;
  wire m00_couplers_to_auto_ds_RREADY;
  wire [1:0]m00_couplers_to_auto_ds_RRESP;
  wire m00_couplers_to_auto_ds_RVALID;
  wire [63:0]m00_couplers_to_auto_ds_WDATA;
  wire m00_couplers_to_auto_ds_WREADY;
  wire [7:0]m00_couplers_to_auto_ds_WSTRB;
  wire m00_couplers_to_auto_ds_WVALID;

  assign M_AXI_araddr[16:0] = auto_ds_to_m00_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = auto_ds_to_m00_couplers_ARPROT;
  assign M_AXI_arvalid = auto_ds_to_m00_couplers_ARVALID;
  assign M_AXI_awaddr[16:0] = auto_ds_to_m00_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = auto_ds_to_m00_couplers_AWPROT;
  assign M_AXI_awvalid = auto_ds_to_m00_couplers_AWVALID;
  assign M_AXI_bready = auto_ds_to_m00_couplers_BREADY;
  assign M_AXI_rready = auto_ds_to_m00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_ds_to_m00_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = auto_ds_to_m00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_ds_to_m00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = m00_couplers_to_auto_ds_ARREADY;
  assign S_AXI_awready = m00_couplers_to_auto_ds_AWREADY;
  assign S_AXI_bresp[1:0] = m00_couplers_to_auto_ds_BRESP;
  assign S_AXI_bvalid = m00_couplers_to_auto_ds_BVALID;
  assign S_AXI_rdata[63:0] = m00_couplers_to_auto_ds_RDATA;
  assign S_AXI_rresp[1:0] = m00_couplers_to_auto_ds_RRESP;
  assign S_AXI_rvalid = m00_couplers_to_auto_ds_RVALID;
  assign S_AXI_wready = m00_couplers_to_auto_ds_WREADY;
  assign auto_ds_to_m00_couplers_ARREADY = M_AXI_arready;
  assign auto_ds_to_m00_couplers_AWREADY = M_AXI_awready;
  assign auto_ds_to_m00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_ds_to_m00_couplers_BVALID = M_AXI_bvalid;
  assign auto_ds_to_m00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_ds_to_m00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_ds_to_m00_couplers_RVALID = M_AXI_rvalid;
  assign auto_ds_to_m00_couplers_WREADY = M_AXI_wready;
  assign m00_couplers_to_auto_ds_ARADDR = S_AXI_araddr[31:0];
  assign m00_couplers_to_auto_ds_ARPROT = S_AXI_arprot[2:0];
  assign m00_couplers_to_auto_ds_ARVALID = S_AXI_arvalid;
  assign m00_couplers_to_auto_ds_AWADDR = S_AXI_awaddr[31:0];
  assign m00_couplers_to_auto_ds_AWPROT = S_AXI_awprot[2:0];
  assign m00_couplers_to_auto_ds_AWVALID = S_AXI_awvalid;
  assign m00_couplers_to_auto_ds_BREADY = S_AXI_bready;
  assign m00_couplers_to_auto_ds_RREADY = S_AXI_rready;
  assign m00_couplers_to_auto_ds_WDATA = S_AXI_wdata[63:0];
  assign m00_couplers_to_auto_ds_WSTRB = S_AXI_wstrb[7:0];
  assign m00_couplers_to_auto_ds_WVALID = S_AXI_wvalid;
  cerberus_auto_ds_2 auto_ds
       (.m_axi_araddr(auto_ds_to_m00_couplers_ARADDR),
        .m_axi_arprot(auto_ds_to_m00_couplers_ARPROT),
        .m_axi_arready(auto_ds_to_m00_couplers_ARREADY),
        .m_axi_arvalid(auto_ds_to_m00_couplers_ARVALID),
        .m_axi_awaddr(auto_ds_to_m00_couplers_AWADDR),
        .m_axi_awprot(auto_ds_to_m00_couplers_AWPROT),
        .m_axi_awready(auto_ds_to_m00_couplers_AWREADY),
        .m_axi_awvalid(auto_ds_to_m00_couplers_AWVALID),
        .m_axi_bready(auto_ds_to_m00_couplers_BREADY),
        .m_axi_bresp(auto_ds_to_m00_couplers_BRESP),
        .m_axi_bvalid(auto_ds_to_m00_couplers_BVALID),
        .m_axi_rdata(auto_ds_to_m00_couplers_RDATA),
        .m_axi_rready(auto_ds_to_m00_couplers_RREADY),
        .m_axi_rresp(auto_ds_to_m00_couplers_RRESP),
        .m_axi_rvalid(auto_ds_to_m00_couplers_RVALID),
        .m_axi_wdata(auto_ds_to_m00_couplers_WDATA),
        .m_axi_wready(auto_ds_to_m00_couplers_WREADY),
        .m_axi_wstrb(auto_ds_to_m00_couplers_WSTRB),
        .m_axi_wvalid(auto_ds_to_m00_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(m00_couplers_to_auto_ds_ARADDR[16:0]),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arprot(m00_couplers_to_auto_ds_ARPROT),
        .s_axi_arready(m00_couplers_to_auto_ds_ARREADY),
        .s_axi_arvalid(m00_couplers_to_auto_ds_ARVALID),
        .s_axi_awaddr(m00_couplers_to_auto_ds_AWADDR[16:0]),
        .s_axi_awprot(m00_couplers_to_auto_ds_AWPROT),
        .s_axi_awready(m00_couplers_to_auto_ds_AWREADY),
        .s_axi_awvalid(m00_couplers_to_auto_ds_AWVALID),
        .s_axi_bready(m00_couplers_to_auto_ds_BREADY),
        .s_axi_bresp(m00_couplers_to_auto_ds_BRESP),
        .s_axi_bvalid(m00_couplers_to_auto_ds_BVALID),
        .s_axi_rdata(m00_couplers_to_auto_ds_RDATA),
        .s_axi_rready(m00_couplers_to_auto_ds_RREADY),
        .s_axi_rresp(m00_couplers_to_auto_ds_RRESP),
        .s_axi_rvalid(m00_couplers_to_auto_ds_RVALID),
        .s_axi_wdata(m00_couplers_to_auto_ds_WDATA),
        .s_axi_wready(m00_couplers_to_auto_ds_WREADY),
        .s_axi_wstrb(m00_couplers_to_auto_ds_WSTRB),
        .s_axi_wvalid(m00_couplers_to_auto_ds_WVALID));
endmodule

module m00_couplers_imp_4W37PH
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [16:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [16:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [63:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [63:0]S_AXI_wdata;
  output S_AXI_wready;
  input [7:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [16:0]auto_ds_to_m00_couplers_ARADDR;
  wire [2:0]auto_ds_to_m00_couplers_ARPROT;
  wire auto_ds_to_m00_couplers_ARREADY;
  wire auto_ds_to_m00_couplers_ARVALID;
  wire [16:0]auto_ds_to_m00_couplers_AWADDR;
  wire [2:0]auto_ds_to_m00_couplers_AWPROT;
  wire auto_ds_to_m00_couplers_AWREADY;
  wire auto_ds_to_m00_couplers_AWVALID;
  wire auto_ds_to_m00_couplers_BREADY;
  wire [1:0]auto_ds_to_m00_couplers_BRESP;
  wire auto_ds_to_m00_couplers_BVALID;
  wire [31:0]auto_ds_to_m00_couplers_RDATA;
  wire auto_ds_to_m00_couplers_RREADY;
  wire [1:0]auto_ds_to_m00_couplers_RRESP;
  wire auto_ds_to_m00_couplers_RVALID;
  wire [31:0]auto_ds_to_m00_couplers_WDATA;
  wire auto_ds_to_m00_couplers_WREADY;
  wire [3:0]auto_ds_to_m00_couplers_WSTRB;
  wire auto_ds_to_m00_couplers_WVALID;
  wire [31:0]m00_couplers_to_auto_ds_ARADDR;
  wire [2:0]m00_couplers_to_auto_ds_ARPROT;
  wire m00_couplers_to_auto_ds_ARREADY;
  wire m00_couplers_to_auto_ds_ARVALID;
  wire [31:0]m00_couplers_to_auto_ds_AWADDR;
  wire [2:0]m00_couplers_to_auto_ds_AWPROT;
  wire m00_couplers_to_auto_ds_AWREADY;
  wire m00_couplers_to_auto_ds_AWVALID;
  wire m00_couplers_to_auto_ds_BREADY;
  wire [1:0]m00_couplers_to_auto_ds_BRESP;
  wire m00_couplers_to_auto_ds_BVALID;
  wire [63:0]m00_couplers_to_auto_ds_RDATA;
  wire m00_couplers_to_auto_ds_RREADY;
  wire [1:0]m00_couplers_to_auto_ds_RRESP;
  wire m00_couplers_to_auto_ds_RVALID;
  wire [63:0]m00_couplers_to_auto_ds_WDATA;
  wire m00_couplers_to_auto_ds_WREADY;
  wire [7:0]m00_couplers_to_auto_ds_WSTRB;
  wire m00_couplers_to_auto_ds_WVALID;

  assign M_AXI_araddr[16:0] = auto_ds_to_m00_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = auto_ds_to_m00_couplers_ARPROT;
  assign M_AXI_arvalid = auto_ds_to_m00_couplers_ARVALID;
  assign M_AXI_awaddr[16:0] = auto_ds_to_m00_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = auto_ds_to_m00_couplers_AWPROT;
  assign M_AXI_awvalid = auto_ds_to_m00_couplers_AWVALID;
  assign M_AXI_bready = auto_ds_to_m00_couplers_BREADY;
  assign M_AXI_rready = auto_ds_to_m00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_ds_to_m00_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = auto_ds_to_m00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_ds_to_m00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = m00_couplers_to_auto_ds_ARREADY;
  assign S_AXI_awready = m00_couplers_to_auto_ds_AWREADY;
  assign S_AXI_bresp[1:0] = m00_couplers_to_auto_ds_BRESP;
  assign S_AXI_bvalid = m00_couplers_to_auto_ds_BVALID;
  assign S_AXI_rdata[63:0] = m00_couplers_to_auto_ds_RDATA;
  assign S_AXI_rresp[1:0] = m00_couplers_to_auto_ds_RRESP;
  assign S_AXI_rvalid = m00_couplers_to_auto_ds_RVALID;
  assign S_AXI_wready = m00_couplers_to_auto_ds_WREADY;
  assign auto_ds_to_m00_couplers_ARREADY = M_AXI_arready;
  assign auto_ds_to_m00_couplers_AWREADY = M_AXI_awready;
  assign auto_ds_to_m00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_ds_to_m00_couplers_BVALID = M_AXI_bvalid;
  assign auto_ds_to_m00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_ds_to_m00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_ds_to_m00_couplers_RVALID = M_AXI_rvalid;
  assign auto_ds_to_m00_couplers_WREADY = M_AXI_wready;
  assign m00_couplers_to_auto_ds_ARADDR = S_AXI_araddr[31:0];
  assign m00_couplers_to_auto_ds_ARPROT = S_AXI_arprot[2:0];
  assign m00_couplers_to_auto_ds_ARVALID = S_AXI_arvalid;
  assign m00_couplers_to_auto_ds_AWADDR = S_AXI_awaddr[31:0];
  assign m00_couplers_to_auto_ds_AWPROT = S_AXI_awprot[2:0];
  assign m00_couplers_to_auto_ds_AWVALID = S_AXI_awvalid;
  assign m00_couplers_to_auto_ds_BREADY = S_AXI_bready;
  assign m00_couplers_to_auto_ds_RREADY = S_AXI_rready;
  assign m00_couplers_to_auto_ds_WDATA = S_AXI_wdata[63:0];
  assign m00_couplers_to_auto_ds_WSTRB = S_AXI_wstrb[7:0];
  assign m00_couplers_to_auto_ds_WVALID = S_AXI_wvalid;
  cerberus_auto_ds_0 auto_ds
       (.m_axi_araddr(auto_ds_to_m00_couplers_ARADDR),
        .m_axi_arprot(auto_ds_to_m00_couplers_ARPROT),
        .m_axi_arready(auto_ds_to_m00_couplers_ARREADY),
        .m_axi_arvalid(auto_ds_to_m00_couplers_ARVALID),
        .m_axi_awaddr(auto_ds_to_m00_couplers_AWADDR),
        .m_axi_awprot(auto_ds_to_m00_couplers_AWPROT),
        .m_axi_awready(auto_ds_to_m00_couplers_AWREADY),
        .m_axi_awvalid(auto_ds_to_m00_couplers_AWVALID),
        .m_axi_bready(auto_ds_to_m00_couplers_BREADY),
        .m_axi_bresp(auto_ds_to_m00_couplers_BRESP),
        .m_axi_bvalid(auto_ds_to_m00_couplers_BVALID),
        .m_axi_rdata(auto_ds_to_m00_couplers_RDATA),
        .m_axi_rready(auto_ds_to_m00_couplers_RREADY),
        .m_axi_rresp(auto_ds_to_m00_couplers_RRESP),
        .m_axi_rvalid(auto_ds_to_m00_couplers_RVALID),
        .m_axi_wdata(auto_ds_to_m00_couplers_WDATA),
        .m_axi_wready(auto_ds_to_m00_couplers_WREADY),
        .m_axi_wstrb(auto_ds_to_m00_couplers_WSTRB),
        .m_axi_wvalid(auto_ds_to_m00_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(m00_couplers_to_auto_ds_ARADDR[16:0]),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arprot(m00_couplers_to_auto_ds_ARPROT),
        .s_axi_arready(m00_couplers_to_auto_ds_ARREADY),
        .s_axi_arvalid(m00_couplers_to_auto_ds_ARVALID),
        .s_axi_awaddr(m00_couplers_to_auto_ds_AWADDR[16:0]),
        .s_axi_awprot(m00_couplers_to_auto_ds_AWPROT),
        .s_axi_awready(m00_couplers_to_auto_ds_AWREADY),
        .s_axi_awvalid(m00_couplers_to_auto_ds_AWVALID),
        .s_axi_bready(m00_couplers_to_auto_ds_BREADY),
        .s_axi_bresp(m00_couplers_to_auto_ds_BRESP),
        .s_axi_bvalid(m00_couplers_to_auto_ds_BVALID),
        .s_axi_rdata(m00_couplers_to_auto_ds_RDATA),
        .s_axi_rready(m00_couplers_to_auto_ds_RREADY),
        .s_axi_rresp(m00_couplers_to_auto_ds_RRESP),
        .s_axi_rvalid(m00_couplers_to_auto_ds_RVALID),
        .s_axi_wdata(m00_couplers_to_auto_ds_WDATA),
        .s_axi_wready(m00_couplers_to_auto_ds_WREADY),
        .s_axi_wstrb(m00_couplers_to_auto_ds_WSTRB),
        .s_axi_wvalid(m00_couplers_to_auto_ds_WVALID));
endmodule

module m00_couplers_imp_LTHPB
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [3:0]M_AXI_arlen;
  output [1:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [3:0]M_AXI_awlen;
  output [1:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [63:0]M_AXI_rdata;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [63:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [7:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [63:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [63:0]S_AXI_wdata;
  output S_AXI_wready;
  input [7:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_pc_to_m00_couplers_ARADDR;
  wire [1:0]auto_pc_to_m00_couplers_ARBURST;
  wire [3:0]auto_pc_to_m00_couplers_ARCACHE;
  wire [3:0]auto_pc_to_m00_couplers_ARLEN;
  wire [1:0]auto_pc_to_m00_couplers_ARLOCK;
  wire [2:0]auto_pc_to_m00_couplers_ARPROT;
  wire [3:0]auto_pc_to_m00_couplers_ARQOS;
  wire auto_pc_to_m00_couplers_ARREADY;
  wire [2:0]auto_pc_to_m00_couplers_ARSIZE;
  wire auto_pc_to_m00_couplers_ARVALID;
  wire [31:0]auto_pc_to_m00_couplers_AWADDR;
  wire [1:0]auto_pc_to_m00_couplers_AWBURST;
  wire [3:0]auto_pc_to_m00_couplers_AWCACHE;
  wire [3:0]auto_pc_to_m00_couplers_AWLEN;
  wire [1:0]auto_pc_to_m00_couplers_AWLOCK;
  wire [2:0]auto_pc_to_m00_couplers_AWPROT;
  wire [3:0]auto_pc_to_m00_couplers_AWQOS;
  wire auto_pc_to_m00_couplers_AWREADY;
  wire [2:0]auto_pc_to_m00_couplers_AWSIZE;
  wire auto_pc_to_m00_couplers_AWVALID;
  wire auto_pc_to_m00_couplers_BREADY;
  wire [1:0]auto_pc_to_m00_couplers_BRESP;
  wire auto_pc_to_m00_couplers_BVALID;
  wire [63:0]auto_pc_to_m00_couplers_RDATA;
  wire auto_pc_to_m00_couplers_RLAST;
  wire auto_pc_to_m00_couplers_RREADY;
  wire [1:0]auto_pc_to_m00_couplers_RRESP;
  wire auto_pc_to_m00_couplers_RVALID;
  wire [63:0]auto_pc_to_m00_couplers_WDATA;
  wire auto_pc_to_m00_couplers_WLAST;
  wire auto_pc_to_m00_couplers_WREADY;
  wire [7:0]auto_pc_to_m00_couplers_WSTRB;
  wire auto_pc_to_m00_couplers_WVALID;
  wire [31:0]m00_couplers_to_auto_pc_ARADDR;
  wire [2:0]m00_couplers_to_auto_pc_ARPROT;
  wire m00_couplers_to_auto_pc_ARREADY;
  wire m00_couplers_to_auto_pc_ARVALID;
  wire [31:0]m00_couplers_to_auto_pc_AWADDR;
  wire [2:0]m00_couplers_to_auto_pc_AWPROT;
  wire m00_couplers_to_auto_pc_AWREADY;
  wire m00_couplers_to_auto_pc_AWVALID;
  wire m00_couplers_to_auto_pc_BREADY;
  wire [1:0]m00_couplers_to_auto_pc_BRESP;
  wire m00_couplers_to_auto_pc_BVALID;
  wire [63:0]m00_couplers_to_auto_pc_RDATA;
  wire m00_couplers_to_auto_pc_RREADY;
  wire [1:0]m00_couplers_to_auto_pc_RRESP;
  wire m00_couplers_to_auto_pc_RVALID;
  wire [63:0]m00_couplers_to_auto_pc_WDATA;
  wire m00_couplers_to_auto_pc_WREADY;
  wire [7:0]m00_couplers_to_auto_pc_WSTRB;
  wire m00_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[31:0] = auto_pc_to_m00_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_pc_to_m00_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_pc_to_m00_couplers_ARCACHE;
  assign M_AXI_arlen[3:0] = auto_pc_to_m00_couplers_ARLEN;
  assign M_AXI_arlock[1:0] = auto_pc_to_m00_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_pc_to_m00_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_pc_to_m00_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = auto_pc_to_m00_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_pc_to_m00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_pc_to_m00_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = auto_pc_to_m00_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_pc_to_m00_couplers_AWCACHE;
  assign M_AXI_awlen[3:0] = auto_pc_to_m00_couplers_AWLEN;
  assign M_AXI_awlock[1:0] = auto_pc_to_m00_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_pc_to_m00_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_pc_to_m00_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = auto_pc_to_m00_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_pc_to_m00_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_m00_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_m00_couplers_RREADY;
  assign M_AXI_wdata[63:0] = auto_pc_to_m00_couplers_WDATA;
  assign M_AXI_wlast = auto_pc_to_m00_couplers_WLAST;
  assign M_AXI_wstrb[7:0] = auto_pc_to_m00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_m00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = m00_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = m00_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bresp[1:0] = m00_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = m00_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[63:0] = m00_couplers_to_auto_pc_RDATA;
  assign S_AXI_rresp[1:0] = m00_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = m00_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = m00_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_m00_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_m00_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_m00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_m00_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_m00_couplers_RDATA = M_AXI_rdata[63:0];
  assign auto_pc_to_m00_couplers_RLAST = M_AXI_rlast;
  assign auto_pc_to_m00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_m00_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_m00_couplers_WREADY = M_AXI_wready;
  assign m00_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign m00_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign m00_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign m00_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign m00_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign m00_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign m00_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign m00_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign m00_couplers_to_auto_pc_WDATA = S_AXI_wdata[63:0];
  assign m00_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[7:0];
  assign m00_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
  cerberus_auto_pc_3 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_m00_couplers_ARADDR),
        .m_axi_arburst(auto_pc_to_m00_couplers_ARBURST),
        .m_axi_arcache(auto_pc_to_m00_couplers_ARCACHE),
        .m_axi_arlen(auto_pc_to_m00_couplers_ARLEN),
        .m_axi_arlock(auto_pc_to_m00_couplers_ARLOCK),
        .m_axi_arprot(auto_pc_to_m00_couplers_ARPROT),
        .m_axi_arqos(auto_pc_to_m00_couplers_ARQOS),
        .m_axi_arready(auto_pc_to_m00_couplers_ARREADY),
        .m_axi_arsize(auto_pc_to_m00_couplers_ARSIZE),
        .m_axi_arvalid(auto_pc_to_m00_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_m00_couplers_AWADDR),
        .m_axi_awburst(auto_pc_to_m00_couplers_AWBURST),
        .m_axi_awcache(auto_pc_to_m00_couplers_AWCACHE),
        .m_axi_awlen(auto_pc_to_m00_couplers_AWLEN),
        .m_axi_awlock(auto_pc_to_m00_couplers_AWLOCK),
        .m_axi_awprot(auto_pc_to_m00_couplers_AWPROT),
        .m_axi_awqos(auto_pc_to_m00_couplers_AWQOS),
        .m_axi_awready(auto_pc_to_m00_couplers_AWREADY),
        .m_axi_awsize(auto_pc_to_m00_couplers_AWSIZE),
        .m_axi_awvalid(auto_pc_to_m00_couplers_AWVALID),
        .m_axi_bready(auto_pc_to_m00_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_m00_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_m00_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_m00_couplers_RDATA),
        .m_axi_rlast(auto_pc_to_m00_couplers_RLAST),
        .m_axi_rready(auto_pc_to_m00_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_m00_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_m00_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_m00_couplers_WDATA),
        .m_axi_wlast(auto_pc_to_m00_couplers_WLAST),
        .m_axi_wready(auto_pc_to_m00_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_m00_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_m00_couplers_WVALID),
        .s_axi_araddr(m00_couplers_to_auto_pc_ARADDR),
        .s_axi_arprot(m00_couplers_to_auto_pc_ARPROT),
        .s_axi_arready(m00_couplers_to_auto_pc_ARREADY),
        .s_axi_arvalid(m00_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(m00_couplers_to_auto_pc_AWADDR),
        .s_axi_awprot(m00_couplers_to_auto_pc_AWPROT),
        .s_axi_awready(m00_couplers_to_auto_pc_AWREADY),
        .s_axi_awvalid(m00_couplers_to_auto_pc_AWVALID),
        .s_axi_bready(m00_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(m00_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(m00_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(m00_couplers_to_auto_pc_RDATA),
        .s_axi_rready(m00_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(m00_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(m00_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(m00_couplers_to_auto_pc_WDATA),
        .s_axi_wready(m00_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(m00_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(m00_couplers_to_auto_pc_WVALID));
endmodule

module m00_couplers_imp_PLT5EQ
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [16:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [16:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [63:0]S_AXI_rdata;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [63:0]S_AXI_wdata;
  output S_AXI_wready;
  input [7:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [16:0]auto_ds_to_m00_couplers_ARADDR;
  wire [2:0]auto_ds_to_m00_couplers_ARPROT;
  wire auto_ds_to_m00_couplers_ARREADY;
  wire auto_ds_to_m00_couplers_ARVALID;
  wire [16:0]auto_ds_to_m00_couplers_AWADDR;
  wire [2:0]auto_ds_to_m00_couplers_AWPROT;
  wire auto_ds_to_m00_couplers_AWREADY;
  wire auto_ds_to_m00_couplers_AWVALID;
  wire auto_ds_to_m00_couplers_BREADY;
  wire [1:0]auto_ds_to_m00_couplers_BRESP;
  wire auto_ds_to_m00_couplers_BVALID;
  wire [31:0]auto_ds_to_m00_couplers_RDATA;
  wire auto_ds_to_m00_couplers_RREADY;
  wire [1:0]auto_ds_to_m00_couplers_RRESP;
  wire auto_ds_to_m00_couplers_RVALID;
  wire [31:0]auto_ds_to_m00_couplers_WDATA;
  wire auto_ds_to_m00_couplers_WREADY;
  wire [3:0]auto_ds_to_m00_couplers_WSTRB;
  wire auto_ds_to_m00_couplers_WVALID;
  wire [31:0]m00_couplers_to_auto_ds_ARADDR;
  wire [2:0]m00_couplers_to_auto_ds_ARPROT;
  wire m00_couplers_to_auto_ds_ARREADY;
  wire m00_couplers_to_auto_ds_ARVALID;
  wire [31:0]m00_couplers_to_auto_ds_AWADDR;
  wire [2:0]m00_couplers_to_auto_ds_AWPROT;
  wire m00_couplers_to_auto_ds_AWREADY;
  wire m00_couplers_to_auto_ds_AWVALID;
  wire m00_couplers_to_auto_ds_BREADY;
  wire [1:0]m00_couplers_to_auto_ds_BRESP;
  wire m00_couplers_to_auto_ds_BVALID;
  wire [63:0]m00_couplers_to_auto_ds_RDATA;
  wire m00_couplers_to_auto_ds_RREADY;
  wire [1:0]m00_couplers_to_auto_ds_RRESP;
  wire m00_couplers_to_auto_ds_RVALID;
  wire [63:0]m00_couplers_to_auto_ds_WDATA;
  wire m00_couplers_to_auto_ds_WREADY;
  wire [7:0]m00_couplers_to_auto_ds_WSTRB;
  wire m00_couplers_to_auto_ds_WVALID;

  assign M_AXI_araddr[16:0] = auto_ds_to_m00_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = auto_ds_to_m00_couplers_ARPROT;
  assign M_AXI_arvalid = auto_ds_to_m00_couplers_ARVALID;
  assign M_AXI_awaddr[16:0] = auto_ds_to_m00_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = auto_ds_to_m00_couplers_AWPROT;
  assign M_AXI_awvalid = auto_ds_to_m00_couplers_AWVALID;
  assign M_AXI_bready = auto_ds_to_m00_couplers_BREADY;
  assign M_AXI_rready = auto_ds_to_m00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_ds_to_m00_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = auto_ds_to_m00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_ds_to_m00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = m00_couplers_to_auto_ds_ARREADY;
  assign S_AXI_awready = m00_couplers_to_auto_ds_AWREADY;
  assign S_AXI_bresp[1:0] = m00_couplers_to_auto_ds_BRESP;
  assign S_AXI_bvalid = m00_couplers_to_auto_ds_BVALID;
  assign S_AXI_rdata[63:0] = m00_couplers_to_auto_ds_RDATA;
  assign S_AXI_rresp[1:0] = m00_couplers_to_auto_ds_RRESP;
  assign S_AXI_rvalid = m00_couplers_to_auto_ds_RVALID;
  assign S_AXI_wready = m00_couplers_to_auto_ds_WREADY;
  assign auto_ds_to_m00_couplers_ARREADY = M_AXI_arready;
  assign auto_ds_to_m00_couplers_AWREADY = M_AXI_awready;
  assign auto_ds_to_m00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_ds_to_m00_couplers_BVALID = M_AXI_bvalid;
  assign auto_ds_to_m00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_ds_to_m00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_ds_to_m00_couplers_RVALID = M_AXI_rvalid;
  assign auto_ds_to_m00_couplers_WREADY = M_AXI_wready;
  assign m00_couplers_to_auto_ds_ARADDR = S_AXI_araddr[31:0];
  assign m00_couplers_to_auto_ds_ARPROT = S_AXI_arprot[2:0];
  assign m00_couplers_to_auto_ds_ARVALID = S_AXI_arvalid;
  assign m00_couplers_to_auto_ds_AWADDR = S_AXI_awaddr[31:0];
  assign m00_couplers_to_auto_ds_AWPROT = S_AXI_awprot[2:0];
  assign m00_couplers_to_auto_ds_AWVALID = S_AXI_awvalid;
  assign m00_couplers_to_auto_ds_BREADY = S_AXI_bready;
  assign m00_couplers_to_auto_ds_RREADY = S_AXI_rready;
  assign m00_couplers_to_auto_ds_WDATA = S_AXI_wdata[63:0];
  assign m00_couplers_to_auto_ds_WSTRB = S_AXI_wstrb[7:0];
  assign m00_couplers_to_auto_ds_WVALID = S_AXI_wvalid;
  cerberus_auto_ds_3 auto_ds
       (.m_axi_araddr(auto_ds_to_m00_couplers_ARADDR),
        .m_axi_arprot(auto_ds_to_m00_couplers_ARPROT),
        .m_axi_arready(auto_ds_to_m00_couplers_ARREADY),
        .m_axi_arvalid(auto_ds_to_m00_couplers_ARVALID),
        .m_axi_awaddr(auto_ds_to_m00_couplers_AWADDR),
        .m_axi_awprot(auto_ds_to_m00_couplers_AWPROT),
        .m_axi_awready(auto_ds_to_m00_couplers_AWREADY),
        .m_axi_awvalid(auto_ds_to_m00_couplers_AWVALID),
        .m_axi_bready(auto_ds_to_m00_couplers_BREADY),
        .m_axi_bresp(auto_ds_to_m00_couplers_BRESP),
        .m_axi_bvalid(auto_ds_to_m00_couplers_BVALID),
        .m_axi_rdata(auto_ds_to_m00_couplers_RDATA),
        .m_axi_rready(auto_ds_to_m00_couplers_RREADY),
        .m_axi_rresp(auto_ds_to_m00_couplers_RRESP),
        .m_axi_rvalid(auto_ds_to_m00_couplers_RVALID),
        .m_axi_wdata(auto_ds_to_m00_couplers_WDATA),
        .m_axi_wready(auto_ds_to_m00_couplers_WREADY),
        .m_axi_wstrb(auto_ds_to_m00_couplers_WSTRB),
        .m_axi_wvalid(auto_ds_to_m00_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(m00_couplers_to_auto_ds_ARADDR[16:0]),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arprot(m00_couplers_to_auto_ds_ARPROT),
        .s_axi_arready(m00_couplers_to_auto_ds_ARREADY),
        .s_axi_arvalid(m00_couplers_to_auto_ds_ARVALID),
        .s_axi_awaddr(m00_couplers_to_auto_ds_AWADDR[16:0]),
        .s_axi_awprot(m00_couplers_to_auto_ds_AWPROT),
        .s_axi_awready(m00_couplers_to_auto_ds_AWREADY),
        .s_axi_awvalid(m00_couplers_to_auto_ds_AWVALID),
        .s_axi_bready(m00_couplers_to_auto_ds_BREADY),
        .s_axi_bresp(m00_couplers_to_auto_ds_BRESP),
        .s_axi_bvalid(m00_couplers_to_auto_ds_BVALID),
        .s_axi_rdata(m00_couplers_to_auto_ds_RDATA),
        .s_axi_rready(m00_couplers_to_auto_ds_RREADY),
        .s_axi_rresp(m00_couplers_to_auto_ds_RRESP),
        .s_axi_rvalid(m00_couplers_to_auto_ds_RVALID),
        .s_axi_wdata(m00_couplers_to_auto_ds_WDATA),
        .s_axi_wready(m00_couplers_to_auto_ds_WREADY),
        .s_axi_wstrb(m00_couplers_to_auto_ds_WSTRB),
        .s_axi_wvalid(m00_couplers_to_auto_ds_WVALID));
endmodule

module m01_couplers_imp_1DP09XF
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [63:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [63:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [7:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [63:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [63:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [7:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [31:0]m01_couplers_to_m01_couplers_ARADDR;
  wire [2:0]m01_couplers_to_m01_couplers_ARPROT;
  wire [0:0]m01_couplers_to_m01_couplers_ARREADY;
  wire [0:0]m01_couplers_to_m01_couplers_ARVALID;
  wire [31:0]m01_couplers_to_m01_couplers_AWADDR;
  wire [2:0]m01_couplers_to_m01_couplers_AWPROT;
  wire [0:0]m01_couplers_to_m01_couplers_AWREADY;
  wire [0:0]m01_couplers_to_m01_couplers_AWVALID;
  wire [0:0]m01_couplers_to_m01_couplers_BREADY;
  wire [1:0]m01_couplers_to_m01_couplers_BRESP;
  wire [0:0]m01_couplers_to_m01_couplers_BVALID;
  wire [63:0]m01_couplers_to_m01_couplers_RDATA;
  wire [0:0]m01_couplers_to_m01_couplers_RREADY;
  wire [1:0]m01_couplers_to_m01_couplers_RRESP;
  wire [0:0]m01_couplers_to_m01_couplers_RVALID;
  wire [63:0]m01_couplers_to_m01_couplers_WDATA;
  wire [0:0]m01_couplers_to_m01_couplers_WREADY;
  wire [7:0]m01_couplers_to_m01_couplers_WSTRB;
  wire [0:0]m01_couplers_to_m01_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m01_couplers_to_m01_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = m01_couplers_to_m01_couplers_ARPROT;
  assign M_AXI_arvalid[0] = m01_couplers_to_m01_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m01_couplers_to_m01_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = m01_couplers_to_m01_couplers_AWPROT;
  assign M_AXI_awvalid[0] = m01_couplers_to_m01_couplers_AWVALID;
  assign M_AXI_bready[0] = m01_couplers_to_m01_couplers_BREADY;
  assign M_AXI_rready[0] = m01_couplers_to_m01_couplers_RREADY;
  assign M_AXI_wdata[63:0] = m01_couplers_to_m01_couplers_WDATA;
  assign M_AXI_wstrb[7:0] = m01_couplers_to_m01_couplers_WSTRB;
  assign M_AXI_wvalid[0] = m01_couplers_to_m01_couplers_WVALID;
  assign S_AXI_arready[0] = m01_couplers_to_m01_couplers_ARREADY;
  assign S_AXI_awready[0] = m01_couplers_to_m01_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m01_couplers_to_m01_couplers_BRESP;
  assign S_AXI_bvalid[0] = m01_couplers_to_m01_couplers_BVALID;
  assign S_AXI_rdata[63:0] = m01_couplers_to_m01_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m01_couplers_to_m01_couplers_RRESP;
  assign S_AXI_rvalid[0] = m01_couplers_to_m01_couplers_RVALID;
  assign S_AXI_wready[0] = m01_couplers_to_m01_couplers_WREADY;
  assign m01_couplers_to_m01_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m01_couplers_to_m01_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m01_couplers_to_m01_couplers_ARREADY = M_AXI_arready[0];
  assign m01_couplers_to_m01_couplers_ARVALID = S_AXI_arvalid[0];
  assign m01_couplers_to_m01_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m01_couplers_to_m01_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m01_couplers_to_m01_couplers_AWREADY = M_AXI_awready[0];
  assign m01_couplers_to_m01_couplers_AWVALID = S_AXI_awvalid[0];
  assign m01_couplers_to_m01_couplers_BREADY = S_AXI_bready[0];
  assign m01_couplers_to_m01_couplers_BRESP = M_AXI_bresp[1:0];
  assign m01_couplers_to_m01_couplers_BVALID = M_AXI_bvalid[0];
  assign m01_couplers_to_m01_couplers_RDATA = M_AXI_rdata[63:0];
  assign m01_couplers_to_m01_couplers_RREADY = S_AXI_rready[0];
  assign m01_couplers_to_m01_couplers_RRESP = M_AXI_rresp[1:0];
  assign m01_couplers_to_m01_couplers_RVALID = M_AXI_rvalid[0];
  assign m01_couplers_to_m01_couplers_WDATA = S_AXI_wdata[63:0];
  assign m01_couplers_to_m01_couplers_WREADY = M_AXI_wready[0];
  assign m01_couplers_to_m01_couplers_WSTRB = S_AXI_wstrb[7:0];
  assign m01_couplers_to_m01_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m01_couplers_imp_1YQL89W
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [63:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [63:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [7:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [63:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [63:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [7:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [31:0]m01_couplers_to_m01_couplers_ARADDR;
  wire [2:0]m01_couplers_to_m01_couplers_ARPROT;
  wire [0:0]m01_couplers_to_m01_couplers_ARREADY;
  wire [0:0]m01_couplers_to_m01_couplers_ARVALID;
  wire [31:0]m01_couplers_to_m01_couplers_AWADDR;
  wire [2:0]m01_couplers_to_m01_couplers_AWPROT;
  wire [0:0]m01_couplers_to_m01_couplers_AWREADY;
  wire [0:0]m01_couplers_to_m01_couplers_AWVALID;
  wire [0:0]m01_couplers_to_m01_couplers_BREADY;
  wire [1:0]m01_couplers_to_m01_couplers_BRESP;
  wire [0:0]m01_couplers_to_m01_couplers_BVALID;
  wire [63:0]m01_couplers_to_m01_couplers_RDATA;
  wire [0:0]m01_couplers_to_m01_couplers_RREADY;
  wire [1:0]m01_couplers_to_m01_couplers_RRESP;
  wire [0:0]m01_couplers_to_m01_couplers_RVALID;
  wire [63:0]m01_couplers_to_m01_couplers_WDATA;
  wire [0:0]m01_couplers_to_m01_couplers_WREADY;
  wire [7:0]m01_couplers_to_m01_couplers_WSTRB;
  wire [0:0]m01_couplers_to_m01_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m01_couplers_to_m01_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = m01_couplers_to_m01_couplers_ARPROT;
  assign M_AXI_arvalid[0] = m01_couplers_to_m01_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m01_couplers_to_m01_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = m01_couplers_to_m01_couplers_AWPROT;
  assign M_AXI_awvalid[0] = m01_couplers_to_m01_couplers_AWVALID;
  assign M_AXI_bready[0] = m01_couplers_to_m01_couplers_BREADY;
  assign M_AXI_rready[0] = m01_couplers_to_m01_couplers_RREADY;
  assign M_AXI_wdata[63:0] = m01_couplers_to_m01_couplers_WDATA;
  assign M_AXI_wstrb[7:0] = m01_couplers_to_m01_couplers_WSTRB;
  assign M_AXI_wvalid[0] = m01_couplers_to_m01_couplers_WVALID;
  assign S_AXI_arready[0] = m01_couplers_to_m01_couplers_ARREADY;
  assign S_AXI_awready[0] = m01_couplers_to_m01_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m01_couplers_to_m01_couplers_BRESP;
  assign S_AXI_bvalid[0] = m01_couplers_to_m01_couplers_BVALID;
  assign S_AXI_rdata[63:0] = m01_couplers_to_m01_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m01_couplers_to_m01_couplers_RRESP;
  assign S_AXI_rvalid[0] = m01_couplers_to_m01_couplers_RVALID;
  assign S_AXI_wready[0] = m01_couplers_to_m01_couplers_WREADY;
  assign m01_couplers_to_m01_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m01_couplers_to_m01_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m01_couplers_to_m01_couplers_ARREADY = M_AXI_arready[0];
  assign m01_couplers_to_m01_couplers_ARVALID = S_AXI_arvalid[0];
  assign m01_couplers_to_m01_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m01_couplers_to_m01_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m01_couplers_to_m01_couplers_AWREADY = M_AXI_awready[0];
  assign m01_couplers_to_m01_couplers_AWVALID = S_AXI_awvalid[0];
  assign m01_couplers_to_m01_couplers_BREADY = S_AXI_bready[0];
  assign m01_couplers_to_m01_couplers_BRESP = M_AXI_bresp[1:0];
  assign m01_couplers_to_m01_couplers_BVALID = M_AXI_bvalid[0];
  assign m01_couplers_to_m01_couplers_RDATA = M_AXI_rdata[63:0];
  assign m01_couplers_to_m01_couplers_RREADY = S_AXI_rready[0];
  assign m01_couplers_to_m01_couplers_RRESP = M_AXI_rresp[1:0];
  assign m01_couplers_to_m01_couplers_RVALID = M_AXI_rvalid[0];
  assign m01_couplers_to_m01_couplers_WDATA = S_AXI_wdata[63:0];
  assign m01_couplers_to_m01_couplers_WREADY = M_AXI_wready[0];
  assign m01_couplers_to_m01_couplers_WSTRB = S_AXI_wstrb[7:0];
  assign m01_couplers_to_m01_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m01_couplers_imp_4WKJTW
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [63:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [63:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [7:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [63:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [63:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [7:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [31:0]m01_couplers_to_m01_couplers_ARADDR;
  wire [2:0]m01_couplers_to_m01_couplers_ARPROT;
  wire [0:0]m01_couplers_to_m01_couplers_ARREADY;
  wire [0:0]m01_couplers_to_m01_couplers_ARVALID;
  wire [31:0]m01_couplers_to_m01_couplers_AWADDR;
  wire [2:0]m01_couplers_to_m01_couplers_AWPROT;
  wire [0:0]m01_couplers_to_m01_couplers_AWREADY;
  wire [0:0]m01_couplers_to_m01_couplers_AWVALID;
  wire [0:0]m01_couplers_to_m01_couplers_BREADY;
  wire [1:0]m01_couplers_to_m01_couplers_BRESP;
  wire [0:0]m01_couplers_to_m01_couplers_BVALID;
  wire [63:0]m01_couplers_to_m01_couplers_RDATA;
  wire [0:0]m01_couplers_to_m01_couplers_RREADY;
  wire [1:0]m01_couplers_to_m01_couplers_RRESP;
  wire [0:0]m01_couplers_to_m01_couplers_RVALID;
  wire [63:0]m01_couplers_to_m01_couplers_WDATA;
  wire [0:0]m01_couplers_to_m01_couplers_WREADY;
  wire [7:0]m01_couplers_to_m01_couplers_WSTRB;
  wire [0:0]m01_couplers_to_m01_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m01_couplers_to_m01_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = m01_couplers_to_m01_couplers_ARPROT;
  assign M_AXI_arvalid[0] = m01_couplers_to_m01_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m01_couplers_to_m01_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = m01_couplers_to_m01_couplers_AWPROT;
  assign M_AXI_awvalid[0] = m01_couplers_to_m01_couplers_AWVALID;
  assign M_AXI_bready[0] = m01_couplers_to_m01_couplers_BREADY;
  assign M_AXI_rready[0] = m01_couplers_to_m01_couplers_RREADY;
  assign M_AXI_wdata[63:0] = m01_couplers_to_m01_couplers_WDATA;
  assign M_AXI_wstrb[7:0] = m01_couplers_to_m01_couplers_WSTRB;
  assign M_AXI_wvalid[0] = m01_couplers_to_m01_couplers_WVALID;
  assign S_AXI_arready[0] = m01_couplers_to_m01_couplers_ARREADY;
  assign S_AXI_awready[0] = m01_couplers_to_m01_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m01_couplers_to_m01_couplers_BRESP;
  assign S_AXI_bvalid[0] = m01_couplers_to_m01_couplers_BVALID;
  assign S_AXI_rdata[63:0] = m01_couplers_to_m01_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m01_couplers_to_m01_couplers_RRESP;
  assign S_AXI_rvalid[0] = m01_couplers_to_m01_couplers_RVALID;
  assign S_AXI_wready[0] = m01_couplers_to_m01_couplers_WREADY;
  assign m01_couplers_to_m01_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m01_couplers_to_m01_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m01_couplers_to_m01_couplers_ARREADY = M_AXI_arready[0];
  assign m01_couplers_to_m01_couplers_ARVALID = S_AXI_arvalid[0];
  assign m01_couplers_to_m01_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m01_couplers_to_m01_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m01_couplers_to_m01_couplers_AWREADY = M_AXI_awready[0];
  assign m01_couplers_to_m01_couplers_AWVALID = S_AXI_awvalid[0];
  assign m01_couplers_to_m01_couplers_BREADY = S_AXI_bready[0];
  assign m01_couplers_to_m01_couplers_BRESP = M_AXI_bresp[1:0];
  assign m01_couplers_to_m01_couplers_BVALID = M_AXI_bvalid[0];
  assign m01_couplers_to_m01_couplers_RDATA = M_AXI_rdata[63:0];
  assign m01_couplers_to_m01_couplers_RREADY = S_AXI_rready[0];
  assign m01_couplers_to_m01_couplers_RRESP = M_AXI_rresp[1:0];
  assign m01_couplers_to_m01_couplers_RVALID = M_AXI_rvalid[0];
  assign m01_couplers_to_m01_couplers_WDATA = S_AXI_wdata[63:0];
  assign m01_couplers_to_m01_couplers_WREADY = M_AXI_wready[0];
  assign m01_couplers_to_m01_couplers_WSTRB = S_AXI_wstrb[7:0];
  assign m01_couplers_to_m01_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m01_couplers_imp_HD0VXD
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [63:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [63:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [7:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [63:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [63:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [7:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [31:0]m01_couplers_to_m01_couplers_ARADDR;
  wire [2:0]m01_couplers_to_m01_couplers_ARPROT;
  wire [0:0]m01_couplers_to_m01_couplers_ARREADY;
  wire [0:0]m01_couplers_to_m01_couplers_ARVALID;
  wire [31:0]m01_couplers_to_m01_couplers_AWADDR;
  wire [2:0]m01_couplers_to_m01_couplers_AWPROT;
  wire [0:0]m01_couplers_to_m01_couplers_AWREADY;
  wire [0:0]m01_couplers_to_m01_couplers_AWVALID;
  wire [0:0]m01_couplers_to_m01_couplers_BREADY;
  wire [1:0]m01_couplers_to_m01_couplers_BRESP;
  wire [0:0]m01_couplers_to_m01_couplers_BVALID;
  wire [63:0]m01_couplers_to_m01_couplers_RDATA;
  wire [0:0]m01_couplers_to_m01_couplers_RREADY;
  wire [1:0]m01_couplers_to_m01_couplers_RRESP;
  wire [0:0]m01_couplers_to_m01_couplers_RVALID;
  wire [63:0]m01_couplers_to_m01_couplers_WDATA;
  wire [0:0]m01_couplers_to_m01_couplers_WREADY;
  wire [7:0]m01_couplers_to_m01_couplers_WSTRB;
  wire [0:0]m01_couplers_to_m01_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m01_couplers_to_m01_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = m01_couplers_to_m01_couplers_ARPROT;
  assign M_AXI_arvalid[0] = m01_couplers_to_m01_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m01_couplers_to_m01_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = m01_couplers_to_m01_couplers_AWPROT;
  assign M_AXI_awvalid[0] = m01_couplers_to_m01_couplers_AWVALID;
  assign M_AXI_bready[0] = m01_couplers_to_m01_couplers_BREADY;
  assign M_AXI_rready[0] = m01_couplers_to_m01_couplers_RREADY;
  assign M_AXI_wdata[63:0] = m01_couplers_to_m01_couplers_WDATA;
  assign M_AXI_wstrb[7:0] = m01_couplers_to_m01_couplers_WSTRB;
  assign M_AXI_wvalid[0] = m01_couplers_to_m01_couplers_WVALID;
  assign S_AXI_arready[0] = m01_couplers_to_m01_couplers_ARREADY;
  assign S_AXI_awready[0] = m01_couplers_to_m01_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = m01_couplers_to_m01_couplers_BRESP;
  assign S_AXI_bvalid[0] = m01_couplers_to_m01_couplers_BVALID;
  assign S_AXI_rdata[63:0] = m01_couplers_to_m01_couplers_RDATA;
  assign S_AXI_rresp[1:0] = m01_couplers_to_m01_couplers_RRESP;
  assign S_AXI_rvalid[0] = m01_couplers_to_m01_couplers_RVALID;
  assign S_AXI_wready[0] = m01_couplers_to_m01_couplers_WREADY;
  assign m01_couplers_to_m01_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m01_couplers_to_m01_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m01_couplers_to_m01_couplers_ARREADY = M_AXI_arready[0];
  assign m01_couplers_to_m01_couplers_ARVALID = S_AXI_arvalid[0];
  assign m01_couplers_to_m01_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m01_couplers_to_m01_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m01_couplers_to_m01_couplers_AWREADY = M_AXI_awready[0];
  assign m01_couplers_to_m01_couplers_AWVALID = S_AXI_awvalid[0];
  assign m01_couplers_to_m01_couplers_BREADY = S_AXI_bready[0];
  assign m01_couplers_to_m01_couplers_BRESP = M_AXI_bresp[1:0];
  assign m01_couplers_to_m01_couplers_BVALID = M_AXI_bvalid[0];
  assign m01_couplers_to_m01_couplers_RDATA = M_AXI_rdata[63:0];
  assign m01_couplers_to_m01_couplers_RREADY = S_AXI_rready[0];
  assign m01_couplers_to_m01_couplers_RRESP = M_AXI_rresp[1:0];
  assign m01_couplers_to_m01_couplers_RVALID = M_AXI_rvalid[0];
  assign m01_couplers_to_m01_couplers_WDATA = S_AXI_wdata[63:0];
  assign m01_couplers_to_m01_couplers_WREADY = M_AXI_wready[0];
  assign m01_couplers_to_m01_couplers_WSTRB = S_AXI_wstrb[7:0];
  assign m01_couplers_to_m01_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module m01_couplers_imp_K7OWQG
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arregion,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awregion,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input [0:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [3:0]S_AXI_arregion;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input [0:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [3:0]S_AXI_awregion;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_pc_to_m01_couplers_ARADDR;
  wire auto_pc_to_m01_couplers_ARREADY;
  wire auto_pc_to_m01_couplers_ARVALID;
  wire [31:0]auto_pc_to_m01_couplers_AWADDR;
  wire auto_pc_to_m01_couplers_AWREADY;
  wire auto_pc_to_m01_couplers_AWVALID;
  wire auto_pc_to_m01_couplers_BREADY;
  wire [1:0]auto_pc_to_m01_couplers_BRESP;
  wire auto_pc_to_m01_couplers_BVALID;
  wire [31:0]auto_pc_to_m01_couplers_RDATA;
  wire auto_pc_to_m01_couplers_RREADY;
  wire [1:0]auto_pc_to_m01_couplers_RRESP;
  wire auto_pc_to_m01_couplers_RVALID;
  wire [31:0]auto_pc_to_m01_couplers_WDATA;
  wire auto_pc_to_m01_couplers_WREADY;
  wire [3:0]auto_pc_to_m01_couplers_WSTRB;
  wire auto_pc_to_m01_couplers_WVALID;
  wire [31:0]m01_couplers_to_auto_pc_ARADDR;
  wire [1:0]m01_couplers_to_auto_pc_ARBURST;
  wire [3:0]m01_couplers_to_auto_pc_ARCACHE;
  wire [11:0]m01_couplers_to_auto_pc_ARID;
  wire [7:0]m01_couplers_to_auto_pc_ARLEN;
  wire [0:0]m01_couplers_to_auto_pc_ARLOCK;
  wire [2:0]m01_couplers_to_auto_pc_ARPROT;
  wire [3:0]m01_couplers_to_auto_pc_ARQOS;
  wire m01_couplers_to_auto_pc_ARREADY;
  wire [3:0]m01_couplers_to_auto_pc_ARREGION;
  wire [2:0]m01_couplers_to_auto_pc_ARSIZE;
  wire m01_couplers_to_auto_pc_ARVALID;
  wire [31:0]m01_couplers_to_auto_pc_AWADDR;
  wire [1:0]m01_couplers_to_auto_pc_AWBURST;
  wire [3:0]m01_couplers_to_auto_pc_AWCACHE;
  wire [11:0]m01_couplers_to_auto_pc_AWID;
  wire [7:0]m01_couplers_to_auto_pc_AWLEN;
  wire [0:0]m01_couplers_to_auto_pc_AWLOCK;
  wire [2:0]m01_couplers_to_auto_pc_AWPROT;
  wire [3:0]m01_couplers_to_auto_pc_AWQOS;
  wire m01_couplers_to_auto_pc_AWREADY;
  wire [3:0]m01_couplers_to_auto_pc_AWREGION;
  wire [2:0]m01_couplers_to_auto_pc_AWSIZE;
  wire m01_couplers_to_auto_pc_AWVALID;
  wire [11:0]m01_couplers_to_auto_pc_BID;
  wire m01_couplers_to_auto_pc_BREADY;
  wire [1:0]m01_couplers_to_auto_pc_BRESP;
  wire m01_couplers_to_auto_pc_BVALID;
  wire [31:0]m01_couplers_to_auto_pc_RDATA;
  wire [11:0]m01_couplers_to_auto_pc_RID;
  wire m01_couplers_to_auto_pc_RLAST;
  wire m01_couplers_to_auto_pc_RREADY;
  wire [1:0]m01_couplers_to_auto_pc_RRESP;
  wire m01_couplers_to_auto_pc_RVALID;
  wire [31:0]m01_couplers_to_auto_pc_WDATA;
  wire m01_couplers_to_auto_pc_WLAST;
  wire m01_couplers_to_auto_pc_WREADY;
  wire [3:0]m01_couplers_to_auto_pc_WSTRB;
  wire m01_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[31:0] = auto_pc_to_m01_couplers_ARADDR;
  assign M_AXI_arvalid = auto_pc_to_m01_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_pc_to_m01_couplers_AWADDR;
  assign M_AXI_awvalid = auto_pc_to_m01_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_m01_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_m01_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_m01_couplers_WDATA;
  assign M_AXI_wstrb[3:0] = auto_pc_to_m01_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_m01_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = m01_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = m01_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[11:0] = m01_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = m01_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = m01_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = m01_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[11:0] = m01_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = m01_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = m01_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = m01_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = m01_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_m01_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_m01_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_m01_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_m01_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_m01_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_m01_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_m01_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_m01_couplers_WREADY = M_AXI_wready;
  assign m01_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign m01_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign m01_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign m01_couplers_to_auto_pc_ARID = S_AXI_arid[11:0];
  assign m01_couplers_to_auto_pc_ARLEN = S_AXI_arlen[7:0];
  assign m01_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[0];
  assign m01_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign m01_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign m01_couplers_to_auto_pc_ARREGION = S_AXI_arregion[3:0];
  assign m01_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign m01_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign m01_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign m01_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign m01_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign m01_couplers_to_auto_pc_AWID = S_AXI_awid[11:0];
  assign m01_couplers_to_auto_pc_AWLEN = S_AXI_awlen[7:0];
  assign m01_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[0];
  assign m01_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign m01_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign m01_couplers_to_auto_pc_AWREGION = S_AXI_awregion[3:0];
  assign m01_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign m01_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign m01_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign m01_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign m01_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign m01_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign m01_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign m01_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
  cerberus_auto_pc_1 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_m01_couplers_ARADDR),
        .m_axi_arready(auto_pc_to_m01_couplers_ARREADY),
        .m_axi_arvalid(auto_pc_to_m01_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_m01_couplers_AWADDR),
        .m_axi_awready(auto_pc_to_m01_couplers_AWREADY),
        .m_axi_awvalid(auto_pc_to_m01_couplers_AWVALID),
        .m_axi_bready(auto_pc_to_m01_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_m01_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_m01_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_m01_couplers_RDATA),
        .m_axi_rready(auto_pc_to_m01_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_m01_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_m01_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_m01_couplers_WDATA),
        .m_axi_wready(auto_pc_to_m01_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_m01_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_m01_couplers_WVALID),
        .s_axi_araddr(m01_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(m01_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(m01_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(m01_couplers_to_auto_pc_ARID),
        .s_axi_arlen(m01_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(m01_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(m01_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(m01_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(m01_couplers_to_auto_pc_ARREADY),
        .s_axi_arregion(m01_couplers_to_auto_pc_ARREGION),
        .s_axi_arsize(m01_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(m01_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(m01_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(m01_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(m01_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(m01_couplers_to_auto_pc_AWID),
        .s_axi_awlen(m01_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(m01_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(m01_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(m01_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(m01_couplers_to_auto_pc_AWREADY),
        .s_axi_awregion(m01_couplers_to_auto_pc_AWREGION),
        .s_axi_awsize(m01_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(m01_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(m01_couplers_to_auto_pc_BID),
        .s_axi_bready(m01_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(m01_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(m01_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(m01_couplers_to_auto_pc_RDATA),
        .s_axi_rid(m01_couplers_to_auto_pc_RID),
        .s_axi_rlast(m01_couplers_to_auto_pc_RLAST),
        .s_axi_rready(m01_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(m01_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(m01_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(m01_couplers_to_auto_pc_WDATA),
        .s_axi_wlast(m01_couplers_to_auto_pc_WLAST),
        .s_axi_wready(m01_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(m01_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(m01_couplers_to_auto_pc_WVALID));
endmodule

module m02_couplers_imp_191VXEI
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [11:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [11:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  input [11:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [11:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m02_couplers_to_m02_couplers_ARADDR;
  wire [1:0]m02_couplers_to_m02_couplers_ARBURST;
  wire [3:0]m02_couplers_to_m02_couplers_ARCACHE;
  wire [11:0]m02_couplers_to_m02_couplers_ARID;
  wire [7:0]m02_couplers_to_m02_couplers_ARLEN;
  wire m02_couplers_to_m02_couplers_ARLOCK;
  wire [2:0]m02_couplers_to_m02_couplers_ARPROT;
  wire m02_couplers_to_m02_couplers_ARREADY;
  wire [2:0]m02_couplers_to_m02_couplers_ARSIZE;
  wire m02_couplers_to_m02_couplers_ARVALID;
  wire [31:0]m02_couplers_to_m02_couplers_AWADDR;
  wire [1:0]m02_couplers_to_m02_couplers_AWBURST;
  wire [3:0]m02_couplers_to_m02_couplers_AWCACHE;
  wire [11:0]m02_couplers_to_m02_couplers_AWID;
  wire [7:0]m02_couplers_to_m02_couplers_AWLEN;
  wire m02_couplers_to_m02_couplers_AWLOCK;
  wire [2:0]m02_couplers_to_m02_couplers_AWPROT;
  wire m02_couplers_to_m02_couplers_AWREADY;
  wire [2:0]m02_couplers_to_m02_couplers_AWSIZE;
  wire m02_couplers_to_m02_couplers_AWVALID;
  wire [11:0]m02_couplers_to_m02_couplers_BID;
  wire m02_couplers_to_m02_couplers_BREADY;
  wire [1:0]m02_couplers_to_m02_couplers_BRESP;
  wire m02_couplers_to_m02_couplers_BVALID;
  wire [31:0]m02_couplers_to_m02_couplers_RDATA;
  wire [11:0]m02_couplers_to_m02_couplers_RID;
  wire m02_couplers_to_m02_couplers_RLAST;
  wire m02_couplers_to_m02_couplers_RREADY;
  wire [1:0]m02_couplers_to_m02_couplers_RRESP;
  wire m02_couplers_to_m02_couplers_RVALID;
  wire [31:0]m02_couplers_to_m02_couplers_WDATA;
  wire m02_couplers_to_m02_couplers_WLAST;
  wire m02_couplers_to_m02_couplers_WREADY;
  wire [3:0]m02_couplers_to_m02_couplers_WSTRB;
  wire m02_couplers_to_m02_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m02_couplers_to_m02_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = m02_couplers_to_m02_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = m02_couplers_to_m02_couplers_ARCACHE;
  assign M_AXI_arid[11:0] = m02_couplers_to_m02_couplers_ARID;
  assign M_AXI_arlen[7:0] = m02_couplers_to_m02_couplers_ARLEN;
  assign M_AXI_arlock = m02_couplers_to_m02_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = m02_couplers_to_m02_couplers_ARPROT;
  assign M_AXI_arsize[2:0] = m02_couplers_to_m02_couplers_ARSIZE;
  assign M_AXI_arvalid = m02_couplers_to_m02_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m02_couplers_to_m02_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = m02_couplers_to_m02_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = m02_couplers_to_m02_couplers_AWCACHE;
  assign M_AXI_awid[11:0] = m02_couplers_to_m02_couplers_AWID;
  assign M_AXI_awlen[7:0] = m02_couplers_to_m02_couplers_AWLEN;
  assign M_AXI_awlock = m02_couplers_to_m02_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = m02_couplers_to_m02_couplers_AWPROT;
  assign M_AXI_awsize[2:0] = m02_couplers_to_m02_couplers_AWSIZE;
  assign M_AXI_awvalid = m02_couplers_to_m02_couplers_AWVALID;
  assign M_AXI_bready = m02_couplers_to_m02_couplers_BREADY;
  assign M_AXI_rready = m02_couplers_to_m02_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m02_couplers_to_m02_couplers_WDATA;
  assign M_AXI_wlast = m02_couplers_to_m02_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = m02_couplers_to_m02_couplers_WSTRB;
  assign M_AXI_wvalid = m02_couplers_to_m02_couplers_WVALID;
  assign S_AXI_arready = m02_couplers_to_m02_couplers_ARREADY;
  assign S_AXI_awready = m02_couplers_to_m02_couplers_AWREADY;
  assign S_AXI_bid[11:0] = m02_couplers_to_m02_couplers_BID;
  assign S_AXI_bresp[1:0] = m02_couplers_to_m02_couplers_BRESP;
  assign S_AXI_bvalid = m02_couplers_to_m02_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m02_couplers_to_m02_couplers_RDATA;
  assign S_AXI_rid[11:0] = m02_couplers_to_m02_couplers_RID;
  assign S_AXI_rlast = m02_couplers_to_m02_couplers_RLAST;
  assign S_AXI_rresp[1:0] = m02_couplers_to_m02_couplers_RRESP;
  assign S_AXI_rvalid = m02_couplers_to_m02_couplers_RVALID;
  assign S_AXI_wready = m02_couplers_to_m02_couplers_WREADY;
  assign m02_couplers_to_m02_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m02_couplers_to_m02_couplers_ARBURST = S_AXI_arburst[1:0];
  assign m02_couplers_to_m02_couplers_ARCACHE = S_AXI_arcache[3:0];
  assign m02_couplers_to_m02_couplers_ARID = S_AXI_arid[11:0];
  assign m02_couplers_to_m02_couplers_ARLEN = S_AXI_arlen[7:0];
  assign m02_couplers_to_m02_couplers_ARLOCK = S_AXI_arlock;
  assign m02_couplers_to_m02_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m02_couplers_to_m02_couplers_ARREADY = M_AXI_arready;
  assign m02_couplers_to_m02_couplers_ARSIZE = S_AXI_arsize[2:0];
  assign m02_couplers_to_m02_couplers_ARVALID = S_AXI_arvalid;
  assign m02_couplers_to_m02_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m02_couplers_to_m02_couplers_AWBURST = S_AXI_awburst[1:0];
  assign m02_couplers_to_m02_couplers_AWCACHE = S_AXI_awcache[3:0];
  assign m02_couplers_to_m02_couplers_AWID = S_AXI_awid[11:0];
  assign m02_couplers_to_m02_couplers_AWLEN = S_AXI_awlen[7:0];
  assign m02_couplers_to_m02_couplers_AWLOCK = S_AXI_awlock;
  assign m02_couplers_to_m02_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m02_couplers_to_m02_couplers_AWREADY = M_AXI_awready;
  assign m02_couplers_to_m02_couplers_AWSIZE = S_AXI_awsize[2:0];
  assign m02_couplers_to_m02_couplers_AWVALID = S_AXI_awvalid;
  assign m02_couplers_to_m02_couplers_BID = M_AXI_bid[11:0];
  assign m02_couplers_to_m02_couplers_BREADY = S_AXI_bready;
  assign m02_couplers_to_m02_couplers_BRESP = M_AXI_bresp[1:0];
  assign m02_couplers_to_m02_couplers_BVALID = M_AXI_bvalid;
  assign m02_couplers_to_m02_couplers_RDATA = M_AXI_rdata[31:0];
  assign m02_couplers_to_m02_couplers_RID = M_AXI_rid[11:0];
  assign m02_couplers_to_m02_couplers_RLAST = M_AXI_rlast;
  assign m02_couplers_to_m02_couplers_RREADY = S_AXI_rready;
  assign m02_couplers_to_m02_couplers_RRESP = M_AXI_rresp[1:0];
  assign m02_couplers_to_m02_couplers_RVALID = M_AXI_rvalid;
  assign m02_couplers_to_m02_couplers_WDATA = S_AXI_wdata[31:0];
  assign m02_couplers_to_m02_couplers_WLAST = S_AXI_wlast;
  assign m02_couplers_to_m02_couplers_WREADY = M_AXI_wready;
  assign m02_couplers_to_m02_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m02_couplers_to_m02_couplers_WVALID = S_AXI_wvalid;
endmodule

module m03_couplers_imp_LDNU4B
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [11:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [11:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  input [11:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [11:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m03_couplers_to_m03_couplers_ARADDR;
  wire [1:0]m03_couplers_to_m03_couplers_ARBURST;
  wire [3:0]m03_couplers_to_m03_couplers_ARCACHE;
  wire [11:0]m03_couplers_to_m03_couplers_ARID;
  wire [7:0]m03_couplers_to_m03_couplers_ARLEN;
  wire m03_couplers_to_m03_couplers_ARLOCK;
  wire [2:0]m03_couplers_to_m03_couplers_ARPROT;
  wire m03_couplers_to_m03_couplers_ARREADY;
  wire [2:0]m03_couplers_to_m03_couplers_ARSIZE;
  wire m03_couplers_to_m03_couplers_ARVALID;
  wire [31:0]m03_couplers_to_m03_couplers_AWADDR;
  wire [1:0]m03_couplers_to_m03_couplers_AWBURST;
  wire [3:0]m03_couplers_to_m03_couplers_AWCACHE;
  wire [11:0]m03_couplers_to_m03_couplers_AWID;
  wire [7:0]m03_couplers_to_m03_couplers_AWLEN;
  wire m03_couplers_to_m03_couplers_AWLOCK;
  wire [2:0]m03_couplers_to_m03_couplers_AWPROT;
  wire m03_couplers_to_m03_couplers_AWREADY;
  wire [2:0]m03_couplers_to_m03_couplers_AWSIZE;
  wire m03_couplers_to_m03_couplers_AWVALID;
  wire [11:0]m03_couplers_to_m03_couplers_BID;
  wire m03_couplers_to_m03_couplers_BREADY;
  wire [1:0]m03_couplers_to_m03_couplers_BRESP;
  wire m03_couplers_to_m03_couplers_BVALID;
  wire [31:0]m03_couplers_to_m03_couplers_RDATA;
  wire [11:0]m03_couplers_to_m03_couplers_RID;
  wire m03_couplers_to_m03_couplers_RLAST;
  wire m03_couplers_to_m03_couplers_RREADY;
  wire [1:0]m03_couplers_to_m03_couplers_RRESP;
  wire m03_couplers_to_m03_couplers_RVALID;
  wire [31:0]m03_couplers_to_m03_couplers_WDATA;
  wire m03_couplers_to_m03_couplers_WLAST;
  wire m03_couplers_to_m03_couplers_WREADY;
  wire [3:0]m03_couplers_to_m03_couplers_WSTRB;
  wire m03_couplers_to_m03_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m03_couplers_to_m03_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = m03_couplers_to_m03_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = m03_couplers_to_m03_couplers_ARCACHE;
  assign M_AXI_arid[11:0] = m03_couplers_to_m03_couplers_ARID;
  assign M_AXI_arlen[7:0] = m03_couplers_to_m03_couplers_ARLEN;
  assign M_AXI_arlock = m03_couplers_to_m03_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = m03_couplers_to_m03_couplers_ARPROT;
  assign M_AXI_arsize[2:0] = m03_couplers_to_m03_couplers_ARSIZE;
  assign M_AXI_arvalid = m03_couplers_to_m03_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m03_couplers_to_m03_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = m03_couplers_to_m03_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = m03_couplers_to_m03_couplers_AWCACHE;
  assign M_AXI_awid[11:0] = m03_couplers_to_m03_couplers_AWID;
  assign M_AXI_awlen[7:0] = m03_couplers_to_m03_couplers_AWLEN;
  assign M_AXI_awlock = m03_couplers_to_m03_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = m03_couplers_to_m03_couplers_AWPROT;
  assign M_AXI_awsize[2:0] = m03_couplers_to_m03_couplers_AWSIZE;
  assign M_AXI_awvalid = m03_couplers_to_m03_couplers_AWVALID;
  assign M_AXI_bready = m03_couplers_to_m03_couplers_BREADY;
  assign M_AXI_rready = m03_couplers_to_m03_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m03_couplers_to_m03_couplers_WDATA;
  assign M_AXI_wlast = m03_couplers_to_m03_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = m03_couplers_to_m03_couplers_WSTRB;
  assign M_AXI_wvalid = m03_couplers_to_m03_couplers_WVALID;
  assign S_AXI_arready = m03_couplers_to_m03_couplers_ARREADY;
  assign S_AXI_awready = m03_couplers_to_m03_couplers_AWREADY;
  assign S_AXI_bid[11:0] = m03_couplers_to_m03_couplers_BID;
  assign S_AXI_bresp[1:0] = m03_couplers_to_m03_couplers_BRESP;
  assign S_AXI_bvalid = m03_couplers_to_m03_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m03_couplers_to_m03_couplers_RDATA;
  assign S_AXI_rid[11:0] = m03_couplers_to_m03_couplers_RID;
  assign S_AXI_rlast = m03_couplers_to_m03_couplers_RLAST;
  assign S_AXI_rresp[1:0] = m03_couplers_to_m03_couplers_RRESP;
  assign S_AXI_rvalid = m03_couplers_to_m03_couplers_RVALID;
  assign S_AXI_wready = m03_couplers_to_m03_couplers_WREADY;
  assign m03_couplers_to_m03_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m03_couplers_to_m03_couplers_ARBURST = S_AXI_arburst[1:0];
  assign m03_couplers_to_m03_couplers_ARCACHE = S_AXI_arcache[3:0];
  assign m03_couplers_to_m03_couplers_ARID = S_AXI_arid[11:0];
  assign m03_couplers_to_m03_couplers_ARLEN = S_AXI_arlen[7:0];
  assign m03_couplers_to_m03_couplers_ARLOCK = S_AXI_arlock;
  assign m03_couplers_to_m03_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m03_couplers_to_m03_couplers_ARREADY = M_AXI_arready;
  assign m03_couplers_to_m03_couplers_ARSIZE = S_AXI_arsize[2:0];
  assign m03_couplers_to_m03_couplers_ARVALID = S_AXI_arvalid;
  assign m03_couplers_to_m03_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m03_couplers_to_m03_couplers_AWBURST = S_AXI_awburst[1:0];
  assign m03_couplers_to_m03_couplers_AWCACHE = S_AXI_awcache[3:0];
  assign m03_couplers_to_m03_couplers_AWID = S_AXI_awid[11:0];
  assign m03_couplers_to_m03_couplers_AWLEN = S_AXI_awlen[7:0];
  assign m03_couplers_to_m03_couplers_AWLOCK = S_AXI_awlock;
  assign m03_couplers_to_m03_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m03_couplers_to_m03_couplers_AWREADY = M_AXI_awready;
  assign m03_couplers_to_m03_couplers_AWSIZE = S_AXI_awsize[2:0];
  assign m03_couplers_to_m03_couplers_AWVALID = S_AXI_awvalid;
  assign m03_couplers_to_m03_couplers_BID = M_AXI_bid[11:0];
  assign m03_couplers_to_m03_couplers_BREADY = S_AXI_bready;
  assign m03_couplers_to_m03_couplers_BRESP = M_AXI_bresp[1:0];
  assign m03_couplers_to_m03_couplers_BVALID = M_AXI_bvalid;
  assign m03_couplers_to_m03_couplers_RDATA = M_AXI_rdata[31:0];
  assign m03_couplers_to_m03_couplers_RID = M_AXI_rid[11:0];
  assign m03_couplers_to_m03_couplers_RLAST = M_AXI_rlast;
  assign m03_couplers_to_m03_couplers_RREADY = S_AXI_rready;
  assign m03_couplers_to_m03_couplers_RRESP = M_AXI_rresp[1:0];
  assign m03_couplers_to_m03_couplers_RVALID = M_AXI_rvalid;
  assign m03_couplers_to_m03_couplers_WDATA = S_AXI_wdata[31:0];
  assign m03_couplers_to_m03_couplers_WLAST = S_AXI_wlast;
  assign m03_couplers_to_m03_couplers_WREADY = M_AXI_wready;
  assign m03_couplers_to_m03_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m03_couplers_to_m03_couplers_WVALID = S_AXI_wvalid;
endmodule

module m04_couplers_imp_1BRG4TB
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [11:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [11:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  input [11:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [11:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m04_couplers_to_m04_couplers_ARADDR;
  wire [1:0]m04_couplers_to_m04_couplers_ARBURST;
  wire [3:0]m04_couplers_to_m04_couplers_ARCACHE;
  wire [11:0]m04_couplers_to_m04_couplers_ARID;
  wire [7:0]m04_couplers_to_m04_couplers_ARLEN;
  wire m04_couplers_to_m04_couplers_ARLOCK;
  wire [2:0]m04_couplers_to_m04_couplers_ARPROT;
  wire m04_couplers_to_m04_couplers_ARREADY;
  wire [2:0]m04_couplers_to_m04_couplers_ARSIZE;
  wire m04_couplers_to_m04_couplers_ARVALID;
  wire [31:0]m04_couplers_to_m04_couplers_AWADDR;
  wire [1:0]m04_couplers_to_m04_couplers_AWBURST;
  wire [3:0]m04_couplers_to_m04_couplers_AWCACHE;
  wire [11:0]m04_couplers_to_m04_couplers_AWID;
  wire [7:0]m04_couplers_to_m04_couplers_AWLEN;
  wire m04_couplers_to_m04_couplers_AWLOCK;
  wire [2:0]m04_couplers_to_m04_couplers_AWPROT;
  wire m04_couplers_to_m04_couplers_AWREADY;
  wire [2:0]m04_couplers_to_m04_couplers_AWSIZE;
  wire m04_couplers_to_m04_couplers_AWVALID;
  wire [11:0]m04_couplers_to_m04_couplers_BID;
  wire m04_couplers_to_m04_couplers_BREADY;
  wire [1:0]m04_couplers_to_m04_couplers_BRESP;
  wire m04_couplers_to_m04_couplers_BVALID;
  wire [31:0]m04_couplers_to_m04_couplers_RDATA;
  wire [11:0]m04_couplers_to_m04_couplers_RID;
  wire m04_couplers_to_m04_couplers_RLAST;
  wire m04_couplers_to_m04_couplers_RREADY;
  wire [1:0]m04_couplers_to_m04_couplers_RRESP;
  wire m04_couplers_to_m04_couplers_RVALID;
  wire [31:0]m04_couplers_to_m04_couplers_WDATA;
  wire m04_couplers_to_m04_couplers_WLAST;
  wire m04_couplers_to_m04_couplers_WREADY;
  wire [3:0]m04_couplers_to_m04_couplers_WSTRB;
  wire m04_couplers_to_m04_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m04_couplers_to_m04_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = m04_couplers_to_m04_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = m04_couplers_to_m04_couplers_ARCACHE;
  assign M_AXI_arid[11:0] = m04_couplers_to_m04_couplers_ARID;
  assign M_AXI_arlen[7:0] = m04_couplers_to_m04_couplers_ARLEN;
  assign M_AXI_arlock = m04_couplers_to_m04_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = m04_couplers_to_m04_couplers_ARPROT;
  assign M_AXI_arsize[2:0] = m04_couplers_to_m04_couplers_ARSIZE;
  assign M_AXI_arvalid = m04_couplers_to_m04_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m04_couplers_to_m04_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = m04_couplers_to_m04_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = m04_couplers_to_m04_couplers_AWCACHE;
  assign M_AXI_awid[11:0] = m04_couplers_to_m04_couplers_AWID;
  assign M_AXI_awlen[7:0] = m04_couplers_to_m04_couplers_AWLEN;
  assign M_AXI_awlock = m04_couplers_to_m04_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = m04_couplers_to_m04_couplers_AWPROT;
  assign M_AXI_awsize[2:0] = m04_couplers_to_m04_couplers_AWSIZE;
  assign M_AXI_awvalid = m04_couplers_to_m04_couplers_AWVALID;
  assign M_AXI_bready = m04_couplers_to_m04_couplers_BREADY;
  assign M_AXI_rready = m04_couplers_to_m04_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m04_couplers_to_m04_couplers_WDATA;
  assign M_AXI_wlast = m04_couplers_to_m04_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = m04_couplers_to_m04_couplers_WSTRB;
  assign M_AXI_wvalid = m04_couplers_to_m04_couplers_WVALID;
  assign S_AXI_arready = m04_couplers_to_m04_couplers_ARREADY;
  assign S_AXI_awready = m04_couplers_to_m04_couplers_AWREADY;
  assign S_AXI_bid[11:0] = m04_couplers_to_m04_couplers_BID;
  assign S_AXI_bresp[1:0] = m04_couplers_to_m04_couplers_BRESP;
  assign S_AXI_bvalid = m04_couplers_to_m04_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m04_couplers_to_m04_couplers_RDATA;
  assign S_AXI_rid[11:0] = m04_couplers_to_m04_couplers_RID;
  assign S_AXI_rlast = m04_couplers_to_m04_couplers_RLAST;
  assign S_AXI_rresp[1:0] = m04_couplers_to_m04_couplers_RRESP;
  assign S_AXI_rvalid = m04_couplers_to_m04_couplers_RVALID;
  assign S_AXI_wready = m04_couplers_to_m04_couplers_WREADY;
  assign m04_couplers_to_m04_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m04_couplers_to_m04_couplers_ARBURST = S_AXI_arburst[1:0];
  assign m04_couplers_to_m04_couplers_ARCACHE = S_AXI_arcache[3:0];
  assign m04_couplers_to_m04_couplers_ARID = S_AXI_arid[11:0];
  assign m04_couplers_to_m04_couplers_ARLEN = S_AXI_arlen[7:0];
  assign m04_couplers_to_m04_couplers_ARLOCK = S_AXI_arlock;
  assign m04_couplers_to_m04_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m04_couplers_to_m04_couplers_ARREADY = M_AXI_arready;
  assign m04_couplers_to_m04_couplers_ARSIZE = S_AXI_arsize[2:0];
  assign m04_couplers_to_m04_couplers_ARVALID = S_AXI_arvalid;
  assign m04_couplers_to_m04_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m04_couplers_to_m04_couplers_AWBURST = S_AXI_awburst[1:0];
  assign m04_couplers_to_m04_couplers_AWCACHE = S_AXI_awcache[3:0];
  assign m04_couplers_to_m04_couplers_AWID = S_AXI_awid[11:0];
  assign m04_couplers_to_m04_couplers_AWLEN = S_AXI_awlen[7:0];
  assign m04_couplers_to_m04_couplers_AWLOCK = S_AXI_awlock;
  assign m04_couplers_to_m04_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m04_couplers_to_m04_couplers_AWREADY = M_AXI_awready;
  assign m04_couplers_to_m04_couplers_AWSIZE = S_AXI_awsize[2:0];
  assign m04_couplers_to_m04_couplers_AWVALID = S_AXI_awvalid;
  assign m04_couplers_to_m04_couplers_BID = M_AXI_bid[11:0];
  assign m04_couplers_to_m04_couplers_BREADY = S_AXI_bready;
  assign m04_couplers_to_m04_couplers_BRESP = M_AXI_bresp[1:0];
  assign m04_couplers_to_m04_couplers_BVALID = M_AXI_bvalid;
  assign m04_couplers_to_m04_couplers_RDATA = M_AXI_rdata[31:0];
  assign m04_couplers_to_m04_couplers_RID = M_AXI_rid[11:0];
  assign m04_couplers_to_m04_couplers_RLAST = M_AXI_rlast;
  assign m04_couplers_to_m04_couplers_RREADY = S_AXI_rready;
  assign m04_couplers_to_m04_couplers_RRESP = M_AXI_rresp[1:0];
  assign m04_couplers_to_m04_couplers_RVALID = M_AXI_rvalid;
  assign m04_couplers_to_m04_couplers_WDATA = S_AXI_wdata[31:0];
  assign m04_couplers_to_m04_couplers_WLAST = S_AXI_wlast;
  assign m04_couplers_to_m04_couplers_WREADY = M_AXI_wready;
  assign m04_couplers_to_m04_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m04_couplers_to_m04_couplers_WVALID = S_AXI_wvalid;
endmodule

module m05_couplers_imp_IPNQ9Q
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [11:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [11:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  input [11:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [11:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [7:0]S_AXI_arlen;
  input S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [7:0]S_AXI_awlen;
  input S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire [31:0]m05_couplers_to_m05_couplers_ARADDR;
  wire [1:0]m05_couplers_to_m05_couplers_ARBURST;
  wire [3:0]m05_couplers_to_m05_couplers_ARCACHE;
  wire [11:0]m05_couplers_to_m05_couplers_ARID;
  wire [7:0]m05_couplers_to_m05_couplers_ARLEN;
  wire m05_couplers_to_m05_couplers_ARLOCK;
  wire [2:0]m05_couplers_to_m05_couplers_ARPROT;
  wire m05_couplers_to_m05_couplers_ARREADY;
  wire [2:0]m05_couplers_to_m05_couplers_ARSIZE;
  wire m05_couplers_to_m05_couplers_ARVALID;
  wire [31:0]m05_couplers_to_m05_couplers_AWADDR;
  wire [1:0]m05_couplers_to_m05_couplers_AWBURST;
  wire [3:0]m05_couplers_to_m05_couplers_AWCACHE;
  wire [11:0]m05_couplers_to_m05_couplers_AWID;
  wire [7:0]m05_couplers_to_m05_couplers_AWLEN;
  wire m05_couplers_to_m05_couplers_AWLOCK;
  wire [2:0]m05_couplers_to_m05_couplers_AWPROT;
  wire m05_couplers_to_m05_couplers_AWREADY;
  wire [2:0]m05_couplers_to_m05_couplers_AWSIZE;
  wire m05_couplers_to_m05_couplers_AWVALID;
  wire [11:0]m05_couplers_to_m05_couplers_BID;
  wire m05_couplers_to_m05_couplers_BREADY;
  wire [1:0]m05_couplers_to_m05_couplers_BRESP;
  wire m05_couplers_to_m05_couplers_BVALID;
  wire [31:0]m05_couplers_to_m05_couplers_RDATA;
  wire [11:0]m05_couplers_to_m05_couplers_RID;
  wire m05_couplers_to_m05_couplers_RLAST;
  wire m05_couplers_to_m05_couplers_RREADY;
  wire [1:0]m05_couplers_to_m05_couplers_RRESP;
  wire m05_couplers_to_m05_couplers_RVALID;
  wire [31:0]m05_couplers_to_m05_couplers_WDATA;
  wire m05_couplers_to_m05_couplers_WLAST;
  wire m05_couplers_to_m05_couplers_WREADY;
  wire [3:0]m05_couplers_to_m05_couplers_WSTRB;
  wire m05_couplers_to_m05_couplers_WVALID;

  assign M_AXI_araddr[31:0] = m05_couplers_to_m05_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = m05_couplers_to_m05_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = m05_couplers_to_m05_couplers_ARCACHE;
  assign M_AXI_arid[11:0] = m05_couplers_to_m05_couplers_ARID;
  assign M_AXI_arlen[7:0] = m05_couplers_to_m05_couplers_ARLEN;
  assign M_AXI_arlock = m05_couplers_to_m05_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = m05_couplers_to_m05_couplers_ARPROT;
  assign M_AXI_arsize[2:0] = m05_couplers_to_m05_couplers_ARSIZE;
  assign M_AXI_arvalid = m05_couplers_to_m05_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = m05_couplers_to_m05_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = m05_couplers_to_m05_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = m05_couplers_to_m05_couplers_AWCACHE;
  assign M_AXI_awid[11:0] = m05_couplers_to_m05_couplers_AWID;
  assign M_AXI_awlen[7:0] = m05_couplers_to_m05_couplers_AWLEN;
  assign M_AXI_awlock = m05_couplers_to_m05_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = m05_couplers_to_m05_couplers_AWPROT;
  assign M_AXI_awsize[2:0] = m05_couplers_to_m05_couplers_AWSIZE;
  assign M_AXI_awvalid = m05_couplers_to_m05_couplers_AWVALID;
  assign M_AXI_bready = m05_couplers_to_m05_couplers_BREADY;
  assign M_AXI_rready = m05_couplers_to_m05_couplers_RREADY;
  assign M_AXI_wdata[31:0] = m05_couplers_to_m05_couplers_WDATA;
  assign M_AXI_wlast = m05_couplers_to_m05_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = m05_couplers_to_m05_couplers_WSTRB;
  assign M_AXI_wvalid = m05_couplers_to_m05_couplers_WVALID;
  assign S_AXI_arready = m05_couplers_to_m05_couplers_ARREADY;
  assign S_AXI_awready = m05_couplers_to_m05_couplers_AWREADY;
  assign S_AXI_bid[11:0] = m05_couplers_to_m05_couplers_BID;
  assign S_AXI_bresp[1:0] = m05_couplers_to_m05_couplers_BRESP;
  assign S_AXI_bvalid = m05_couplers_to_m05_couplers_BVALID;
  assign S_AXI_rdata[31:0] = m05_couplers_to_m05_couplers_RDATA;
  assign S_AXI_rid[11:0] = m05_couplers_to_m05_couplers_RID;
  assign S_AXI_rlast = m05_couplers_to_m05_couplers_RLAST;
  assign S_AXI_rresp[1:0] = m05_couplers_to_m05_couplers_RRESP;
  assign S_AXI_rvalid = m05_couplers_to_m05_couplers_RVALID;
  assign S_AXI_wready = m05_couplers_to_m05_couplers_WREADY;
  assign m05_couplers_to_m05_couplers_ARADDR = S_AXI_araddr[31:0];
  assign m05_couplers_to_m05_couplers_ARBURST = S_AXI_arburst[1:0];
  assign m05_couplers_to_m05_couplers_ARCACHE = S_AXI_arcache[3:0];
  assign m05_couplers_to_m05_couplers_ARID = S_AXI_arid[11:0];
  assign m05_couplers_to_m05_couplers_ARLEN = S_AXI_arlen[7:0];
  assign m05_couplers_to_m05_couplers_ARLOCK = S_AXI_arlock;
  assign m05_couplers_to_m05_couplers_ARPROT = S_AXI_arprot[2:0];
  assign m05_couplers_to_m05_couplers_ARREADY = M_AXI_arready;
  assign m05_couplers_to_m05_couplers_ARSIZE = S_AXI_arsize[2:0];
  assign m05_couplers_to_m05_couplers_ARVALID = S_AXI_arvalid;
  assign m05_couplers_to_m05_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign m05_couplers_to_m05_couplers_AWBURST = S_AXI_awburst[1:0];
  assign m05_couplers_to_m05_couplers_AWCACHE = S_AXI_awcache[3:0];
  assign m05_couplers_to_m05_couplers_AWID = S_AXI_awid[11:0];
  assign m05_couplers_to_m05_couplers_AWLEN = S_AXI_awlen[7:0];
  assign m05_couplers_to_m05_couplers_AWLOCK = S_AXI_awlock;
  assign m05_couplers_to_m05_couplers_AWPROT = S_AXI_awprot[2:0];
  assign m05_couplers_to_m05_couplers_AWREADY = M_AXI_awready;
  assign m05_couplers_to_m05_couplers_AWSIZE = S_AXI_awsize[2:0];
  assign m05_couplers_to_m05_couplers_AWVALID = S_AXI_awvalid;
  assign m05_couplers_to_m05_couplers_BID = M_AXI_bid[11:0];
  assign m05_couplers_to_m05_couplers_BREADY = S_AXI_bready;
  assign m05_couplers_to_m05_couplers_BRESP = M_AXI_bresp[1:0];
  assign m05_couplers_to_m05_couplers_BVALID = M_AXI_bvalid;
  assign m05_couplers_to_m05_couplers_RDATA = M_AXI_rdata[31:0];
  assign m05_couplers_to_m05_couplers_RID = M_AXI_rid[11:0];
  assign m05_couplers_to_m05_couplers_RLAST = M_AXI_rlast;
  assign m05_couplers_to_m05_couplers_RREADY = S_AXI_rready;
  assign m05_couplers_to_m05_couplers_RRESP = M_AXI_rresp[1:0];
  assign m05_couplers_to_m05_couplers_RVALID = M_AXI_rvalid;
  assign m05_couplers_to_m05_couplers_WDATA = S_AXI_wdata[31:0];
  assign m05_couplers_to_m05_couplers_WLAST = S_AXI_wlast;
  assign m05_couplers_to_m05_couplers_WREADY = M_AXI_wready;
  assign m05_couplers_to_m05_couplers_WSTRB = S_AXI_wstrb[3:0];
  assign m05_couplers_to_m05_couplers_WVALID = S_AXI_wvalid;
endmodule

module s00_couplers_imp_19WQW9S
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [63:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [63:0]M_AXI_wdata;
  input M_AXI_wready;
  output [7:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_us_to_s00_couplers_ARADDR;
  wire [2:0]auto_us_to_s00_couplers_ARPROT;
  wire auto_us_to_s00_couplers_ARREADY;
  wire auto_us_to_s00_couplers_ARVALID;
  wire [31:0]auto_us_to_s00_couplers_AWADDR;
  wire [2:0]auto_us_to_s00_couplers_AWPROT;
  wire auto_us_to_s00_couplers_AWREADY;
  wire auto_us_to_s00_couplers_AWVALID;
  wire auto_us_to_s00_couplers_BREADY;
  wire [1:0]auto_us_to_s00_couplers_BRESP;
  wire auto_us_to_s00_couplers_BVALID;
  wire [63:0]auto_us_to_s00_couplers_RDATA;
  wire auto_us_to_s00_couplers_RREADY;
  wire [1:0]auto_us_to_s00_couplers_RRESP;
  wire auto_us_to_s00_couplers_RVALID;
  wire [63:0]auto_us_to_s00_couplers_WDATA;
  wire auto_us_to_s00_couplers_WREADY;
  wire [7:0]auto_us_to_s00_couplers_WSTRB;
  wire auto_us_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_us_ARADDR;
  wire [2:0]s00_couplers_to_auto_us_ARPROT;
  wire s00_couplers_to_auto_us_ARREADY;
  wire s00_couplers_to_auto_us_ARVALID;
  wire [31:0]s00_couplers_to_auto_us_AWADDR;
  wire [2:0]s00_couplers_to_auto_us_AWPROT;
  wire s00_couplers_to_auto_us_AWREADY;
  wire s00_couplers_to_auto_us_AWVALID;
  wire s00_couplers_to_auto_us_BREADY;
  wire s00_couplers_to_auto_us_BVALID;
  wire [31:0]s00_couplers_to_auto_us_RDATA;
  wire s00_couplers_to_auto_us_RREADY;
  wire s00_couplers_to_auto_us_RVALID;
  wire [31:0]s00_couplers_to_auto_us_WDATA;
  wire s00_couplers_to_auto_us_WREADY;
  wire [3:0]s00_couplers_to_auto_us_WSTRB;
  wire s00_couplers_to_auto_us_WVALID;

  assign M_AXI_araddr[31:0] = auto_us_to_s00_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = auto_us_to_s00_couplers_ARPROT;
  assign M_AXI_arvalid = auto_us_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_us_to_s00_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = auto_us_to_s00_couplers_AWPROT;
  assign M_AXI_awvalid = auto_us_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_us_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_us_to_s00_couplers_RREADY;
  assign M_AXI_wdata[63:0] = auto_us_to_s00_couplers_WDATA;
  assign M_AXI_wstrb[7:0] = auto_us_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_us_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = s00_couplers_to_auto_us_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_us_AWREADY;
  assign S_AXI_bvalid = s00_couplers_to_auto_us_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_us_RDATA;
  assign S_AXI_rvalid = s00_couplers_to_auto_us_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_us_WREADY;
  assign auto_us_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_us_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_us_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_us_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_us_to_s00_couplers_RDATA = M_AXI_rdata[63:0];
  assign auto_us_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_us_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_us_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_us_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_us_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_us_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_us_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_us_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_us_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_us_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_us_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_us_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_us_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_us_WVALID = S_AXI_wvalid;
  cerberus_auto_us_3 auto_us
       (.m_axi_araddr(auto_us_to_s00_couplers_ARADDR),
        .m_axi_arprot(auto_us_to_s00_couplers_ARPROT),
        .m_axi_arready(auto_us_to_s00_couplers_ARREADY),
        .m_axi_arvalid(auto_us_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_us_to_s00_couplers_AWADDR),
        .m_axi_awprot(auto_us_to_s00_couplers_AWPROT),
        .m_axi_awready(auto_us_to_s00_couplers_AWREADY),
        .m_axi_awvalid(auto_us_to_s00_couplers_AWVALID),
        .m_axi_bready(auto_us_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_us_to_s00_couplers_BRESP),
        .m_axi_bvalid(auto_us_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_us_to_s00_couplers_RDATA),
        .m_axi_rready(auto_us_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_us_to_s00_couplers_RRESP),
        .m_axi_rvalid(auto_us_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_us_to_s00_couplers_WDATA),
        .m_axi_wready(auto_us_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_us_to_s00_couplers_WSTRB),
        .m_axi_wvalid(auto_us_to_s00_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s00_couplers_to_auto_us_ARADDR),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arprot(s00_couplers_to_auto_us_ARPROT),
        .s_axi_arready(s00_couplers_to_auto_us_ARREADY),
        .s_axi_arvalid(s00_couplers_to_auto_us_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_us_AWADDR),
        .s_axi_awprot(s00_couplers_to_auto_us_AWPROT),
        .s_axi_awready(s00_couplers_to_auto_us_AWREADY),
        .s_axi_awvalid(s00_couplers_to_auto_us_AWVALID),
        .s_axi_bready(s00_couplers_to_auto_us_BREADY),
        .s_axi_bvalid(s00_couplers_to_auto_us_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_us_RDATA),
        .s_axi_rready(s00_couplers_to_auto_us_RREADY),
        .s_axi_rvalid(s00_couplers_to_auto_us_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_us_WDATA),
        .s_axi_wready(s00_couplers_to_auto_us_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_us_WSTRB),
        .s_axi_wvalid(s00_couplers_to_auto_us_WVALID));
endmodule

module s00_couplers_imp_1SJJVK7
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [63:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [63:0]M_AXI_wdata;
  input M_AXI_wready;
  output [7:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_us_to_s00_couplers_ARADDR;
  wire [2:0]auto_us_to_s00_couplers_ARPROT;
  wire auto_us_to_s00_couplers_ARREADY;
  wire auto_us_to_s00_couplers_ARVALID;
  wire [31:0]auto_us_to_s00_couplers_AWADDR;
  wire [2:0]auto_us_to_s00_couplers_AWPROT;
  wire auto_us_to_s00_couplers_AWREADY;
  wire auto_us_to_s00_couplers_AWVALID;
  wire auto_us_to_s00_couplers_BREADY;
  wire [1:0]auto_us_to_s00_couplers_BRESP;
  wire auto_us_to_s00_couplers_BVALID;
  wire [63:0]auto_us_to_s00_couplers_RDATA;
  wire auto_us_to_s00_couplers_RREADY;
  wire [1:0]auto_us_to_s00_couplers_RRESP;
  wire auto_us_to_s00_couplers_RVALID;
  wire [63:0]auto_us_to_s00_couplers_WDATA;
  wire auto_us_to_s00_couplers_WREADY;
  wire [7:0]auto_us_to_s00_couplers_WSTRB;
  wire auto_us_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_us_ARADDR;
  wire [2:0]s00_couplers_to_auto_us_ARPROT;
  wire s00_couplers_to_auto_us_ARREADY;
  wire s00_couplers_to_auto_us_ARVALID;
  wire [31:0]s00_couplers_to_auto_us_AWADDR;
  wire [2:0]s00_couplers_to_auto_us_AWPROT;
  wire s00_couplers_to_auto_us_AWREADY;
  wire s00_couplers_to_auto_us_AWVALID;
  wire s00_couplers_to_auto_us_BREADY;
  wire s00_couplers_to_auto_us_BVALID;
  wire [31:0]s00_couplers_to_auto_us_RDATA;
  wire s00_couplers_to_auto_us_RREADY;
  wire s00_couplers_to_auto_us_RVALID;
  wire [31:0]s00_couplers_to_auto_us_WDATA;
  wire s00_couplers_to_auto_us_WREADY;
  wire [3:0]s00_couplers_to_auto_us_WSTRB;
  wire s00_couplers_to_auto_us_WVALID;

  assign M_AXI_araddr[31:0] = auto_us_to_s00_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = auto_us_to_s00_couplers_ARPROT;
  assign M_AXI_arvalid = auto_us_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_us_to_s00_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = auto_us_to_s00_couplers_AWPROT;
  assign M_AXI_awvalid = auto_us_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_us_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_us_to_s00_couplers_RREADY;
  assign M_AXI_wdata[63:0] = auto_us_to_s00_couplers_WDATA;
  assign M_AXI_wstrb[7:0] = auto_us_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_us_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = s00_couplers_to_auto_us_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_us_AWREADY;
  assign S_AXI_bvalid = s00_couplers_to_auto_us_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_us_RDATA;
  assign S_AXI_rvalid = s00_couplers_to_auto_us_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_us_WREADY;
  assign auto_us_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_us_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_us_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_us_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_us_to_s00_couplers_RDATA = M_AXI_rdata[63:0];
  assign auto_us_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_us_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_us_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_us_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_us_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_us_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_us_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_us_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_us_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_us_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_us_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_us_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_us_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_us_WVALID = S_AXI_wvalid;
  cerberus_auto_us_0 auto_us
       (.m_axi_araddr(auto_us_to_s00_couplers_ARADDR),
        .m_axi_arprot(auto_us_to_s00_couplers_ARPROT),
        .m_axi_arready(auto_us_to_s00_couplers_ARREADY),
        .m_axi_arvalid(auto_us_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_us_to_s00_couplers_AWADDR),
        .m_axi_awprot(auto_us_to_s00_couplers_AWPROT),
        .m_axi_awready(auto_us_to_s00_couplers_AWREADY),
        .m_axi_awvalid(auto_us_to_s00_couplers_AWVALID),
        .m_axi_bready(auto_us_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_us_to_s00_couplers_BRESP),
        .m_axi_bvalid(auto_us_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_us_to_s00_couplers_RDATA),
        .m_axi_rready(auto_us_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_us_to_s00_couplers_RRESP),
        .m_axi_rvalid(auto_us_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_us_to_s00_couplers_WDATA),
        .m_axi_wready(auto_us_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_us_to_s00_couplers_WSTRB),
        .m_axi_wvalid(auto_us_to_s00_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s00_couplers_to_auto_us_ARADDR),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arprot(s00_couplers_to_auto_us_ARPROT),
        .s_axi_arready(s00_couplers_to_auto_us_ARREADY),
        .s_axi_arvalid(s00_couplers_to_auto_us_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_us_AWADDR),
        .s_axi_awprot(s00_couplers_to_auto_us_AWPROT),
        .s_axi_awready(s00_couplers_to_auto_us_AWREADY),
        .s_axi_awvalid(s00_couplers_to_auto_us_AWVALID),
        .s_axi_bready(s00_couplers_to_auto_us_BREADY),
        .s_axi_bvalid(s00_couplers_to_auto_us_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_us_RDATA),
        .s_axi_rready(s00_couplers_to_auto_us_RREADY),
        .s_axi_rvalid(s00_couplers_to_auto_us_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_us_WDATA),
        .s_axi_wready(s00_couplers_to_auto_us_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_us_WSTRB),
        .s_axi_wvalid(s00_couplers_to_auto_us_WVALID));
endmodule

module s00_couplers_imp_1XNV4IL
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [63:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [63:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [7:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [63:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [63:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [7:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [31:0]s00_couplers_to_s00_couplers_ARADDR;
  wire [2:0]s00_couplers_to_s00_couplers_ARPROT;
  wire [0:0]s00_couplers_to_s00_couplers_ARREADY;
  wire [0:0]s00_couplers_to_s00_couplers_ARVALID;
  wire [31:0]s00_couplers_to_s00_couplers_AWADDR;
  wire [2:0]s00_couplers_to_s00_couplers_AWPROT;
  wire [0:0]s00_couplers_to_s00_couplers_AWREADY;
  wire [0:0]s00_couplers_to_s00_couplers_AWVALID;
  wire [0:0]s00_couplers_to_s00_couplers_BREADY;
  wire [1:0]s00_couplers_to_s00_couplers_BRESP;
  wire [0:0]s00_couplers_to_s00_couplers_BVALID;
  wire [63:0]s00_couplers_to_s00_couplers_RDATA;
  wire [0:0]s00_couplers_to_s00_couplers_RREADY;
  wire [1:0]s00_couplers_to_s00_couplers_RRESP;
  wire [0:0]s00_couplers_to_s00_couplers_RVALID;
  wire [63:0]s00_couplers_to_s00_couplers_WDATA;
  wire [0:0]s00_couplers_to_s00_couplers_WREADY;
  wire [7:0]s00_couplers_to_s00_couplers_WSTRB;
  wire [0:0]s00_couplers_to_s00_couplers_WVALID;

  assign M_AXI_araddr[31:0] = s00_couplers_to_s00_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = s00_couplers_to_s00_couplers_ARPROT;
  assign M_AXI_arvalid[0] = s00_couplers_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = s00_couplers_to_s00_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = s00_couplers_to_s00_couplers_AWPROT;
  assign M_AXI_awvalid[0] = s00_couplers_to_s00_couplers_AWVALID;
  assign M_AXI_bready[0] = s00_couplers_to_s00_couplers_BREADY;
  assign M_AXI_rready[0] = s00_couplers_to_s00_couplers_RREADY;
  assign M_AXI_wdata[63:0] = s00_couplers_to_s00_couplers_WDATA;
  assign M_AXI_wstrb[7:0] = s00_couplers_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid[0] = s00_couplers_to_s00_couplers_WVALID;
  assign S_AXI_arready[0] = s00_couplers_to_s00_couplers_ARREADY;
  assign S_AXI_awready[0] = s00_couplers_to_s00_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = s00_couplers_to_s00_couplers_BRESP;
  assign S_AXI_bvalid[0] = s00_couplers_to_s00_couplers_BVALID;
  assign S_AXI_rdata[63:0] = s00_couplers_to_s00_couplers_RDATA;
  assign S_AXI_rresp[1:0] = s00_couplers_to_s00_couplers_RRESP;
  assign S_AXI_rvalid[0] = s00_couplers_to_s00_couplers_RVALID;
  assign S_AXI_wready[0] = s00_couplers_to_s00_couplers_WREADY;
  assign s00_couplers_to_s00_couplers_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_s00_couplers_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_s00_couplers_ARREADY = M_AXI_arready[0];
  assign s00_couplers_to_s00_couplers_ARVALID = S_AXI_arvalid[0];
  assign s00_couplers_to_s00_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_s00_couplers_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_s00_couplers_AWREADY = M_AXI_awready[0];
  assign s00_couplers_to_s00_couplers_AWVALID = S_AXI_awvalid[0];
  assign s00_couplers_to_s00_couplers_BREADY = S_AXI_bready[0];
  assign s00_couplers_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign s00_couplers_to_s00_couplers_BVALID = M_AXI_bvalid[0];
  assign s00_couplers_to_s00_couplers_RDATA = M_AXI_rdata[63:0];
  assign s00_couplers_to_s00_couplers_RREADY = S_AXI_rready[0];
  assign s00_couplers_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign s00_couplers_to_s00_couplers_RVALID = M_AXI_rvalid[0];
  assign s00_couplers_to_s00_couplers_WDATA = S_AXI_wdata[63:0];
  assign s00_couplers_to_s00_couplers_WREADY = M_AXI_wready[0];
  assign s00_couplers_to_s00_couplers_WSTRB = S_AXI_wstrb[7:0];
  assign s00_couplers_to_s00_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module s00_couplers_imp_25GATJ
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [63:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [63:0]M_AXI_wdata;
  input M_AXI_wready;
  output [7:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_us_to_s00_couplers_ARADDR;
  wire [2:0]auto_us_to_s00_couplers_ARPROT;
  wire auto_us_to_s00_couplers_ARREADY;
  wire auto_us_to_s00_couplers_ARVALID;
  wire [31:0]auto_us_to_s00_couplers_AWADDR;
  wire [2:0]auto_us_to_s00_couplers_AWPROT;
  wire auto_us_to_s00_couplers_AWREADY;
  wire auto_us_to_s00_couplers_AWVALID;
  wire auto_us_to_s00_couplers_BREADY;
  wire [1:0]auto_us_to_s00_couplers_BRESP;
  wire auto_us_to_s00_couplers_BVALID;
  wire [63:0]auto_us_to_s00_couplers_RDATA;
  wire auto_us_to_s00_couplers_RREADY;
  wire [1:0]auto_us_to_s00_couplers_RRESP;
  wire auto_us_to_s00_couplers_RVALID;
  wire [63:0]auto_us_to_s00_couplers_WDATA;
  wire auto_us_to_s00_couplers_WREADY;
  wire [7:0]auto_us_to_s00_couplers_WSTRB;
  wire auto_us_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_us_ARADDR;
  wire [2:0]s00_couplers_to_auto_us_ARPROT;
  wire s00_couplers_to_auto_us_ARREADY;
  wire s00_couplers_to_auto_us_ARVALID;
  wire [31:0]s00_couplers_to_auto_us_AWADDR;
  wire [2:0]s00_couplers_to_auto_us_AWPROT;
  wire s00_couplers_to_auto_us_AWREADY;
  wire s00_couplers_to_auto_us_AWVALID;
  wire s00_couplers_to_auto_us_BREADY;
  wire s00_couplers_to_auto_us_BVALID;
  wire [31:0]s00_couplers_to_auto_us_RDATA;
  wire s00_couplers_to_auto_us_RREADY;
  wire s00_couplers_to_auto_us_RVALID;
  wire [31:0]s00_couplers_to_auto_us_WDATA;
  wire s00_couplers_to_auto_us_WREADY;
  wire [3:0]s00_couplers_to_auto_us_WSTRB;
  wire s00_couplers_to_auto_us_WVALID;

  assign M_AXI_araddr[31:0] = auto_us_to_s00_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = auto_us_to_s00_couplers_ARPROT;
  assign M_AXI_arvalid = auto_us_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_us_to_s00_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = auto_us_to_s00_couplers_AWPROT;
  assign M_AXI_awvalid = auto_us_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_us_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_us_to_s00_couplers_RREADY;
  assign M_AXI_wdata[63:0] = auto_us_to_s00_couplers_WDATA;
  assign M_AXI_wstrb[7:0] = auto_us_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_us_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = s00_couplers_to_auto_us_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_us_AWREADY;
  assign S_AXI_bvalid = s00_couplers_to_auto_us_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_us_RDATA;
  assign S_AXI_rvalid = s00_couplers_to_auto_us_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_us_WREADY;
  assign auto_us_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_us_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_us_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_us_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_us_to_s00_couplers_RDATA = M_AXI_rdata[63:0];
  assign auto_us_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_us_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_us_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_us_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_us_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_us_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_us_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_us_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_us_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_us_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_us_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_us_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_us_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_us_WVALID = S_AXI_wvalid;
  cerberus_auto_us_2 auto_us
       (.m_axi_araddr(auto_us_to_s00_couplers_ARADDR),
        .m_axi_arprot(auto_us_to_s00_couplers_ARPROT),
        .m_axi_arready(auto_us_to_s00_couplers_ARREADY),
        .m_axi_arvalid(auto_us_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_us_to_s00_couplers_AWADDR),
        .m_axi_awprot(auto_us_to_s00_couplers_AWPROT),
        .m_axi_awready(auto_us_to_s00_couplers_AWREADY),
        .m_axi_awvalid(auto_us_to_s00_couplers_AWVALID),
        .m_axi_bready(auto_us_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_us_to_s00_couplers_BRESP),
        .m_axi_bvalid(auto_us_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_us_to_s00_couplers_RDATA),
        .m_axi_rready(auto_us_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_us_to_s00_couplers_RRESP),
        .m_axi_rvalid(auto_us_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_us_to_s00_couplers_WDATA),
        .m_axi_wready(auto_us_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_us_to_s00_couplers_WSTRB),
        .m_axi_wvalid(auto_us_to_s00_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s00_couplers_to_auto_us_ARADDR),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arprot(s00_couplers_to_auto_us_ARPROT),
        .s_axi_arready(s00_couplers_to_auto_us_ARREADY),
        .s_axi_arvalid(s00_couplers_to_auto_us_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_us_AWADDR),
        .s_axi_awprot(s00_couplers_to_auto_us_AWPROT),
        .s_axi_awready(s00_couplers_to_auto_us_AWREADY),
        .s_axi_awvalid(s00_couplers_to_auto_us_AWVALID),
        .s_axi_bready(s00_couplers_to_auto_us_BREADY),
        .s_axi_bvalid(s00_couplers_to_auto_us_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_us_RDATA),
        .s_axi_rready(s00_couplers_to_auto_us_RREADY),
        .s_axi_rvalid(s00_couplers_to_auto_us_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_us_WDATA),
        .s_axi_wready(s00_couplers_to_auto_us_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_us_WSTRB),
        .s_axi_wvalid(s00_couplers_to_auto_us_WVALID));
endmodule

module s00_couplers_imp_BDHSLE
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input M_AXI_arready;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input M_AXI_awready;
  output M_AXI_awvalid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [63:0]M_AXI_rdata;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [63:0]M_AXI_wdata;
  input M_AXI_wready;
  output [7:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output S_AXI_arready;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output S_AXI_awready;
  input S_AXI_awvalid;
  input S_AXI_bready;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  input S_AXI_rready;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_us_to_s00_couplers_ARADDR;
  wire [2:0]auto_us_to_s00_couplers_ARPROT;
  wire auto_us_to_s00_couplers_ARREADY;
  wire auto_us_to_s00_couplers_ARVALID;
  wire [31:0]auto_us_to_s00_couplers_AWADDR;
  wire [2:0]auto_us_to_s00_couplers_AWPROT;
  wire auto_us_to_s00_couplers_AWREADY;
  wire auto_us_to_s00_couplers_AWVALID;
  wire auto_us_to_s00_couplers_BREADY;
  wire [1:0]auto_us_to_s00_couplers_BRESP;
  wire auto_us_to_s00_couplers_BVALID;
  wire [63:0]auto_us_to_s00_couplers_RDATA;
  wire auto_us_to_s00_couplers_RREADY;
  wire [1:0]auto_us_to_s00_couplers_RRESP;
  wire auto_us_to_s00_couplers_RVALID;
  wire [63:0]auto_us_to_s00_couplers_WDATA;
  wire auto_us_to_s00_couplers_WREADY;
  wire [7:0]auto_us_to_s00_couplers_WSTRB;
  wire auto_us_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_us_ARADDR;
  wire [2:0]s00_couplers_to_auto_us_ARPROT;
  wire s00_couplers_to_auto_us_ARREADY;
  wire s00_couplers_to_auto_us_ARVALID;
  wire [31:0]s00_couplers_to_auto_us_AWADDR;
  wire [2:0]s00_couplers_to_auto_us_AWPROT;
  wire s00_couplers_to_auto_us_AWREADY;
  wire s00_couplers_to_auto_us_AWVALID;
  wire s00_couplers_to_auto_us_BREADY;
  wire s00_couplers_to_auto_us_BVALID;
  wire [31:0]s00_couplers_to_auto_us_RDATA;
  wire s00_couplers_to_auto_us_RREADY;
  wire s00_couplers_to_auto_us_RVALID;
  wire [31:0]s00_couplers_to_auto_us_WDATA;
  wire s00_couplers_to_auto_us_WREADY;
  wire [3:0]s00_couplers_to_auto_us_WSTRB;
  wire s00_couplers_to_auto_us_WVALID;

  assign M_AXI_araddr[31:0] = auto_us_to_s00_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = auto_us_to_s00_couplers_ARPROT;
  assign M_AXI_arvalid = auto_us_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_us_to_s00_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = auto_us_to_s00_couplers_AWPROT;
  assign M_AXI_awvalid = auto_us_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_us_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_us_to_s00_couplers_RREADY;
  assign M_AXI_wdata[63:0] = auto_us_to_s00_couplers_WDATA;
  assign M_AXI_wstrb[7:0] = auto_us_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_us_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = s00_couplers_to_auto_us_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_us_AWREADY;
  assign S_AXI_bvalid = s00_couplers_to_auto_us_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_us_RDATA;
  assign S_AXI_rvalid = s00_couplers_to_auto_us_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_us_WREADY;
  assign auto_us_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_us_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_us_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_us_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_us_to_s00_couplers_RDATA = M_AXI_rdata[63:0];
  assign auto_us_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_us_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_us_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_us_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_us_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_us_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_us_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_us_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_us_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_us_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_us_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_us_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_us_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_us_WVALID = S_AXI_wvalid;
  cerberus_auto_us_1 auto_us
       (.m_axi_araddr(auto_us_to_s00_couplers_ARADDR),
        .m_axi_arprot(auto_us_to_s00_couplers_ARPROT),
        .m_axi_arready(auto_us_to_s00_couplers_ARREADY),
        .m_axi_arvalid(auto_us_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_us_to_s00_couplers_AWADDR),
        .m_axi_awprot(auto_us_to_s00_couplers_AWPROT),
        .m_axi_awready(auto_us_to_s00_couplers_AWREADY),
        .m_axi_awvalid(auto_us_to_s00_couplers_AWVALID),
        .m_axi_bready(auto_us_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_us_to_s00_couplers_BRESP),
        .m_axi_bvalid(auto_us_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_us_to_s00_couplers_RDATA),
        .m_axi_rready(auto_us_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_us_to_s00_couplers_RRESP),
        .m_axi_rvalid(auto_us_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_us_to_s00_couplers_WDATA),
        .m_axi_wready(auto_us_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_us_to_s00_couplers_WSTRB),
        .m_axi_wvalid(auto_us_to_s00_couplers_WVALID),
        .s_axi_aclk(S_ACLK_1),
        .s_axi_araddr(s00_couplers_to_auto_us_ARADDR),
        .s_axi_aresetn(S_ARESETN_1),
        .s_axi_arprot(s00_couplers_to_auto_us_ARPROT),
        .s_axi_arready(s00_couplers_to_auto_us_ARREADY),
        .s_axi_arvalid(s00_couplers_to_auto_us_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_us_AWADDR),
        .s_axi_awprot(s00_couplers_to_auto_us_AWPROT),
        .s_axi_awready(s00_couplers_to_auto_us_AWREADY),
        .s_axi_awvalid(s00_couplers_to_auto_us_AWVALID),
        .s_axi_bready(s00_couplers_to_auto_us_BREADY),
        .s_axi_bvalid(s00_couplers_to_auto_us_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_us_RDATA),
        .s_axi_rready(s00_couplers_to_auto_us_RREADY),
        .s_axi_rvalid(s00_couplers_to_auto_us_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_us_WDATA),
        .s_axi_wready(s00_couplers_to_auto_us_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_us_WSTRB),
        .s_axi_wvalid(s00_couplers_to_auto_us_WVALID));
endmodule

module s00_couplers_imp_QA0Z6J
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arburst,
    M_AXI_arcache,
    M_AXI_arid,
    M_AXI_arlen,
    M_AXI_arlock,
    M_AXI_arprot,
    M_AXI_arqos,
    M_AXI_arready,
    M_AXI_arsize,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awburst,
    M_AXI_awcache,
    M_AXI_awid,
    M_AXI_awlen,
    M_AXI_awlock,
    M_AXI_awprot,
    M_AXI_awqos,
    M_AXI_awready,
    M_AXI_awsize,
    M_AXI_awvalid,
    M_AXI_bid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rid,
    M_AXI_rlast,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wlast,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arburst,
    S_AXI_arcache,
    S_AXI_arid,
    S_AXI_arlen,
    S_AXI_arlock,
    S_AXI_arprot,
    S_AXI_arqos,
    S_AXI_arready,
    S_AXI_arsize,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awburst,
    S_AXI_awcache,
    S_AXI_awid,
    S_AXI_awlen,
    S_AXI_awlock,
    S_AXI_awprot,
    S_AXI_awqos,
    S_AXI_awready,
    S_AXI_awsize,
    S_AXI_awvalid,
    S_AXI_bid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rid,
    S_AXI_rlast,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wid,
    S_AXI_wlast,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [1:0]M_AXI_arburst;
  output [3:0]M_AXI_arcache;
  output [11:0]M_AXI_arid;
  output [7:0]M_AXI_arlen;
  output [0:0]M_AXI_arlock;
  output [2:0]M_AXI_arprot;
  output [3:0]M_AXI_arqos;
  input M_AXI_arready;
  output [2:0]M_AXI_arsize;
  output M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [1:0]M_AXI_awburst;
  output [3:0]M_AXI_awcache;
  output [11:0]M_AXI_awid;
  output [7:0]M_AXI_awlen;
  output [0:0]M_AXI_awlock;
  output [2:0]M_AXI_awprot;
  output [3:0]M_AXI_awqos;
  input M_AXI_awready;
  output [2:0]M_AXI_awsize;
  output M_AXI_awvalid;
  input [11:0]M_AXI_bid;
  output M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input M_AXI_bvalid;
  input [31:0]M_AXI_rdata;
  input [11:0]M_AXI_rid;
  input M_AXI_rlast;
  output M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input M_AXI_rvalid;
  output [31:0]M_AXI_wdata;
  output M_AXI_wlast;
  input M_AXI_wready;
  output [3:0]M_AXI_wstrb;
  output M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [1:0]S_AXI_arburst;
  input [3:0]S_AXI_arcache;
  input [11:0]S_AXI_arid;
  input [3:0]S_AXI_arlen;
  input [1:0]S_AXI_arlock;
  input [2:0]S_AXI_arprot;
  input [3:0]S_AXI_arqos;
  output S_AXI_arready;
  input [2:0]S_AXI_arsize;
  input S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [1:0]S_AXI_awburst;
  input [3:0]S_AXI_awcache;
  input [11:0]S_AXI_awid;
  input [3:0]S_AXI_awlen;
  input [1:0]S_AXI_awlock;
  input [2:0]S_AXI_awprot;
  input [3:0]S_AXI_awqos;
  output S_AXI_awready;
  input [2:0]S_AXI_awsize;
  input S_AXI_awvalid;
  output [11:0]S_AXI_bid;
  input S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output S_AXI_bvalid;
  output [31:0]S_AXI_rdata;
  output [11:0]S_AXI_rid;
  output S_AXI_rlast;
  input S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output S_AXI_rvalid;
  input [31:0]S_AXI_wdata;
  input [11:0]S_AXI_wid;
  input S_AXI_wlast;
  output S_AXI_wready;
  input [3:0]S_AXI_wstrb;
  input S_AXI_wvalid;

  wire S_ACLK_1;
  wire S_ARESETN_1;
  wire [31:0]auto_pc_to_s00_couplers_ARADDR;
  wire [1:0]auto_pc_to_s00_couplers_ARBURST;
  wire [3:0]auto_pc_to_s00_couplers_ARCACHE;
  wire [11:0]auto_pc_to_s00_couplers_ARID;
  wire [7:0]auto_pc_to_s00_couplers_ARLEN;
  wire [0:0]auto_pc_to_s00_couplers_ARLOCK;
  wire [2:0]auto_pc_to_s00_couplers_ARPROT;
  wire [3:0]auto_pc_to_s00_couplers_ARQOS;
  wire auto_pc_to_s00_couplers_ARREADY;
  wire [2:0]auto_pc_to_s00_couplers_ARSIZE;
  wire auto_pc_to_s00_couplers_ARVALID;
  wire [31:0]auto_pc_to_s00_couplers_AWADDR;
  wire [1:0]auto_pc_to_s00_couplers_AWBURST;
  wire [3:0]auto_pc_to_s00_couplers_AWCACHE;
  wire [11:0]auto_pc_to_s00_couplers_AWID;
  wire [7:0]auto_pc_to_s00_couplers_AWLEN;
  wire [0:0]auto_pc_to_s00_couplers_AWLOCK;
  wire [2:0]auto_pc_to_s00_couplers_AWPROT;
  wire [3:0]auto_pc_to_s00_couplers_AWQOS;
  wire auto_pc_to_s00_couplers_AWREADY;
  wire [2:0]auto_pc_to_s00_couplers_AWSIZE;
  wire auto_pc_to_s00_couplers_AWVALID;
  wire [11:0]auto_pc_to_s00_couplers_BID;
  wire auto_pc_to_s00_couplers_BREADY;
  wire [1:0]auto_pc_to_s00_couplers_BRESP;
  wire auto_pc_to_s00_couplers_BVALID;
  wire [31:0]auto_pc_to_s00_couplers_RDATA;
  wire [11:0]auto_pc_to_s00_couplers_RID;
  wire auto_pc_to_s00_couplers_RLAST;
  wire auto_pc_to_s00_couplers_RREADY;
  wire [1:0]auto_pc_to_s00_couplers_RRESP;
  wire auto_pc_to_s00_couplers_RVALID;
  wire [31:0]auto_pc_to_s00_couplers_WDATA;
  wire auto_pc_to_s00_couplers_WLAST;
  wire auto_pc_to_s00_couplers_WREADY;
  wire [3:0]auto_pc_to_s00_couplers_WSTRB;
  wire auto_pc_to_s00_couplers_WVALID;
  wire [31:0]s00_couplers_to_auto_pc_ARADDR;
  wire [1:0]s00_couplers_to_auto_pc_ARBURST;
  wire [3:0]s00_couplers_to_auto_pc_ARCACHE;
  wire [11:0]s00_couplers_to_auto_pc_ARID;
  wire [3:0]s00_couplers_to_auto_pc_ARLEN;
  wire [1:0]s00_couplers_to_auto_pc_ARLOCK;
  wire [2:0]s00_couplers_to_auto_pc_ARPROT;
  wire [3:0]s00_couplers_to_auto_pc_ARQOS;
  wire s00_couplers_to_auto_pc_ARREADY;
  wire [2:0]s00_couplers_to_auto_pc_ARSIZE;
  wire s00_couplers_to_auto_pc_ARVALID;
  wire [31:0]s00_couplers_to_auto_pc_AWADDR;
  wire [1:0]s00_couplers_to_auto_pc_AWBURST;
  wire [3:0]s00_couplers_to_auto_pc_AWCACHE;
  wire [11:0]s00_couplers_to_auto_pc_AWID;
  wire [3:0]s00_couplers_to_auto_pc_AWLEN;
  wire [1:0]s00_couplers_to_auto_pc_AWLOCK;
  wire [2:0]s00_couplers_to_auto_pc_AWPROT;
  wire [3:0]s00_couplers_to_auto_pc_AWQOS;
  wire s00_couplers_to_auto_pc_AWREADY;
  wire [2:0]s00_couplers_to_auto_pc_AWSIZE;
  wire s00_couplers_to_auto_pc_AWVALID;
  wire [11:0]s00_couplers_to_auto_pc_BID;
  wire s00_couplers_to_auto_pc_BREADY;
  wire [1:0]s00_couplers_to_auto_pc_BRESP;
  wire s00_couplers_to_auto_pc_BVALID;
  wire [31:0]s00_couplers_to_auto_pc_RDATA;
  wire [11:0]s00_couplers_to_auto_pc_RID;
  wire s00_couplers_to_auto_pc_RLAST;
  wire s00_couplers_to_auto_pc_RREADY;
  wire [1:0]s00_couplers_to_auto_pc_RRESP;
  wire s00_couplers_to_auto_pc_RVALID;
  wire [31:0]s00_couplers_to_auto_pc_WDATA;
  wire [11:0]s00_couplers_to_auto_pc_WID;
  wire s00_couplers_to_auto_pc_WLAST;
  wire s00_couplers_to_auto_pc_WREADY;
  wire [3:0]s00_couplers_to_auto_pc_WSTRB;
  wire s00_couplers_to_auto_pc_WVALID;

  assign M_AXI_araddr[31:0] = auto_pc_to_s00_couplers_ARADDR;
  assign M_AXI_arburst[1:0] = auto_pc_to_s00_couplers_ARBURST;
  assign M_AXI_arcache[3:0] = auto_pc_to_s00_couplers_ARCACHE;
  assign M_AXI_arid[11:0] = auto_pc_to_s00_couplers_ARID;
  assign M_AXI_arlen[7:0] = auto_pc_to_s00_couplers_ARLEN;
  assign M_AXI_arlock[0] = auto_pc_to_s00_couplers_ARLOCK;
  assign M_AXI_arprot[2:0] = auto_pc_to_s00_couplers_ARPROT;
  assign M_AXI_arqos[3:0] = auto_pc_to_s00_couplers_ARQOS;
  assign M_AXI_arsize[2:0] = auto_pc_to_s00_couplers_ARSIZE;
  assign M_AXI_arvalid = auto_pc_to_s00_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = auto_pc_to_s00_couplers_AWADDR;
  assign M_AXI_awburst[1:0] = auto_pc_to_s00_couplers_AWBURST;
  assign M_AXI_awcache[3:0] = auto_pc_to_s00_couplers_AWCACHE;
  assign M_AXI_awid[11:0] = auto_pc_to_s00_couplers_AWID;
  assign M_AXI_awlen[7:0] = auto_pc_to_s00_couplers_AWLEN;
  assign M_AXI_awlock[0] = auto_pc_to_s00_couplers_AWLOCK;
  assign M_AXI_awprot[2:0] = auto_pc_to_s00_couplers_AWPROT;
  assign M_AXI_awqos[3:0] = auto_pc_to_s00_couplers_AWQOS;
  assign M_AXI_awsize[2:0] = auto_pc_to_s00_couplers_AWSIZE;
  assign M_AXI_awvalid = auto_pc_to_s00_couplers_AWVALID;
  assign M_AXI_bready = auto_pc_to_s00_couplers_BREADY;
  assign M_AXI_rready = auto_pc_to_s00_couplers_RREADY;
  assign M_AXI_wdata[31:0] = auto_pc_to_s00_couplers_WDATA;
  assign M_AXI_wlast = auto_pc_to_s00_couplers_WLAST;
  assign M_AXI_wstrb[3:0] = auto_pc_to_s00_couplers_WSTRB;
  assign M_AXI_wvalid = auto_pc_to_s00_couplers_WVALID;
  assign S_ACLK_1 = S_ACLK;
  assign S_ARESETN_1 = S_ARESETN;
  assign S_AXI_arready = s00_couplers_to_auto_pc_ARREADY;
  assign S_AXI_awready = s00_couplers_to_auto_pc_AWREADY;
  assign S_AXI_bid[11:0] = s00_couplers_to_auto_pc_BID;
  assign S_AXI_bresp[1:0] = s00_couplers_to_auto_pc_BRESP;
  assign S_AXI_bvalid = s00_couplers_to_auto_pc_BVALID;
  assign S_AXI_rdata[31:0] = s00_couplers_to_auto_pc_RDATA;
  assign S_AXI_rid[11:0] = s00_couplers_to_auto_pc_RID;
  assign S_AXI_rlast = s00_couplers_to_auto_pc_RLAST;
  assign S_AXI_rresp[1:0] = s00_couplers_to_auto_pc_RRESP;
  assign S_AXI_rvalid = s00_couplers_to_auto_pc_RVALID;
  assign S_AXI_wready = s00_couplers_to_auto_pc_WREADY;
  assign auto_pc_to_s00_couplers_ARREADY = M_AXI_arready;
  assign auto_pc_to_s00_couplers_AWREADY = M_AXI_awready;
  assign auto_pc_to_s00_couplers_BID = M_AXI_bid[11:0];
  assign auto_pc_to_s00_couplers_BRESP = M_AXI_bresp[1:0];
  assign auto_pc_to_s00_couplers_BVALID = M_AXI_bvalid;
  assign auto_pc_to_s00_couplers_RDATA = M_AXI_rdata[31:0];
  assign auto_pc_to_s00_couplers_RID = M_AXI_rid[11:0];
  assign auto_pc_to_s00_couplers_RLAST = M_AXI_rlast;
  assign auto_pc_to_s00_couplers_RRESP = M_AXI_rresp[1:0];
  assign auto_pc_to_s00_couplers_RVALID = M_AXI_rvalid;
  assign auto_pc_to_s00_couplers_WREADY = M_AXI_wready;
  assign s00_couplers_to_auto_pc_ARADDR = S_AXI_araddr[31:0];
  assign s00_couplers_to_auto_pc_ARBURST = S_AXI_arburst[1:0];
  assign s00_couplers_to_auto_pc_ARCACHE = S_AXI_arcache[3:0];
  assign s00_couplers_to_auto_pc_ARID = S_AXI_arid[11:0];
  assign s00_couplers_to_auto_pc_ARLEN = S_AXI_arlen[3:0];
  assign s00_couplers_to_auto_pc_ARLOCK = S_AXI_arlock[1:0];
  assign s00_couplers_to_auto_pc_ARPROT = S_AXI_arprot[2:0];
  assign s00_couplers_to_auto_pc_ARQOS = S_AXI_arqos[3:0];
  assign s00_couplers_to_auto_pc_ARSIZE = S_AXI_arsize[2:0];
  assign s00_couplers_to_auto_pc_ARVALID = S_AXI_arvalid;
  assign s00_couplers_to_auto_pc_AWADDR = S_AXI_awaddr[31:0];
  assign s00_couplers_to_auto_pc_AWBURST = S_AXI_awburst[1:0];
  assign s00_couplers_to_auto_pc_AWCACHE = S_AXI_awcache[3:0];
  assign s00_couplers_to_auto_pc_AWID = S_AXI_awid[11:0];
  assign s00_couplers_to_auto_pc_AWLEN = S_AXI_awlen[3:0];
  assign s00_couplers_to_auto_pc_AWLOCK = S_AXI_awlock[1:0];
  assign s00_couplers_to_auto_pc_AWPROT = S_AXI_awprot[2:0];
  assign s00_couplers_to_auto_pc_AWQOS = S_AXI_awqos[3:0];
  assign s00_couplers_to_auto_pc_AWSIZE = S_AXI_awsize[2:0];
  assign s00_couplers_to_auto_pc_AWVALID = S_AXI_awvalid;
  assign s00_couplers_to_auto_pc_BREADY = S_AXI_bready;
  assign s00_couplers_to_auto_pc_RREADY = S_AXI_rready;
  assign s00_couplers_to_auto_pc_WDATA = S_AXI_wdata[31:0];
  assign s00_couplers_to_auto_pc_WID = S_AXI_wid[11:0];
  assign s00_couplers_to_auto_pc_WLAST = S_AXI_wlast;
  assign s00_couplers_to_auto_pc_WSTRB = S_AXI_wstrb[3:0];
  assign s00_couplers_to_auto_pc_WVALID = S_AXI_wvalid;
  cerberus_auto_pc_2 auto_pc
       (.aclk(S_ACLK_1),
        .aresetn(S_ARESETN_1),
        .m_axi_araddr(auto_pc_to_s00_couplers_ARADDR),
        .m_axi_arburst(auto_pc_to_s00_couplers_ARBURST),
        .m_axi_arcache(auto_pc_to_s00_couplers_ARCACHE),
        .m_axi_arid(auto_pc_to_s00_couplers_ARID),
        .m_axi_arlen(auto_pc_to_s00_couplers_ARLEN),
        .m_axi_arlock(auto_pc_to_s00_couplers_ARLOCK),
        .m_axi_arprot(auto_pc_to_s00_couplers_ARPROT),
        .m_axi_arqos(auto_pc_to_s00_couplers_ARQOS),
        .m_axi_arready(auto_pc_to_s00_couplers_ARREADY),
        .m_axi_arsize(auto_pc_to_s00_couplers_ARSIZE),
        .m_axi_arvalid(auto_pc_to_s00_couplers_ARVALID),
        .m_axi_awaddr(auto_pc_to_s00_couplers_AWADDR),
        .m_axi_awburst(auto_pc_to_s00_couplers_AWBURST),
        .m_axi_awcache(auto_pc_to_s00_couplers_AWCACHE),
        .m_axi_awid(auto_pc_to_s00_couplers_AWID),
        .m_axi_awlen(auto_pc_to_s00_couplers_AWLEN),
        .m_axi_awlock(auto_pc_to_s00_couplers_AWLOCK),
        .m_axi_awprot(auto_pc_to_s00_couplers_AWPROT),
        .m_axi_awqos(auto_pc_to_s00_couplers_AWQOS),
        .m_axi_awready(auto_pc_to_s00_couplers_AWREADY),
        .m_axi_awsize(auto_pc_to_s00_couplers_AWSIZE),
        .m_axi_awvalid(auto_pc_to_s00_couplers_AWVALID),
        .m_axi_bid(auto_pc_to_s00_couplers_BID),
        .m_axi_bready(auto_pc_to_s00_couplers_BREADY),
        .m_axi_bresp(auto_pc_to_s00_couplers_BRESP),
        .m_axi_bvalid(auto_pc_to_s00_couplers_BVALID),
        .m_axi_rdata(auto_pc_to_s00_couplers_RDATA),
        .m_axi_rid(auto_pc_to_s00_couplers_RID),
        .m_axi_rlast(auto_pc_to_s00_couplers_RLAST),
        .m_axi_rready(auto_pc_to_s00_couplers_RREADY),
        .m_axi_rresp(auto_pc_to_s00_couplers_RRESP),
        .m_axi_rvalid(auto_pc_to_s00_couplers_RVALID),
        .m_axi_wdata(auto_pc_to_s00_couplers_WDATA),
        .m_axi_wlast(auto_pc_to_s00_couplers_WLAST),
        .m_axi_wready(auto_pc_to_s00_couplers_WREADY),
        .m_axi_wstrb(auto_pc_to_s00_couplers_WSTRB),
        .m_axi_wvalid(auto_pc_to_s00_couplers_WVALID),
        .s_axi_araddr(s00_couplers_to_auto_pc_ARADDR),
        .s_axi_arburst(s00_couplers_to_auto_pc_ARBURST),
        .s_axi_arcache(s00_couplers_to_auto_pc_ARCACHE),
        .s_axi_arid(s00_couplers_to_auto_pc_ARID),
        .s_axi_arlen(s00_couplers_to_auto_pc_ARLEN),
        .s_axi_arlock(s00_couplers_to_auto_pc_ARLOCK),
        .s_axi_arprot(s00_couplers_to_auto_pc_ARPROT),
        .s_axi_arqos(s00_couplers_to_auto_pc_ARQOS),
        .s_axi_arready(s00_couplers_to_auto_pc_ARREADY),
        .s_axi_arsize(s00_couplers_to_auto_pc_ARSIZE),
        .s_axi_arvalid(s00_couplers_to_auto_pc_ARVALID),
        .s_axi_awaddr(s00_couplers_to_auto_pc_AWADDR),
        .s_axi_awburst(s00_couplers_to_auto_pc_AWBURST),
        .s_axi_awcache(s00_couplers_to_auto_pc_AWCACHE),
        .s_axi_awid(s00_couplers_to_auto_pc_AWID),
        .s_axi_awlen(s00_couplers_to_auto_pc_AWLEN),
        .s_axi_awlock(s00_couplers_to_auto_pc_AWLOCK),
        .s_axi_awprot(s00_couplers_to_auto_pc_AWPROT),
        .s_axi_awqos(s00_couplers_to_auto_pc_AWQOS),
        .s_axi_awready(s00_couplers_to_auto_pc_AWREADY),
        .s_axi_awsize(s00_couplers_to_auto_pc_AWSIZE),
        .s_axi_awvalid(s00_couplers_to_auto_pc_AWVALID),
        .s_axi_bid(s00_couplers_to_auto_pc_BID),
        .s_axi_bready(s00_couplers_to_auto_pc_BREADY),
        .s_axi_bresp(s00_couplers_to_auto_pc_BRESP),
        .s_axi_bvalid(s00_couplers_to_auto_pc_BVALID),
        .s_axi_rdata(s00_couplers_to_auto_pc_RDATA),
        .s_axi_rid(s00_couplers_to_auto_pc_RID),
        .s_axi_rlast(s00_couplers_to_auto_pc_RLAST),
        .s_axi_rready(s00_couplers_to_auto_pc_RREADY),
        .s_axi_rresp(s00_couplers_to_auto_pc_RRESP),
        .s_axi_rvalid(s00_couplers_to_auto_pc_RVALID),
        .s_axi_wdata(s00_couplers_to_auto_pc_WDATA),
        .s_axi_wid(s00_couplers_to_auto_pc_WID),
        .s_axi_wlast(s00_couplers_to_auto_pc_WLAST),
        .s_axi_wready(s00_couplers_to_auto_pc_WREADY),
        .s_axi_wstrb(s00_couplers_to_auto_pc_WSTRB),
        .s_axi_wvalid(s00_couplers_to_auto_pc_WVALID));
endmodule

module s01_couplers_imp_5MEJR0
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [63:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [63:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [7:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [63:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [63:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [7:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [31:0]s01_couplers_to_s01_couplers_ARADDR;
  wire [2:0]s01_couplers_to_s01_couplers_ARPROT;
  wire [0:0]s01_couplers_to_s01_couplers_ARREADY;
  wire [0:0]s01_couplers_to_s01_couplers_ARVALID;
  wire [31:0]s01_couplers_to_s01_couplers_AWADDR;
  wire [2:0]s01_couplers_to_s01_couplers_AWPROT;
  wire [0:0]s01_couplers_to_s01_couplers_AWREADY;
  wire [0:0]s01_couplers_to_s01_couplers_AWVALID;
  wire [0:0]s01_couplers_to_s01_couplers_BREADY;
  wire [1:0]s01_couplers_to_s01_couplers_BRESP;
  wire [0:0]s01_couplers_to_s01_couplers_BVALID;
  wire [63:0]s01_couplers_to_s01_couplers_RDATA;
  wire [0:0]s01_couplers_to_s01_couplers_RREADY;
  wire [1:0]s01_couplers_to_s01_couplers_RRESP;
  wire [0:0]s01_couplers_to_s01_couplers_RVALID;
  wire [63:0]s01_couplers_to_s01_couplers_WDATA;
  wire [0:0]s01_couplers_to_s01_couplers_WREADY;
  wire [7:0]s01_couplers_to_s01_couplers_WSTRB;
  wire [0:0]s01_couplers_to_s01_couplers_WVALID;

  assign M_AXI_araddr[31:0] = s01_couplers_to_s01_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = s01_couplers_to_s01_couplers_ARPROT;
  assign M_AXI_arvalid[0] = s01_couplers_to_s01_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = s01_couplers_to_s01_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = s01_couplers_to_s01_couplers_AWPROT;
  assign M_AXI_awvalid[0] = s01_couplers_to_s01_couplers_AWVALID;
  assign M_AXI_bready[0] = s01_couplers_to_s01_couplers_BREADY;
  assign M_AXI_rready[0] = s01_couplers_to_s01_couplers_RREADY;
  assign M_AXI_wdata[63:0] = s01_couplers_to_s01_couplers_WDATA;
  assign M_AXI_wstrb[7:0] = s01_couplers_to_s01_couplers_WSTRB;
  assign M_AXI_wvalid[0] = s01_couplers_to_s01_couplers_WVALID;
  assign S_AXI_arready[0] = s01_couplers_to_s01_couplers_ARREADY;
  assign S_AXI_awready[0] = s01_couplers_to_s01_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = s01_couplers_to_s01_couplers_BRESP;
  assign S_AXI_bvalid[0] = s01_couplers_to_s01_couplers_BVALID;
  assign S_AXI_rdata[63:0] = s01_couplers_to_s01_couplers_RDATA;
  assign S_AXI_rresp[1:0] = s01_couplers_to_s01_couplers_RRESP;
  assign S_AXI_rvalid[0] = s01_couplers_to_s01_couplers_RVALID;
  assign S_AXI_wready[0] = s01_couplers_to_s01_couplers_WREADY;
  assign s01_couplers_to_s01_couplers_ARADDR = S_AXI_araddr[31:0];
  assign s01_couplers_to_s01_couplers_ARPROT = S_AXI_arprot[2:0];
  assign s01_couplers_to_s01_couplers_ARREADY = M_AXI_arready[0];
  assign s01_couplers_to_s01_couplers_ARVALID = S_AXI_arvalid[0];
  assign s01_couplers_to_s01_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign s01_couplers_to_s01_couplers_AWPROT = S_AXI_awprot[2:0];
  assign s01_couplers_to_s01_couplers_AWREADY = M_AXI_awready[0];
  assign s01_couplers_to_s01_couplers_AWVALID = S_AXI_awvalid[0];
  assign s01_couplers_to_s01_couplers_BREADY = S_AXI_bready[0];
  assign s01_couplers_to_s01_couplers_BRESP = M_AXI_bresp[1:0];
  assign s01_couplers_to_s01_couplers_BVALID = M_AXI_bvalid[0];
  assign s01_couplers_to_s01_couplers_RDATA = M_AXI_rdata[63:0];
  assign s01_couplers_to_s01_couplers_RREADY = S_AXI_rready[0];
  assign s01_couplers_to_s01_couplers_RRESP = M_AXI_rresp[1:0];
  assign s01_couplers_to_s01_couplers_RVALID = M_AXI_rvalid[0];
  assign s01_couplers_to_s01_couplers_WDATA = S_AXI_wdata[63:0];
  assign s01_couplers_to_s01_couplers_WREADY = M_AXI_wready[0];
  assign s01_couplers_to_s01_couplers_WSTRB = S_AXI_wstrb[7:0];
  assign s01_couplers_to_s01_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module s02_couplers_imp_1YOULGE
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [63:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [63:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [7:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [63:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [63:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [7:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [31:0]s02_couplers_to_s02_couplers_ARADDR;
  wire [2:0]s02_couplers_to_s02_couplers_ARPROT;
  wire [0:0]s02_couplers_to_s02_couplers_ARREADY;
  wire [0:0]s02_couplers_to_s02_couplers_ARVALID;
  wire [31:0]s02_couplers_to_s02_couplers_AWADDR;
  wire [2:0]s02_couplers_to_s02_couplers_AWPROT;
  wire [0:0]s02_couplers_to_s02_couplers_AWREADY;
  wire [0:0]s02_couplers_to_s02_couplers_AWVALID;
  wire [0:0]s02_couplers_to_s02_couplers_BREADY;
  wire [1:0]s02_couplers_to_s02_couplers_BRESP;
  wire [0:0]s02_couplers_to_s02_couplers_BVALID;
  wire [63:0]s02_couplers_to_s02_couplers_RDATA;
  wire [0:0]s02_couplers_to_s02_couplers_RREADY;
  wire [1:0]s02_couplers_to_s02_couplers_RRESP;
  wire [0:0]s02_couplers_to_s02_couplers_RVALID;
  wire [63:0]s02_couplers_to_s02_couplers_WDATA;
  wire [0:0]s02_couplers_to_s02_couplers_WREADY;
  wire [7:0]s02_couplers_to_s02_couplers_WSTRB;
  wire [0:0]s02_couplers_to_s02_couplers_WVALID;

  assign M_AXI_araddr[31:0] = s02_couplers_to_s02_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = s02_couplers_to_s02_couplers_ARPROT;
  assign M_AXI_arvalid[0] = s02_couplers_to_s02_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = s02_couplers_to_s02_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = s02_couplers_to_s02_couplers_AWPROT;
  assign M_AXI_awvalid[0] = s02_couplers_to_s02_couplers_AWVALID;
  assign M_AXI_bready[0] = s02_couplers_to_s02_couplers_BREADY;
  assign M_AXI_rready[0] = s02_couplers_to_s02_couplers_RREADY;
  assign M_AXI_wdata[63:0] = s02_couplers_to_s02_couplers_WDATA;
  assign M_AXI_wstrb[7:0] = s02_couplers_to_s02_couplers_WSTRB;
  assign M_AXI_wvalid[0] = s02_couplers_to_s02_couplers_WVALID;
  assign S_AXI_arready[0] = s02_couplers_to_s02_couplers_ARREADY;
  assign S_AXI_awready[0] = s02_couplers_to_s02_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = s02_couplers_to_s02_couplers_BRESP;
  assign S_AXI_bvalid[0] = s02_couplers_to_s02_couplers_BVALID;
  assign S_AXI_rdata[63:0] = s02_couplers_to_s02_couplers_RDATA;
  assign S_AXI_rresp[1:0] = s02_couplers_to_s02_couplers_RRESP;
  assign S_AXI_rvalid[0] = s02_couplers_to_s02_couplers_RVALID;
  assign S_AXI_wready[0] = s02_couplers_to_s02_couplers_WREADY;
  assign s02_couplers_to_s02_couplers_ARADDR = S_AXI_araddr[31:0];
  assign s02_couplers_to_s02_couplers_ARPROT = S_AXI_arprot[2:0];
  assign s02_couplers_to_s02_couplers_ARREADY = M_AXI_arready[0];
  assign s02_couplers_to_s02_couplers_ARVALID = S_AXI_arvalid[0];
  assign s02_couplers_to_s02_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign s02_couplers_to_s02_couplers_AWPROT = S_AXI_awprot[2:0];
  assign s02_couplers_to_s02_couplers_AWREADY = M_AXI_awready[0];
  assign s02_couplers_to_s02_couplers_AWVALID = S_AXI_awvalid[0];
  assign s02_couplers_to_s02_couplers_BREADY = S_AXI_bready[0];
  assign s02_couplers_to_s02_couplers_BRESP = M_AXI_bresp[1:0];
  assign s02_couplers_to_s02_couplers_BVALID = M_AXI_bvalid[0];
  assign s02_couplers_to_s02_couplers_RDATA = M_AXI_rdata[63:0];
  assign s02_couplers_to_s02_couplers_RREADY = S_AXI_rready[0];
  assign s02_couplers_to_s02_couplers_RRESP = M_AXI_rresp[1:0];
  assign s02_couplers_to_s02_couplers_RVALID = M_AXI_rvalid[0];
  assign s02_couplers_to_s02_couplers_WDATA = S_AXI_wdata[63:0];
  assign s02_couplers_to_s02_couplers_WREADY = M_AXI_wready[0];
  assign s02_couplers_to_s02_couplers_WSTRB = S_AXI_wstrb[7:0];
  assign s02_couplers_to_s02_couplers_WVALID = S_AXI_wvalid[0];
endmodule

module s03_couplers_imp_4VPA7Z
   (M_ACLK,
    M_ARESETN,
    M_AXI_araddr,
    M_AXI_arprot,
    M_AXI_arready,
    M_AXI_arvalid,
    M_AXI_awaddr,
    M_AXI_awprot,
    M_AXI_awready,
    M_AXI_awvalid,
    M_AXI_bready,
    M_AXI_bresp,
    M_AXI_bvalid,
    M_AXI_rdata,
    M_AXI_rready,
    M_AXI_rresp,
    M_AXI_rvalid,
    M_AXI_wdata,
    M_AXI_wready,
    M_AXI_wstrb,
    M_AXI_wvalid,
    S_ACLK,
    S_ARESETN,
    S_AXI_araddr,
    S_AXI_arprot,
    S_AXI_arready,
    S_AXI_arvalid,
    S_AXI_awaddr,
    S_AXI_awprot,
    S_AXI_awready,
    S_AXI_awvalid,
    S_AXI_bready,
    S_AXI_bresp,
    S_AXI_bvalid,
    S_AXI_rdata,
    S_AXI_rready,
    S_AXI_rresp,
    S_AXI_rvalid,
    S_AXI_wdata,
    S_AXI_wready,
    S_AXI_wstrb,
    S_AXI_wvalid);
  input M_ACLK;
  input M_ARESETN;
  output [31:0]M_AXI_araddr;
  output [2:0]M_AXI_arprot;
  input [0:0]M_AXI_arready;
  output [0:0]M_AXI_arvalid;
  output [31:0]M_AXI_awaddr;
  output [2:0]M_AXI_awprot;
  input [0:0]M_AXI_awready;
  output [0:0]M_AXI_awvalid;
  output [0:0]M_AXI_bready;
  input [1:0]M_AXI_bresp;
  input [0:0]M_AXI_bvalid;
  input [63:0]M_AXI_rdata;
  output [0:0]M_AXI_rready;
  input [1:0]M_AXI_rresp;
  input [0:0]M_AXI_rvalid;
  output [63:0]M_AXI_wdata;
  input [0:0]M_AXI_wready;
  output [7:0]M_AXI_wstrb;
  output [0:0]M_AXI_wvalid;
  input S_ACLK;
  input S_ARESETN;
  input [31:0]S_AXI_araddr;
  input [2:0]S_AXI_arprot;
  output [0:0]S_AXI_arready;
  input [0:0]S_AXI_arvalid;
  input [31:0]S_AXI_awaddr;
  input [2:0]S_AXI_awprot;
  output [0:0]S_AXI_awready;
  input [0:0]S_AXI_awvalid;
  input [0:0]S_AXI_bready;
  output [1:0]S_AXI_bresp;
  output [0:0]S_AXI_bvalid;
  output [63:0]S_AXI_rdata;
  input [0:0]S_AXI_rready;
  output [1:0]S_AXI_rresp;
  output [0:0]S_AXI_rvalid;
  input [63:0]S_AXI_wdata;
  output [0:0]S_AXI_wready;
  input [7:0]S_AXI_wstrb;
  input [0:0]S_AXI_wvalid;

  wire [31:0]s03_couplers_to_s03_couplers_ARADDR;
  wire [2:0]s03_couplers_to_s03_couplers_ARPROT;
  wire [0:0]s03_couplers_to_s03_couplers_ARREADY;
  wire [0:0]s03_couplers_to_s03_couplers_ARVALID;
  wire [31:0]s03_couplers_to_s03_couplers_AWADDR;
  wire [2:0]s03_couplers_to_s03_couplers_AWPROT;
  wire [0:0]s03_couplers_to_s03_couplers_AWREADY;
  wire [0:0]s03_couplers_to_s03_couplers_AWVALID;
  wire [0:0]s03_couplers_to_s03_couplers_BREADY;
  wire [1:0]s03_couplers_to_s03_couplers_BRESP;
  wire [0:0]s03_couplers_to_s03_couplers_BVALID;
  wire [63:0]s03_couplers_to_s03_couplers_RDATA;
  wire [0:0]s03_couplers_to_s03_couplers_RREADY;
  wire [1:0]s03_couplers_to_s03_couplers_RRESP;
  wire [0:0]s03_couplers_to_s03_couplers_RVALID;
  wire [63:0]s03_couplers_to_s03_couplers_WDATA;
  wire [0:0]s03_couplers_to_s03_couplers_WREADY;
  wire [7:0]s03_couplers_to_s03_couplers_WSTRB;
  wire [0:0]s03_couplers_to_s03_couplers_WVALID;

  assign M_AXI_araddr[31:0] = s03_couplers_to_s03_couplers_ARADDR;
  assign M_AXI_arprot[2:0] = s03_couplers_to_s03_couplers_ARPROT;
  assign M_AXI_arvalid[0] = s03_couplers_to_s03_couplers_ARVALID;
  assign M_AXI_awaddr[31:0] = s03_couplers_to_s03_couplers_AWADDR;
  assign M_AXI_awprot[2:0] = s03_couplers_to_s03_couplers_AWPROT;
  assign M_AXI_awvalid[0] = s03_couplers_to_s03_couplers_AWVALID;
  assign M_AXI_bready[0] = s03_couplers_to_s03_couplers_BREADY;
  assign M_AXI_rready[0] = s03_couplers_to_s03_couplers_RREADY;
  assign M_AXI_wdata[63:0] = s03_couplers_to_s03_couplers_WDATA;
  assign M_AXI_wstrb[7:0] = s03_couplers_to_s03_couplers_WSTRB;
  assign M_AXI_wvalid[0] = s03_couplers_to_s03_couplers_WVALID;
  assign S_AXI_arready[0] = s03_couplers_to_s03_couplers_ARREADY;
  assign S_AXI_awready[0] = s03_couplers_to_s03_couplers_AWREADY;
  assign S_AXI_bresp[1:0] = s03_couplers_to_s03_couplers_BRESP;
  assign S_AXI_bvalid[0] = s03_couplers_to_s03_couplers_BVALID;
  assign S_AXI_rdata[63:0] = s03_couplers_to_s03_couplers_RDATA;
  assign S_AXI_rresp[1:0] = s03_couplers_to_s03_couplers_RRESP;
  assign S_AXI_rvalid[0] = s03_couplers_to_s03_couplers_RVALID;
  assign S_AXI_wready[0] = s03_couplers_to_s03_couplers_WREADY;
  assign s03_couplers_to_s03_couplers_ARADDR = S_AXI_araddr[31:0];
  assign s03_couplers_to_s03_couplers_ARPROT = S_AXI_arprot[2:0];
  assign s03_couplers_to_s03_couplers_ARREADY = M_AXI_arready[0];
  assign s03_couplers_to_s03_couplers_ARVALID = S_AXI_arvalid[0];
  assign s03_couplers_to_s03_couplers_AWADDR = S_AXI_awaddr[31:0];
  assign s03_couplers_to_s03_couplers_AWPROT = S_AXI_awprot[2:0];
  assign s03_couplers_to_s03_couplers_AWREADY = M_AXI_awready[0];
  assign s03_couplers_to_s03_couplers_AWVALID = S_AXI_awvalid[0];
  assign s03_couplers_to_s03_couplers_BREADY = S_AXI_bready[0];
  assign s03_couplers_to_s03_couplers_BRESP = M_AXI_bresp[1:0];
  assign s03_couplers_to_s03_couplers_BVALID = M_AXI_bvalid[0];
  assign s03_couplers_to_s03_couplers_RDATA = M_AXI_rdata[63:0];
  assign s03_couplers_to_s03_couplers_RREADY = S_AXI_rready[0];
  assign s03_couplers_to_s03_couplers_RRESP = M_AXI_rresp[1:0];
  assign s03_couplers_to_s03_couplers_RVALID = M_AXI_rvalid[0];
  assign s03_couplers_to_s03_couplers_WDATA = S_AXI_wdata[63:0];
  assign s03_couplers_to_s03_couplers_WREADY = M_AXI_wready[0];
  assign s03_couplers_to_s03_couplers_WSTRB = S_AXI_wstrb[7:0];
  assign s03_couplers_to_s03_couplers_WVALID = S_AXI_wvalid[0];
endmodule
