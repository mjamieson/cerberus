-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Thu Jun  6 23:57:19 2019
-- Host        : talisker running 64-bit Debian GNU/Linux 9.7 (stretch)
-- Command     : write_vhdl -force -mode synth_stub
--               /home/maurice/build/Cerberus8DRAM/Cerberus8DRAM.srcs/sources_1/bd/cerberus/ip/cerberus_resetSlice_0/cerberus_resetSlice_0_stub.vhdl
-- Design      : cerberus_resetSlice_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity cerberus_resetSlice_0 is
  Port ( 
    Din : in STD_LOGIC_VECTOR ( 7 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );

end cerberus_resetSlice_0;

architecture stub of cerberus_resetSlice_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "Din[7:0],Dout[0:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "xlslice_v1_0_1_xlslice,Vivado 2018.2";
begin
end;
