-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Thu Jun  6 23:57:19 2019
-- Host        : talisker running 64-bit Debian GNU/Linux 9.7 (stretch)
-- Command     : write_vhdl -force -mode funcsim
--               /home/maurice/build/Cerberus8DRAM/Cerberus8DRAM.srcs/sources_1/bd/cerberus/ip/cerberus_resetSlice_0/cerberus_resetSlice_0_sim_netlist.vhdl
-- Design      : cerberus_resetSlice_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity cerberus_resetSlice_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 7 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of cerberus_resetSlice_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of cerberus_resetSlice_0 : entity is "cerberus_resetSlice_0,xlslice_v1_0_1_xlslice,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of cerberus_resetSlice_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of cerberus_resetSlice_0 : entity is "xlslice_v1_0_1_xlslice,Vivado 2018.2";
end cerberus_resetSlice_0;

architecture STRUCTURE of cerberus_resetSlice_0 is
  signal \^din\ : STD_LOGIC_VECTOR ( 7 downto 0 );
begin
  Dout(0) <= \^din\(0);
  \^din\(0) <= Din(0);
end STRUCTURE;
