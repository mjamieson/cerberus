-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Fri Mar 29 17:05:41 2019
-- Host        : talisker running 64-bit Debian GNU/Linux 9.7 (stretch)
-- Command     : write_vhdl -force -mode funcsim
--               /home/maurice/build/CerberusMicroBlaze8/CerberusMicroBlaze8.srcs/sources_1/bd/base/ip/base_logic_1_9/base_logic_1_9_sim_netlist.vhdl
-- Design      : base_logic_1_9
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity base_logic_1_9 is
  port (
    dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of base_logic_1_9 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of base_logic_1_9 : entity is "base_logic_1_9,xlconstant_v1_1_5_xlconstant,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of base_logic_1_9 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of base_logic_1_9 : entity is "xlconstant_v1_1_5_xlconstant,Vivado 2018.2";
end base_logic_1_9;

architecture STRUCTURE of base_logic_1_9 is
  signal \<const1>\ : STD_LOGIC;
begin
  dout(0) <= \<const1>\;
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
end STRUCTURE;
