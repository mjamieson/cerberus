-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Fri Mar 29 17:05:41 2019
-- Host        : talisker running 64-bit Debian GNU/Linux 9.7 (stretch)
-- Command     : write_vhdl -force -mode synth_stub
--               /home/maurice/build/CerberusMicroBlaze8/CerberusMicroBlaze8.srcs/sources_1/bd/base/ip/base_logic_1_9/base_logic_1_9_stub.vhdl
-- Design      : base_logic_1_9
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity base_logic_1_9 is
  Port ( 
    dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );

end base_logic_1_9;

architecture stub of base_logic_1_9 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "dout[0:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "xlconstant_v1_1_5_xlconstant,Vivado 2018.2";
begin
end;
