// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Wed Jan 23 13:39:11 2019
// Host        : talisker running 64-bit Debian GNU/Linux 9.6 (stretch)
// Command     : write_verilog -force -mode synth_stub
//               /home/maurice/build/PYNQ-Z2_MicroBlaze/PYNQ-Z2_MicroBlaze.srcs/sources_1/bd/base/ip/base_mb_iop_pmoda_reset_0/base_mb_iop_pmoda_reset_0_stub.v
// Design      : base_mb_iop_pmoda_reset_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "xlslice_v1_0_1_xlslice,Vivado 2018.2" *)
module base_mb_iop_pmoda_reset_0(Din, Dout)
/* synthesis syn_black_box black_box_pad_pin="Din[9:0],Dout[0:0]" */;
  input [9:0]Din;
  output [0:0]Dout;
endmodule
