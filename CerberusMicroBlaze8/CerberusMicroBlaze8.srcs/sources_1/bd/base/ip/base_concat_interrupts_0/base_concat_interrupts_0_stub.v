// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Sun Jan 27 21:51:31 2019
// Host        : talisker running 64-bit Debian GNU/Linux 9.6 (stretch)
// Command     : write_verilog -force -mode synth_stub
//               /home/maurice/build/CerberusMicroBlaze8/CerberusMicroBlaze8.srcs/sources_1/bd/base/ip/base_concat_interrupts_0/base_concat_interrupts_0_stub.v
// Design      : base_concat_interrupts_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "xlconcat_v2_1_1_xlconcat,Vivado 2018.2" *)
module base_concat_interrupts_0(In0, In1, In2, In3, In4, In5, In6, dout)
/* synthesis syn_black_box black_box_pad_pin="In0[5:0],In1[0:0],In2[0:0],In3[0:0],In4[7:0],In5[0:0],In6[0:0],dout[18:0]" */;
  input [5:0]In0;
  input [0:0]In1;
  input [0:0]In2;
  input [0:0]In3;
  input [7:0]In4;
  input [0:0]In5;
  input [0:0]In6;
  output [18:0]dout;
endmodule
