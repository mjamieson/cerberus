-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Fri Mar 29 17:11:01 2019
-- Host        : talisker running 64-bit Debian GNU/Linux 9.7 (stretch)
-- Command     : write_vhdl -force -mode synth_stub
--               /home/maurice/build/CerberusMicroBlaze8/CerberusMicroBlaze8.srcs/sources_1/bd/base/ip/base_dff_en_reset_vector_0_8/base_dff_en_reset_vector_0_8_stub.vhdl
-- Design      : base_dff_en_reset_vector_0_8
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity base_dff_en_reset_vector_0_8 is
  Port ( 
    d : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC;
    en : in STD_LOGIC;
    reset : in STD_LOGIC;
    q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );

end base_dff_en_reset_vector_0_8;

architecture stub of base_dff_en_reset_vector_0_8 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "d[0:0],clk,en,reset,q[0:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "dff_en_reset_vector,Vivado 2018.2";
begin
end;
