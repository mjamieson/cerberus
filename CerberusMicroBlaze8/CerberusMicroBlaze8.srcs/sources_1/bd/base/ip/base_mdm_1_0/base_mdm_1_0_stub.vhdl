-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Sun Jan 27 17:55:23 2019
-- Host        : talisker running 64-bit Debian GNU/Linux 9.6 (stretch)
-- Command     : write_vhdl -force -mode synth_stub
--               /home/maurice/build/CerberusMicroBlaze/CerberusMicroBlaze.srcs/sources_1/bd/base/ip/base_mdm_1_0/base_mdm_1_0_stub.vhdl
-- Design      : base_mdm_1_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity base_mdm_1_0 is
  Port ( 
    Debug_SYS_Rst : out STD_LOGIC;
    Dbg_Clk_0 : out STD_LOGIC;
    Dbg_TDI_0 : out STD_LOGIC;
    Dbg_TDO_0 : in STD_LOGIC;
    Dbg_Reg_En_0 : out STD_LOGIC_VECTOR ( 0 to 7 );
    Dbg_Capture_0 : out STD_LOGIC;
    Dbg_Shift_0 : out STD_LOGIC;
    Dbg_Update_0 : out STD_LOGIC;
    Dbg_Rst_0 : out STD_LOGIC;
    Dbg_Disable_0 : out STD_LOGIC;
    Dbg_Clk_1 : out STD_LOGIC;
    Dbg_TDI_1 : out STD_LOGIC;
    Dbg_TDO_1 : in STD_LOGIC;
    Dbg_Reg_En_1 : out STD_LOGIC_VECTOR ( 0 to 7 );
    Dbg_Capture_1 : out STD_LOGIC;
    Dbg_Shift_1 : out STD_LOGIC;
    Dbg_Update_1 : out STD_LOGIC;
    Dbg_Rst_1 : out STD_LOGIC;
    Dbg_Disable_1 : out STD_LOGIC;
    Dbg_Clk_2 : out STD_LOGIC;
    Dbg_TDI_2 : out STD_LOGIC;
    Dbg_TDO_2 : in STD_LOGIC;
    Dbg_Reg_En_2 : out STD_LOGIC_VECTOR ( 0 to 7 );
    Dbg_Capture_2 : out STD_LOGIC;
    Dbg_Shift_2 : out STD_LOGIC;
    Dbg_Update_2 : out STD_LOGIC;
    Dbg_Rst_2 : out STD_LOGIC;
    Dbg_Disable_2 : out STD_LOGIC;
    Dbg_Clk_3 : out STD_LOGIC;
    Dbg_TDI_3 : out STD_LOGIC;
    Dbg_TDO_3 : in STD_LOGIC;
    Dbg_Reg_En_3 : out STD_LOGIC_VECTOR ( 0 to 7 );
    Dbg_Capture_3 : out STD_LOGIC;
    Dbg_Shift_3 : out STD_LOGIC;
    Dbg_Update_3 : out STD_LOGIC;
    Dbg_Rst_3 : out STD_LOGIC;
    Dbg_Disable_3 : out STD_LOGIC;
    Dbg_Clk_4 : out STD_LOGIC;
    Dbg_TDI_4 : out STD_LOGIC;
    Dbg_TDO_4 : in STD_LOGIC;
    Dbg_Reg_En_4 : out STD_LOGIC_VECTOR ( 0 to 7 );
    Dbg_Capture_4 : out STD_LOGIC;
    Dbg_Shift_4 : out STD_LOGIC;
    Dbg_Update_4 : out STD_LOGIC;
    Dbg_Rst_4 : out STD_LOGIC;
    Dbg_Disable_4 : out STD_LOGIC;
    Dbg_Clk_5 : out STD_LOGIC;
    Dbg_TDI_5 : out STD_LOGIC;
    Dbg_TDO_5 : in STD_LOGIC;
    Dbg_Reg_En_5 : out STD_LOGIC_VECTOR ( 0 to 7 );
    Dbg_Capture_5 : out STD_LOGIC;
    Dbg_Shift_5 : out STD_LOGIC;
    Dbg_Update_5 : out STD_LOGIC;
    Dbg_Rst_5 : out STD_LOGIC;
    Dbg_Disable_5 : out STD_LOGIC;
    Dbg_Clk_6 : out STD_LOGIC;
    Dbg_TDI_6 : out STD_LOGIC;
    Dbg_TDO_6 : in STD_LOGIC;
    Dbg_Reg_En_6 : out STD_LOGIC_VECTOR ( 0 to 7 );
    Dbg_Capture_6 : out STD_LOGIC;
    Dbg_Shift_6 : out STD_LOGIC;
    Dbg_Update_6 : out STD_LOGIC;
    Dbg_Rst_6 : out STD_LOGIC;
    Dbg_Disable_6 : out STD_LOGIC;
    Dbg_Clk_7 : out STD_LOGIC;
    Dbg_TDI_7 : out STD_LOGIC;
    Dbg_TDO_7 : in STD_LOGIC;
    Dbg_Reg_En_7 : out STD_LOGIC_VECTOR ( 0 to 7 );
    Dbg_Capture_7 : out STD_LOGIC;
    Dbg_Shift_7 : out STD_LOGIC;
    Dbg_Update_7 : out STD_LOGIC;
    Dbg_Rst_7 : out STD_LOGIC;
    Dbg_Disable_7 : out STD_LOGIC
  );

end base_mdm_1_0;

architecture stub of base_mdm_1_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "Debug_SYS_Rst,Dbg_Clk_0,Dbg_TDI_0,Dbg_TDO_0,Dbg_Reg_En_0[0:7],Dbg_Capture_0,Dbg_Shift_0,Dbg_Update_0,Dbg_Rst_0,Dbg_Disable_0,Dbg_Clk_1,Dbg_TDI_1,Dbg_TDO_1,Dbg_Reg_En_1[0:7],Dbg_Capture_1,Dbg_Shift_1,Dbg_Update_1,Dbg_Rst_1,Dbg_Disable_1,Dbg_Clk_2,Dbg_TDI_2,Dbg_TDO_2,Dbg_Reg_En_2[0:7],Dbg_Capture_2,Dbg_Shift_2,Dbg_Update_2,Dbg_Rst_2,Dbg_Disable_2,Dbg_Clk_3,Dbg_TDI_3,Dbg_TDO_3,Dbg_Reg_En_3[0:7],Dbg_Capture_3,Dbg_Shift_3,Dbg_Update_3,Dbg_Rst_3,Dbg_Disable_3,Dbg_Clk_4,Dbg_TDI_4,Dbg_TDO_4,Dbg_Reg_En_4[0:7],Dbg_Capture_4,Dbg_Shift_4,Dbg_Update_4,Dbg_Rst_4,Dbg_Disable_4,Dbg_Clk_5,Dbg_TDI_5,Dbg_TDO_5,Dbg_Reg_En_5[0:7],Dbg_Capture_5,Dbg_Shift_5,Dbg_Update_5,Dbg_Rst_5,Dbg_Disable_5,Dbg_Clk_6,Dbg_TDI_6,Dbg_TDO_6,Dbg_Reg_En_6[0:7],Dbg_Capture_6,Dbg_Shift_6,Dbg_Update_6,Dbg_Rst_6,Dbg_Disable_6,Dbg_Clk_7,Dbg_TDI_7,Dbg_TDO_7,Dbg_Reg_En_7[0:7],Dbg_Capture_7,Dbg_Shift_7,Dbg_Update_7,Dbg_Rst_7,Dbg_Disable_7";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "MDM,Vivado 2018.2";
begin
end;
