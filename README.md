# Cerberus micro-core

Cerberus is a micro-core based on the RISC-V [PicoRV32](https://github.com/YosysHQ/picorv32)
or Xilinx MicroBlaze for the [PYNQ](http://www.pynq.io/) SBC.

There are 4 bitstreams:
1. MicroBlaze 8-core 64KB per core
2. PicoRV32 8-core 64KB per core
3. PicoRV32 4-core 128KB per core
4. PicoRV32 single 128KB core

